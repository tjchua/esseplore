package com.essplore.essplore.api.response;

import com.essplore.essplore.models.CityHighlight;
import com.google.gson.annotations.SerializedName;
import java.util.ArrayList;
import java.util.List;

public class CityHighlightResponse {

    @SerializedName("highlight") private List<CityHighlight> cityHighlightList = new ArrayList<>();

    public List<CityHighlight> getCityHighlightList() {
        return cityHighlightList;
    }
}
