package com.essplore.essplore.api.response;

import com.essplore.essplore.models.User;
import com.google.gson.annotations.SerializedName;

public class LoginResponse {

  @SerializedName("access_token") private String accessToken;

  @SerializedName("refresh_token") private String refreshToken;

  @SerializedName("user") private User user;

  private String message;

  public LoginResponse() {
    // Intended to be empty
  }

  public String getAccessToken() {
    return accessToken;
  }

  public String getRefreshToken() {
    return refreshToken;
  }

  public String getMessage() {
    return message;
  }

  public User getUser() {
    return user;
  }
}
