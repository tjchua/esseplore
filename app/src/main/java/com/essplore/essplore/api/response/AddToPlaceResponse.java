package com.essplore.essplore.api.response;

import com.google.gson.annotations.SerializedName;

public class AddToPlaceResponse {

  @SerializedName("status")
  private int status;

  @SerializedName("message")
  private String message;

  public AddToPlaceResponse() {
  }

  public int getStatus() {
    return status;
  }

  public String getMessage() {
    return message;
  }
}
