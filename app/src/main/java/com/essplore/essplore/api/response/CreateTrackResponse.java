package com.essplore.essplore.api.response;

import android.os.Parcel;
import android.os.Parcelable;
import com.essplore.essplore.models.MyTrack;
import com.essplore.essplore.models.Tracks;
import com.google.gson.annotations.SerializedName;
import java.util.ArrayList;
import java.util.List;

public class CreateTrackResponse implements Parcelable {

  private int status;

  private String message;

  @SerializedName("details") private MyTrack myTrack;
  @SerializedName("track") private List<Tracks> tracks = new ArrayList<>();

  public static final Creator<CreateTrackResponse> CREATOR = new Creator<CreateTrackResponse>() {
    @Override public CreateTrackResponse createFromParcel(Parcel in) {
      return new CreateTrackResponse(in);
    }

    @Override public CreateTrackResponse[] newArray(int size) {
      return new CreateTrackResponse[size];
    }
  };

  protected CreateTrackResponse(Parcel in) {
    myTrack = (MyTrack) in.readValue(MyTrack.class.getClassLoader());
    tracks = in.createTypedArrayList(Tracks.CREATOR);
  }

  @Override public int describeContents() {
    return 0;
  }

  @Override public void writeToParcel(Parcel parcel, int i) {
    parcel.writeValue(myTrack);
    parcel.writeTypedList(tracks);
  }

  public CreateTrackResponse() {
  }

  public int getStatus() {
    return status;
  }

  public String getMessage() {
    return message;
  }

  public MyTrack getMyTrack() {
    return myTrack;
  }

  public List<Tracks> getTracks() {
    return tracks;
  }
}
