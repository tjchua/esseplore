package com.essplore.essplore.api.requests;

import com.google.gson.annotations.SerializedName;

public class AddPlaceToTrackRequest {

    @SerializedName("track_id")
    private int trackId;

    @SerializedName("place_id")
    private int placeId;


    public AddPlaceToTrackRequest(int trackId, int placeId) {
        this.trackId = trackId;
        this.placeId = placeId;
    }

    public int getTrackId() {
        return trackId;
    }

    public int getPlaceId() {
        return placeId;
    }
}
