package com.essplore.essplore.api.response;

public class ErrorResponse {

  private int status;

  private String message;

  public ErrorResponse() {
    //Intended to be empty
  }

  public int getStatus() {
    return status;
  }

  public String getMessage() {
    return message;
  }
}
