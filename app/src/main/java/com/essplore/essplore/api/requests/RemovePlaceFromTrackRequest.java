package com.essplore.essplore.api.requests;

import com.google.gson.annotations.SerializedName;

public class RemovePlaceFromTrackRequest {

    @SerializedName("track_record_id")
    private int trackPlaceId;

    public RemovePlaceFromTrackRequest(int trackPlaceId) {
        this.trackPlaceId = trackPlaceId;
    }

    public int getTrackPlaceId() {
        return trackPlaceId;
    }
}
