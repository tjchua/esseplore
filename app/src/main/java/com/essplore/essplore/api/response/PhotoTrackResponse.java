package com.essplore.essplore.api.response;

import android.os.Parcel;
import android.os.Parcelable;

import com.essplore.essplore.models.TrackPicture;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class PhotoTrackResponse implements Parcelable {

  private int status;

  private String message;

  @SerializedName("pictures") private List<TrackPicture> trackPictures = new ArrayList<>();

  public static final Creator<PhotoTrackResponse> CREATOR = new Creator<PhotoTrackResponse>() {
    @Override public PhotoTrackResponse createFromParcel(Parcel in) {
      return new PhotoTrackResponse(in);
    }

    @Override public PhotoTrackResponse[] newArray(int size) {
      return new PhotoTrackResponse[size];
    }
  };

  protected PhotoTrackResponse(Parcel in) {
    trackPictures = in.createTypedArrayList(TrackPicture.CREATOR);
  }

  @Override public int describeContents() {
    return 0;
  }

  @Override public void writeToParcel(Parcel parcel, int i) {
    parcel.writeTypedList(trackPictures);
  }

  public PhotoTrackResponse() {
  }

  public int getStatus() {
    return status;
  }

  public String getMessage() {
    return message;
  }

  public List<TrackPicture> getPhotoTracks() {
    return trackPictures;
  }
}
