package com.essplore.essplore.api.response;

import com.essplore.essplore.models.CivilStatus;
import com.essplore.essplore.models.Country;
import com.google.gson.annotations.SerializedName;
import java.util.ArrayList;
import java.util.List;

public class CountryResponse {

  @SerializedName("Countries") private List<Country> countryList = new ArrayList<>();

  @SerializedName("CivilStatus") private List<CivilStatus> civilStatusList = new ArrayList<>();

  public List<Country> getCountryList() {
    return countryList;
  }
}
