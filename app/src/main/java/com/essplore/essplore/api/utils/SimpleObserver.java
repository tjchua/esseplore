package com.essplore.essplore.api.utils;

import io.reactivex.Observer;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Consumer;

public class SimpleObserver<T> implements Observer<T>, Consumer<T> {

  @Override public void onSubscribe(Disposable d) {

  }

  @Override public void onNext(T t) {

  }

  @Override public void onError(Throwable e) {

  }

  @Override public void onComplete() {

  }

  @Override public void accept(T t) throws Exception {
  }

}
