package com.essplore.essplore.api.requests;

import com.google.gson.annotations.SerializedName;

public class UserEditRequest {

    @SerializedName("email")
    private String emailAddress;

    @SerializedName("country_id")
    private int countryId;

    @SerializedName("birthday")
    private String bday;

    @SerializedName("civil_status_id")
    private int civilStatus;

    @SerializedName("mobile")
    private String mobileNum;


    public UserEditRequest() {
    }

    public UserEditRequest(String emailAddress, int countryId) {
        this.emailAddress = emailAddress;
        this.countryId = countryId;
    }

    public UserEditRequest(String emailAddress, int countryId, String bday, String mobileNum, int civilStatus) {
        this.emailAddress = emailAddress;
        this.countryId = countryId;
        this.bday = bday;
        this.mobileNum = mobileNum;
        this.civilStatus = civilStatus;
    }

    public String getBday() {
        return bday;
    }

    public int getCivilStatus() {
        return civilStatus;
    }

    public String getMobileNum() {
        return mobileNum;
    }

    public String getEmailAddress() {
        return emailAddress;
    }

    public int getCountryId() {
        return countryId;
    }
}
