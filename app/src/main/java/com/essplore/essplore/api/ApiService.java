package com.essplore.essplore.api;

import com.essplore.essplore.api.requests.AddPlaceToTrackRequest;
import com.essplore.essplore.api.requests.AddUserPersonalityRequest;
import com.essplore.essplore.api.requests.ChangePasswordRequest;
import com.essplore.essplore.api.requests.ChangeRecordStatusRequest;
import com.essplore.essplore.api.requests.CityHighlightRequest;
import com.essplore.essplore.api.requests.CityRequest;
import com.essplore.essplore.api.requests.CreateTrackRequest;
import com.essplore.essplore.api.requests.DeleteSpecificTrackRequest;
import com.essplore.essplore.api.requests.LoginRequest;
import com.essplore.essplore.api.requests.RegisterRequest;
import com.essplore.essplore.api.requests.RemovePlaceFromTrackRequest;
import com.essplore.essplore.api.requests.ResetPasswordRequest;
import com.essplore.essplore.api.requests.UserEditRequest;
import com.essplore.essplore.api.response.AddToPlaceResponse;
import com.essplore.essplore.api.response.AddUserPersonalityResponse;
import com.essplore.essplore.api.response.CityHighlightResponse;
import com.essplore.essplore.api.response.CityResponse;
import com.essplore.essplore.api.response.CountryResponse;
import com.essplore.essplore.api.response.CreateTrackResponse;
import com.essplore.essplore.api.response.DeleteSpecificTrackResponse;
import com.essplore.essplore.api.response.GetTrackListResponse;
import com.essplore.essplore.api.response.LoginResponse;
import com.essplore.essplore.api.response.PhotoTrackResponse;
import com.essplore.essplore.api.response.UploadProfileImageResponse;
import com.essplore.essplore.api.response.UserEditResponse;
import com.essplore.essplore.application.AppConstants;
import io.reactivex.Flowable;
import okhttp3.MultipartBody;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Query;

public interface ApiService {

  @POST(AppConstants.LOGIN_API) Flowable<LoginResponse> loginUser(@Body LoginRequest request);

  @POST(AppConstants.REGISTER_API) Flowable<LoginResponse> registerUser(@Body RegisterRequest request);

  @GET(AppConstants.COUNTRIES_API) Flowable<CountryResponse> getCountries();

  @POST(AppConstants.FORGOT_PASSWORD_API) Flowable<String> resetPassword(
      @Body ResetPasswordRequest resetPasswordRequest);

  @POST(AppConstants.USER_EDIT_API) Flowable<UserEditResponse> editUserInfo(@Header(AppConstants.HEADER_PARAM) String header,
                                                                            @Body UserEditRequest userEditRequest);

  @POST(AppConstants.CHANGE_PASSWORD_API) Flowable<String> changePassword(
      @Header(AppConstants.HEADER_PARAM) String header, @Body ChangePasswordRequest changePasswordRequest);

  @Multipart @POST(AppConstants.PROFILE_PIC_API) Flowable<UploadProfileImageResponse> uploadProfile(
      @Header(AppConstants.HEADER_PARAM) String header, @Part MultipartBody.Part file);

  @POST(AppConstants.CITY_API) Flowable<CityResponse> getCity(@Body CityRequest cityRequest);

  @POST(AppConstants.CHANGE_RECORD_STATUS) Flowable<String> changeRecordStatus(
          @Header(AppConstants.HEADER_PARAM) String header, @Body ChangeRecordStatusRequest changeRecordStatusRequest);

  @POST(AppConstants.CITY_HIGHLIGHTS_API) Flowable<CityHighlightResponse> getCityHighlight(
      @Body CityHighlightRequest cityHighlightRequest);

  @POST(AppConstants.CREATE_TRACK_API) Flowable<CreateTrackResponse> createTrack(
      @Header(AppConstants.HEADER_PARAM) String header, @Body CreateTrackRequest createTrackRequest);

  @GET(AppConstants.CREATE_GET_TRACK_LIST) Flowable<GetTrackListResponse> getTrackList(
      @Header(AppConstants.HEADER_PARAM) String header);

  @POST(AppConstants.ADD_PLACE_TO_TRACK) Flowable<AddToPlaceResponse> addPlaceToTrack(
      @Header(AppConstants.HEADER_PARAM) String header, @Body AddPlaceToTrackRequest addPlaceToTrackRequest);

  @GET(AppConstants.GET_SPECIFIC_TRACK) Flowable<CreateTrackResponse> getSpecificTrack(
      @Header(AppConstants.HEADER_PARAM) String header, @Query(AppConstants.TRACK_ID_API) int trackId);

  @GET(AppConstants.GET_SPECIFIC_TRACK) Flowable<PhotoTrackResponse> getPhotoTrack(
          @Header(AppConstants.HEADER_PARAM) String header, @Query(AppConstants.TRACK_ID_API) int trackId);

  @POST(AppConstants.REMOVE_PLACE_FROM_TRACK) Flowable<String> removePlaceFromTrack(
          @Header(AppConstants.HEADER_PARAM) String header, @Body RemovePlaceFromTrackRequest removePlaceinTrackRequest);

  @POST(AppConstants.DELETE_SPECIFIC_TRACK) Flowable<DeleteSpecificTrackResponse> deleteSpecificTrack(
          @Header(AppConstants.HEADER_PARAM) String header, @Body DeleteSpecificTrackRequest deleteSpecificTrackRequest);

  @POST(AppConstants.ADD_USER_PERSONALITY) Flowable<AddUserPersonalityResponse> addUserPersonality(
          @Header(AppConstants.HEADER_PARAM) String header, @Body AddUserPersonalityRequest addUserPersonalityRequest);
}
