package com.essplore.essplore.api.requests;

import com.google.gson.annotations.SerializedName;

public class AddUserPersonalityRequest {

    @SerializedName("city_id") private int cityId;
    @SerializedName("travel_buddy") private int travelBuddy;
    @SerializedName("number_visit") private String numberOfVisit;
    @SerializedName("type_of_spots") private String typeOfSpots;
    @SerializedName("interest") private String interests;

    public AddUserPersonalityRequest(int cityId, int travelBuddy, String numberOfVisit, String typeOfSpots, String interests) {
        this.cityId = cityId;
        this.travelBuddy = travelBuddy;
        this.numberOfVisit = numberOfVisit;
        this.typeOfSpots = typeOfSpots;
        this.interests = interests;
    }

    public int getCityId() {
        return cityId;
    }

    public int getTravelBuddy() {
        return travelBuddy;
    }

    public String getNumberOfVisit() {
        return numberOfVisit;
    }

    public String getTypeOfSpots() {
        return typeOfSpots;
    }

    public String getInterests() {
        return interests;
    }
}
