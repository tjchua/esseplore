package com.essplore.essplore.api.requests;

import com.google.gson.annotations.SerializedName;

public class DeleteSpecificTrackRequest {

    @SerializedName("track_id")
    private int trackId;

    public DeleteSpecificTrackRequest(int trackId) {
        this.trackId = trackId;
    }

    public int getTrackId() {
        return trackId;
    }
}
