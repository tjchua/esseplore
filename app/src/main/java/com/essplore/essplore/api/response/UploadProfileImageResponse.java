package com.essplore.essplore.api.response;

import com.essplore.essplore.models.User;

public class UploadProfileImageResponse {

    private String status;
    private String message;

    private User user;


    public String getStatus() {
        return status;
    }

    public String getMessage() {
        return message;
    }

    public User getUser() {
        return user;
    }
}
