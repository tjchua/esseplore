package com.essplore.essplore.api.managers;

import android.util.Log;

import com.essplore.essplore.api.ApiService;
import com.essplore.essplore.api.requests.AddPlaceToTrackRequest;
import com.essplore.essplore.api.requests.AddUserPersonalityRequest;
import com.essplore.essplore.api.requests.ChangePasswordRequest;
import com.essplore.essplore.api.requests.ChangeRecordStatusRequest;
import com.essplore.essplore.api.requests.CityHighlightRequest;
import com.essplore.essplore.api.requests.CityRequest;
import com.essplore.essplore.api.requests.CreateTrackRequest;
import com.essplore.essplore.api.requests.DeleteSpecificTrackRequest;
import com.essplore.essplore.api.requests.LoginRequest;
import com.essplore.essplore.api.requests.RegisterRequest;
import com.essplore.essplore.api.requests.RemovePlaceFromTrackRequest;
import com.essplore.essplore.api.requests.ResetPasswordRequest;
import com.essplore.essplore.api.requests.UserEditRequest;
import com.essplore.essplore.api.response.AddToPlaceResponse;
import com.essplore.essplore.api.response.AddUserPersonalityResponse;
import com.essplore.essplore.api.response.CityHighlightResponse;
import com.essplore.essplore.api.response.CityResponse;
import com.essplore.essplore.api.response.CountryResponse;
import com.essplore.essplore.api.response.CreateTrackResponse;
import com.essplore.essplore.api.response.DeleteSpecificTrackResponse;
import com.essplore.essplore.api.response.GetTrackListResponse;
import com.essplore.essplore.api.response.LoginResponse;
import com.essplore.essplore.api.response.PhotoTrackResponse;
import com.essplore.essplore.api.response.UploadProfileImageResponse;
import com.essplore.essplore.api.response.UserEditResponse;
import com.essplore.essplore.application.AppConstants;
import com.google.gson.Gson;

import io.reactivex.Flowable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import okhttp3.MultipartBody;

public class ApiManager {

  private final ApiService apiService;

  public ApiManager(final ApiService apiService) {
    this.apiService = apiService;
  }

  public Flowable<LoginResponse> loginUser(LoginRequest request) {
    return apiService.loginUser(request).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread());
  }

  public Flowable<LoginResponse> registerUser(RegisterRequest registerRequest) {
    return apiService.registerUser(registerRequest)
        .subscribeOn(Schedulers.io())
        .observeOn(AndroidSchedulers.mainThread());
  }

  public Flowable<CountryResponse> getCountries() {
    return apiService.getCountries().subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread());
  }

  public Flowable<String> resetPasswordRequest(ResetPasswordRequest resetPasswordRequest) {
    return apiService.resetPassword(resetPasswordRequest)
        .subscribeOn(Schedulers.io())
        .observeOn(AndroidSchedulers.mainThread());
  }

  public Flowable<UserEditResponse> userEditRequest(UserEditRequest userEditRequest, String token) {
    Log.e("Token", token);
    Log.e("UserEditRequest bday", userEditRequest.getBday());
    Log.e("UserEditRequest email", userEditRequest.getEmailAddress());
    Log.e("UserEditRequest mobile", userEditRequest.getMobileNum());
    Log.e("UserEditRequest status", String.valueOf(userEditRequest.getCivilStatus()));
    Log.e("UserEditRequest country", String.valueOf(userEditRequest.getCountryId()));
    Gson gson = new Gson();
      Log.e("UserEditRequest country", gson.toJson(userEditRequest));

    return apiService.editUserInfo(String.format(AppConstants.AUTHORIZATION_STRING_FORMAT, token), userEditRequest)
        .subscribeOn(Schedulers.io())
        .observeOn(AndroidSchedulers.mainThread());
  }

  public Flowable<String> changePassword(ChangePasswordRequest changePasswordRequest, String token) {
    return apiService.changePassword(String.format(AppConstants.AUTHORIZATION_STRING_FORMAT, token),
        changePasswordRequest).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread());
  }

  public Flowable<CityResponse> getCity(CityRequest cityRequest, String token) {
    return apiService.getCity(cityRequest).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread());
  }

  public Flowable<String> changeRecordStatus(ChangeRecordStatusRequest changeRecordStatusRequest, String token) {
    return apiService.changeRecordStatus(String.format(AppConstants.AUTHORIZATION_STRING_FORMAT, token), changeRecordStatusRequest).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread());
  }

  public Flowable<CityHighlightResponse> getCityHighlight(CityHighlightRequest cityHighlightRequest) {
    return apiService.getCityHighlight(cityHighlightRequest)
        .subscribeOn(Schedulers.io())
        .observeOn(AndroidSchedulers.mainThread());
  }

  public Flowable<UploadProfileImageResponse> uploadProfilePicture(MultipartBody.Part file, String token) {
    return apiService.uploadProfile(String.format(AppConstants.AUTHORIZATION_STRING_FORMAT, token), file)
        .subscribeOn(Schedulers.io())
        .observeOn(AndroidSchedulers.mainThread());
  }

  public Flowable<CreateTrackResponse> createTrack(CreateTrackRequest createTrackRequest, String token) {
    return apiService.createTrack(String.format(AppConstants.AUTHORIZATION_STRING_FORMAT, token), createTrackRequest)
        .subscribeOn(Schedulers.io())
        .observeOn(AndroidSchedulers.mainThread());
  }

  public Flowable<GetTrackListResponse> getTrackList(String token) {
    return apiService.getTrackList(String.format(AppConstants.AUTHORIZATION_STRING_FORMAT, token))
        .subscribeOn(Schedulers.io())
        .observeOn(AndroidSchedulers.mainThread());
  }

  public Flowable<AddToPlaceResponse> addPlaceToTrack(AddPlaceToTrackRequest addPlaceToTrackRequest, String token) {
    return apiService.addPlaceToTrack(String.format(AppConstants.AUTHORIZATION_STRING_FORMAT, token),
        addPlaceToTrackRequest).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread());
  }

  public Flowable<CreateTrackResponse> getSpecificTrack(int trackId, String token) {
    return apiService.getSpecificTrack(String.format(AppConstants.AUTHORIZATION_STRING_FORMAT, token), trackId)
        .subscribeOn(Schedulers.io())
        .observeOn(AndroidSchedulers.mainThread());
  }

  public Flowable<PhotoTrackResponse> getPhotoTrack(int trackId, String token) {
    return apiService.getPhotoTrack(String.format(AppConstants.AUTHORIZATION_STRING_FORMAT, token), trackId)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread());
  }

  public Flowable<String> removePlaceFromTrack(RemovePlaceFromTrackRequest removePlaceFromTrackRequest, String token) {
    return apiService.removePlaceFromTrack(String.format(AppConstants.AUTHORIZATION_STRING_FORMAT, token), removePlaceFromTrackRequest)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread());
  }

  public Flowable<DeleteSpecificTrackResponse> deleteSpecificTrack(DeleteSpecificTrackRequest deleteSpecificTrackRequest, String token) {
    return apiService.deleteSpecificTrack(String.format(AppConstants.AUTHORIZATION_STRING_FORMAT, token), deleteSpecificTrackRequest)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread());
  }

  public Flowable<AddUserPersonalityResponse> addUserPersonality(AddUserPersonalityRequest addUserPersonalityRequest, String token) {
    return apiService.addUserPersonality(String.format(AppConstants.AUTHORIZATION_STRING_FORMAT, token), addUserPersonalityRequest)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread());
  }


}
