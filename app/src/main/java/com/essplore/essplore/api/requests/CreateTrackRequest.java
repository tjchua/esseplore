package com.essplore.essplore.api.requests;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.gson.annotations.SerializedName;
import org.apache.commons.lang3.StringUtils;

public class CreateTrackRequest implements Parcelable {

  public static final Creator<CreateTrackRequest> CREATOR = new Creator<CreateTrackRequest>() {
    @Override public CreateTrackRequest createFromParcel(Parcel in) {
      return new CreateTrackRequest(in);
    }

    @Override public CreateTrackRequest[] newArray(int size) {
      return new CreateTrackRequest[size];
    }
  };

  @SerializedName("user_id") private int userId;
  @SerializedName("city_id") private int cityId;
  @SerializedName("country_id") private int countryId;
  @SerializedName("track_name") private String trackName;
  @SerializedName("date_from") private String dateFrom;
  @SerializedName("date_to") private String dateTo;

  public CreateTrackRequest() {
  }

  public CreateTrackRequest(int userId, int cityId, int countryId, String trackName, String dateFrom, String dateTo) {
    this.userId = userId;
    this.cityId = cityId;
    this.countryId = countryId;
    this.trackName = trackName;
    this.dateFrom = dateFrom;
    this.dateTo = dateTo;
  }

  public CreateTrackRequest(Builder builder) {
    this.userId = builder.userId;
    this.cityId = builder.cityId;
    this.countryId = builder.countryId;
    this.trackName = builder.trackName;
    this.dateFrom = builder.dateFrom;
    this.dateTo = builder.dateTo;
  }

  protected CreateTrackRequest(Parcel in) {
    userId = in.readInt();
    cityId = in.readInt();
    countryId = in.readInt();
    trackName = in.readString();
    dateFrom = in.readString();
    dateTo = in.readString();
  }

  public int getLoggedInUserId() {
    return userId;
  }

  public int getCityId() {
    return cityId;
  }

  public int getCountryId() {
    return countryId;
  }

  public String getDateFrom() {
    return dateFrom;
  }

  public String getDateTo() {
    return dateTo;
  }

  public String getTrackName() {
    return trackName;
  }

  @Override public int describeContents() {
    return 0;
  }

  @Override public void writeToParcel(Parcel dest, int flags) {
    dest.writeInt(userId);
    dest.writeInt(cityId);
    dest.writeInt(countryId);
    dest.writeString(trackName);
    dest.writeString(dateFrom);
    dest.writeString(dateTo);
  }

  public static class Builder {

    private int userId;
    private int cityId;
    private int countryId;
    private String trackName;
    private String dateFrom;
    private String dateTo;

    public Builder(int userId, int cityId, int countryId, String trackName) {
      this.userId = userId;
      this.cityId = cityId;
      this.countryId = countryId;
      this.trackName = trackName;
      this.dateFrom = StringUtils.EMPTY;
      this.dateTo = StringUtils.EMPTY;
    }

    public CreateTrackRequest build() {
      return new CreateTrackRequest(this);
    }
  }
}