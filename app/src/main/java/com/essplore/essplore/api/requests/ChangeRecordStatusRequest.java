package com.essplore.essplore.api.requests;

import com.google.gson.annotations.SerializedName;

public class ChangeRecordStatusRequest {

    @SerializedName("track_record_id")
    private int trackPlaceId;

    @SerializedName("track_status_id")
    private int trackStatusId;

    public int getTrackPlaceId() {
        return trackPlaceId;
    }

    public int getTrackStatusId() {
        return trackStatusId;
    }

    public ChangeRecordStatusRequest(int trackPlaceId, int trackStatusId) {
        this.trackPlaceId = trackPlaceId;
        this.trackStatusId = trackStatusId;
    }

}
