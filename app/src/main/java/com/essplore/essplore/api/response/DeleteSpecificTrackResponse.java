package com.essplore.essplore.api.response;

public class DeleteSpecificTrackResponse {

    private int status;
    private String message;

    public int getStatus() {
        return status;
    }

    public String getMessage() {
        return message;
    }
}
