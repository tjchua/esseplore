package com.essplore.essplore.api.requests;

import com.essplore.essplore.models.CityHighlight;
import com.google.gson.annotations.SerializedName;
import java.util.ArrayList;
import java.util.List;

public class CityHighlightRequest {

    @SerializedName("city_id") private int cityId;
    @SerializedName("country_id") private int countryId;

    public CityHighlightRequest(int cityId, int countryId) {
        this.cityId = cityId;
        this.countryId = countryId;
    }

    public int getCityId() {
        return cityId;
    }

    public int getCountryId() {
        return countryId;
    }
}
