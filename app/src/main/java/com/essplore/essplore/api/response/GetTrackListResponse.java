package com.essplore.essplore.api.response;

import com.essplore.essplore.models.MyTrack;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class GetTrackListResponse {

    private int status;

    private String message;

    @SerializedName("tracks") private List<MyTrack> myTracks;

    public GetTrackListResponse() {
    }

    public int getStatus() {
        return status;
    }

    public String getMessage() {
        return message;
    }

    public List<MyTrack> getMyTracks() {
        return myTracks;
    }
}
