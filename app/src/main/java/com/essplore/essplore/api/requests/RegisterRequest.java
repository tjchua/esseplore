package com.essplore.essplore.api.requests;

import com.google.gson.annotations.SerializedName;

public class RegisterRequest {

  private String username;
  private String password;
  @SerializedName("country_id") private int countryId;
  private String birthday;
  private String mobile;
  private String email;

  public RegisterRequest() {
    // Intended to be empty.
  }

  public RegisterRequest(String username, String password, String mobile, int countryId,
      String birthday, String email) {
    this.username = username;
    this.password = password;
    this.countryId = countryId;
    this.birthday = birthday;
    this.mobile = mobile;
    this.email = email;
  }

  public String getUsername() {
    return username;
  }

  public String getPassword() {
    return password;
  }

  public String getBirthday() {
    return birthday;
  }

  public int getCountryId() {
    return countryId;
  }

  public String getMobile() {
    return mobile;
  }

  public String getEmail() {
    return email;
  }
}
