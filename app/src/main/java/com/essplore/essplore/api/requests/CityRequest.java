package com.essplore.essplore.api.requests;

import com.google.gson.annotations.SerializedName;

public class CityRequest {

    @SerializedName("city_name") private String cityName;

    public CityRequest(String cityName) {
        this.cityName = cityName;
    }

    public String getCityName() {
        return cityName;
    }
}