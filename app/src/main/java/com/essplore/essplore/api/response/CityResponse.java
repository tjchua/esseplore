package com.essplore.essplore.api.response;

import com.essplore.essplore.models.City;
import com.google.gson.annotations.SerializedName;
import java.util.ArrayList;
import java.util.List;

public class CityResponse {

    @SerializedName("cities") private List<City> countryList = new ArrayList<>();

    public List<City> getCityList() {
        return countryList;
    }
}
