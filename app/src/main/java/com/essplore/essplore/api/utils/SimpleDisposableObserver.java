package com.essplore.essplore.api.utils;

import io.reactivex.observers.DisposableObserver;

public class SimpleDisposableObserver<T> extends DisposableObserver<T> {
  @Override public void onNext(T t) {

  }

  @Override public void onError(Throwable e) {

  }

  @Override public void onComplete() {

  }
}
