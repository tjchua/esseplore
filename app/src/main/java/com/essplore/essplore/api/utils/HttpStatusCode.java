package com.essplore.essplore.api.utils;

public final class HttpStatusCode {
  public static final int UNAUTHORIZED = 401;
  public static final int SUCCESS_THROWABLE = 200;
}
