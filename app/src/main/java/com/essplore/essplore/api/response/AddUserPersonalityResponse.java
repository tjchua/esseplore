package com.essplore.essplore.api.response;

public class AddUserPersonalityResponse {

    private int status;
    private String message;

    public int getStatus() {
        return status;
    }

    public String getMessage() {
        return message;
    }
}
