package com.essplore.essplore.api.requests;

public class ResetPasswordRequest {

  private String email;

  public ResetPasswordRequest(String email) {
    this.email = email;
  }

  public String getEmail() {
    return email;
  }
}
