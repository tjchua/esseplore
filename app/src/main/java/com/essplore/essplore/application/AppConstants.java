package com.essplore.essplore.application;

public class AppConstants {

  public static final String LOGIN_API = "login_with_email";
  public static final String HEADER_PARAM = "Authorization";
  public static final String REGISTER_API = "register_with_email";
  public static final String COUNTRIES_API = "predefined_info";
  public static final String FORGOT_PASSWORD_API = "forgot_password";
  public static final String USER_EDIT_API = "edit_user";
  public static final String CHANGE_PASSWORD_API = "change_password";
  public static final String PROFILE_PIC_API = "upload_profile_image";
  public static final String CITY_API = "get_city";
  public static final String PICTURE_PARAM = "picture";
  public static final String CITY_HIGHLIGHTS_API = "get_city_highlights";
  public static final String CREATE_TRACK_API = "create_track";
  public static final String CREATE_GET_TRACK_LIST = "get_track_list";
  public static final String ADD_PLACE_TO_TRACK = "add_place_to_track";
  public static final String GET_SPECIFIC_TRACK = "get_specific_track";
  public static final String REMOVE_PLACE_FROM_TRACK = "remove_place_from_track";
  public static final String DELETE_SPECIFIC_TRACK = "delete_specific_track";
  public static final String ADD_USER_PERSONALITY = "add_user_personnality";
  public static final String TRACK_ID_API = "track_id";
  public static final String CHANGE_RECORD_STATUS = "change_record_status";

  public static final String DATE_PICKER_TAG = "DatepickerDialog";
  public static final String CHANGE_PASSWORD_TAG = "ChangepasswordTag";
  public static final String AUTHORIZATION_STRING_FORMAT = "Bearer %s";
  public static final String DEFAULT_DATE_VALUE = "0";

  public static final String CITY_OBJECT = "cityObject";
  public static final String CITY_HIGHLIGHT_OBJECT = "cityHighlightObject";
  public static final String CREATE_TRACK_OBJECT = "createTrackObject";
  public static final String CREATE_TRACK_RESPONSE_OBJECT = "createTrackResponseObject";
  public static final String MY_TRACK_OBJECT = "myTrackObject";

  public static final String CITY_ID = "cityId";
  public static final String GET_COUNTRY_ID = "country_Id";
  public static final String GET_STRIPE_ACCESS = "get_stripe_access";
  public static final String AUTHORIZATION = "Authorization";
  public static final String PUBLISHASBLE_KEY = "pk_test_qMLDRona8rBX1jO2OvKultdX";
  public static final String BEARER = "Bearer";
  public static final String JSON_MEDIA_TYPE = "application/json; charset=utf-8";
  public static final String TOKEN = "token";
  public static final String AMOUNT = "amount";
  public static final String PHOTO_TRACK = "photo_track";
  public static final String PHOTO_TRACK_TRACK_ID_PARAM = "track_id";

  public static final int DEFAULT_INT_VALUE = 0;
  public static final int NON_EXISTING_OBJECT_VALUE = -1;
  public static final int LOGIN_FRAGMENT_COUNT = 2;
  public static final int HOME_FRAGMENT_COUNT = 5;

  public static final int TAKE_PICTURE_REQUEST = 1000;
  public static final int PICK_PHOTO = 1001;
  public static final String PLACES_PROVINCE_STRING_FORMAT = "%s, %s";
  public static final String ADD_PLACE_TO_TRACK_OBJECT = "addPlaceToTrackObject";
  public static final int ADD_PLACE_REQUEST = 1002;
  public static final int PLACE_HIGHLIGHT_REQUEST = 1003;
  public static final String TRACKS_OBJECT = "tracksObject";
  public static final String ISATTRACTIONNEXT = "isAttractionNext";
  public static final int STATUS_SUCCESS = 200;

}

