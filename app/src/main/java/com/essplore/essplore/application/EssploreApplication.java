package com.essplore.essplore.application;

import android.content.Context;

import com.essplore.essplore.dagger.components.ApplicationComponent;
import com.essplore.essplore.dagger.components.DaggerApplicationComponent;
import com.essplore.essplore.dagger.components.activities.AddTripComponent;
import com.essplore.essplore.dagger.components.activities.CalendarComponent;
import com.essplore.essplore.dagger.components.activities.CityHighlightsComponent;
import com.essplore.essplore.dagger.components.activities.DirectionsComponent;
import com.essplore.essplore.dagger.components.activities.FiltersComponent;
import com.essplore.essplore.dagger.components.activities.HomeComponent;
import com.essplore.essplore.dagger.components.activities.LoginComponent;
import com.essplore.essplore.dagger.components.activities.MainComponent;
import com.essplore.essplore.dagger.components.activities.PlaceHighlightsComponent;
import com.essplore.essplore.dagger.components.activities.PlacesComponent;
import com.essplore.essplore.dagger.components.activities.ProfileComponent;
import com.essplore.essplore.dagger.components.activities.TimelineComponent;
import com.essplore.essplore.dagger.components.fragments.AccountComponent;
import com.essplore.essplore.dagger.components.fragments.ChangePasswordComponent;
import com.essplore.essplore.dagger.components.fragments.ForgotPasswordFragmentComponent;
import com.essplore.essplore.dagger.components.fragments.LoginFragmentComponent;
import com.essplore.essplore.dagger.components.fragments.RegisterFragmentComponent;
import com.essplore.essplore.dagger.components.fragments.SearchComponent;
import com.essplore.essplore.dagger.components.fragments.TripsComponent;
import com.essplore.essplore.dagger.modules.ApplicationModule;
import com.essplore.essplore.dagger.modules.activities.AddTripModule;
import com.essplore.essplore.dagger.modules.activities.CalendarModule;
import com.essplore.essplore.dagger.modules.activities.CityHighlightsModule;
import com.essplore.essplore.dagger.modules.activities.DirectionsModule;
import com.essplore.essplore.dagger.modules.activities.FiltersModule;
import com.essplore.essplore.dagger.modules.activities.HomeModule;
import com.essplore.essplore.dagger.modules.activities.LoginModule;
import com.essplore.essplore.dagger.modules.activities.MainModule;
import com.essplore.essplore.dagger.modules.activities.PlaceHighlightsModule;
import com.essplore.essplore.dagger.modules.activities.PlacesModule;
import com.essplore.essplore.dagger.modules.activities.ProfileModule;
import com.essplore.essplore.dagger.modules.activities.TimelineModule;
import com.essplore.essplore.dagger.modules.fragments.AccountModule;
import com.essplore.essplore.dagger.modules.fragments.ChangePasswordModule;
import com.essplore.essplore.dagger.modules.fragments.ForgotPasswordFragmentModule;
import com.essplore.essplore.dagger.modules.fragments.LoginFragmentModule;
import com.essplore.essplore.dagger.modules.fragments.RegisterFragmentModule;
import com.essplore.essplore.dagger.modules.fragments.SearchModule;
import com.essplore.essplore.dagger.modules.fragments.TripsModule;
import com.essplore.essplore.ui.activities.MainActivity;
import com.essplore.essplore.ui.activities.calendar.view.CalendarActivity;
import com.essplore.essplore.ui.activities.cityhighlights.view.CityHighlightsActivity;
import com.essplore.essplore.ui.activities.directions.view.DirectionsActivity;
import com.essplore.essplore.ui.activities.filters.view.FiltersActivity;
import com.essplore.essplore.ui.activities.home.fragments.view.AccountFragment;
import com.essplore.essplore.ui.activities.home.fragments.view.SearchFragment;
import com.essplore.essplore.ui.activities.home.fragments.view.TripsFragment;
import com.essplore.essplore.ui.activities.home.view.HomeActivity;
import com.essplore.essplore.ui.activities.login.LoginActivity;
import com.essplore.essplore.ui.activities.login.fragments.view.ForgotPasswordFragment;
import com.essplore.essplore.ui.activities.login.fragments.view.LoginFragment;
import com.essplore.essplore.ui.activities.login.fragments.view.RegisterFragment;
import com.essplore.essplore.ui.activities.placehighlights.view.PlaceHighlightsActivity;
import com.essplore.essplore.ui.activities.places.view.PlacesActivity;
import com.essplore.essplore.ui.activities.profile.fragments.view.ChangePasswordFragment;
import com.essplore.essplore.ui.activities.profile.view.ProfileActivity;
import com.essplore.essplore.ui.activities.timeline.view.TimelineActivity;
import com.essplore.essplore.ui.activities.trip.view.AddTripActivity;

public class EssploreApplication extends BaseApplication {

  private ApplicationComponent applicationComponent;
  private MainComponent mainComponent;
  private LoginComponent loginComponent;
  private LoginFragmentComponent loginFragmentComponent;
  private RegisterFragmentComponent registerFragmentComponent;
  private ForgotPasswordFragmentComponent forgotPasswordFragmentComponent;
  private HomeComponent homeComponent;
  private TripsComponent tripsComponent;
  private SearchComponent searchComponent;
  private AccountComponent accountComponent;
  private ProfileComponent profileComponent;
  private ChangePasswordComponent changePasswordComponent;
  private CityHighlightsComponent cityHighlightsComponent;
  private PlaceHighlightsComponent placeHighlightsComponent;
  private AddTripComponent addTripComponent;
  private CalendarComponent calendarComponent;
  private PlacesComponent placesComponent;
  private TimelineComponent timelineComponent;
  private DirectionsComponent directionsComponent;
  private FiltersComponent filtersComponent;

  public static EssploreApplication get(Context context) {
    return (EssploreApplication) context.getApplicationContext();
  }

  @Override public void onCreate() {
    super.onCreate();
    initAppComponent();
  }

  private void initAppComponent() {
    applicationComponent =
        DaggerApplicationComponent.builder().applicationModule(new ApplicationModule(this)).build();
  }

  public LoginComponent createLoginComponent(LoginActivity activity) {
    loginComponent = applicationComponent.plus(new LoginModule(activity));
    return loginComponent;
  }

  public MainComponent createMainComponent(MainActivity activity) {
    mainComponent = applicationComponent.plus(new MainModule(activity));
    return mainComponent;
  }

  public LoginFragmentComponent createLoginFragmentComponent(LoginFragment fragment) {
    loginFragmentComponent = applicationComponent.plus(new LoginFragmentModule(fragment));
    return loginFragmentComponent;
  }

  public RegisterFragmentComponent createRegisterFragmentComponent(RegisterFragment fragment) {
    registerFragmentComponent = applicationComponent.plus(new RegisterFragmentModule(fragment));
    return registerFragmentComponent;
  }

  public ForgotPasswordFragmentComponent createForgotPasswordComponent(
      ForgotPasswordFragment fragment) {
    forgotPasswordFragmentComponent =
        applicationComponent.plus(new ForgotPasswordFragmentModule(fragment));
    return forgotPasswordFragmentComponent;
  }

  public HomeComponent createHomeComponent(HomeActivity activity) {
    homeComponent = applicationComponent.plus(new HomeModule(activity));
    return homeComponent;
  }

  public TripsComponent createTripsComponent(TripsFragment fragment) {
    tripsComponent = applicationComponent.plus(new TripsModule(fragment));
    return tripsComponent;
  }

  public SearchComponent createSearchComponent(SearchFragment fragment) {
    searchComponent = applicationComponent.plus(new SearchModule(fragment));
    return searchComponent;
  }

  public AccountComponent createAccountComponent(AccountFragment fragment) {
    accountComponent = applicationComponent.plus(new AccountModule(fragment));
    return accountComponent;
  }

  public ProfileComponent createProfileComponent(ProfileActivity activity) {
    profileComponent = applicationComponent.plus(new ProfileModule(activity));
    return profileComponent;
  }

  public ChangePasswordComponent createChangePasswordComponent(ChangePasswordFragment fragment) {
    changePasswordComponent = applicationComponent.plus(new ChangePasswordModule(fragment));
    return changePasswordComponent;
  }

  public CityHighlightsComponent createCityHighlightsComponent(CityHighlightsActivity activity) {
    cityHighlightsComponent = applicationComponent.plus(new CityHighlightsModule(activity));
    return cityHighlightsComponent;
  }

  public PlaceHighlightsComponent createPlaceHighlightsComponent(PlaceHighlightsActivity activity) {
    placeHighlightsComponent = applicationComponent.plus(new PlaceHighlightsModule(activity));
    return placeHighlightsComponent;
  }

  public AddTripComponent createAddTripComponent(AddTripActivity activity) {
    addTripComponent = applicationComponent.plus(new AddTripModule(activity));
    return addTripComponent;
  }

  public CalendarComponent createCalendarComponent(CalendarActivity activity) {
    calendarComponent = applicationComponent.plus(new CalendarModule(activity));
    return calendarComponent;
  }

  public PlacesComponent createPlacesComponent(PlacesActivity activity) {
    placesComponent = applicationComponent.plus(new PlacesModule(activity));
    return placesComponent;
  }

  public TimelineComponent createTimelineComponent(TimelineActivity activity) {
    timelineComponent = applicationComponent.plus(new TimelineModule(activity));
    return timelineComponent;
  }

  public DirectionsComponent createDirectionsComponent(DirectionsActivity activity) {
    directionsComponent = applicationComponent.plus(new DirectionsModule(activity));
    return directionsComponent;
  }

  public FiltersComponent createFiltersComponent(FiltersActivity activity) {
    filtersComponent = applicationComponent.plus(new FiltersModule(activity));
    return filtersComponent;
  }

  public void releaseLoginComponent() {
    loginComponent = null;
  }

  public void releaseMainComponent() {
    mainComponent = null;
  }

  public void releaseLoginFragmentComponent() {
    loginFragmentComponent = null;
  }

  public void releaseRegisterFragmentComponent() {
    registerFragmentComponent = null;
  }

  public void releaseForgotPasswordFragmentComponent() {
    forgotPasswordFragmentComponent = null;
  }

  public void releaseHomeComponent() {
    homeComponent = null;
  }

  public void releaseTripsComponent() {
    tripsComponent = null;
  }

  public void releaseSearchComponent() {
    searchComponent = null;
  }

  public void releaseAccountComponent() {
    accountComponent = null;
  }

  public void releaseProfileComponent() {
    profileComponent = null;
  }

  public void releaseChangePasswordComponent() {
    changePasswordComponent = null;
  }

  public void releaseCityHighlightsComponent() {
    cityHighlightsComponent = null;
  }

  public void releasePlaceHighlightsComponent() {
    placeHighlightsComponent = null;
  }

  public void releaseAddTripComponent() {
    addTripComponent = null;
  }

  public void releaseCalendarComponent() {
    calendarComponent = null;
  }

  public void releasePlacesComponent() {
    placesComponent = null;
  }

  public void releaseFiltersComponent() {
    filtersComponent = null;
  }
}
