package com.essplore.essplore.dagger.modules.api;

import com.essplore.essplore.api.ApiService;
import com.essplore.essplore.api.managers.ApiManager;
import com.essplore.essplore.dagger.scopes.UserScope;
import dagger.Module;
import dagger.Provides;
import retrofit2.Retrofit;

@UserScope @Module public class MainApiModule {

  @Provides @UserScope ApiService provideApiService(Retrofit retrofit) {
    return retrofit.create(ApiService.class);
  }

  @Provides @UserScope ApiManager provideApiManager(ApiService apiService) {
    return new ApiManager(apiService);
  }
}
