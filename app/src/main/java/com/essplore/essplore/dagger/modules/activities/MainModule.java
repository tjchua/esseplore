package com.essplore.essplore.dagger.modules.activities;

import android.support.v7.app.AlertDialog;
import com.essplore.essplore.dagger.scopes.UserScope;
import com.essplore.essplore.ui.activities.MainActivity;
import dagger.Module;
import dagger.Provides;
import io.reactivex.disposables.CompositeDisposable;

@UserScope @Module public class MainModule {
  private final MainActivity activity;

  public MainModule(final MainActivity activity) {
    this.activity = activity;
  }

  @Provides @UserScope MainActivity provideMainActivity() {
    return activity;
  }

  @Provides @UserScope AlertDialog provideAlertDialog(){
    return new AlertDialog.Builder(activity).create();
  }

  @Provides @UserScope CompositeDisposable provideCompositeDisposable() {
    return new CompositeDisposable();
  }
}
