package com.essplore.essplore.dagger.modules.activities;

import android.support.v7.app.AlertDialog;
import com.essplore.essplore.api.managers.ApiManager;
import com.essplore.essplore.dagger.scopes.UserScope;
import com.essplore.essplore.ui.activities.cityhighlights.presenter.CityHighlightsPresenterImpl;
import com.essplore.essplore.ui.activities.cityhighlights.view.CityHighlightsActivity;
import dagger.Module;
import dagger.Provides;
import io.reactivex.disposables.CompositeDisposable;

@UserScope @Module public class CityHighlightsModule {

  private CityHighlightsActivity activity;

  public CityHighlightsModule(CityHighlightsActivity activity) {
    this.activity = activity;
  }

  @Provides @UserScope CityHighlightsActivity provideCityHighlightsActivity() {
    return activity;
  }

  @Provides @UserScope CityHighlightsPresenterImpl provideCityHighlightsPresenterImpl(
      ApiManager manager) {
    return new CityHighlightsPresenterImpl(activity, manager);
  }

  @Provides @UserScope AlertDialog provideAlertDialog() {
    return new AlertDialog.Builder(activity).create();
  }

  @Provides @UserScope CompositeDisposable provideCompositeDisposable() {
    return new CompositeDisposable();
  }
}
