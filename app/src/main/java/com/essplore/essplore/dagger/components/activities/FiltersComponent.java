package com.essplore.essplore.dagger.components.activities;

import com.essplore.essplore.dagger.modules.activities.FiltersModule;
import com.essplore.essplore.dagger.modules.api.MainApiModule;
import com.essplore.essplore.dagger.scopes.UserScope;
import com.essplore.essplore.ui.activities.filters.view.FiltersActivity;
import dagger.Subcomponent;

@UserScope @Subcomponent(modules = { FiltersModule.class, MainApiModule.class })
public interface FiltersComponent {
  FiltersActivity inject(FiltersActivity activity);
}
