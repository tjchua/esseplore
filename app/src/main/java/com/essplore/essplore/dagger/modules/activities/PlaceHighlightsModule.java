package com.essplore.essplore.dagger.modules.activities;

import android.support.v7.app.AlertDialog;
import com.essplore.essplore.api.managers.ApiManager;
import com.essplore.essplore.dagger.scopes.UserScope;
import com.essplore.essplore.ui.activities.placehighlights.presenter.PlaceHighlightsPresenterImpl;
import com.essplore.essplore.ui.activities.placehighlights.view.PlaceHighlightsActivity;
import dagger.Module;
import dagger.Provides;
import io.reactivex.disposables.CompositeDisposable;

@UserScope @Module public class PlaceHighlightsModule {

  private final PlaceHighlightsActivity activity;

  public PlaceHighlightsModule(PlaceHighlightsActivity activity) {
    this.activity = activity;
  }

  @Provides @UserScope PlaceHighlightsActivity providePlaceHighlightsActivity() {
    return activity;
  }

  @Provides @UserScope AlertDialog provideAlertDialog() {
    return new AlertDialog.Builder(activity).create();
  }

  @Provides @UserScope PlaceHighlightsPresenterImpl providePlaceHighlightsImpl(ApiManager manager) {
    return new PlaceHighlightsPresenterImpl(activity, manager);
  }

  @Provides @UserScope CompositeDisposable provideCompositeDisposable() {
    return new CompositeDisposable();
  }
}
