package com.essplore.essplore.dagger.components.fragments;

import com.essplore.essplore.dagger.modules.api.MainApiModule;
import com.essplore.essplore.dagger.modules.fragments.ChangePasswordModule;
import com.essplore.essplore.dagger.scopes.UserScope;
import com.essplore.essplore.ui.activities.profile.fragments.view.ChangePasswordFragment;
import dagger.Subcomponent;

@UserScope @Subcomponent(modules = { ChangePasswordModule.class, MainApiModule.class })
public interface ChangePasswordComponent {
  ChangePasswordFragment inject(ChangePasswordFragment fragment);
}
