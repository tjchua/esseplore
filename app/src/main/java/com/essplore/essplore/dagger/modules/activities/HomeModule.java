package com.essplore.essplore.dagger.modules.activities;

import android.support.v7.app.AlertDialog;
import com.essplore.essplore.dagger.scopes.UserScope;
import com.essplore.essplore.ui.activities.home.adapter.HomeFragmentPagerAdapter;
import com.essplore.essplore.ui.activities.home.view.HomeActivity;
import dagger.Module;
import dagger.Provides;
import io.reactivex.disposables.CompositeDisposable;

@UserScope @Module public class HomeModule {

  private final HomeActivity activity;

  public HomeModule(HomeActivity activity) {
    this.activity = activity;
  }

  @Provides @UserScope HomeActivity provideHomeActivity() {
    return activity;
  }

  @Provides @UserScope AlertDialog provideAlertDialog() {
    return new AlertDialog.Builder(activity).create();
  }

  @Provides @UserScope HomeFragmentPagerAdapter provideHomeFragmentPagerAdapter() {
    return new HomeFragmentPagerAdapter(activity.getSupportFragmentManager(), activity);
  }

  @Provides @UserScope CompositeDisposable provideCompositeDisposable() {
    return new CompositeDisposable();
  }
}
