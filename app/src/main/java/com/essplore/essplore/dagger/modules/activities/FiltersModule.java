package com.essplore.essplore.dagger.modules.activities;

import android.support.v7.app.AlertDialog;
import com.essplore.essplore.api.managers.ApiManager;
import com.essplore.essplore.dagger.scopes.UserScope;
import com.essplore.essplore.ui.activities.filters.presenter.FiltersPresenterImpl;
import com.essplore.essplore.ui.activities.filters.view.FiltersActivity;
import dagger.Module;
import dagger.Provides;
import io.reactivex.disposables.CompositeDisposable;

@UserScope @Module public class FiltersModule {

  private final FiltersActivity activity;

  public FiltersModule(FiltersActivity activity) {
    this.activity = activity;
  }

  @Provides @UserScope FiltersActivity provideFiltersActivity() {
    return activity;
  }

  @Provides @UserScope CompositeDisposable provideCompositeDiposable() {
    return new CompositeDisposable();
  }

  @Provides @UserScope AlertDialog provideAlertDialog() {
    return new AlertDialog.Builder(activity).create();
  }

  @Provides @UserScope FiltersPresenterImpl provideFiltersPresenterImpl(ApiManager manager) {
    return new FiltersPresenterImpl(activity, manager);
  }
}
