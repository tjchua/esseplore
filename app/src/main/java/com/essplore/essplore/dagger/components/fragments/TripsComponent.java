package com.essplore.essplore.dagger.components.fragments;

import com.essplore.essplore.dagger.modules.api.MainApiModule;
import com.essplore.essplore.dagger.modules.fragments.TripsModule;
import com.essplore.essplore.dagger.scopes.UserScope;
import com.essplore.essplore.ui.activities.home.fragments.view.TripsFragment;
import dagger.Subcomponent;

@UserScope @Subcomponent(modules = { TripsModule.class, MainApiModule.class }) public interface TripsComponent {
  TripsFragment inject(TripsFragment fragment);
}
