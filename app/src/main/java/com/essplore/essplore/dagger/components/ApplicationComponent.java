package com.essplore.essplore.dagger.components;

import com.essplore.essplore.dagger.components.activities.AddTripComponent;
import com.essplore.essplore.dagger.components.activities.CalendarComponent;
import com.essplore.essplore.dagger.components.activities.CityHighlightsComponent;
import com.essplore.essplore.dagger.components.activities.DirectionsComponent;
import com.essplore.essplore.dagger.components.activities.FiltersComponent;
import com.essplore.essplore.dagger.components.activities.HomeComponent;
import com.essplore.essplore.dagger.components.activities.LoginComponent;
import com.essplore.essplore.dagger.components.activities.MainComponent;
import com.essplore.essplore.dagger.components.activities.PlaceHighlightsComponent;
import com.essplore.essplore.dagger.components.activities.PlacesComponent;
import com.essplore.essplore.dagger.components.activities.ProfileComponent;
import com.essplore.essplore.dagger.components.activities.TimelineComponent;
import com.essplore.essplore.dagger.components.fragments.AccountComponent;
import com.essplore.essplore.dagger.components.fragments.ChangePasswordComponent;
import com.essplore.essplore.dagger.components.fragments.ForgotPasswordFragmentComponent;
import com.essplore.essplore.dagger.components.fragments.LoginFragmentComponent;
import com.essplore.essplore.dagger.components.fragments.RegisterFragmentComponent;
import com.essplore.essplore.dagger.components.fragments.SearchComponent;
import com.essplore.essplore.dagger.components.fragments.TripsComponent;
import com.essplore.essplore.dagger.modules.ApplicationModule;
import com.essplore.essplore.dagger.modules.activities.AddTripModule;
import com.essplore.essplore.dagger.modules.activities.CalendarModule;
import com.essplore.essplore.dagger.modules.activities.CityHighlightsModule;
import com.essplore.essplore.dagger.modules.activities.DirectionsModule;
import com.essplore.essplore.dagger.modules.activities.FiltersModule;
import com.essplore.essplore.dagger.modules.activities.HomeModule;
import com.essplore.essplore.dagger.modules.activities.LoginModule;
import com.essplore.essplore.dagger.modules.activities.MainModule;
import com.essplore.essplore.dagger.modules.activities.PlaceHighlightsModule;
import com.essplore.essplore.dagger.modules.activities.PlacesModule;
import com.essplore.essplore.dagger.modules.activities.ProfileModule;
import com.essplore.essplore.dagger.modules.activities.TimelineModule;
import com.essplore.essplore.dagger.modules.api.ApiModule;
import com.essplore.essplore.dagger.modules.fragments.AccountModule;
import com.essplore.essplore.dagger.modules.fragments.ChangePasswordModule;
import com.essplore.essplore.dagger.modules.fragments.ForgotPasswordFragmentModule;
import com.essplore.essplore.dagger.modules.fragments.LoginFragmentModule;
import com.essplore.essplore.dagger.modules.fragments.RegisterFragmentModule;
import com.essplore.essplore.dagger.modules.fragments.SearchModule;
import com.essplore.essplore.dagger.modules.fragments.TripsModule;

import javax.inject.Singleton;

import dagger.Component;

@Singleton @Component(modules = { ApplicationModule.class, ApiModule.class }) public interface ApplicationComponent {
  LoginComponent plus(LoginModule module);

  MainComponent plus(MainModule module);

  LoginFragmentComponent plus(LoginFragmentModule module);

  RegisterFragmentComponent plus(RegisterFragmentModule module);

  ForgotPasswordFragmentComponent plus(ForgotPasswordFragmentModule module);

  HomeComponent plus(HomeModule module);

  TripsComponent plus(TripsModule module);

  SearchComponent plus(SearchModule module);

  AccountComponent plus(AccountModule module);

  ProfileComponent plus(ProfileModule module);

  ChangePasswordComponent plus(ChangePasswordModule module);

  CityHighlightsComponent plus(CityHighlightsModule module);

  PlaceHighlightsComponent plus(PlaceHighlightsModule module);

  AddTripComponent plus(AddTripModule module);

  CalendarComponent plus(CalendarModule module);

  PlacesComponent plus(PlacesModule module);

  FiltersComponent plus(FiltersModule module);

  TimelineComponent plus(TimelineModule module);

  DirectionsComponent plus(DirectionsModule module);
}
