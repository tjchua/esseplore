package com.essplore.essplore.dagger.modules.fragments;

import android.support.v7.app.AlertDialog;
import com.essplore.essplore.api.managers.ApiManager;
import com.essplore.essplore.dagger.scopes.UserScope;
import com.essplore.essplore.ui.activities.login.fragments.presenter.RegisterFragmentPresenterImpl;
import com.essplore.essplore.ui.activities.login.fragments.view.RegisterFragment;
import dagger.Module;
import dagger.Provides;
import io.reactivex.disposables.CompositeDisposable;
import javax.inject.Singleton;

@UserScope @Module public class RegisterFragmentModule {
  private final RegisterFragment fragment;

  public RegisterFragmentModule(final RegisterFragment fragment) {
    this.fragment = fragment;
  }

  @Provides @UserScope RegisterFragment provideRegisterFragmentModule() {
    return fragment;
  }

  @Provides @UserScope RegisterFragmentPresenterImpl provideRegisterFragmentPresenterImpl(
      ApiManager manager) {
    return new RegisterFragmentPresenterImpl(fragment, manager);
  }

  @Provides @UserScope AlertDialog provideAlertDialog() {
    return new AlertDialog.Builder(fragment.getActivity()).create();
  }

  @Provides @UserScope CompositeDisposable provideCompositeDisposable() {
    return new CompositeDisposable();
  }
}
