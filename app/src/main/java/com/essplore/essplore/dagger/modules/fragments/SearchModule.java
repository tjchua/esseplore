package com.essplore.essplore.dagger.modules.fragments;

import android.support.v7.app.AlertDialog;
import com.essplore.essplore.api.managers.ApiManager;
import com.essplore.essplore.dagger.scopes.UserScope;
import com.essplore.essplore.ui.activities.home.fragments.presenter.SearchPresenterImpl;
import com.essplore.essplore.ui.activities.home.fragments.view.SearchFragment;
import dagger.Module;
import dagger.Provides;
import io.reactivex.disposables.CompositeDisposable;

@UserScope @Module public class SearchModule {

  private final SearchFragment fragment;

  public SearchModule(SearchFragment fragment) {
    this.fragment = fragment;
  }

  @Provides @UserScope SearchFragment provideSearchFragment() {
    return fragment;
  }

  @Provides @UserScope AlertDialog provideAlertDialog() {
    return new AlertDialog.Builder(fragment.getActivity()).create();
  }

  @Provides @UserScope SearchPresenterImpl provideSearchPresenterImpl(ApiManager manager) {
    return new SearchPresenterImpl(fragment, manager);
  }

  @Provides @UserScope CompositeDisposable provideCompositeDisposable() {
    return new CompositeDisposable();
  }
}
