package com.essplore.essplore.dagger.components.activities;

import com.essplore.essplore.dagger.modules.activities.CityHighlightsModule;
import com.essplore.essplore.dagger.modules.api.MainApiModule;
import com.essplore.essplore.dagger.scopes.UserScope;
import com.essplore.essplore.ui.activities.cityhighlights.view.CityHighlightsActivity;
import dagger.Subcomponent;

@UserScope @Subcomponent(modules = { CityHighlightsModule.class, MainApiModule.class })
public interface CityHighlightsComponent {
  CityHighlightsActivity inject(CityHighlightsActivity activity);
}
