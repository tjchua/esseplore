package com.essplore.essplore.dagger.components.fragments;

import com.essplore.essplore.dagger.modules.api.MainApiModule;
import com.essplore.essplore.dagger.modules.fragments.LoginFragmentModule;
import com.essplore.essplore.dagger.scopes.UserScope;
import com.essplore.essplore.ui.activities.login.fragments.view.LoginFragment;
import dagger.Subcomponent;

@UserScope @Subcomponent(modules = { LoginFragmentModule.class, MainApiModule.class })
public interface LoginFragmentComponent {
  LoginFragment inject(LoginFragment loginFragment);
}
