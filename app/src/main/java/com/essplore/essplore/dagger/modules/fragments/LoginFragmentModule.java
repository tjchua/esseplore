package com.essplore.essplore.dagger.modules.fragments;

import android.support.v7.app.AlertDialog;
import com.essplore.essplore.api.managers.ApiManager;
import com.essplore.essplore.dagger.scopes.UserScope;
import com.essplore.essplore.ui.activities.login.fragments.presenter.LoginFragmentPresenterImpl;
import com.essplore.essplore.ui.activities.login.fragments.view.LoginFragment;
import dagger.Module;
import dagger.Provides;
import io.reactivex.disposables.CompositeDisposable;

@UserScope @Module public class LoginFragmentModule {
  private final LoginFragment fragment;

  public LoginFragmentModule(final LoginFragment fragment) {
    this.fragment = fragment;
  }

  @Provides @UserScope LoginFragment provideLoginFragment() {
    return fragment;
  }

  @Provides @UserScope LoginFragmentPresenterImpl provideLoginFragmentPresenterImpl(ApiManager apiManager) {
    return new LoginFragmentPresenterImpl(fragment, apiManager);
  }

  @Provides @UserScope AlertDialog provideAlertDialog() {
    return new AlertDialog.Builder(fragment.getActivity()).create();
  }

  @Provides @UserScope CompositeDisposable provideCompositeDisposable() {
    return new CompositeDisposable();
  }
}
