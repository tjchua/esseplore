package com.essplore.essplore.dagger.components.activities;

import com.essplore.essplore.dagger.modules.activities.HomeModule;
import com.essplore.essplore.dagger.modules.api.MainApiModule;
import com.essplore.essplore.dagger.scopes.UserScope;
import com.essplore.essplore.ui.activities.home.view.HomeActivity;
import dagger.Subcomponent;

@UserScope @Subcomponent(modules = { HomeModule.class, MainApiModule.class }) public interface HomeComponent {
  HomeActivity inject(HomeActivity activity);
}
