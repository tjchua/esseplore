package com.essplore.essplore.dagger.components.fragments;

import com.essplore.essplore.dagger.modules.api.MainApiModule;
import com.essplore.essplore.dagger.modules.fragments.SearchModule;
import com.essplore.essplore.dagger.scopes.UserScope;
import com.essplore.essplore.ui.activities.home.fragments.view.SearchFragment;
import dagger.Subcomponent;

@UserScope @Subcomponent(modules = { SearchModule.class, MainApiModule.class }) public interface SearchComponent {
  SearchFragment inject(SearchFragment fragment);
}
