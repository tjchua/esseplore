package com.essplore.essplore.dagger.components.activities;

import com.essplore.essplore.dagger.modules.activities.TimelineModule;
import com.essplore.essplore.dagger.modules.api.MainApiModule;
import com.essplore.essplore.dagger.scopes.UserScope;
import com.essplore.essplore.ui.activities.timeline.view.TimelineActivity;

import dagger.Subcomponent;

@UserScope @Subcomponent(modules = { TimelineModule.class, MainApiModule.class }) public interface TimelineComponent {
    TimelineActivity inject(TimelineActivity activity);
}
