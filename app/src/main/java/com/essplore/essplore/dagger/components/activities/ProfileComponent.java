package com.essplore.essplore.dagger.components.activities;

import com.essplore.essplore.dagger.modules.activities.ProfileModule;
import com.essplore.essplore.dagger.modules.api.MainApiModule;
import com.essplore.essplore.dagger.scopes.UserScope;
import com.essplore.essplore.ui.activities.profile.view.ProfileActivity;
import dagger.Subcomponent;

@UserScope @Subcomponent(modules = { ProfileModule.class, MainApiModule.class })
public interface ProfileComponent {
  ProfileActivity inject(ProfileActivity activity);
}
