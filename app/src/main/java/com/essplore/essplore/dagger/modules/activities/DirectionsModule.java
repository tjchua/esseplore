package com.essplore.essplore.dagger.modules.activities;

import android.support.v7.app.AlertDialog;

import com.essplore.essplore.api.managers.ApiManager;
import com.essplore.essplore.dagger.scopes.UserScope;
import com.essplore.essplore.ui.activities.directions.presenter.DirectionsPresenterImpl;
import com.essplore.essplore.ui.activities.directions.view.DirectionsActivity;

import dagger.Module;
import dagger.Provides;
import io.reactivex.disposables.CompositeDisposable;

@UserScope @Module public class DirectionsModule {

    private final DirectionsActivity activity;

    public DirectionsModule(DirectionsActivity activity) {
        this.activity = activity;
    }

    @Provides @UserScope
    DirectionsActivity provideDirectionsActivity() {
        return activity;
    }

    @Provides @UserScope AlertDialog provideAlertDialog() {
        return new AlertDialog.Builder(activity).create();
    }

    @Provides @UserScope
    DirectionsPresenterImpl provideDirectionsImpl(ApiManager manager) {
        return new DirectionsPresenterImpl(activity, manager);
    }

    @Provides @UserScope CompositeDisposable provideCompositeDisposable() {
        return new CompositeDisposable();
    }
}