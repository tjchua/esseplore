package com.essplore.essplore.dagger.components.activities;

import com.essplore.essplore.dagger.modules.activities.LoginModule;
import com.essplore.essplore.dagger.modules.api.MainApiModule;
import com.essplore.essplore.dagger.scopes.UserScope;
import com.essplore.essplore.ui.activities.login.LoginActivity;
import dagger.Subcomponent;

@UserScope @Subcomponent(modules = { LoginModule.class, MainApiModule.class })
public interface LoginComponent {
  LoginActivity inject(LoginActivity activity);
}
