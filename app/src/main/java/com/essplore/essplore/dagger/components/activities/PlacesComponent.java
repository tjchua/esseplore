package com.essplore.essplore.dagger.components.activities;

import com.essplore.essplore.dagger.modules.activities.PlacesModule;
import com.essplore.essplore.dagger.modules.api.MainApiModule;
import com.essplore.essplore.dagger.scopes.UserScope;
import com.essplore.essplore.ui.activities.places.view.PlacesActivity;
import dagger.Subcomponent;

@UserScope @Subcomponent(modules = { PlacesModule.class, MainApiModule.class }) public interface PlacesComponent {
  PlacesActivity inject(PlacesActivity activity);
}
