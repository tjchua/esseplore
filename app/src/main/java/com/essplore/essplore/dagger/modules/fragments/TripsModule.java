package com.essplore.essplore.dagger.modules.fragments;

import android.support.v7.app.AlertDialog;
import com.essplore.essplore.api.managers.ApiManager;
import com.essplore.essplore.dagger.scopes.UserScope;
import com.essplore.essplore.ui.activities.home.fragments.presenter.SearchPresenterImpl;
import com.essplore.essplore.ui.activities.home.fragments.presenter.TripPresenterImpl;
import com.essplore.essplore.ui.activities.home.fragments.view.TripsFragment;
import dagger.Module;
import dagger.Provides;
import io.reactivex.disposables.CompositeDisposable;

@UserScope @Module public class TripsModule {

  private final TripsFragment fragment;

  public TripsModule(TripsFragment fragment) {
    this.fragment = fragment;
  }

  @Provides @UserScope TripsFragment provideTripsFragment() {
    return fragment;
  }

  @Provides @UserScope AlertDialog provideAlertDialog() {
    return new AlertDialog.Builder(fragment.getActivity()).create();
  }

  @Provides @UserScope SearchPresenterImpl provideSearchPresenterImpl(ApiManager manager) {
    return new SearchPresenterImpl(fragment, manager);
  }

  @Provides @UserScope CompositeDisposable provideCompositeDisposable() {
    return new CompositeDisposable();
  }

  @Provides @UserScope TripPresenterImpl provideTripPresenterImpl(ApiManager manager) {
    return new TripPresenterImpl(fragment, manager);
  }
}
