package com.essplore.essplore.dagger.modules.activities;

import android.support.v7.app.AlertDialog;
import com.essplore.essplore.api.managers.ApiManager;
import com.essplore.essplore.dagger.scopes.UserScope;
import com.essplore.essplore.ui.activities.profile.fragments.view.ChangePasswordFragment;
import com.essplore.essplore.ui.activities.profile.presenter.ProfilePresenterImpl;
import com.essplore.essplore.ui.activities.profile.view.ProfileActivity;
import dagger.Module;
import dagger.Provides;
import io.reactivex.disposables.CompositeDisposable;

@UserScope @Module public class ProfileModule {

  private final ProfileActivity activity;

  public ProfileModule(ProfileActivity activity) {
    this.activity = activity;
  }

  @Provides @UserScope ProfileActivity provideProfileActivity() {
    return activity;
  }

  @Provides @UserScope AlertDialog provideAlertDialog() {
    return new AlertDialog.Builder(activity).create();
  }

  @Provides @UserScope ProfilePresenterImpl provideProfilePresenterImpl(ApiManager manager) {
    return new ProfilePresenterImpl(activity, manager);
  }

  @Provides @UserScope ChangePasswordFragment provideChangePasswordFragment() {
    return new ChangePasswordFragment();
  }

  @Provides @UserScope CompositeDisposable provideCompositeDisposable() {
    return new CompositeDisposable();
  }
}
