package com.essplore.essplore.dagger.components.activities;

import com.essplore.essplore.dagger.modules.activities.AddTripModule;
import com.essplore.essplore.dagger.modules.api.MainApiModule;
import com.essplore.essplore.dagger.scopes.UserScope;
import com.essplore.essplore.ui.activities.trip.view.AddTripActivity;
import dagger.Subcomponent;

@UserScope @Subcomponent(modules = { AddTripModule.class, MainApiModule.class }) public interface AddTripComponent {
  AddTripActivity inject(AddTripActivity activity);
}
