package com.essplore.essplore.dagger.components.activities;

import com.essplore.essplore.dagger.modules.activities.CalendarModule;
import com.essplore.essplore.dagger.modules.api.MainApiModule;
import com.essplore.essplore.dagger.scopes.UserScope;
import com.essplore.essplore.ui.activities.calendar.view.CalendarActivity;
import dagger.Subcomponent;

@UserScope @Subcomponent(modules = { CalendarModule.class, MainApiModule.class }) public interface CalendarComponent {

  CalendarActivity inject(CalendarActivity activity);
}
