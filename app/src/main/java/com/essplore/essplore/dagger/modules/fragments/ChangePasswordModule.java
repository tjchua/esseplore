package com.essplore.essplore.dagger.modules.fragments;

import android.support.v7.app.AlertDialog;
import com.essplore.essplore.api.managers.ApiManager;
import com.essplore.essplore.dagger.scopes.UserScope;
import com.essplore.essplore.ui.activities.profile.fragments.presenter.ChangePasswordPresenterImpl;
import com.essplore.essplore.ui.activities.profile.fragments.view.ChangePasswordFragment;
import dagger.Module;
import dagger.Provides;
import io.reactivex.disposables.CompositeDisposable;

@UserScope @Module public class ChangePasswordModule {

  private final ChangePasswordFragment fragment;

  public ChangePasswordModule(ChangePasswordFragment fragment) {
    this.fragment = fragment;
  }

  @Provides @UserScope ChangePasswordFragment provideChangePasswordFragment() {
    return fragment;
  }

  @Provides @UserScope ChangePasswordPresenterImpl provideChangePasswordPresenterImpl(ApiManager apiManager) {
    return new ChangePasswordPresenterImpl(fragment, apiManager);
  }

  @Provides @UserScope AlertDialog provideAlertDialog() {
    return new AlertDialog.Builder(fragment.getActivity()).create();
  }

  @Provides @UserScope CompositeDisposable provideCompositeDisposable() {
    return new CompositeDisposable();
  }
}
