package com.essplore.essplore.dagger.components.activities;

import com.essplore.essplore.dagger.modules.activities.MainModule;
import com.essplore.essplore.dagger.scopes.UserScope;
import com.essplore.essplore.ui.activities.MainActivity;
import dagger.Subcomponent;

@UserScope @Subcomponent(modules = { MainModule.class }) public interface MainComponent {
  MainActivity inject(MainActivity activity);
}
