package com.essplore.essplore.dagger.components.fragments;

import com.essplore.essplore.dagger.modules.api.MainApiModule;
import com.essplore.essplore.dagger.modules.fragments.RegisterFragmentModule;
import com.essplore.essplore.dagger.scopes.UserScope;
import com.essplore.essplore.ui.activities.login.fragments.view.RegisterFragment;
import dagger.Subcomponent;

@UserScope @Subcomponent(modules = { RegisterFragmentModule.class, MainApiModule.class })
public interface RegisterFragmentComponent {
  RegisterFragment inject(RegisterFragment fragment);
}
