package com.essplore.essplore.dagger.modules.activities;

import android.support.v7.app.AlertDialog;

import com.essplore.essplore.api.managers.ApiManager;
import com.essplore.essplore.dagger.scopes.UserScope;
import com.essplore.essplore.ui.activities.timeline.presenter.TimelinePresenterImpl;
import com.essplore.essplore.ui.activities.timeline.view.TimelineActivity;

import dagger.Module;
import dagger.Provides;
import io.reactivex.disposables.CompositeDisposable;

@UserScope @Module public class TimelineModule {

    private final TimelineActivity activity;

    public TimelineModule(TimelineActivity activity) {
        this.activity = activity;
    }

    @Provides @UserScope
    TimelineActivity provideTimelineActivity() {
        return activity;
    }

    @Provides @UserScope AlertDialog provideAlertDialog() {
        return new AlertDialog.Builder(activity).create();
    }

    @Provides @UserScope
    TimelinePresenterImpl provideTimelineImpl(ApiManager manager) {
        return new TimelinePresenterImpl(activity, manager);
    }

    @Provides @UserScope CompositeDisposable provideCompositeDisposable() {
        return new CompositeDisposable();
    }
}