package com.essplore.essplore.dagger.modules;

import android.app.Application;
import android.content.Context;
import com.essplore.essplore.managers.AppActivityManager;
import com.essplore.essplore.managers.SharedPreferenceKeys;
import com.essplore.essplore.managers.SharedPreferenceManager;
import dagger.Module;
import dagger.Provides;
import io.reactivex.disposables.CompositeDisposable;
import javax.inject.Singleton;

@Singleton @Module public class ApplicationModule {

  private final Application application;

  public ApplicationModule(final Application application) {
    this.application = application;
  }

  @Provides @Singleton Application provideApplication() {
    return application;
  }

  @Provides @Singleton AppActivityManager provideAppActivityManager() {
    return new AppActivityManager();
  }

  @Provides @Singleton SharedPreferenceManager provideSharedPreferenceManager() {
    return new SharedPreferenceManager(
        application.getSharedPreferences(SharedPreferenceKeys.ESSPLORE_PREFERENCE,
            Context.MODE_PRIVATE));
  }
}
