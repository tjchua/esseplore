package com.essplore.essplore.dagger.components.activities;

import com.essplore.essplore.dagger.modules.activities.DirectionsModule;
import com.essplore.essplore.dagger.modules.api.MainApiModule;
import com.essplore.essplore.dagger.scopes.UserScope;
import com.essplore.essplore.ui.activities.directions.view.DirectionsActivity;

import dagger.Subcomponent;

@UserScope @Subcomponent(modules = { DirectionsModule.class, MainApiModule.class }) public interface DirectionsComponent {
    DirectionsActivity inject(DirectionsActivity activity);
}
