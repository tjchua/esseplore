package com.essplore.essplore.dagger.modules.activities;

import android.support.v7.app.AlertDialog;
import com.essplore.essplore.api.managers.ApiManager;
import com.essplore.essplore.dagger.scopes.UserScope;
import com.essplore.essplore.ui.activities.calendar.presenter.CalendarPresenterImpl;
import com.essplore.essplore.ui.activities.calendar.view.CalendarActivity;
import dagger.Module;
import dagger.Provides;
import io.reactivex.disposables.CompositeDisposable;

@UserScope @Module public class CalendarModule {

  private final CalendarActivity activity;

  public CalendarModule(CalendarActivity activity) {
    this.activity = activity;
  }

  @Provides @UserScope CalendarActivity provideCalendarActivity() {
    return activity;
  }

  @Provides @UserScope AlertDialog provideAlertDialog() {
    return new AlertDialog.Builder(activity).create();
  }

  @Provides @UserScope CompositeDisposable provideCompositeDisposable() {
    return new CompositeDisposable();
  }

  @Provides @UserScope CalendarPresenterImpl provideCalendarPresenter(ApiManager manager) {
    return new CalendarPresenterImpl(activity, manager);
  }
}
