package com.essplore.essplore.dagger.modules.fragments;

import android.support.v7.app.AlertDialog;
import com.essplore.essplore.api.managers.ApiManager;
import com.essplore.essplore.dagger.scopes.UserScope;
import com.essplore.essplore.ui.activities.login.fragments.presenter.ForgotPasswordPresenterImpl;
import com.essplore.essplore.ui.activities.login.fragments.view.ForgotPasswordFragment;
import dagger.Module;
import dagger.Provides;
import io.reactivex.disposables.CompositeDisposable;

@UserScope @Module public class ForgotPasswordFragmentModule {

  private final ForgotPasswordFragment fragment;

  public ForgotPasswordFragmentModule(ForgotPasswordFragment fragment) {
    this.fragment = fragment;
  }

  @Provides @UserScope ForgotPasswordFragment provideForgotPasswordFragment() {
    return fragment;
  }

  @Provides @UserScope ForgotPasswordPresenterImpl provideForgotPasswordImpl(ApiManager manager) {
    return new ForgotPasswordPresenterImpl(fragment, manager);
  }

  @Provides @UserScope AlertDialog provideAlertDialog() {
    return new AlertDialog.Builder(fragment.getActivity()).create();
  }

  @Provides @UserScope CompositeDisposable provideCompositeDisposable() {
    return new CompositeDisposable();
  }
}
