package com.essplore.essplore.dagger.components.activities;

import com.essplore.essplore.dagger.modules.activities.PlaceHighlightsModule;
import com.essplore.essplore.dagger.modules.api.MainApiModule;
import com.essplore.essplore.dagger.scopes.UserScope;
import com.essplore.essplore.ui.activities.placehighlights.view.PlaceHighlightsActivity;
import dagger.Subcomponent;

@UserScope @Subcomponent(modules = { PlaceHighlightsModule.class, MainApiModule.class })
public interface PlaceHighlightsComponent {
  PlaceHighlightsActivity inject(PlaceHighlightsActivity activity);
}
