package com.essplore.essplore.dagger.modules.activities;

import android.support.v7.app.AlertDialog;
import com.essplore.essplore.api.managers.ApiManager;
import com.essplore.essplore.dagger.scopes.UserScope;
import com.essplore.essplore.ui.activities.places.presenter.PlacesPresenterImpl;
import com.essplore.essplore.ui.activities.places.view.PlacesActivity;
import dagger.Module;
import dagger.Provides;
import io.reactivex.disposables.CompositeDisposable;

@UserScope @Module public class PlacesModule {

  private final PlacesActivity activity;

  public PlacesModule(PlacesActivity activity) {
    this.activity = activity;
  }

  @Provides @UserScope PlacesActivity providePlacesActivity() {
    return activity;
  }

  @Provides @UserScope AlertDialog provideAlertDialog() {
    return new AlertDialog.Builder(activity).create();
  }

  @Provides @UserScope CompositeDisposable provideCompositeDisposable() {
    return new CompositeDisposable();
  }

  @Provides @UserScope PlacesPresenterImpl providePlacesPresenterImpl(ApiManager manager) {
    return new PlacesPresenterImpl(activity, manager);
  }
}
