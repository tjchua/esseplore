package com.essplore.essplore.dagger.modules.activities;

import android.support.v7.app.AlertDialog;
import com.essplore.essplore.dagger.scopes.UserScope;
import com.essplore.essplore.ui.activities.login.LoginActivity;
import com.essplore.essplore.ui.activities.login.adapter.LoginFragmentPagerAdapter;
import com.essplore.essplore.ui.activities.login.fragments.view.ForgotPasswordFragment;
import dagger.Module;
import dagger.Provides;
import io.reactivex.disposables.CompositeDisposable;

@UserScope @Module public class LoginModule {

  private final LoginActivity activity;

  public LoginModule(final LoginActivity activity) {
    this.activity = activity;
  }

  @Provides @UserScope LoginActivity provideLoginActivity() {
    return this.activity;
  }

  @Provides @UserScope AlertDialog provideAlertDialog() {
    return new AlertDialog.Builder(activity).create();
  }

  @Provides @UserScope LoginFragmentPagerAdapter provideLoginFragmentPagerAdapter() {
    return new LoginFragmentPagerAdapter(activity.getSupportFragmentManager(), activity);
  }

  @Provides @UserScope ForgotPasswordFragment provideForgotPasswordFragment(){
    return new ForgotPasswordFragment();
  }

  @Provides @UserScope CompositeDisposable provideCompositeDisposable() {
    return new CompositeDisposable();
  }
}
