package com.essplore.essplore.dagger.components.fragments;

import com.essplore.essplore.dagger.modules.api.MainApiModule;
import com.essplore.essplore.dagger.modules.fragments.AccountModule;
import com.essplore.essplore.dagger.scopes.UserScope;
import com.essplore.essplore.ui.activities.home.fragments.view.AccountFragment;
import dagger.Subcomponent;

@UserScope @Subcomponent(modules = { AccountModule.class, MainApiModule.class })
public interface AccountComponent {
  AccountFragment inject(AccountFragment fragment);
}
