package com.essplore.essplore.dagger.modules.fragments;

import android.support.v7.app.AlertDialog;
import com.essplore.essplore.dagger.scopes.UserScope;
import com.essplore.essplore.ui.activities.home.fragments.view.AccountFragment;
import dagger.Module;
import dagger.Provides;
import io.reactivex.disposables.CompositeDisposable;

@UserScope @Module public class AccountModule {

  private final AccountFragment fragment;

  public AccountModule(AccountFragment fragment) {
    this.fragment = fragment;
  }

  @Provides @UserScope AccountFragment provideAccountFragment() {
    return fragment;
  }

  @Provides @UserScope AlertDialog provideAlertDialog() {
    return new AlertDialog.Builder(fragment.getActivity()).create();
  }

  @Provides @UserScope CompositeDisposable provideCompositeDisposable() {
    return new CompositeDisposable();
  }
}
