package com.essplore.essplore.dagger.modules.activities;

import android.support.v7.app.AlertDialog;
import com.essplore.essplore.api.managers.ApiManager;
import com.essplore.essplore.dagger.scopes.UserScope;
import com.essplore.essplore.ui.activities.trip.presenter.AddTripPresenterImpl;
import com.essplore.essplore.ui.activities.trip.view.AddTripActivity;
import dagger.Module;
import dagger.Provides;
import io.reactivex.disposables.CompositeDisposable;

@UserScope @Module public class AddTripModule {

  private final AddTripActivity activity;

  public AddTripModule(AddTripActivity activity) {
    this.activity = activity;
  }

  @Provides @UserScope AddTripActivity provideAddTripActivity() {
    return activity;
  }

  @Provides @UserScope AlertDialog provideAlertDialog() {
    return new AlertDialog.Builder(activity).create();
  }

  @Provides @UserScope CompositeDisposable provideCompositeDisposable() {
    return new CompositeDisposable();
  }

  @Provides @UserScope AddTripPresenterImpl provideAddTripPresenterImpl(ApiManager manager) {
    return new AddTripPresenterImpl(activity, manager);
  }
}
