package com.essplore.essplore.dagger.components.fragments;

import com.essplore.essplore.dagger.modules.api.MainApiModule;
import com.essplore.essplore.dagger.modules.fragments.ForgotPasswordFragmentModule;
import com.essplore.essplore.dagger.scopes.UserScope;
import com.essplore.essplore.ui.activities.login.fragments.view.ForgotPasswordFragment;
import dagger.Subcomponent;

@UserScope @Subcomponent(modules = { ForgotPasswordFragmentModule.class, MainApiModule.class })
public interface ForgotPasswordFragmentComponent {
  ForgotPasswordFragment inject(ForgotPasswordFragment fragment);
}
