package com.essplore.essplore.models;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.gson.annotations.SerializedName;
import java.util.ArrayList;
import java.util.List;

public class MyTrack implements Parcelable {

  @SerializedName("id") private int trackId;
  @SerializedName("track_name") private String trackName;
  @SerializedName("city_id") private int cityId;
  @SerializedName("city_name") private String cityName;
  @SerializedName("province_id") private int provinceId;
  @SerializedName("country_name") private String countryName;
  @SerializedName("country_id") private int countryId;
  @SerializedName("province_name") private String provinceName;
  @SerializedName("date_from") private String dateFrom;
  @SerializedName("date_to") private String dateTo;
  @SerializedName("track_picts") List<TrackPicture> trackPictures = new ArrayList<>();

  public MyTrack() {
    //Intended to be empty
  }

  protected MyTrack(Parcel in) {
    trackId = in.readInt();
    trackName = in.readString();
    cityId = in.readInt();
    cityName = in.readString();
    countryId = in.readInt();
    countryName = in.readString();
    provinceId = in.readInt();
    provinceName = in.readString();
    dateFrom = in.readString();
    dateTo = in.readString();
    trackPictures = in.createTypedArrayList(TrackPicture.CREATOR);
  }

  public static final Creator<MyTrack> CREATOR = new Creator<MyTrack>() {
    @Override public MyTrack createFromParcel(Parcel in) {
      return new MyTrack(in);
    }

    @Override public MyTrack[] newArray(int size) {
      return new MyTrack[size];
    }
  };

  public int getTrackId() {
    return trackId;
  }

  public String getTrackName() {
    return trackName;
  }

  public String getCityName() {
    return cityName;
  }

  public String getCountryName() {
    return countryName;
  }

  public String getProvinceName() {
    return provinceName;
  }

  public String getDateFrom() {
    return dateFrom;
  }

  public String getDateTo() {
    return dateTo;
  }

  public int getCityId() {
    return cityId;
  }

  public int getProvinceId() {
    return provinceId;
  }

  public int getCountryId() {
    return countryId;
  }

  public List<TrackPicture> getTrackPictures() {
    return trackPictures;
  }

  @Override public int describeContents() {
    return 0;
  }

  @Override public void writeToParcel(Parcel dest, int flags) {
    dest.writeInt(trackId);
    dest.writeString(trackName);
    dest.writeInt(cityId);
    dest.writeString(cityName);
    dest.writeInt(countryId);
    dest.writeString(countryName);
    dest.writeInt(countryId);
    dest.writeString(provinceName);
    dest.writeString(dateFrom);
    dest.writeString(dateTo);
    dest.writeTypedList(trackPictures);
  }
}
