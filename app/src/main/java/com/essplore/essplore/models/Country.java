package com.essplore.essplore.models;

import com.google.gson.annotations.SerializedName;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

public class Country {

  @SerializedName("id") private int id;

  @SerializedName("country_name") private String countryName;

  @SerializedName("country_code") private String countryCode;

  @SerializedName("country_phone_code") private String countryPhoneCode;

  public Country() {
    // Intended to be empty
  }

  public int getId() {
    return id;
  }

  public String getCountryName() {
    return countryName;
  }

  public String getCountryCode() {
    return countryCode;
  }

  public String getCountryPhoneCode() {
    return countryPhoneCode;
  }

  @Override public String toString() {
    return countryName + " (+" + countryPhoneCode + ")";
  }
}
