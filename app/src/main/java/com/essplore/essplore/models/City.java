package com.essplore.essplore.models;

import android.os.Parcel;
import android.os.Parcelable;
import com.essplore.essplore.application.AppConstants;
import com.google.gson.annotations.SerializedName;
import org.apache.commons.lang3.StringUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class City implements Parcelable {

  private int id;
  @SerializedName("country_id") private int countryId;
  @SerializedName("province_id") private int provinceId;
  @SerializedName("city_name") private String cityName;
  @SerializedName("country_name") private String countryName;
  @SerializedName("province_name") private String provinceName;
  @SerializedName("picture_url") private String pictureUrl;
  @SerializedName("avatar_url") private String avatarUrl;
  private int trackId;

  protected City(Parcel in) {
    id = in.readInt();
    countryId = in.readInt();
    provinceId = in.readInt();
    cityName = in.readString();
    countryName = in.readString();
    provinceName = in.readString();
    pictureUrl = in.readString();
    avatarUrl = in.readString();
    trackId = in.readInt();
  }

  public static final Creator<City> CREATOR = new Creator<City>() {
    @Override public City createFromParcel(Parcel in) {
      return new City(in);
    }

    @Override public City[] newArray(int size) {
      return new City[size];
    }
  };

  public City(Builder builder) {
    this.id = builder.id;
    this.countryId = builder.countryId;
    this.provinceId = builder.provinceId;
    this.cityName = builder.cityName;
    this.countryName = builder.countryName;
    this.provinceName = builder.provinceName;
    this.pictureUrl = builder.pictureUrl;
    this.avatarUrl = builder.avatarUrl;
    this.trackId = builder.trackId;
  }

  public City(String json){
    final String CITIES = "cities";
    final String COUNTRY_ID_KEY = "country_id";
    final String ID_KEY = "id";
    try {

      JSONObject jsonObject = new JSONObject(json);
      JSONArray jsonArray = jsonObject.getJSONArray(CITIES);

      for (int i=0; i < jsonArray.length(); i++){

        this.id = jsonArray.getJSONObject(i).getInt(ID_KEY);
        this.countryId = jsonArray.getJSONObject(i).getInt(COUNTRY_ID_KEY);
        this.provinceId = jsonArray.getJSONObject(i).getInt("province_id");
        this.cityName = jsonArray.getJSONObject(i).getString("city_name");
        this.countryName = jsonArray.getJSONObject(i).getString("country_name");
        this.provinceName = jsonArray.getJSONObject(i).getString("province_name");
        this.pictureUrl = jsonArray.getJSONObject(i).getString("picture_url");
        this.avatarUrl = jsonArray.getJSONObject(i).getString("avatar_url");

      }

    } catch (JSONException e) {

      e.printStackTrace();

    }

  }

  public int getId() {
    return id;
  }

  public String getCityName() {
    return cityName;
  }

  public String getCountryName() {
    return countryName;
  }

  public String getProvinceName() {
    return provinceName;
  }

  public int getCountryId() {
    return countryId;
  }

  public int getProvinceId() {
    return provinceId;
  }

  public String getPictureUrl() {
    return pictureUrl;
  }

  public String getAvatarUrl() {
    return avatarUrl;
  }

  public int getTrackId() {
    return trackId;
  }

  @Override public int describeContents() {
    return 0;
  }

  @Override public void writeToParcel(Parcel dest, int flags) {
    dest.writeInt(id);
    dest.writeInt(countryId);
    dest.writeInt(provinceId);
    dest.writeString(cityName);
    dest.writeString(countryName);
    dest.writeString(provinceName);
    dest.writeString(pictureUrl);
    dest.writeString(avatarUrl);
    dest.writeInt(trackId);
  }

  public static class Builder {
    private int id;
    private int countryId;
    private int provinceId;
    private String cityName;
    private String countryName;
    private String provinceName;
    private String pictureUrl;
    private String avatarUrl;
    private int trackId;

    public Builder(int cityId, int countryId, int trackId) {
      this.id = cityId;
      this.countryId = countryId;
      this.trackId = trackId;
      this.provinceId = AppConstants.DEFAULT_INT_VALUE;
      this.cityName = StringUtils.EMPTY;
      this.countryName = StringUtils.EMPTY;
      this.provinceName = StringUtils.EMPTY;
      this.pictureUrl = StringUtils.EMPTY;
      this.avatarUrl = StringUtils.EMPTY;
      this.trackId = trackId;
    }

    public City build() {
      return new City(this);
    }
  }
}
