package com.essplore.essplore.models;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.gson.annotations.SerializedName;
import java.util.List;

public class Tracks implements Parcelable {

    @SerializedName("place_id") private int placeId;
  @SerializedName("track_record_id") private int trackRecordId;
  @SerializedName("track_name") private String trackName;
  @SerializedName("city_id") private int cityId;
  @SerializedName("city_name") private String cityName;
  @SerializedName("province_id") private int provinceId;
  @SerializedName("province_name") private String provinceName;
  @SerializedName("country_id") private int countryId;
  @SerializedName("country_name") private String countryName;
  @SerializedName("address1") private String address1;
  @SerializedName("address2") private String address2;
  @SerializedName("postal_code") private String postalCode;
  @SerializedName("opening_hours") private String openingHours;
  @SerializedName("price") private String price;
  @SerializedName("website") private String website;
  @SerializedName("rank") private Integer rank;
  @SerializedName("name") private String name;
  @SerializedName("type") private String type;
  @SerializedName("description") private String description;
  @SerializedName("lat") private String lat;
  @SerializedName("lng") private String lng;
  @SerializedName("keywords") private String keywords;
  @SerializedName("guide_speach") private String guideSpeach;
  @SerializedName("status") private String status;
  @SerializedName("place_picture_list") private List<PlacePicture> placePictureList;
  @SerializedName("avatar_url") private String avatarUrl;
  @SerializedName("collection_picture") private List<CollectionPicture> collectionPictureList;
  @SerializedName("category") private String category;

  public Tracks() {
  }

  protected Tracks(Parcel in) {
      placeId = in.readInt();
    trackRecordId = in.readInt();
    trackName = in.readString();
    cityId = in.readInt();
    cityName = in.readString();
    provinceId = in.readInt();
    provinceName = in.readString();
    countryId = in.readInt();
    countryName = in.readString();
    address1 = in.readString();
    address2 = in.readString();
    postalCode = in.readString();
    openingHours = in.readString();
    price = in.readString();
    website = in.readString();
    if (in.readByte() == 0) {
      rank = null;
    } else {
      rank = in.readInt();
    }
    name = in.readString();
    type = in.readString();
    description = in.readString();
    lat = in.readString();
    lng = in.readString();
    keywords = in.readString();
    guideSpeach = in.readString();
    status = in.readString();
    placePictureList = in.createTypedArrayList(PlacePicture.CREATOR);
    avatarUrl = in.readString();
    collectionPictureList = in.createTypedArrayList(CollectionPicture.CREATOR);
    category = in.readString();
  }

  public static final Creator<Tracks> CREATOR = new Creator<Tracks>() {
    @Override public Tracks createFromParcel(Parcel in) {
      return new Tracks(in);
    }

    @Override public Tracks[] newArray(int size) {
      return new Tracks[size];
    }
  };

    public int getPlaceId() {
        return placeId;
    }

  public String getTrackName() {
    return trackName;
  }

  public String getCityName() {
    return cityName;
  }

  public String getProvinceName() {
    return provinceName;
  }

  public String getCountryName() {
    return countryName;
  }

  public String getAddress1() {
    return address1;
  }

  public Object getAddress2() {
    return address2;
  }

  public String getPostalCode() {
    return postalCode;
  }

  public String getOpeningHours() {
    return openingHours;
  }

  public String getPrice() {
    return price;
  }

  public String getWebsite() {
    return website;
  }

  public Integer getRank() {
    return rank;
  }

  public String getName() {
    return name;
  }

  public String getType() {
    return type;
  }

  public String getDescription() {
    return description;
  }

  public String getLat() {
    return lat;
  }

  public String getLng() {
    return lng;
  }

  public String getKeywords() {
    return keywords;
  }

  public String getGuideSpeach() {
    return guideSpeach;
  }

  public String getStatus() {
    return status;
  }

  public List<PlacePicture> getPlacePictureList() {
    return placePictureList;
  }

  public int getTrackRecordId() {
    return trackRecordId;
  }

  public int getCityId() {
    return cityId;
  }

  public int getProvinceId() {
    return provinceId;
  }

  public int getCountryId() {
    return countryId;
  }

  public String getAvatarUrl() {
    return avatarUrl;
  }

  public List<CollectionPicture> getCollectionPictureList() {
    return collectionPictureList;
  }

  public String getCategory() {
    return category;
  }

  @Override public int describeContents() {
    return 0;
  }

  @Override public void writeToParcel(Parcel dest, int flags) {
      dest.writeInt(placeId);
    dest.writeInt(trackRecordId);
    dest.writeString(trackName);
    dest.writeInt(cityId);
    dest.writeString(cityName);
    dest.writeInt(provinceId);
    dest.writeString(provinceName);
    dest.writeInt(countryId);
    dest.writeString(countryName);
    dest.writeString(address1);
    dest.writeString(address2);
    dest.writeString(postalCode);
    dest.writeString(openingHours);
    dest.writeString(price);
    dest.writeString(website);
    if (rank == null) {
      dest.writeByte((byte) 0);
    } else {
      dest.writeByte((byte) 1);
      dest.writeInt(rank);
    }
    dest.writeString(name);
    dest.writeString(type);
    dest.writeString(description);
    dest.writeString(lat);
    dest.writeString(lng);
    dest.writeString(keywords);
    dest.writeString(guideSpeach);
    dest.writeString(status);
    dest.writeTypedList(placePictureList);
    dest.writeString(avatarUrl);
    dest.writeTypedList(collectionPictureList);
    dest.writeString(category);
  }



  public void setLat(String lat) {
    this.lat = lat;
  }

  public void setLng(String lng) {
    this.lng = lng;
  }

  public void setStatus(String status) {
    this.status = status;
  }
}
