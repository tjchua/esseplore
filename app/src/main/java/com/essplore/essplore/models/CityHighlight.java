package com.essplore.essplore.models;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.gson.annotations.SerializedName;
import java.util.List;

public class CityHighlight implements Parcelable {

  @SerializedName("track_record_id") private int trackRecordId;
  private int id;
  private String name;
  private String type;
  private String address1;
  private Object address2;
  @SerializedName("postal_code") private String postalCode;
  @SerializedName("city_id") private int cityId;
  @SerializedName("city_name") private String cityName;
  @SerializedName("country_id") private int countryId;
  @SerializedName("country_name") private String countryName;
  @SerializedName("province_id") private int provinceId;
  @SerializedName("province_name") private String provinceName;
  private String description;
  private String lat;
  private String lng;
  private String keywords;
  @SerializedName("guide_speach") private String guideSpeach;
  @SerializedName("avatar_url") private String avatarUrl;
  @SerializedName("opening_hours") private String openingHours;
  @SerializedName("rank") private int rank;
  private String price;
  private String website;
  @SerializedName("place_picture_list") private List<PlacePicture> placePictureList;
  private String status;

  public CityHighlight() {
  }

  public CityHighlight(Tracks tracks) {
    this.trackRecordId = tracks.getTrackRecordId();
    this.name = tracks.getName();
    this.type = tracks.getType();
    this.address1 = tracks.getAddress1();
    this.postalCode = tracks.getPostalCode();
    this.cityId = tracks.getCityId();
    this.cityName = tracks.getCityName();
    this.countryId = tracks.getCountryId();
    this.countryName = tracks.getCountryName();
    this.provinceId = tracks.getProvinceId();
    this.provinceName = tracks.getProvinceName();
    this.description = tracks.getDescription();
    this.lat = tracks.getLat();
    this.lng = tracks.getLng();
    this.keywords = tracks.getKeywords();
    this.guideSpeach = tracks.getGuideSpeach();
    this.avatarUrl = tracks.getAvatarUrl();
    this.openingHours = tracks.getOpeningHours();
    this.rank = tracks.getRank();
    this.price = tracks.getPrice();
    this.website = tracks.getWebsite();
    this.placePictureList = tracks.getPlacePictureList();
    this.status = tracks.getStatus();
  }

  protected CityHighlight(Parcel in) {
    trackRecordId = in.readInt();
    id = in.readInt();
    name = in.readString();
    type = in.readString();
    address1 = in.readString();
    postalCode = in.readString();
    cityId = in.readInt();
    cityName = in.readString();
    countryId = in.readInt();
    countryName = in.readString();
    provinceId = in.readInt();
    provinceName = in.readString();
    description = in.readString();
    lat = in.readString();
    lng = in.readString();
    keywords = in.readString();
    guideSpeach = in.readString();
    avatarUrl = in.readString();
    openingHours = in.readString();
    rank = in.readInt();
    price = in.readString();
    website = in.readString();
    placePictureList = in.createTypedArrayList(PlacePicture.CREATOR);
    status = in.readString();
  }

  @Override public void writeToParcel(Parcel dest, int flags) {
    dest.writeInt(trackRecordId);
    dest.writeInt(id);
    dest.writeString(name);
    dest.writeString(type);
    dest.writeString(address1);
    dest.writeString(postalCode);
    dest.writeInt(cityId);
    dest.writeString(cityName);
    dest.writeInt(countryId);
    dest.writeString(countryName);
    dest.writeInt(provinceId);
    dest.writeString(provinceName);
    dest.writeString(description);
    dest.writeString(lat);
    dest.writeString(lng);
    dest.writeString(keywords);
    dest.writeString(guideSpeach);
    dest.writeString(avatarUrl);
    dest.writeString(openingHours);
    dest.writeInt(rank);
    dest.writeString(price);
    dest.writeString(website);
    dest.writeTypedList(placePictureList);
    dest.writeString(status);
  }

  @Override public int describeContents() {
    return 0;
  }

  public static final Creator<CityHighlight> CREATOR = new Creator<CityHighlight>() {
    @Override public CityHighlight createFromParcel(Parcel in) {
      return new CityHighlight(in);
    }

    @Override public CityHighlight[] newArray(int size) {
      return new CityHighlight[size];
    }
  };

  public int getId() {
    return id;
  }

  public String getOpeningHours() {
    return openingHours;
  }

  public String getPrice() {
    return price;
  }

  public String getWebsite() {
    return website;
  }

  public String getName() {
    return name;
  }

  public String getType() {
    return type;
  }

  public String getAddress1() {
    return address1;
  }

  public Object getAddress2() {
    return address2;
  }

  public String getPostalCode() {
    return postalCode;
  }

  public String getCityName() {
    return cityName;
  }

  public String getCountryName() {
    return countryName;
  }

  public String getProvinceName() {
    return provinceName;
  }

  public String getDescription() {
    return description;
  }

  public String getLat() {
    return lat;
  }

  public String getLng() {
    return lng;
  }

  public String getKeywords() {
    return keywords;
  }

  public String getGuideSpeach() {
    return guideSpeach;
  }

  public String getAvatarUrl() {
    return avatarUrl;
  }

  public List<PlacePicture> getPlacePictureList() {
    return placePictureList;
  }

  public int getTrackRecordId() {
    return trackRecordId;
  }

  public int getCityId() {
    return cityId;
  }

  public int getCountryId() {
    return countryId;
  }

  public int getProvinceId() {
    return provinceId;
  }

  public int getRank() {
    return rank;
  }

  public String getStatus() {
    return status;
  }
}
