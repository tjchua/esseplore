package com.essplore.essplore.models;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.gson.annotations.SerializedName;

public class PlacePicture implements Parcelable {

  @SerializedName("place_picture_url") private String placePictureUrl;

  public PlacePicture() {
    //Intended to be empty.
  }

  protected PlacePicture(Parcel in) {
    placePictureUrl = in.readString();
  }

  public static final Creator<PlacePicture> CREATOR = new Creator<PlacePicture>() {
    @Override public PlacePicture createFromParcel(Parcel in) {
      return new PlacePicture(in);
    }

    @Override public PlacePicture[] newArray(int size) {
      return new PlacePicture[size];
    }
  };

  public String getPlacePictureUrl() {
    return placePictureUrl;
  }

  @Override public int describeContents() {
    return 0;
  }

  @Override public void writeToParcel(Parcel dest, int flags) {
    dest.writeString(placePictureUrl);
  }
}
