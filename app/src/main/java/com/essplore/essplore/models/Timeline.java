package com.essplore.essplore.models;

import android.graphics.Bitmap;
import android.os.Parcel;
import android.os.Parcelable;

public class Timeline  implements Parcelable {
    protected Timeline(Parcel in) {
    }

    public static final Creator<Timeline> CREATOR = new Creator<Timeline>() {
        @Override
        public Timeline createFromParcel(Parcel in) {
            return new Timeline(in);
        }

        @Override
        public Timeline[] newArray(int size) {
            return new Timeline[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
    }

    /*private String mAvatarUrl;
    private String mAddress;
    private String mCityName;
    private double mLatFrom;
    private double mLngFrom;
    private double mLatTo;
    private double mLngTo;
    private Bitmap imgURL;
    private String category;
    private String mStatus;
    private String description;

    public String getmAvatarUrl() {
        return mAvatarUrl;
    }

    public void setmAvatarUrl(String mAvatarUrl) {
        this.mAvatarUrl = mAvatarUrl;
    }

    public String getmAddress() {
        return mAddress;
    }

    public void setmAddress(String mAddress) {
        this.mAddress = mAddress;
    }

    public double getmLatFrom() {
        return mLatFrom;
    }

    public void setmLatFrom(double mLatFrom) {
        this.mLatFrom = mLatFrom;
    }

    public double getmLngFrom() {
        return mLngFrom;
    }

    public void setmLngFrom(double mLngFrom) {
        this.mLngFrom = mLngFrom;
    }

    public double getmLatTo() {
        return mLatTo;
    }

    public void setmLatTo(double mLatTo) {
        this.mLatTo = mLatTo;
    }

    public double getmLngTo() {
        return mLngTo;
    }

    public void setmLngTo(double mLngTo) {
        this.mLngTo = mLngTo;
    }

    public TimelineMarker getmTimelineMarker() {
        return mTimelineMarker;
    }

    public void setmTimelineMarker(TimelineMarker mTimelineMarker) {
        this.mTimelineMarker = mTimelineMarker;
    }

    public String getmCityName() {
        return mCityName;
    }

    public void setmCityName(String mCityName) {
        this.mCityName = mCityName;
    }

    public Bitmap getImgURL() {
        return imgURL;
    }

    public void setImgURL(Bitmap imgURL) {
        this.imgURL = imgURL;
    }

    public String getmStatus() {
        return mStatus;
    }

    public void setmStatus(String mStatus) {
        this.mStatus = mStatus;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Timeline(String mAvatarUrl, String mAddress, String mCityName, String status, String description, TimelineMarker mTimelineMarker) {
        this.mAvatarUrl = mAvatarUrl;
        this.mAddress = mAddress;
        this.mCityName = mCityName;
        this.mStatus = status;
        this.mTimelineMarker = mTimelineMarker;
        this.description = description;
    }

    public Timeline(double mLatFrom, double mLngFrom, double mLatTo, double mLngTo, TimelineMarker mTimelineMarker) {
        this.mLatFrom = mLatFrom;
        this.mLngFrom = mLngFrom;
        this.mLatTo = mLatTo;
        this.mLngTo = mLngTo;
        this.mTimelineMarker = mTimelineMarker;
    }

    public Timeline(Bitmap imgURL, String category) {
        this.imgURL = imgURL;
        this.mTimelineMarker = mTimelineMarker;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.mAvatarUrl);
        dest.writeString(this.mAddress);
        dest.writeDouble(this.mLatFrom);
        dest.writeDouble(this.mLngFrom);
        dest.writeDouble(this.mLatTo);
        dest.writeDouble(this.mLngTo);
        dest.writeInt(this.mTimelineMarker == null ? -1 : this.mTimelineMarker.ordinal());
    }

    protected Timeline(Parcel in) {
        this.mAvatarUrl = in.readString();
        this.mAddress = in.readString();
        this.mLatFrom = in.readDouble();
        this.mLngFrom = in.readDouble();
        this.mLatTo = in.readDouble();
        this.mLngTo = in.readDouble();
        int tmpMTimelineMaker = in.readInt();
        this.mTimelineMarker = tmpMTimelineMaker == -1 ? null : TimelineMarker.values()[tmpMTimelineMaker];
    }

    public static final Parcelable.Creator<Timeline> CREATOR = new Parcelable.Creator<Timeline>() {
        @Override
        public Timeline createFromParcel(Parcel source) {
            return new Timeline(source);
        }

        @Override
        public Timeline[] newArray(int size) {
            return new Timeline[size];
        }
    };*/
}

