package com.essplore.essplore.models;

import com.google.gson.annotations.SerializedName;

public class CivilStatus {

  @SerializedName("id") private int id;
  @SerializedName("title") private String title;

  public CivilStatus() {
    // Intended to be empty.
  }

  public int getId() {
    return id;
  }

  public String getTitle() {
    return title;
  }
}
