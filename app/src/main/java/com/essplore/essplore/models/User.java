package com.essplore.essplore.models;

import com.essplore.essplore.api.requests.RegisterRequest;
import com.google.gson.annotations.SerializedName;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

public class User {

  @SerializedName("id") private String userid;

  @SerializedName("username") private String username;

  @SerializedName("email") private String email;

  @SerializedName("country_id") private int countryId;

  @SerializedName("mobile") private String mobile;

  @SerializedName("birthday") private String birthday;

  @SerializedName("civil_status_id") private int civilStatusId;

  @SerializedName("profile_image") private String profileImage;

  public User() {
    // Intended to be empty.
  }

  public User(RegisterRequest registerRequest) {
    this.username = registerRequest.getUsername();
    this.email = registerRequest.getEmail();
    this.countryId = registerRequest.getCountryId();
    this.mobile = registerRequest.getMobile();
    this.birthday = registerRequest.getBirthday();
  }

  public String getUsername() {
    return username;
  }

  public String getEmail() {
    return email;
  }

  public int getCountryId() {
    return countryId;
  }

  public String getMobile() {
    return mobile;
  }

  public String getBirthday() {
    return birthday;
  }

  public int getCivilStatusId() {
    return civilStatusId;
  }

  public String getUserid() {
    return userid;
  }

  public String getProfileImage() {
    return profileImage;
  }

  @Override public String toString() {
    return ToStringBuilder.reflectionToString(this, ToStringStyle.JSON_STYLE);
  }
}
