package com.essplore.essplore.models;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

public class CollectionPicture implements Parcelable {

  @SerializedName("id") private int id;
  @SerializedName("track_id") private int trackId;
  @SerializedName("user_id") private int userId;
  @SerializedName("current_place_id") private int currentPlaceId;
  @SerializedName("description") private String description;
  @SerializedName("lat") private double lat;
  @SerializedName("lng") private double lng;
  @SerializedName("picture_url") private String pictureUrl;
  @SerializedName("avatar_url") private String avatarUrl;
  @SerializedName("shared_on") private String sharedOn;
  @SerializedName("created_at") private String createdAt;

  public CollectionPicture() {
    //Intended to be empty.
  }

  protected CollectionPicture(Parcel in) {

    id = in.readInt();
    trackId = in.readInt();
    userId = in.readInt();
    currentPlaceId = in.readInt();
    description = in.readString();
    lat = in.readDouble();
    lng = in.readDouble();
    pictureUrl = in.readString();
    avatarUrl = in.readString();
    sharedOn = in.readString();
    createdAt = in.readString();
  }

  public static final Creator<CollectionPicture> CREATOR = new Creator<CollectionPicture>() {
    @Override public CollectionPicture createFromParcel(Parcel in) {
      return new CollectionPicture(in);
    }

    @Override public CollectionPicture[] newArray(int size) {
      return new CollectionPicture[size];
    }
  };

  public int getId() {
    return id;
  }

  public int getTrackId() {
    return trackId;
  }

  public int getUserId() {
    return userId;
  }

  public int getCurrentPlaceId() {
    return currentPlaceId;
  }

  public String getDescription() {
    return description;
  }

  public double getLat() {
    return lat;
  }

  public double getLng() {
    return lng;
  }

  public String getPictureUrl() {
    return pictureUrl;
  }

  public String getAvatarUrl() {
    return avatarUrl;
  }

  public String getSharedOn() {
    return sharedOn;
  }

  public String getCreatedAt() {
    return createdAt;
  }

  @Override public int describeContents() {
    return 0;
  }

  @Override public void writeToParcel(Parcel dest, int flags) {

    dest.writeInt(id);
    dest.writeInt(trackId);
    dest.writeInt(userId);
    dest.writeInt(currentPlaceId);
    dest.writeString(description);
    dest.writeDouble(lat);
    dest.writeDouble(lng);
    dest.writeString(pictureUrl);
    dest.writeString(avatarUrl);
    dest.writeString(sharedOn);
    dest.writeString(createdAt);
  }
}
