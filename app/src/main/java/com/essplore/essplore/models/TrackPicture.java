package com.essplore.essplore.models;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.gson.annotations.SerializedName;

public class TrackPicture implements Parcelable {

  @SerializedName("track_avatar_url") private String trackAvatarUrl;
  @SerializedName("track_picture_url") private String trackPictureUrl;

  public TrackPicture() {
  }

  protected TrackPicture(Parcel in) {
    trackAvatarUrl = in.readString();
    trackPictureUrl = in.readString();
  }

  public static final Creator<TrackPicture> CREATOR = new Creator<TrackPicture>() {
    @Override public TrackPicture createFromParcel(Parcel in) {
      return new TrackPicture(in);
    }

    @Override public TrackPicture[] newArray(int size) {
      return new TrackPicture[size];
    }
  };

  public String getTrackAvatarUrl() {
    return trackAvatarUrl;
  }

  public String getTrackPictureUrl() {
    return trackPictureUrl;
  }

  @Override public int describeContents() {
    return 0;
  }

  @Override public void writeToParcel(Parcel dest, int flags) {
    dest.writeString(trackAvatarUrl);
    dest.writeString(trackPictureUrl);
  }
}
