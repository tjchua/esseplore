package com.essplore.essplore.models;

import android.os.Parcel;
import android.os.Parcelable;
import com.essplore.essplore.api.response.CreateTrackResponse;
import com.google.gson.annotations.SerializedName;
import java.util.List;

public class MyTrip implements Parcelable {

  @SerializedName("details") private MyTrack myTrack;
  @SerializedName("track") private List<Tracks> tracks;

  public MyTrip() {
  }

  public MyTrip(CreateTrackResponse createTrackResponse) {
    this.myTrack = createTrackResponse.getMyTrack();
    this.tracks = createTrackResponse.getTracks();
  }

  protected MyTrip(Parcel in) {
    myTrack = in.readParcelable(MyTrack.class.getClassLoader());
    tracks = in.createTypedArrayList(Tracks.CREATOR);
  }

  public static final Creator<MyTrip> CREATOR = new Creator<MyTrip>() {
    @Override public MyTrip createFromParcel(Parcel in) {
      return new MyTrip(in);
    }

    @Override public MyTrip[] newArray(int size) {
      return new MyTrip[size];
    }
  };

  public MyTrack getMyTrack() {
    return myTrack;
  }

  public List<Tracks> getTracks() {
    return tracks;
  }

  @Override public int describeContents() {
    return 0;
  }

  @Override public void writeToParcel(Parcel dest, int flags) {
    dest.writeParcelable(myTrack, flags);
    dest.writeTypedList(tracks);
  }
}
