package com.essplore.essplore.managers;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;

import com.essplore.essplore.R;
import com.essplore.essplore.api.requests.CreateTrackRequest;
import com.essplore.essplore.application.AppConstants;
import com.essplore.essplore.models.City;
import com.essplore.essplore.models.CityHighlight;
import com.essplore.essplore.models.MyTrack;
import com.essplore.essplore.models.MyTrip;
import com.essplore.essplore.models.Tracks;
import com.essplore.essplore.ui.activities.album.AlbumActivity;
import com.essplore.essplore.ui.activities.calendar.view.CalendarActivity;
import com.essplore.essplore.ui.activities.camera.CameraActivity;
import com.essplore.essplore.ui.activities.cityhighlights.view.CityHighlightsActivity;
import com.essplore.essplore.ui.activities.directions.view.DirectionsActivity;
import com.essplore.essplore.ui.activities.filters.view.FiltersActivity;
import com.essplore.essplore.ui.activities.home.view.HomeActivity;
import com.essplore.essplore.ui.activities.login.LoginActivity;
import com.essplore.essplore.ui.activities.login.fragments.view.ForgotPasswordFragment;
import com.essplore.essplore.ui.activities.placehighlights.view.PlaceHighlightsActivity;
import com.essplore.essplore.ui.activities.places.view.PlacesActivity;
import com.essplore.essplore.ui.activities.profile.view.ProfileActivity;
import com.essplore.essplore.ui.activities.timeline.view.TimelineActivity;
import com.essplore.essplore.ui.activities.trip.view.AddTripActivity;
import com.essplore.essplore.ui.activities.wallet.WalletActivity;

import java.io.Serializable;
import java.util.List;

import javax.inject.Singleton;

@Singleton public class AppActivityManager {

  public void launchLoginActivity(Activity activity) {
    Intent intent = new Intent(activity, LoginActivity.class);
    activity.startActivity(intent);
  }

  public void launchHomeActivity(Activity activity) {
    Intent intent = new Intent(activity, HomeActivity.class);
    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
    activity.startActivity(intent);
    activity.finish();
  }

  public void launchForgotPassword(LoginActivity activity, int layout, ForgotPasswordFragment fragment) {
    FragmentTransaction ft = activity.getSupportFragmentManager().beginTransaction();
    ft.setCustomAnimations(R.anim.push_left_in_no_alpha, R.anim.push_right_out_no_alpha, R.anim.push_right_in_no_alpha,
        R.anim.push_left_out_no_alpha);
    ft.replace(layout, fragment);
    ft.commit();
  }

  public void launchProfileActivity(Activity activity) {
    Intent intent = new Intent(activity, ProfileActivity.class);
    activity.startActivity(intent);
  }

  public void launchPlaceHighlights(Activity activity, City city) {
    Intent intent = new Intent(activity, PlaceHighlightsActivity.class);
    intent.putExtra(AppConstants.CITY_OBJECT, city);
    activity.startActivityForResult(intent, AppConstants.PLACE_HIGHLIGHT_REQUEST);
  }

  public void launchCityHighlights(Activity activity, CityHighlight cityHighlight, City city) {
    Intent intent = new Intent(activity, CityHighlightsActivity.class);
    intent.putExtra(AppConstants.CITY_HIGHLIGHT_OBJECT, cityHighlight);
    intent.putExtra(AppConstants.CITY_OBJECT, city);
    activity.startActivityForResult(intent, AppConstants.ADD_PLACE_REQUEST);
  }

  public void launchCityHighlightsForViewing(Activity activity, Tracks tracks, City city) {
    Intent intent = new Intent(activity, CityHighlightsActivity.class);
    intent.putExtra(AppConstants.TRACKS_OBJECT, tracks);
    intent.putExtra(AppConstants.CITY_OBJECT, city);
    activity.startActivity(intent);
  }

  /** Chyll
   * added 07/12/2018
   * @param activity
   * @param bundle
   */
  public void launchTimeline(Activity activity, Bundle bundle) {
    Intent intent = new Intent(activity, TimelineActivity.class);
    intent.putExtra(AppConstants.MY_TRACK_OBJECT, bundle);
    activity.startActivity(intent);
  }

  /**
   * Chyll
   * added 07/16/2018
   * @param activity
   */
  public void launchDirections(Activity activity, int track_id) {
    Intent intent = new Intent(activity, DirectionsActivity.class);
      intent.putExtra(AppConstants.TRACKS_OBJECT, track_id);
    activity.startActivity(intent);
  }


  public void launchAddTrips(Activity activity, City city) {
    Intent intent = new Intent(activity, AddTripActivity.class);
    intent.putExtra(AppConstants.CITY_OBJECT, city);
    activity.startActivity(intent);
  }

  public void launchFilters(Activity activity, CreateTrackRequest createTrackRequest) {
    Intent intent = new Intent(activity, FiltersActivity.class);
    intent.putExtra(AppConstants.CREATE_TRACK_OBJECT, createTrackRequest);
    activity.startActivity(intent);
    activity.finish();
  }

  public void launchCalendar(Activity activity, CreateTrackRequest createTrackRequest) {
    Intent intent = new Intent(activity, CalendarActivity.class);
    intent.putExtra(AppConstants.CREATE_TRACK_OBJECT, createTrackRequest);
    activity.startActivity(intent);
    activity.finish();
  }

  public void launchPlacesActivity(Activity activity, MyTrip myTrip, int countryId, int cityId) {
    Intent intent = new Intent(activity, PlacesActivity.class);
    intent.putExtra(AppConstants.CREATE_TRACK_RESPONSE_OBJECT, myTrip);
    intent.putExtra(AppConstants.CITY_ID, cityId);
    intent.putExtra(AppConstants.GET_COUNTRY_ID, countryId);
    activity.startActivity(intent);
    activity.finish();
  }

  public void launchPlacesActivityFromTripFragment(Activity activity, MyTrack myTrack) {
    Intent intent = new Intent(activity, PlacesActivity.class);
    intent.putExtra(AppConstants.MY_TRACK_OBJECT, myTrack);
    intent.putExtra(AppConstants.CITY_ID, myTrack.getCityId());
    intent.putExtra(AppConstants.GET_COUNTRY_ID, myTrack.getCountryId());
    activity.startActivity(intent);
  }

  public void launchWalletActivity(Activity activity) {
    Intent intent = new Intent(activity, WalletActivity.class);
    activity.startActivity(intent);
  }

  public void launchCameraActivity(Activity activity) {
    Intent intent = new Intent(activity, CameraActivity.class);
    activity.startActivity(intent);
  }

  public void launchAlbumActivity(Activity activity) {
    Intent intent = new Intent(activity, AlbumActivity.class);
    activity.startActivity(intent);
  }

}
