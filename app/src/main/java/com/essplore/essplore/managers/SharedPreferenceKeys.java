package com.essplore.essplore.managers;

public final class SharedPreferenceKeys {

  public static final String ESSPLORE_PREFERENCE = "essplorePreference";

  public static final String ACCESS_TOKEN = "accessToken";
  public static final String REFRESH_TOKEN = "refreshToken";
  public static final String IS_USER_LOGGED_IN = "isUserLoggedIn";
  public static final String USERID = "userid";
  public static final String USERNAME = "username";
  public static final String EMAIL = "email";
  public static final String COUNTRY_ID = "countryId";
  public static final String MOBILE = "mobile";
  public static final String BIRTHDAY = "birthday";
  public static final String CIVIL_STATUS_ID = "civilStatusId";
  public static final String PROFILE_IMAGE = "profile_image";
}
