package com.essplore.essplore.managers;

import android.content.SharedPreferences;
import com.essplore.essplore.api.response.LoginResponse;
import com.essplore.essplore.application.AppConstants;
import com.essplore.essplore.models.User;
import javax.inject.Singleton;
import org.apache.commons.lang3.StringUtils;

@Singleton public class SharedPreferenceManager {

  private final SharedPreferences sharedPreferences;

  public SharedPreferenceManager(SharedPreferences sharedPreferences) {
    this.sharedPreferences = sharedPreferences;
  }

  public void saveTokens(LoginResponse response) {
    sharedPreferences.edit().putString(SharedPreferenceKeys.ACCESS_TOKEN, response.getAccessToken()).apply();
    sharedPreferences.edit().putString(SharedPreferenceKeys.REFRESH_TOKEN, response.getRefreshToken()).apply();
    sharedPreferences.edit().putBoolean(SharedPreferenceKeys.IS_USER_LOGGED_IN, Boolean.TRUE).apply();
    sharedPreferences.edit().putString(SharedPreferenceKeys.USERID, response.getUser().getUserid()).apply();
    sharedPreferences.edit().putString(SharedPreferenceKeys.USERNAME, response.getUser().getUsername()).apply();
    sharedPreferences.edit().putString(SharedPreferenceKeys.EMAIL, response.getUser().getEmail()).apply();
    sharedPreferences.edit().putInt(SharedPreferenceKeys.COUNTRY_ID, response.getUser().getCountryId()).apply();
    sharedPreferences.edit().putString(SharedPreferenceKeys.MOBILE, response.getUser().getMobile()).apply();
    sharedPreferences.edit().putString(SharedPreferenceKeys.BIRTHDAY, response.getUser().getBirthday()).apply();
    sharedPreferences.edit()
        .putString(SharedPreferenceKeys.PROFILE_IMAGE, response.getUser().getProfileImage())
        .apply();
    sharedPreferences.edit()
        .putInt(SharedPreferenceKeys.CIVIL_STATUS_ID, response.getUser().getCivilStatusId())
        .apply();
  }

  public void saveTokens(LoginResponse response, User user) {
    sharedPreferences.edit().putString(SharedPreferenceKeys.ACCESS_TOKEN, response.getAccessToken()).apply();
    sharedPreferences.edit().putString(SharedPreferenceKeys.REFRESH_TOKEN, response.getRefreshToken()).apply();
    sharedPreferences.edit().putBoolean(SharedPreferenceKeys.IS_USER_LOGGED_IN, Boolean.TRUE).apply();
    sharedPreferences.edit().putString(SharedPreferenceKeys.USERID, user.getUserid()).apply();
    sharedPreferences.edit().putString(SharedPreferenceKeys.USERNAME, user.getUsername()).apply();
    sharedPreferences.edit().putString(SharedPreferenceKeys.EMAIL, user.getEmail()).apply();
    sharedPreferences.edit().putInt(SharedPreferenceKeys.COUNTRY_ID, user.getCountryId()).apply();
    sharedPreferences.edit().putString(SharedPreferenceKeys.MOBILE, user.getMobile()).apply();
    sharedPreferences.edit().putString(SharedPreferenceKeys.BIRTHDAY, user.getBirthday()).apply();

    try {

        sharedPreferences.edit()
                .putString(SharedPreferenceKeys.PROFILE_IMAGE, response.getUser().getProfileImage())
                .apply();

    }catch (Exception e){

        sharedPreferences.edit()
                .putString(SharedPreferenceKeys.PROFILE_IMAGE, "")
                .apply();

    }

    sharedPreferences.edit().putInt(SharedPreferenceKeys.CIVIL_STATUS_ID, user.getCivilStatusId()).apply();
  }


  public void saveImagePath(String imagePath){
    sharedPreferences.edit()
            .putString(SharedPreferenceKeys.PROFILE_IMAGE, imagePath)
            .apply();
  }

  public boolean isUserLoggedIn() {
    return sharedPreferences.getBoolean(SharedPreferenceKeys.IS_USER_LOGGED_IN, Boolean.FALSE);
  }

  public String getLoggedInUserId() {
    return sharedPreferences.getString(SharedPreferenceKeys.USERID, String.valueOf(AppConstants.DEFAULT_INT_VALUE));
  }

  public String getUsername() {
    return sharedPreferences.getString(SharedPreferenceKeys.USERNAME, StringUtils.EMPTY);
  }

  public String getEmail() {
    return sharedPreferences.getString(SharedPreferenceKeys.EMAIL, StringUtils.EMPTY);
  }

  public int getCountryId() {
    return sharedPreferences.getInt(SharedPreferenceKeys.COUNTRY_ID, AppConstants.DEFAULT_INT_VALUE);
  }

  public String getMobileNumber() {
    return sharedPreferences.getString(SharedPreferenceKeys.MOBILE, StringUtils.EMPTY);
  }

  public String getBirthday() {
    return sharedPreferences.getString(SharedPreferenceKeys.BIRTHDAY, StringUtils.EMPTY);
  }

  public String getAccessToken() {
    return sharedPreferences.getString(SharedPreferenceKeys.ACCESS_TOKEN, StringUtils.EMPTY);
  }

  public String getRefreshToken() {
    return sharedPreferences.getString(SharedPreferenceKeys.REFRESH_TOKEN, StringUtils.EMPTY);
  }

  public int getCivilStatusId() {
    return sharedPreferences.getInt(SharedPreferenceKeys.CIVIL_STATUS_ID, AppConstants.DEFAULT_INT_VALUE);
  }

  public String getProfileImagePath() {
    return sharedPreferences.getString(SharedPreferenceKeys.PROFILE_IMAGE, StringUtils.EMPTY);
  }

  public void clearCache() {
    sharedPreferences.edit().clear().apply();
  }
}
