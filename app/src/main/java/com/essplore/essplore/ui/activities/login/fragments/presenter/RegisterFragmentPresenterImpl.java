package com.essplore.essplore.ui.activities.login.fragments.presenter;

import android.location.Location;
import android.util.Log;

import com.essplore.essplore.api.managers.ApiManager;
import com.essplore.essplore.api.requests.RegisterRequest;
import com.essplore.essplore.api.response.CountryResponse;
import com.essplore.essplore.api.response.LoginResponse;
import com.essplore.essplore.api.utils.SimpleObserver;
import com.essplore.essplore.models.User;
import com.essplore.essplore.ui.activities.HttpBasePresenter;
import com.essplore.essplore.ui.activities.login.fragments.view.RegisterFragmentView;
import io.reactivex.disposables.Disposable;
import org.apache.commons.lang3.StringUtils;

public class RegisterFragmentPresenterImpl extends HttpBasePresenter
    implements RegisterFragmentPresenter {

  private final RegisterFragmentView view;
  private final ApiManager manager;

  public RegisterFragmentPresenterImpl(final RegisterFragmentView view, final ApiManager manager) {
    this.view = view;
    this.manager = manager;
  }

  @Override public Disposable getCountries() {
    view.showProgress();
    return manager.getCountries().subscribe(new SimpleObserver<CountryResponse>() {
      @Override public void accept(CountryResponse countryResponse) {
        view.hideProgress();
        view.setupSpinner(countryResponse.getCountryList());
      }
    }, throwable -> {
      view.hideProgress();
      handleHttpError(view, throwable);
    });
  }

  @Override public Disposable registerUser(final RegisterRequest registerRequest) {
    view.showProgress();
    return manager.registerUser(registerRequest).subscribe(new SimpleObserver<LoginResponse>() {
      @Override public void accept(LoginResponse loginResponse) {
        view.hideProgress();
        view.saveResponse(loginResponse, new User(registerRequest));
        view.navigateToHome();
      }
    }, throwable -> {
      view.hideProgress();
      handleHttpError(view, throwable);
    });
  }

  @Override public RegisterRequest buildRequest() {
    Log.e("phone", view.provideMobile());
    return new RegisterRequest(view.provideUsername(), view.providePassword(), view.provideMobile(),
        view.provideCountry(), view.provideBirthday(), view.provideEmail());
  }

  @Override public boolean validateCredentials() {
    if (StringUtils.isEmpty(view.provideUsername())) {
      view.setUsernameError();
      return false;
    }
    if (StringUtils.isEmpty(view.providePassword())) {
      view.setPasswordError();
      return false;
    }
    if (StringUtils.isEmpty(view.provideMobile())) {
      view.setMobileError();
      return false;
    }
    if (StringUtils.isEmpty(view.provideBirthday())) {
      view.setBirthdayError();
      return false;
    }
    return true;
  }
}
