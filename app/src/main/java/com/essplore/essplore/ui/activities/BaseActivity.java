package com.essplore.essplore.ui.activities;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import butterknife.ButterKnife;
import butterknife.Unbinder;

public abstract class BaseActivity extends AppCompatActivity {

  private Unbinder unbinder;

  protected abstract void setupActivityLayout();

  protected abstract void setupViewElements();

  protected abstract void setupToolbar();

  protected abstract void injectDaggerComponent();

  protected abstract boolean isActionbarBackButtonEnabled();

  @Override protected void onCreate(@Nullable Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setupActivityLayout();
    unbinder = ButterKnife.bind(this);
    injectDaggerComponent();
    setupToolbar();
    setupViewElements();
  }

  @Override protected void onDestroy() {
    super.onDestroy();
    unbinder.unbind();
  }
}
