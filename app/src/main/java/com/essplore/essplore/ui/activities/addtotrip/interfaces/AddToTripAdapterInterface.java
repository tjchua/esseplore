package com.essplore.essplore.ui.activities.addtotrip.interfaces;

public interface AddToTripAdapterInterface {

    public void onClickItem(int trackId);

}
