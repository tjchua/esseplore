package com.essplore.essplore.ui.activities.filters.view;

import com.essplore.essplore.api.requests.CreateTrackRequest;
import com.essplore.essplore.ui.activities.HttpBaseView;

public interface FiltersView extends HttpBaseView {

  CreateTrackRequest getCreateTrackRequest();

  String provideToken();

  void showProgressbar();

  void hideProgressbar();

  void launchCalendarActivity();

  String provideInterest();
  String provideNumberOfVisit();
  int provideBuddies();
  String provideSpots();

}
