package com.essplore.essplore.ui.activities.calendar.view;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.view.View;
import android.widget.ProgressBar;
import butterknife.BindView;
import butterknife.OnClick;
import com.essplore.essplore.R;
import com.essplore.essplore.api.requests.CreateTrackRequest;
import com.essplore.essplore.application.AppConstants;
import com.essplore.essplore.application.EssploreApplication;
import com.essplore.essplore.managers.AppActivityManager;
import com.essplore.essplore.managers.SharedPreferenceManager;
import com.essplore.essplore.models.MyTrip;
import com.essplore.essplore.ui.activities.HttpToolBarBaseActivity;
import com.essplore.essplore.ui.activities.calendar.presenter.CalendarPresenterImpl;
import com.essplore.essplore.ui.activities.camera.CameraActivity;
import com.prolificinteractive.materialcalendarview.CalendarDay;
import com.prolificinteractive.materialcalendarview.MaterialCalendarView;
import io.reactivex.disposables.CompositeDisposable;
import java.util.ArrayList;
import java.util.List;
import javax.inject.Inject;

public class CalendarActivity extends HttpToolBarBaseActivity implements CalendarView {

  @Inject CompositeDisposable compositeDisposable;
  @Inject SharedPreferenceManager sharedPreferenceManager;
  @Inject AppActivityManager appActivityManager;
  @Inject AlertDialog alertDialog;
  @Inject CalendarPresenterImpl presenter;

  @BindView(R.id.calendarView) MaterialCalendarView calendarView;
  @BindView(R.id.pbCalendar) ProgressBar pbCalendar;

  private List<CalendarDay> calendarDayList;

  @Override public void onNetworkErrorFound(String message) {
    showAlertDialog(message);
  }

  @Override public void onHttpErrorUnexpectedFound(String message) {
    showAlertDialog(message);
  }

  @Override public void onApiErrorFound(String message) {
    showAlertDialog(message);
  }

  @Override public void onHttpErrorUnexpectedFound() {
    showAlertDialog(getString(R.string.unexpected_error_occurred));
  }

  @Override public void onUnauthorizedErrorFound() {
    showAlertDialog(getString(R.string.unauthorized));
  }

  @Override public CompositeDisposable getCompositeDisposable() {
    return compositeDisposable;
  }

  @Override protected Activity getActivity() {
    return this;
  }

  @Override protected AlertDialog getAlertDialog() {
    return alertDialog;
  }

  @Override protected void setupActivityLayout() {
    setContentView(R.layout.activity_calendar);
  }

  @Override protected void setupViewElements() {
    calendarDayList = new ArrayList<>();
    calendarView.setSelectionMode(MaterialCalendarView.SELECTION_MODE_RANGE);
    calendarView.setOnRangeSelectedListener((widget, dates) -> calendarDayList = dates);
  }

  @Override protected void injectDaggerComponent() {
    EssploreApplication.get(this).createCalendarComponent(this).inject(this);
  }

  @Override protected boolean isActionbarBackButtonEnabled() {
    return Boolean.TRUE;
  }

  @Override protected void onDestroy() {
    super.onDestroy();
    EssploreApplication.get(this).releaseCalendarComponent();
  }

  @OnClick(R.id.btnTouristSpots) void onAddTouristSpotsClicked() {
    if (calendarView.getSelectedDates().size() > 1) {
      compositeDisposable.add(presenter.createTrack());
    } else {
      showAlertDialog(getString(R.string.please_select_proper_date_range));
    }
  }

  @Override public void showProgressbar() {
    pbCalendar.setVisibility(View.VISIBLE);
  }

  @Override public void hideProgressbar() {
    pbCalendar.setVisibility(View.GONE);
  }

  @Override public void launchPlacesActivity(MyTrip myTrip) {

    Intent returnIntent = new Intent();
    setResult(Activity.RESULT_OK, returnIntent);
    finish();
    /*Bundle bundle = new Bundle();
    bundle.putParcelable(AppConstants.MY_TRACK_OBJECT, myTrip.getMyTrack());
    appActivityManager.launchTimeline(getActivity(), bundle);*/
    //appActivityManager.launchPlacesActivity(this, myTrip, getCreateTrackRequest().getCountryId(), getCreateTrackRequest().getCityId());
  }

  @Override public String provideToken() {
    return sharedPreferenceManager.getAccessToken();
  }

  @Override public String provideStartDate() {
    CalendarDay startDate = calendarDayList.get(0);
    return String.format(getString(R.string.date_format), startDate.getYear(), startDate.getMonth(),
        startDate.getDay());
  }

  @Override public String provideEndDate() {
    CalendarDay endDate = calendarDayList.get(calendarDayList.size() - 1);
    return String.format(getString(R.string.date_format), endDate.getYear(), endDate.getMonth(), endDate.getDay());
  }

  @Override public CreateTrackRequest getCreateTrackRequest() {
    return getIntent().getParcelableExtra(AppConstants.CREATE_TRACK_OBJECT);
  }
}
