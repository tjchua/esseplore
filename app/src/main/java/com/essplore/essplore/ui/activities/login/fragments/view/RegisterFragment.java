package com.essplore.essplore.ui.activities.login.fragments.view;

import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputEditText;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.text.Html;
import android.text.Spanned;
import android.text.method.LinkMovementMethod;
import android.text.util.Linkify;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import com.essplore.essplore.R;
import com.essplore.essplore.api.response.LoginResponse;
import com.essplore.essplore.application.AppConstants;
import com.essplore.essplore.application.EssploreApplication;
import com.essplore.essplore.managers.AppActivityManager;
import com.essplore.essplore.managers.SharedPreferenceManager;
import com.essplore.essplore.models.Country;
import com.essplore.essplore.models.User;
import com.essplore.essplore.ui.activities.HttpBaseFragment;
import com.essplore.essplore.ui.activities.login.LoginActivity;
import com.essplore.essplore.ui.activities.login.fragments.presenter.RegisterFragmentPresenterImpl;
import com.essplore.essplore.ui.activities.place.PlaceActivity;
import com.wdullaer.materialdatetimepicker.date.DatePickerDialog;
import io.reactivex.disposables.CompositeDisposable;
import java.util.Calendar;
import java.util.List;
import javax.inject.Inject;
import com.hbb20.CountryCodePicker;


public class RegisterFragment extends HttpBaseFragment implements RegisterFragmentView {

  @Inject RegisterFragmentPresenterImpl presenter;
  @Inject CompositeDisposable disposable;
  @Inject AlertDialog alertDialog;
  @Inject SharedPreferenceManager manager;
  @Inject AppActivityManager appActivityManager;

  @BindView(R.id.edtEmail) TextInputEditText edtEmail;
  @BindView(R.id.edtPassword) TextInputEditText edtPassword;
  @BindView(R.id.edtUsername) TextInputEditText edtUsername;
  @BindView(R.id.edtDatePicker) TextInputEditText edtDatePicker;
  @BindView(R.id.edtMobile) TextInputEditText edtMobile;
  @BindView(R.id.llProgress) LinearLayout llProgress;
  @BindView(R.id.spnCountry) Spinner spnCountry;
  @BindView(R.id.btnRegister) Button btnRegister;
  @BindView(R.id.ccp) CountryCodePicker countryCodePicker;
  @BindView(R.id.terms_policy) TextView terms_policy;

  Unbinder unbinder;

  private LoginActivity activity;

  public RegisterFragment() {
    // Intended to be empty.
  }

  @Nullable @Override public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
      @Nullable Bundle savedInstanceState) {
    View view = inflater.inflate(R.layout.fragment_register, container, false);
    unbinder = ButterKnife.bind(this, view);
    return view;
  }

  @Override public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
    super.onViewCreated(view, savedInstanceState);
    EssploreApplication.get(activity).createRegisterFragmentComponent(this).inject(this);
    disposable.add(presenter.getCountries());
    countryCodePicker.registerCarrierNumberEditText(edtMobile);

    countryCodePicker.isValidFullNumber();
    countryCodePicker.setPhoneNumberValidityChangeListener(new CountryCodePicker.PhoneNumberValidityChangeListener() {
      @Override
      public void onValidityChanged(boolean isValidNumber) {
//        if (!isValidNumber) {
//          edtMobile.setTextColor(Color.RED);
//        } else {
//          edtMobile.setTextColor(Color.BLACK);
//        }
      }
    });
  }

  @Override public void onAttach(Context context) {
    super.onAttach(context);
    this.activity = (LoginActivity) context;
  }

  @Override public void onDestroyView() {
    super.onDestroyView();
    unbinder.unbind();
    EssploreApplication.get(activity).releaseRegisterFragmentComponent();
    disposable.clear();
  }

  @Override public void showProgress() {
    llProgress.setVisibility(View.VISIBLE);
  }

  @Override public void hideProgress() {
    llProgress.setVisibility(View.GONE);
  }

  @Override public void setUsernameError() {
    Toast.makeText(activity, getString(R.string.username_cannot_be_empty), Toast.LENGTH_SHORT).show();
  }

  @Override public void setPasswordError() {
    Toast.makeText(activity, getString(R.string.password_cannot_be_empty), Toast.LENGTH_SHORT).show();
  }

  @Override public void setBirthdayError() {
    Toast.makeText(activity, getString(R.string.birthday_cannot_be_empty), Toast.LENGTH_SHORT).show();
  }

  @Override public void setMobileError() {
    String message = "";
    if (!countryCodePicker.isValidFullNumber() && edtMobile.getText().length() > 0) {
      message = "Please check phone number format.";
    } else if (edtMobile.getText().length() == 0) {
      message = getString(R.string.mobile_cannot_be_empty);
    } else {
      message = "Check Phone number.";
    }
    Toast.makeText(activity, message, Toast.LENGTH_SHORT).show();
  }

  @Override public void navigateToHome() {

    if (goToPlacePage()){

      getActivity().finish();

    }else{

      appActivityManager.launchHomeActivity(activity);

    }

  }

  @Override public void setupSpinner(List<Country> countryList) {
    ArrayAdapter<Country> countryArrayAdapter =
        new ArrayAdapter<>(activity, android.R.layout.simple_spinner_dropdown_item, countryList);
    spnCountry.setAdapter(countryArrayAdapter);
    spnCountry.setSelection(0);
  }

  @Override public void saveResponse(LoginResponse response, User user) {
    manager.saveTokens(response, user);
  }

  @Override public String provideUsername() {
    return edtUsername.getText().toString();
  }

  @Override public String providePassword() {
    return edtPassword.getText().toString();
  }

  @Override public String provideMobile() {
//    if (countryCodePicker.isValidFullNumber()) {
      return countryCodePicker.getFullNumberWithPlus();
//    } else {
//      return null;
//    }
  }

  @Override public String provideBirthday() {
    return edtDatePicker.getText().toString();
  }

  @Override public String provideEmail() {
    return edtEmail.getText().toString();
  }

  @Override public int provideCountry() {
    return ((Country) spnCountry.getSelectedItem()).getId();
  }

  @Override public void showRegistrationError(String message) {
    Toast.makeText(activity, message, Toast.LENGTH_SHORT).show();
  }

  @OnClick(R.id.edtDatePicker) void onDatePickerClicked() {
    final Calendar now = Calendar.getInstance();
    final DatePickerDialog dpd = DatePickerDialog.newInstance((view, year, monthOfYear, dayOfMonth) -> {
      final int month = monthOfYear + 1;
      final String formattedMonth = (month < 10) ? AppConstants.DEFAULT_DATE_VALUE + month : String.valueOf(month);
      final String formattedDay =
          (dayOfMonth < 10) ? AppConstants.DEFAULT_DATE_VALUE + dayOfMonth : String.valueOf(dayOfMonth);
      edtDatePicker.setText(
          String.format(getString(R.string.datepicker_locale), String.valueOf(year), formattedMonth, formattedDay));
    }, now.get(Calendar.YEAR), now.get(Calendar.MONTH), now.get(Calendar.DAY_OF_MONTH));
    dpd.setMaxDate(now);
    dpd.show(activity.getFragmentManager(), AppConstants.DATE_PICKER_TAG);
    dpd.setVersion(DatePickerDialog.Version.VERSION_2);
  }

  @OnClick(R.id.btnRegister) void onRegisterClicked() {
    if (presenter.validateCredentials()) {
      disposable.add(presenter.registerUser(presenter.buildRequest()));
    }
  }

  @Override public void onNetworkErrorFound(String message) {
    showAlertDialog(message);
  }

  @Override public void onHttpErrorUnexpectedFound() {
    showAlertDialog(getString(R.string.unexpected_error_occurred));
  }

  @Override public void onUnauthorizedErrorFound() {
    showAlertDialog(getString(R.string.unauthorized));
  }

  @Override public void onApiErrorFound(String message) {
    showAlertDialog(message);
  }

  @Override protected Fragment getFragment() {
    return this;
  }

  @Override protected AlertDialog getAlertDialog() {
    return alertDialog;
  }

  private boolean goToPlacePage(){

    return getActivity().getIntent().getBooleanExtra(PlaceActivity.BACK_TO_PLACE_INTENT_KEY, false);

  }

}
