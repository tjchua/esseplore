package com.essplore.essplore.ui.activities.login.fragments.presenter;

import com.essplore.essplore.api.requests.RegisterRequest;
import io.reactivex.disposables.Disposable;

public interface RegisterFragmentPresenter {

  Disposable getCountries();

  Disposable registerUser(RegisterRequest registerRequest);

  RegisterRequest buildRequest();

  boolean validateCredentials();
}
