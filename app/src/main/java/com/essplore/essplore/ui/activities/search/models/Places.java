package com.essplore.essplore.ui.activities.search.models;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class Places extends ArrayList<Place>{

    private final String HIGHLIGHT = "highlight";

    public void addPlaces(String json){

        try {

            JSONObject jsonObject = new JSONObject(json);
            JSONArray jsonArray = jsonObject.getJSONArray(HIGHLIGHT);

            for (int i=0; i < jsonArray.length(); i++){

                Place place = new Place(jsonArray.getJSONObject(i).toString());
                this.add(place);

            }

        } catch (JSONException e) {

            e.printStackTrace();

        }

    }

    public Places(){



    }

    public void filter(String filterValue){
        ArrayList<Place> places = new ArrayList<Place>();
        for (int i=0; i<this.size(); i++){

            if (this.get(i).getType().equalsIgnoreCase(filterValue)){

                places.add(this.get(i));

            }

        }
        this.clear();
        this.addAll(places);

    }

}
