package com.essplore.essplore.ui.activities.cityhighlights.view;

import com.essplore.essplore.models.City;
import com.essplore.essplore.models.CityHighlight;
import com.essplore.essplore.ui.activities.HttpBaseView;
import java.util.List;

public interface CityHighlightsView extends HttpBaseView {

  City provideCityObject();

  void hideProgressbar();

  String provideToken();

  CityHighlight provideCityHighlight();

  void setTextViewValues(String description, String address, String openHours);

  void showToastMessage(String message);

  void setIntentResultOk();
}
