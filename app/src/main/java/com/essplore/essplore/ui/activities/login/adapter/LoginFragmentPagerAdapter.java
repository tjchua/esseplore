package com.essplore.essplore.ui.activities.login.adapter;

import android.content.Context;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import com.essplore.essplore.R;
import com.essplore.essplore.application.AppConstants;
import com.essplore.essplore.ui.activities.login.fragments.view.LoginFragment;
import com.essplore.essplore.ui.activities.login.fragments.view.RegisterFragment;

public class LoginFragmentPagerAdapter extends FragmentPagerAdapter {

  private Context context;

  public LoginFragmentPagerAdapter(FragmentManager fm, Context context) {
    super(fm);
    this.context = context;
  }

  @Override public Fragment getItem(int position) {
    switch (position) {
      case 0:
        return new LoginFragment();

      case 1:
        return new RegisterFragment();

      default:
        return new LoginFragment();
    }
  }

  @Override public int getCount() {
    return AppConstants.LOGIN_FRAGMENT_COUNT;
  }

  @Nullable @Override public CharSequence getPageTitle(int position) {
    switch (position) {
      case 0:
        return context.getString(R.string.login_page);

      case 1:
        return context.getString(R.string.register_page);

      default:
        return context.getString(R.string.login_page);
    }
  }
}
