package com.essplore.essplore.ui.activities.login.fragments.view;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputEditText;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import com.essplore.essplore.R;
import com.essplore.essplore.api.response.LoginResponse;
import com.essplore.essplore.application.EssploreApplication;
import com.essplore.essplore.managers.AppActivityManager;
import com.essplore.essplore.managers.SharedPreferenceManager;
import com.essplore.essplore.ui.activities.HttpBaseFragment;
import com.essplore.essplore.ui.activities.login.LoginActivity;
import com.essplore.essplore.ui.activities.login.fragments.presenter.LoginFragmentPresenterImpl;
import com.essplore.essplore.ui.activities.place.PlaceActivity;

import io.reactivex.disposables.CompositeDisposable;
import javax.inject.Inject;

public class LoginFragment extends HttpBaseFragment implements LoginFragmentView {

  @Inject LoginFragmentPresenterImpl presenter;
  @Inject CompositeDisposable compositeDisposable;
  @Inject SharedPreferenceManager manager;
  @Inject AppActivityManager appActivityManager;
  @Inject AlertDialog alertDialog;

  @BindView(R.id.edtEmail) TextInputEditText edtEmail;
  @BindView(R.id.edtPassword) TextInputEditText edtPassword;
  @BindView(R.id.btnLogIn) Button btnLogIn;
  @BindView(R.id.tvForgotPassword) TextView tvForgotPassword;
  @BindView(R.id.pbLogin) ProgressBar pbLogin;
  Unbinder unbinder;

  private LoginActivity activity;

  public LoginFragment() {
    // Intended to be empty
  }

  @Nullable @Override public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
      @Nullable Bundle savedInstanceState) {
    View view = inflater.inflate(R.layout.fragment_login, container, false);
    unbinder = ButterKnife.bind(this, view);
    return view;
  }

  @Override public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
    super.onViewCreated(view, savedInstanceState);
    EssploreApplication.get(activity).createLoginFragmentComponent(this).inject(this);
  }

  @Override public void onAttach(Context context) {
    super.onAttach(context);
    this.activity = (LoginActivity) context;
  }

  @Override public void onDestroyView() {
    super.onDestroyView();
    unbinder.unbind();
    EssploreApplication.get(activity).releaseLoginFragmentComponent();
    compositeDisposable.clear();
  }

  @Override public void showProgress() {
    pbLogin.setVisibility(View.VISIBLE);
  }

  @Override public void hideProgress() {
    pbLogin.setVisibility(View.GONE);
  }

  @Override public void setUsernameError() {
    Toast.makeText(activity, getString(R.string.username_cannot_be_empty), Toast.LENGTH_SHORT).show();
  }

  @Override public void setPasswordError() {
    Toast.makeText(activity, getString(R.string.password_cannot_be_empty), Toast.LENGTH_SHORT).show();
  }

  @Override public void navigateToHome() {

    if (goToPlacePage()){

      getActivity().finish();

    }else{

      appActivityManager.launchHomeActivity(activity);

    }

  }

  @Override public void saveResponse(LoginResponse response) {
    manager.saveTokens(response);
  }

  @OnClick(R.id.tvForgotPassword) void onForgotPasswordClicked() {
    activity.openForgotPassword();
  }

  @Override public String provideUsername() {
    return edtEmail.getText().toString();
  }

  @Override public String providePassword() {
    return edtPassword.getText().toString();
  }

  @OnClick(R.id.btnLogIn) void onLoginClicked() {
    if (presenter.validateCredentials(provideUsername(), providePassword())) {
      compositeDisposable.add(presenter.loginUser(presenter.buildLoginRequest()));
    }
  }

  @Override protected Fragment getFragment() {
    return this;
  }

  @Override protected AlertDialog getAlertDialog() {
    return alertDialog;
  }

  @Override public void onNetworkErrorFound(String message) {
    showAlertDialog(message);
  }

  @Override public void onHttpErrorUnexpectedFound() {
    showAlertDialog(getString(R.string.unexpected_error_occurred));
  }

  @Override public void onUnauthorizedErrorFound() {
    showAlertDialog(getString(R.string.unauthorized));
  }

  @Override public void onApiErrorFound(String message) {
    showAlertDialog(message);
  }

  private boolean goToPlacePage(){

    return getActivity().getIntent().getBooleanExtra(PlaceActivity.BACK_TO_PLACE_INTENT_KEY, false);

  }
}
