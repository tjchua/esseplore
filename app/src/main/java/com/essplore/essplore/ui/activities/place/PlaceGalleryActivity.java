package com.essplore.essplore.ui.activities.place;

import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;

import com.essplore.essplore.R;
import com.essplore.essplore.ui.activities.place.adapter.PlaceGalleryAdapter;
import com.essplore.essplore.ui.activities.place.fragments.PlaceFragment;
import com.essplore.essplore.ui.activities.search.SearchActivity;
import com.essplore.essplore.ui.activities.search.models.Place;

public class PlaceGalleryActivity extends AppCompatActivity implements View.OnClickListener{

    private RecyclerView recyclerView;
    private Place place;
    private PlaceGalleryAdapter placeGalleryAdapter;
    private ImageView backIv;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_place_gallery);
        place = (Place) getIntent().getSerializableExtra(SearchActivity.PLACE_INTENT_KEY);
        initViews();

    }

    @Override
    public void onClick(View v) {

        switch (v.getId()){

            case R.id.back_iv:

                finish();

                break;

        }

    }

    private void initViews() {

        recyclerView = findViewById(R.id.photos_rv);
        recyclerView.setLayoutManager(new GridLayoutManager(this, 2));
        recyclerView.setAdapter(getPlaceGalleryAdapter());
        getBackIv();

    }

    private Place getPlace() {

        if (place == null) {

            place = new Place();

        }

        return place;
    }

    private PlaceGalleryAdapter getPlaceGalleryAdapter(){

        if (placeGalleryAdapter == null){

            placeGalleryAdapter = new PlaceGalleryAdapter(this, getPlace().getPlacePictureList());

        }

        return placeGalleryAdapter;

    }

    private ImageView getBackIv(){

        if (backIv == null){

            backIv = findViewById(R.id.back_iv);
            backIv.setOnClickListener(this);

        }

        return backIv;

    }

}
