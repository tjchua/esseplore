package com.essplore.essplore.ui.activities.paymentform;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.essplore.essplore.R;
import com.essplore.essplore.application.AppConstants;
import com.essplore.essplore.managers.SharedPreferenceKeys;
import com.essplore.essplore.managers.SharedPreferenceManager;
import com.stripe.android.Stripe;
import com.stripe.android.TokenCallback;
import com.stripe.android.model.Card;
import com.stripe.android.model.Token;
import com.stripe.android.view.CardInputWidget;

import org.apache.commons.lang3.StringUtils;
import org.json.JSONException;
import org.json.JSONObject;
import org.w3c.dom.Text;

public class PaymentFormController {

    private AppCompatActivity activity;
    private EditText amountInput;
    private Button verifyBtn;
    private CardInputWidget cardInputWidget;
    private SharedPreferenceManager sharedPreferenceManager;
    private SharedPreferences sharedPreferences;
    private PaymentFormApi paymentFormApi;
    private String apiPublishToken;
    private ProgressDialog dialog;

    public PaymentFormController(AppCompatActivity activity){
        this.activity = activity;
        initViews();
        initAPI();
    }

    private void initAPI(){
        String accessToken = activity.getSharedPreferences(SharedPreferenceKeys.ESSPLORE_PREFERENCE, Context.
                MODE_PRIVATE).getString(SharedPreferenceKeys.ACCESS_TOKEN, StringUtils.EMPTY);
        paymentFormApi = new PaymentFormApi(activity);
    }

    private void initViews(){
        amountInput = (EditText) activity.findViewById(R.id.amount_input);
        amountInput.addTextChangedListener(amountInputTextWatcher);
        amountInput.setOnClickListener(amountInputClickListener);
        verifyBtn = (Button) activity.findViewById(R.id.verify_btn);
        verifyBtn.setOnClickListener(verifyBtnClickListener);
        cardInputWidget = (CardInputWidget) activity.findViewById(R.id.card_input);
        prepareValidationDialog();
    }

    private TextWatcher amountInputTextWatcher = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {

        }

        @Override
        public void afterTextChanged(Editable s) {
            formatAmountInput(amountInput);
        }
    };

    private View.OnClickListener amountInputClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            amountInput.setSelection(amountInput.length());
        }
    };

    private View.OnClickListener verifyBtnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            verifyCard(cardInputWidget, amountInput);
        }
    };

    private void formatAmountInput(EditText editText){
        String currentText = editText.getText().toString();
        String dollarSign = activity.getString(R.string.dollar_sign);

        if(!currentText.contains(dollarSign) && !currentText.isEmpty()) {
            editText.setText(dollarSign + currentText);
            editText.setSelection(amountInput.length());
        }else if(currentText.equalsIgnoreCase(dollarSign)){
            editText.setText("");
        }
    }

    private void verifyCard(CardInputWidget cardInputWidget, EditText amountInput){
        Card cardToSave = cardInputWidget.getCard();
        String amount = amountInput.getText().toString();
        if (cardToSave == null) {
            showError(activity.getString(R.string.invalid_card_data));
            return;
        } else if (amount.isEmpty()){
            showError(activity.getString(R.string.input_amount));
            return;
        }
        createTransacToken(cardToSave);
    }

    private void showError(String message){
        Toast.makeText(activity, message, Toast.LENGTH_SHORT).show();
    }

    private void createTransacToken(Card card){
        Stripe stripe = new Stripe(activity, AppConstants.PUBLISHASBLE_KEY);
        hideKeyboard();
        dialog.show();
        stripe.createToken(card, stripeTokenCallback);
    }

    TokenCallback stripeTokenCallback = new TokenCallback() {
        @Override
        public void onError(Exception error) {
            showError(error.getLocalizedMessage());
            dialog.dismiss();
        }

        @Override
        public void onSuccess(Token token) {
            String amount = amountInput.getText().toString();
            paymentFormApi.sendPaymentToken(paymentTokenJSON(token.getId().toString(), formatAmount(amount)));
            dialog.dismiss();
        }
    };

    private int formatAmount(String amount){
        String stringAmount = amount.replace("$","");
        return Integer.parseInt(stringAmount);
    }

    private String paymentTokenJSON(String paymentToken, int amount){
        JSONObject json = new JSONObject();
        try {
            json.put(AppConstants.TOKEN, paymentToken);
            json.put(AppConstants.AMOUNT, amount);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return json.toString();
    }

    private void prepareValidationDialog(){
        dialog = new ProgressDialog(activity);
        dialog.setCancelable(false);
        dialog.setMessage(activity.getString(R.string.validating));
    }

    private void hideKeyboard(){
        ((InputMethodManager) activity.getSystemService(Context.INPUT_METHOD_SERVICE)).hideSoftInputFromWindow(amountInput.getWindowToken(), 0);
    }



}
