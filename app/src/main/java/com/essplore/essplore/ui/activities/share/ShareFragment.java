package com.essplore.essplore.ui.activities.share;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Switch;
import android.widget.Toast;

import com.essplore.essplore.R;
import com.essplore.essplore.application.AppConstants;
import com.essplore.essplore.models.MyTrack;
import com.essplore.essplore.ui.activities.camera.CameraActivity;
import com.essplore.essplore.ui.activities.share.adapters.ShareAdapter;
import com.essplore.essplore.ui.activities.timeline.view.TimelineActivity;
import com.essplore.essplore.ui.utils.geolocation.GeoLocationUtil;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.share.Sharer;
import com.facebook.share.model.SharePhoto;
import com.facebook.share.model.SharePhotoContent;
import com.facebook.share.widget.ShareDialog;

import java.lang.reflect.Array;
import java.util.ArrayList;

public class ShareFragment extends android.app.Fragment implements ShareAPIinterface, ShareAdapterInterface{

//    private ImageView selectedImageview;
    private Bitmap selectedImage;
    private Context context;
    private Button doneBtn;
    private ShareApi shareApi;
    private Switch fbSwitch;
    private String caption;
    private String location;
    private EditText descriptionEt;
    private EditText titleEt;
    private EditText locationHolder;
    private ArrayList<String> localImages;
    private ShareAdapter shareAdapter;
    private RecyclerView recyclerView;
    private GeoLocationUtil geoLocationUtil;
    private ProgressDialog progressDialog;
    private ShareFragmentInterface shareFragmentInterface;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        shareApi = new ShareApi((ShareActivity)getActivity());
        shareApi.setShareApiInterface(this);
        geoLocationUtil = new GeoLocationUtil(getActivity());
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        final View view =  inflater.inflate(R.layout.fragment_share_2, container, false);
        Intent intent = getActivity().getIntent();
//        caption = intent.getExtras().getString(CameraActivity.CAPTION_INTENT_KEY);
//        location = intent.getExtras().getString(CameraActivity.LOCATION_INTENT_KEY);
        localImages = intent.getExtras().getStringArrayList(CameraActivity.LOCAL_IMAGES_INTENT_KEY);
//        locationHolder.setText(location);
//        description.setText(caption);
//        selectedImage = CameraActivity.SELECTEDIMAGE;
//        selectedImageview.setImageBitmap(selectedImage);
        initViews(view);
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {

        super.onViewCreated(view, savedInstanceState);

        fbSwitch = (Switch) view.findViewById(R.id.fb_switch);

        fbSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {

            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

                if (checkFbInstalled()){

                    shareOnFacebook();

                }else{

                    Toast.makeText((ShareActivity)getActivity(), "Facebook is not installed!", Toast.LENGTH_LONG).show();
                }

            }

        });

    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void onUploadFailed() {

        stopProgress();
        Toast.makeText(getActivity(), "Upload Failed!!!", Toast.LENGTH_SHORT).show();

    }

    @Override
    public void onUploadSuccess(String response) {

    }

    @Override
    public void onUploadSuccess(int position) {

        if (position < getLocalImages().size() - 1){

            shareApi.sendPost(getLocalImages().get(position + 1), geoLocationUtil.getrCurrenetLocation(), position + 1, getMyTrack().getTrackId(), getCurrentPlaceId(), getDescription());

        }else{

            stopProgress();
            Toast.makeText(getActivity(), "Upload Successful!!!", Toast.LENGTH_SHORT).show();
            if (shareFragmentInterface != null)
            shareFragmentInterface.quitWithResult(CameraActivity.ResultStatus.QUIT);

        }

    }

    @Override
    public void addPhoto(ArrayList<String> images) {

        if (shareFragmentInterface != null)
            shareFragmentInterface.quitWithResult(CameraActivity.ResultStatus.UPDATE, images, getTitle(), getDescription());

    }

    private View.OnClickListener doneClickLisener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {

//            GeoLocationUtil geoLocationUtil = new GeoLocationUtil(getActivity());
//            shareApi.sendPost(selectedImage, geoLocationUtil.getrCurrenetLocation());
////            getActivity().finish();
//            if (CameraActivity.instance != null) {
//                CameraActivity.instance.finish();
//            }
//            TimelineActivity.instance.addTimelineItem(selectedImage);
            if(getLocalImages().size() > 1){

                showProgress();
                shareApi.sendPost(getLocalImages().get(1), geoLocationUtil.getrCurrenetLocation(), 1, getMyTrack().getTrackId(), getCurrentPlaceId(), getDescription());

            }

        }
    };

    private void initViews(View view){

//        selectedImageview = (ImageView) view.findViewById(R.id.image);
//        description = view.findViewById(R.id.description);
        locationHolder = view.findViewById(R.id.location);
        doneBtn = (Button) view.findViewById(R.id.done);
        doneBtn.setOnClickListener(doneClickLisener);
        recyclerView = view.findViewById(R.id.recycler_view);
        descriptionEt = view.findViewById(R.id.desc_et);
        titleEt = view.findViewById(R.id.title_et);
        descriptionEt.setText(getDescriptionFromIntent());
        titleEt.setText(getTitleFromIntent());
        prepareRecyclerView();

    }

    private void prepareRecyclerView(){

        recyclerView.setHasFixedSize(true);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity().getApplicationContext(),
                LinearLayoutManager.HORIZONTAL, false);
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setAdapter(getShareAdapter());

    }

    private ShareAdapter getShareAdapter(){

        if (shareAdapter == null){

            shareAdapter = new ShareAdapter(getActivity(), getLocalImages());
            shareAdapter.setShareAdapterInterface(this);

        }

        return shareAdapter;

    }

    private ArrayList<String> getLocalImages(){

        if (localImages == null) {

            localImages = new ArrayList<String>();

        }

        return localImages;

    }

    public void shareOnFacebook(){

        SharePhoto photo = new SharePhoto.Builder()
                .setBitmap(selectedImage)
                .setCaption(caption)
                .build();
        SharePhotoContent content = new SharePhotoContent.Builder()
                .addPhoto(photo)
                .build();
        CallbackManager callbackManager = CallbackManager.Factory.create();
        ShareDialog shareDialog = new ShareDialog((ShareActivity)getActivity());
        // this part is optional
        shareDialog.registerCallback(callbackManager, new FacebookCallback<Sharer.Result>() {
            @Override
            public void onSuccess(Sharer.Result result) {
                Log.e("test", result.toString());
            }

            @Override
            public void onCancel() {
                Log.e("test", "");
            }

            @Override
            public void onError(FacebookException error) {
                Log.e("test", error.toString());
            }
        });
        shareDialog.show(content);

    }

    public Boolean checkFbInstalled() {
        PackageManager pm = ((ShareActivity)getActivity()).getPackageManager();
        boolean flag = false;
        try {
            pm.getPackageInfo("com.facebook.katana",
                    PackageManager.GET_ACTIVITIES);
            flag = true;
        } catch (PackageManager.NameNotFoundException e) {
            flag = false;
        }
        return flag;
    }

    private void showProgress(){

        progressDialog = new ProgressDialog(getActivity());
        progressDialog.setCancelable(false);
        progressDialog.setMessage(getString(R.string.uploading));
        progressDialog.show();

    }

    private void stopProgress(){

        if (progressDialog != null){

            progressDialog.dismiss();

        }

    }

    public void setShareFragmentInterface(ShareFragmentInterface shareFragmentInterface){

        this.shareFragmentInterface = shareFragmentInterface;

    }

    private MyTrack getMyTrack() {
        Bundle bundle = getActivity().getIntent().getBundleExtra(AppConstants.MY_TRACK_OBJECT);
        return bundle.getParcelable(AppConstants.MY_TRACK_OBJECT);
    }

    private String getCurrentPlaceId(){

        return getActivity().getIntent().getStringExtra(TimelineActivity.CURRENT_PLACE_ID_INTENT_KEY);

    }

    private String getTitleFromIntent(){

        return getActivity().getIntent().getStringExtra(CameraActivity.TITLE_INTENT_KEY);

    }

    private String getDescriptionFromIntent(){

        return getActivity().getIntent().getStringExtra(CameraActivity.DESCRIPTION_INTENT_KEY);

    }

    private String getTitle(){

        return titleEt.getText().toString();

    }

    private String getDescription(){

        return descriptionEt.getText().toString();

    }

}
