package com.essplore.essplore.ui.activities.directions.presenter;

import io.reactivex.disposables.Disposable;

public interface DirectionsPresenter {

    Disposable getSpecificTrack(int trackId);
    Disposable getPhotoTrack(int trackId);
    Disposable changeRecordStatus(int trackRecordId, int status);
    Disposable deletePlaceFromTrack(int trackPlaceId);

}