package com.essplore.essplore.ui.activities.camera.models;

import android.graphics.Bitmap;
import android.media.Image;
import android.widget.ImageView;

import com.essplore.essplore.ui.activities.camera.adapter.GalleryAdapter;
import com.google.gson.JsonArray;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class Gallery {

    private int id;
    private int track_id;
    private int user_id;
    private String description;
    private String picture_url;
    private String avatar_url;
    private String shared_on;
    private String created_at;
    private Bitmap bitmap;
    private GalleryAdapter galleryAdapter;
    private boolean selected;

    private String ID = "id";
    private String TRACK_ID = "track_id";
    private String USER_ID = "user_id";
    private String DESCRIPTION = "description";
    private String PICTURE_URL = "picture_url";
    private String AVATAR_URL = "avatar_url";
    private String SHARED_ON = "shared_on";
    private String CREATED_AT = "created_at";


    public Gallery(String galleryJson){
        JSONObject jsonObject;
        try {
            jsonObject = new JSONObject(galleryJson);
            id = jsonObject.getInt(ID);
            track_id = jsonObject.getInt(TRACK_ID);
            user_id = jsonObject.getInt(USER_ID);
            description = jsonObject.getString(DESCRIPTION);
            picture_url = jsonObject.getString(PICTURE_URL);
            avatar_url = jsonObject.getString(AVATAR_URL);
            shared_on = jsonObject.getString(SHARED_ON);
            created_at = jsonObject.getString(CREATED_AT);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getTrack_id() {
        return track_id;
    }

    public void setTrack_id(int track_id) {
        this.track_id = track_id;
    }

    public int getUser_id() {
        return user_id;
    }

    public void setUser_id(int user_id) {
        this.user_id = user_id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getPicture_url() {
        return picture_url;
    }

    public void setPicture_url(String picture_url) {
        this.picture_url = picture_url;
    }

    public String getAvatar_url() {
        return avatar_url;
    }

    public void setAvatar_url(String avatar_url) {
        this.avatar_url = avatar_url;
    }

    public String getShared_on() {
        return shared_on;
    }

    public void setShared_on(String shared_on) {
        this.shared_on = shared_on;
    }

    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    public Bitmap getBitmap() {
        return bitmap;
    }

    public void setBitmap(Bitmap bitmap) {
        this.bitmap = bitmap;
    }

    public GalleryAdapter getGalleryAdapter() {
        return galleryAdapter;
    }

    public void setGalleryAdapter(GalleryAdapter galleryAdapter) {
        this.galleryAdapter = galleryAdapter;
    }

    public boolean isSelected() {

        return selected;

    }

    public void setSelected(boolean selected) {

        this.selected = selected;

    }
}
