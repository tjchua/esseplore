package com.essplore.essplore.ui.activities.profile.fragments.view;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.Toast;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import com.essplore.essplore.R;
import com.essplore.essplore.application.EssploreApplication;
import com.essplore.essplore.managers.SharedPreferenceManager;
import com.essplore.essplore.ui.activities.HttpBaseDialogFragment;
import com.essplore.essplore.ui.activities.profile.fragments.presenter.ChangePasswordPresenterImpl;
import com.essplore.essplore.ui.activities.profile.view.ProfileActivity;
import io.reactivex.disposables.CompositeDisposable;
import javax.inject.Inject;

public class ChangePasswordFragment extends HttpBaseDialogFragment implements ChangePasswordFragmentView {

  @Inject AlertDialog alertDialog;
  @Inject ChangePasswordPresenterImpl presenter;
  @Inject SharedPreferenceManager manager;
  @Inject CompositeDisposable disposable;

  @BindView(R.id.edtOldPassword) EditText edtOldPassword;
  @BindView(R.id.edtNewPassword) EditText edtNewPassword;
  @BindView(R.id.edtConfirmNewPassword) EditText edtConfirmNewPassword;

  private ProfileActivity activity;
  private Unbinder unbinder;

  public ChangePasswordFragment() {
    // Intended to be empty.
  }

  @Nullable @Override public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
      @Nullable Bundle savedInstanceState) {
    View view = inflater.inflate(R.layout.dialog_fragment_change_password, container, false);
    unbinder = ButterKnife.bind(this, view);
    return view;
  }

  @Override public void onAttach(Context context) {
    super.onAttach(context);
    activity = (ProfileActivity) context;
  }

  @Override public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
    super.onViewCreated(view, savedInstanceState);
    EssploreApplication.get(activity).createChangePasswordComponent(this).inject(this);
  }

  @Override public void onDestroy() {
    super.onDestroy();
    EssploreApplication.get(activity).releaseChangePasswordComponent();
  }

  @Override protected DialogFragment getFragment() {
    return this;
  }

  @Override protected AlertDialog getAlertDialog() {
    return alertDialog;
  }

  @Override public String provideOldPassword() {
    return edtOldPassword.getText().toString();
  }

  @Override public String provideNewPassword() {
    return edtNewPassword.getText().toString();
  }

  @Override public String provideConfirmNewPassword() {
    return edtConfirmNewPassword.getText().toString();
  }

  @Override public String provideToken() {
    return manager.getAccessToken();
  }

  @Override public void showFieldError() {
    Toast.makeText(activity, getString(R.string.any_field_cannot_be_empty), Toast.LENGTH_SHORT).show();
  }

  @Override public void showPasswordDoesNotMatchError() {
    Toast.makeText(activity, getString(R.string.password_does_not_match), Toast.LENGTH_SHORT).show();
  }

  @OnClick(R.id.btnChangePassword) void onChangePasswordClicked(){
    if(presenter.isValidCredentials()){
      disposable.add(presenter.changePassword());
    }
  }

  @Override public void onDestroyView() {
    super.onDestroyView();
    unbinder.unbind();
  }
}
