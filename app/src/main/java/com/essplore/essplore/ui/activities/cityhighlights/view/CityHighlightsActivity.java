package com.essplore.essplore.ui.activities.cityhighlights.view;

import android.app.Activity;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;
import butterknife.BindView;
import butterknife.OnClick;
import com.bumptech.glide.Glide;
import com.essplore.essplore.R;
import com.essplore.essplore.application.AppConstants;
import com.essplore.essplore.application.EssploreApplication;
import com.essplore.essplore.managers.SharedPreferenceManager;
import com.essplore.essplore.models.City;
import com.essplore.essplore.models.CityHighlight;
import com.essplore.essplore.models.PlacePicture;
import com.essplore.essplore.ui.activities.HttpToolBarBaseActivity;
import com.essplore.essplore.ui.activities.cityhighlights.adapter.CityHighlightsPlacePictureAdapter;
import com.essplore.essplore.ui.activities.cityhighlights.presenter.CityHighlightsPresenterImpl;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import io.reactivex.disposables.CompositeDisposable;
import java.util.ArrayList;
import java.util.List;
import javax.inject.Inject;

public class CityHighlightsActivity extends HttpToolBarBaseActivity implements CityHighlightsView, OnMapReadyCallback {

  @Inject CompositeDisposable disposable;
  @Inject AlertDialog alertDialog;
  @Inject CityHighlightsPresenterImpl presenter;
  @Inject SharedPreferenceManager sharedPreferenceManager;

  @BindView(R.id.tvCityHighlightDescription) TextView tvCityHighlightDescription;
  @BindView(R.id.rvPhotosFromOtherEsseplorers) RecyclerView rvPhotosFromOtherEsseplorers;
  @BindView(R.id.txAddress) TextView txAddress;
  @BindView(R.id.txOpenHrs) TextView txOpenHrs;
  @BindView(R.id.btnAddTrip) Button btnAddTrip;
  @BindView(R.id.pbCityHighlights) ProgressBar pbCityHighlights;
  @BindView(R.id.ivCoverPhoto) ImageView ivCoverPhoto;
  @BindView(R.id.iv_attraction_visited) ImageView iv_attraction_visited;

  private CityHighlight cityHighlight;
  private CityHighlightsPlacePictureAdapter cityHighlightsPlacePictureAdapter;
  private List<PlacePicture> placePictureList;
  private GoogleMap googleMap;

  @Override public void onNetworkErrorFound(String message) {
    showAlertDialog(message);
  }

  @Override public void onHttpErrorUnexpectedFound(String message) {
    showAlertDialog(message);
  }

  @Override public void onApiErrorFound(String message) {
    showAlertDialog(message);
  }

  @Override public void onHttpErrorUnexpectedFound() {
    showAlertDialog(getString(R.string.unexpected_error_occurred));
  }

  @Override public void onUnauthorizedErrorFound() {
    showAlertDialog(getString(R.string.unauthorized));
  }

  @Override public CompositeDisposable getCompositeDisposable() {
    return disposable;
  }

  @Override protected Activity getActivity() {
    return this;
  }

  @Override protected AlertDialog getAlertDialog() {
    return alertDialog;
  }

  @Override protected void setupActivityLayout() {
    setContentView(R.layout.activity_city_highlights);
  }

  @Override protected void setupViewElements() {
    SupportMapFragment mapFragment =
        (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.mvCityHighlights);
    mapFragment.getMapAsync(this);

    setupCityHighlightPlacePictureListAdapter();
    if (getIntent().getParcelableExtra(AppConstants.CITY_HIGHLIGHT_OBJECT) != null) {
      cityHighlight = getIntent().getParcelableExtra(AppConstants.CITY_HIGHLIGHT_OBJECT);
      setCityHighlightCoverPhoto();
      setPlacePictureListValue(cityHighlight.getPlacePictureList());
      setTextViewValues(cityHighlight.getDescription(), cityHighlight.getAddress1(), cityHighlight.getOpeningHours());
      setupGoogleMapMarker();
      if(cityHighlight.getTrackRecordId() == 0 && provideCityObject().getTrackId() == 0){
        btnAddTrip.setVisibility(View.GONE);
      }
    } else if (getIntent().getParcelableExtra(AppConstants.TRACKS_OBJECT) != null) {
      cityHighlight = new CityHighlight(getIntent().getParcelableExtra(AppConstants.TRACKS_OBJECT));
      setCityHighlightCoverPhoto();
      setPlacePictureListValue(cityHighlight.getPlacePictureList());
      setTextViewValues(cityHighlight.getDescription(), cityHighlight.getAddress1(), cityHighlight.getOpeningHours());
      setupGoogleMapMarker();
      if (cityHighlight.getStatus().equals("Visited")) {
        iv_attraction_visited.setVisibility(View.VISIBLE);
      } else {
        iv_attraction_visited.setVisibility(View.GONE);
      }
      btnAddTrip.setVisibility(View.GONE);
    }
  }

  @Override protected void injectDaggerComponent() {
    EssploreApplication.get(this).createCityHighlightsComponent(this).inject(this);
  }

  @Override protected boolean isActionbarBackButtonEnabled() {
    return Boolean.TRUE;
  }

  @Override protected void onDestroy() {
    super.onDestroy();
    EssploreApplication.get(this).releaseCityHighlightsComponent();
  }

  @Override public City provideCityObject() {
    return getIntent().getParcelableExtra(AppConstants.CITY_OBJECT);
  }

  @Override public void hideProgressbar() {
    pbCityHighlights.setVisibility(View.GONE);
  }

  @Override public void setTextViewValues(String description, String address, String openHours) {
    tvCityHighlightDescription.setText(description);
    txAddress.setText(address);
    txOpenHrs.setText(openHours);
  }

  @Override public void showToastMessage(String message) {
    Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
  }

  @Override public void setIntentResultOk() {
    setResult(Activity.RESULT_OK);
    finish();
  }

  @Override public void onMapReady(GoogleMap googleMap) {
    this.googleMap = googleMap;
    setupGoogleMapMarker();
  }

  @Override public String provideToken() {
    return sharedPreferenceManager.getAccessToken();
  }

  @Override public CityHighlight provideCityHighlight() {
    return cityHighlight;
  }

  @OnClick(R.id.btnAddTrip) void addPlaceToTrip() {
    disposable.add(presenter.addPlaceToTrip());
  }

  private void setCityHighlightCoverPhoto() {
    if (cityHighlight.getAvatarUrl() != null) {
      Glide.with(this).load(cityHighlight.getAvatarUrl()).centerCrop().into(ivCoverPhoto);
    }
  }

  private void setupCityHighlightPlacePictureListAdapter() {
    placePictureList = new ArrayList<>();
    cityHighlightsPlacePictureAdapter = new CityHighlightsPlacePictureAdapter(this, placePictureList, presenter);
    rvPhotosFromOtherEsseplorers.setAdapter(cityHighlightsPlacePictureAdapter);
    rvPhotosFromOtherEsseplorers.setLayoutManager(new LinearLayoutManager(this, LinearLayout.HORIZONTAL, false));
    rvPhotosFromOtherEsseplorers.setNestedScrollingEnabled(false);
  }

  private void setupGoogleMapMarker() {
    if (googleMap != null && cityHighlight.getLat() != null && cityHighlight.getLng() != null) {
      LatLng city = new LatLng(Double.parseDouble(cityHighlight.getLat()), Double.parseDouble(cityHighlight.getLng()));
      googleMap.clear();
      googleMap.addMarker(new MarkerOptions().position(city));
      googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(city, 12.0f));
    }
  }

  private void setPlacePictureListValue(List<PlacePicture> placePictureListValue) {
    if (placePictureListValue != null && placePictureListValue.size() > 0) {
      placePictureList.clear();
      placePictureList.addAll(placePictureListValue);
      cityHighlightsPlacePictureAdapter.notifyDataSetChanged();
    }
  }
}
