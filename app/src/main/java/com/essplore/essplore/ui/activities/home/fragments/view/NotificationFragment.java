package com.essplore.essplore.ui.activities.home.fragments.view;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.essplore.essplore.R;
import com.essplore.essplore.application.EssploreApplication;
import com.essplore.essplore.managers.AppActivityManager;
import com.essplore.essplore.ui.activities.HttpBaseFragment;
import com.essplore.essplore.ui.activities.home.view.HomeActivity;
import javax.inject.Inject;
import butterknife.ButterKnife;
import butterknife.Unbinder;

public class NotificationFragment extends HttpBaseFragment {

    @Inject
    AppActivityManager appActivityManager;

    private Unbinder unbinder;
    private HomeActivity activity;


    public NotificationFragment() {
        // Intended to be empty.
    }

    @Nullable
    @Override public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                                       @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_notif, container, false);
        unbinder = ButterKnife.bind(this, view);
        return view;
    }

    @Override public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }

    @Override public void onAttach(Context context) {
        super.onAttach(context);
        activity = (HomeActivity) context;
    }

    @Override public void onDestroy() {
        super.onDestroy();
        unbinder.unbind();
        EssploreApplication.get(activity).releaseAccountComponent();
    }

    @Override
    protected Fragment getFragment() {
        return null;
    }

    @Override
    protected AlertDialog getAlertDialog() {
        return null;
    }

    @Override
    protected void showAlertDialog(String message) {
        super.showAlertDialog(message);
    }

}
