package com.essplore.essplore.ui.activities.places.view;

import com.essplore.essplore.api.requests.RemovePlaceFromTrackRequest;
import com.essplore.essplore.models.City;
import com.essplore.essplore.models.MyTrack;
import com.essplore.essplore.models.MyTrip;
import com.essplore.essplore.models.TrackPicture;
import com.essplore.essplore.models.Tracks;
import com.essplore.essplore.ui.activities.HttpBaseView;
import java.util.List;

public interface PlacesView extends HttpBaseView {

  MyTrip getMyTrip();

  City buildCityObject();

  int getCountryId();

  int getCityId();

  void showProgressbar();

  void hideProgressbar();

  String provideToken();

  void setTrackPictureList(List<TrackPicture> trackPictureList);

  void setTracksList(List<Tracks> tracksList);

  void launchCityHighlightsForViewing(Tracks tracks);

  RemovePlaceFromTrackRequest buildRequest();

  MyTrack getMyTrack();

  void setMyTrack(MyTrack myTrack);
}
