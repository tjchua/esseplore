package com.essplore.essplore.ui.activities.addtotrip.models;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;
import java.util.ArrayList;

public class Trip implements Serializable{

    private int id;
    private String track_name;
    private String date_from;
    private String date_to;
    private String avatar_url;

    private final String ID_KEY = "id";
    private final String TRACK_NAME_KEY = "track_name";
    private final String DATE_FROM_KEY = "date_from";
    private final String DATE_TO_KEY = "date_to";
    private final String AVATAR_URL_KEY = "avatar_url";

    public Trip(){


    }

    public Trip(String json){

        try {

            JSONObject jsonObject = new JSONObject(json);
            this.id = jsonObject.isNull(ID_KEY) ? 0 : jsonObject.getInt(ID_KEY);
            this.track_name = jsonObject.isNull(TRACK_NAME_KEY) ? "" : jsonObject.getString(TRACK_NAME_KEY);
            this.date_from = jsonObject.isNull(DATE_FROM_KEY) ? "" : jsonObject.getString(DATE_FROM_KEY);
            this.date_to = jsonObject.isNull(DATE_TO_KEY) ? "" : jsonObject.getString(DATE_TO_KEY);
            this.avatar_url = jsonObject.isNull(AVATAR_URL_KEY) ? "" : jsonObject.getString(AVATAR_URL_KEY);

        } catch (JSONException e) {

            e.printStackTrace();

        }

    }

    public int getId() {
        return id;
    }

    public String getTrack_name() {

        return track_name;

    }

    public String getDate_from() {

        return date_from;

    }

    public String getDate_to() {

        return date_to;

    }

    public String getAvatar_url() {
        return avatar_url;
    }
}
