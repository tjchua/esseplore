package com.essplore.essplore.ui.activities.paymentform;

import android.content.Context;
import android.os.AsyncTask;

import com.essplore.essplore.BuildConfig;
import com.essplore.essplore.R;
import com.essplore.essplore.application.AppConstants;
import com.essplore.essplore.managers.SharedPreferenceKeys;

import org.apache.commons.lang3.StringUtils;

import java.io.IOException;

import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public class PaymentFormApi {

    private OkHttpClient client;
    private Context context;
    private  final MediaType JSON;
    private String accessToken;
    private String paymentJSON;

    public PaymentFormApi(Context context){
        this.context = context;
        accessToken = context.getSharedPreferences(SharedPreferenceKeys.ESSPLORE_PREFERENCE, Context.
                MODE_PRIVATE).getString(SharedPreferenceKeys.ACCESS_TOKEN, StringUtils.EMPTY);
        JSON = MediaType.parse(AppConstants.JSON_MEDIA_TYPE);
    }

    public void getTokensRequest() {
        new getTokenTask().execute();
    }

    public void sendPaymentToken(String paymentJSON){
        this.paymentJSON = paymentJSON;
        new sendPaymentToken().execute();
    }

    private class getTokenTask extends AsyncTask<String, String, String>{

        @Override
        protected String doInBackground(String... strings) {
            client = new OkHttpClient();

            String response = null;
            try {
                response = getAccessToken(BuildConfig.BASE_URL + AppConstants.GET_STRIPE_ACCESS);
            } catch (IOException e) {
                e.printStackTrace();
            }
            System.out.println(response);
            return "";
        }
    }

    private class sendPaymentToken extends AsyncTask<String, String, String>{

        @Override
        protected String doInBackground(String... strings) {
            client = new OkHttpClient();
            String response = null;
            try {
               response = sendPaymentTokenRequest(BuildConfig.BASE_URL, paymentJSON);
            } catch (IOException e) {
                e.printStackTrace();
            }
            return "";
        }
    }

    private String getAccessToken(String url) throws IOException {
        Request request = new Request.Builder()
                .url(url)
                .header(AppConstants.AUTHORIZATION, AppConstants.BEARER + StringUtils.SPACE + accessToken)
                .build();

        try (Response response = client.newCall(request).execute()) {
            return response.body().string();
        }
    }

    private String sendPaymentTokenRequest(String url, String json) throws IOException {
        RequestBody body = RequestBody.create(JSON, json);
        Request request = new Request.Builder()
                .url(url)
                .header(AppConstants.AUTHORIZATION, AppConstants.BEARER + StringUtils.SPACE + accessToken)
                .post(body)
                .build();
        Response response = client.newCall(request).execute();
        return response.body().string();
    }
}
