package com.essplore.essplore.ui.activities.paymentform;

import android.os.Bundle;
import android.os.PersistableBundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.widget.EditText;
import android.widget.Toast;

import com.essplore.essplore.R;
import com.essplore.essplore.managers.SharedPreferenceManager;
import com.essplore.essplore.ui.activities.BaseActivity;
import com.essplore.essplore.ui.activities.ToolBarBaseActivity;

import javax.inject.Inject;

public class PaymentFormActivity extends AppCompatActivity{

    @Inject SharedPreferenceManager sharedPreferenceManager;

    private PaymentFormController paymentFormController;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_payment_form);
    }

    @Override
    protected void onResume() {
        super.onResume();
        initController(this);
    }

    private void initController(AppCompatActivity activity){
        paymentFormController = new PaymentFormController(this);
    }
}
