package com.essplore.essplore.ui.activities.places.presenter;

import com.essplore.essplore.api.requests.RemovePlaceFromTrackRequest;

import io.reactivex.disposables.Disposable;

public interface PlacesPresenter {

  Disposable getSpecificTrack(int trackId);
  Disposable deletePlaceFromTrack();
  Disposable getSpecificTrackForTimeline(int trackId);


}
