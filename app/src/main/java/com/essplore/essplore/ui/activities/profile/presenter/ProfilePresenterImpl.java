package com.essplore.essplore.ui.activities.profile.presenter;

import android.content.Context;
import android.net.Uri;
import android.os.Build;
import com.essplore.essplore.api.managers.ApiManager;
import com.essplore.essplore.api.requests.UserEditRequest;
import com.essplore.essplore.api.response.AddToPlaceResponse;
import com.essplore.essplore.api.response.CountryResponse;
import com.essplore.essplore.api.response.UploadProfileImageResponse;
import com.essplore.essplore.api.response.UserEditResponse;
import com.essplore.essplore.api.utils.SimpleObserver;
import com.essplore.essplore.application.AppConstants;
import com.essplore.essplore.models.Country;
import com.essplore.essplore.ui.activities.HttpBasePresenter;
import com.essplore.essplore.ui.activities.profile.view.ProfileView;
import io.reactivex.disposables.Disposable;
import java.util.List;
import okhttp3.MultipartBody;
import org.apache.commons.lang3.StringUtils;
import org.reactivestreams.Subscriber;

public class ProfilePresenterImpl extends HttpBasePresenter implements ProfilePresenter {

  private final ProfileView view;
  private final ApiManager manager;

  public ProfilePresenterImpl(ProfileView view, ApiManager manager) {
    this.view = view;
    this.manager = manager;
  }

  @Override public Disposable getCountries() {
    view.showProgress();
    return manager.getCountries().subscribe(new SimpleObserver<CountryResponse>() {
      @Override public void accept(CountryResponse countryResponse) {
        view.hideProgress();
        view.setupSpinner(countryResponse.getCountryList());
      }
    }, throwable -> {
      view.hideProgress();
      handleHttpError(view, throwable);
    });
  }

  @Override public Disposable updateUserInfo() {
    view.showProgress();
    return manager.userEditRequest(buildUserEditRequest(), view.provideToken()).subscribe(new SimpleObserver<UserEditResponse>() {
      @Override public void accept(UserEditResponse response) {
        view.hideProgress();
      }
    }, throwable -> {
      view.hideProgress();
      handleHttpError(view, throwable);
    });
  }

  @Override public Disposable uploadProfilePicture(Uri fileUri) {
    view.showProgress();
    return manager.uploadProfilePicture(prepareFilePart(fileUri), view.provideToken())
        .subscribe(new SimpleObserver<UploadProfileImageResponse>() {
          @Override public void accept(UploadProfileImageResponse uploadProfileImageResponse) {
            view.hideProgress();
            view.saveProfilePic(uploadProfileImageResponse.getUser().getProfileImage());
          }
        }, throwable -> {
          view.hideProgress();
          handleHttpError(view, throwable);
        });
  }

  @Override public UserEditRequest buildUserEditRequest() {
    return new UserEditRequest(view.provideEmail(), view.provideCountryId(),
            view.provideBday(), view.provideMobileNum(), 1);
  }

  @Override public int findPreSelectedCountry(List<Country> countries, int countryId) {
    for (int x = 0; x < countries.size(); x++) {
      if (countries.get(x).getId() == countryId) {
        return x;
      }
    }
    return AppConstants.DEFAULT_INT_VALUE;
  }

  @Override public boolean isValidCredentials() {
    if (StringUtils.isEmpty(view.provideEmail())) {
      view.showEmptyFieldError();
      return false;
    }

    return true;
  }

  @Override public boolean isPermissionGranted() {
    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
      return view.isPermissionGranted();
    }
    return false;
  }

  @Override public boolean hasPermissions(Context context, String... permissions) {
    if (context != null && permissions != null) {
      for (String permission : permissions) {
        return view.checkSelfPermission(context, permission);
      }
    }
    return false;
  }

  @Override public MultipartBody.Part prepareFilePart(Uri fileUri) {
    return MultipartBody.Part.createFormData(AppConstants.PICTURE_PARAM, view.getImageFile(fileUri).getName(),
        view.getRequestFile(view.getImageFile(fileUri), fileUri));
  }
}
