package com.essplore.essplore.ui.activities.search;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.essplore.essplore.R;
import com.essplore.essplore.application.AppConstants;
import com.essplore.essplore.ui.activities.place.PlaceActivity;
import com.essplore.essplore.ui.activities.search.adapters.SearchAdapter;
import com.essplore.essplore.ui.activities.search.api.SearchApi;
import com.essplore.essplore.ui.activities.search.interfaces.SearchAdapterInterface;
import com.essplore.essplore.ui.activities.search.interfaces.SearchApiInterface;
import com.essplore.essplore.ui.activities.search.models.City;
import com.essplore.essplore.ui.activities.search.models.Place;
import com.essplore.essplore.ui.activities.search.models.Places;
import com.essplore.essplore.ui.activities.searchFilter.SearchFilterActivity;
import com.essplore.essplore.ui.activities.share.adapters.ShareAdapter;

import java.util.ArrayList;

public class SearchActivity extends AppCompatActivity implements SearchApiInterface, SearchAdapterInterface, SearchView.OnQueryTextListener, View.OnClickListener{

    private SearchView searchView;
    private final String SEARCH_DEFAULT = "Singapore";
    private SearchApi searchApi;
    private City city;
    private Places places;
    private RecyclerView recyclerView;
    private SearchAdapter searchAdapter;
    public static final String PLACE_INTENT_KEY = "place";
    private TextView searchNotFoundTv;
    private ProgressBar search_pb;
    private ImageView barIv;
    private ImageView topPicksIv;
    private ImageView rareIv;
    private ImageView foodIv;
    private ImageView shoppingIv;
    private ImageView historyIv;
    private Filter filter;
    private Places origPlaces;
    private com.essplore.essplore.models.City parceableCity;
    private View nearbyContainer;
    private Button nearbyBtn;
    private Button filterBtn;

    private enum Filter{

        TOPPICKS, RARE, FOOD, SHOPPING, BAR, HISTORY, NONE

    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search);
        initViews();
        getSearchApi().getPlacesFromApi(0, 0, "");
        searchCity(SEARCH_DEFAULT);

    }

    @Override
    public void onGetCitySuccess(String response) {

        parceableCity = new com.essplore.essplore.models.City(response);
//        getSearchApi().getPlacesFromApi(city.getId(), city.getCountry_id(), searchView.getQuery().toString());

    }

    @Override
    public void onGetPlacesSuccess(String response) {
        getPlaces().clear();
        getPlaces().addPlaces(response);
        getSearchAdapter().notifyDataSetChanged();
        getSearch_pb().setVisibility(View.INVISIBLE);
        if (getPlaces().size() == 0)
            searchNotFoundTv.setVisibility(View.VISIBLE);
        updateOrigPlaces();

    }

    @Override
    public void onPlaceClick(Place place) {

        showPlace(place);

    }

    @Override
    public boolean onQueryTextSubmit(String query) {
        searchNotFoundTv.setVisibility(View.INVISIBLE);
        getSearch_pb().setVisibility(View.VISIBLE);
        getSearchApi().getPlacesFromApi(0, 0, query);
        return false;
    }

    @Override
    public boolean onQueryTextChange(String newText) {
        searchNotFoundTv.setVisibility(View.INVISIBLE);
        getSearch_pb().setVisibility(View.VISIBLE);
        getSearchApi().getPlacesFromApi(0, 0, newText);
        return false;
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()){

            case R.id.bar:

                if (filter == Filter.BAR){

                    removeFilter();
                    filter = null;

                }else{

                    filter = Filter.BAR;
                    filterSearch("Bar");

                }

                break;

            case R.id.top_picks:

                if (filter == Filter.TOPPICKS){

                    removeFilter();
                    filter = Filter.NONE;

                }else{

                    filter = Filter.TOPPICKS;
                    filterSearch("Top Picks");

                }

                break;

            case R.id.rare:

                if (filter == Filter.RARE){

                    removeFilter();
                    filter = Filter.NONE;

                }else{

                    filter = Filter.RARE;
                    filterSearch("Rare");

                }

                break;

            case R.id.food:

                if (filter == Filter.FOOD){

                    removeFilter();
                    filter = Filter.NONE;

                }else{

                    filter = Filter.FOOD;
                    filterSearch("Food");

                }

                break;

            case R.id.shopping:

                if (filter == Filter.SHOPPING){

                    removeFilter();
                    filter = Filter.NONE;

                }else{

                    filter = Filter.SHOPPING;
                    filterSearch("Shopping");

                }

                break;

            case R.id.history:

                if (filter == Filter.HISTORY){

                    removeFilter();
                    filter = Filter.NONE;

                }else{

                    filter = Filter.HISTORY;
                    filterSearch("History");

                }

                break;

            case R.id.nearbyButton:

                if (getNearbyBtn().isSelected()){

                    int bgColor = ContextCompat.getColor(getApplicationContext(), android.R.color.holo_blue_dark);
                    getNearbyContainer().setBackgroundColor(bgColor);
                    getNearbyBtn().setSelected(false);

                }else{

                    int bgColor = ContextCompat.getColor(getApplicationContext(), R.color.mdtp_light_gray);
                    getNearbyContainer().setBackgroundColor(bgColor);
                    getNearbyBtn().setSelected(true);

                }

                break;

            case R.id.filter_btn:

                showSearchFilterPage();

                break;

                default:

        }

    }

    private void filterSearch(String filterText){

        highLightFilterButtons(filter);
        getPlaces().clear();
        getPlaces().addAll(getOrigPlaces());
        getPlaces().filter(filterText);
        getSearchAdapter().notifyDataSetChanged();

    }

    private void removeFilter(){

        getPlaces().clear();
        getPlaces().addAll(getOrigPlaces());
        getSearchAdapter().notifyDataSetChanged();

    }

    private void showPlace(Place place){

        Intent intentPlace = new Intent(this, PlaceActivity.class);
        intentPlace.putExtra(PLACE_INTENT_KEY, place);
        intentPlace.putExtra(AppConstants.CITY_OBJECT, parceableCity);
        startActivity(intentPlace);

    }

    private void initViews(){

        searchView = findViewById(R.id.searchView);
//        searchView.setQuery(SEARCH_DEFAULT, false);
        recyclerView = findViewById(R.id.recycler_view);
        searchNotFoundTv = findViewById(R.id.search_not_found_tv);
        searchView.onActionViewExpanded();
        searchView.setOnQueryTextListener(this);
        getBarIv();
        getTopPicksIv();
        getRareIv();
        getFoodIv();
        getShoppingIv();
        getHistoryIv();
        getNearbyBtn();
        getFilterBtn();
        prepareRecyclerView();

    }

    private void searchCity(String searchText){

        getSearchApi().getCityFromApi(searchText);

    }

    private SearchApi getSearchApi(){

        if (searchApi == null){

            searchApi = new SearchApi(this);
            searchApi.setShareApiInterface(this);

        }

        return searchApi;

    }

    private City getCity(){

        if (city == null){

            city = new City();

        }

        return city;

    }

    private Places getPlaces(){

        if (places == null){

            places = new Places();

        }

        return places;

    }

    private SearchAdapter getSearchAdapter(){

        if (searchAdapter == null){

            searchAdapter = new SearchAdapter(this, getPlaces());
            searchAdapter.setSearchAdapterInterface(this);

        }

        return searchAdapter;

    }

    private void prepareRecyclerView(){

        recyclerView.setHasFixedSize(true);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(this,
                LinearLayoutManager.VERTICAL, false);
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setAdapter(getSearchAdapter());

    }

    private ProgressBar getSearch_pb(){

        if (search_pb == null){

            search_pb = findViewById(R.id.search_pb);

        }

        return search_pb;

    }

    public ImageView getBarIv() {

        if (barIv == null){

            barIv = findViewById(R.id.bar);
            barIv.setOnClickListener(this);

        }

        return barIv;
    }

    public Places getOrigPlaces() {

        if (origPlaces == null){

            origPlaces = new Places();

        }

        return origPlaces;

    }

    private void updateOrigPlaces(){

        getOrigPlaces().addAll(getPlaces());

    }

    public ImageView getTopPicksIv() {

        if (topPicksIv == null){

            topPicksIv = findViewById(R.id.top_picks);
            topPicksIv.setOnClickListener(this);

        }

        return topPicksIv;

    }

    public ImageView getRareIv() {

        if (rareIv == null){

            rareIv = findViewById(R.id.rare);
            rareIv.setOnClickListener(this);

        }

        return rareIv;

    }

    public ImageView getFoodIv() {

        if (foodIv == null){

            foodIv = findViewById(R.id.food);
            foodIv.setOnClickListener(this);

        }

        return foodIv;

    }

    public ImageView getShoppingIv() {

        if (shoppingIv == null){

            shoppingIv = findViewById(R.id.shopping);
            shoppingIv.setOnClickListener(this);

        }

        return shoppingIv;

    }

    public ImageView getHistoryIv() {

        if (historyIv == null){

            historyIv = findViewById(R.id.history);
            historyIv.setOnClickListener(this);

        }

        return historyIv;

    }

    private void highLightFilterButtons(Filter filter){

        int bgColor = ContextCompat.getColor(this, R.color.colorPrimary);

        switch (filter){

            case BAR:

                unHighlightFilterButtons();
                getBarIv().setBackgroundColor(bgColor);

                break;

            case FOOD:

                unHighlightFilterButtons();
                getFoodIv().setBackgroundColor(bgColor);

                break;

            case TOPPICKS:

                unHighlightFilterButtons();
                getTopPicksIv().setBackgroundColor(bgColor);

                break;

            case SHOPPING:

                unHighlightFilterButtons();
                getShoppingIv().setBackgroundColor(bgColor);

                break;

            case RARE:

                unHighlightFilterButtons();
                getRareIv().setBackgroundColor(bgColor);

                break;

            case HISTORY:

                unHighlightFilterButtons();
                getHistoryIv().setBackgroundColor(bgColor);

                break;

        }

    }

    private void unHighlightFilterButtons(){

        getHistoryIv().setBackgroundColor(Color.TRANSPARENT);
        getRareIv().setBackgroundColor(Color.TRANSPARENT);
        getShoppingIv().setBackgroundColor(Color.TRANSPARENT);
        getTopPicksIv().setBackgroundColor(Color.TRANSPARENT);
        getFoodIv().setBackgroundColor(Color.TRANSPARENT);
        getBarIv().setBackgroundColor(Color.TRANSPARENT);

    }

    private View getNearbyContainer(){

        if (nearbyContainer == null){

            nearbyContainer = findViewById(R.id.nearbyContainer);

        }

        return nearbyContainer;

    }

    public Button getNearbyBtn() {

        if (nearbyBtn == null){

            nearbyBtn = findViewById(R.id.nearbyButton);
            nearbyBtn.setOnClickListener(this);

        }

        return nearbyBtn;
    }

    public Button getFilterBtn() {

        if (filterBtn == null){

            filterBtn = findViewById(R.id.filter_btn);
            filterBtn.setOnClickListener(this);

        }

        return filterBtn;

    }

    private void showSearchFilterPage(){

        Intent intent = new Intent(this, SearchFilterActivity.class);
        startActivity(intent);

    }

}
