package com.essplore.essplore.ui.activities.profile.presenter;

import android.content.Context;
import android.net.Uri;
import com.essplore.essplore.api.requests.UserEditRequest;
import com.essplore.essplore.models.Country;
import io.reactivex.disposables.Disposable;
import java.util.List;
import okhttp3.MultipartBody;

public interface ProfilePresenter {

  Disposable getCountries();

  Disposable updateUserInfo();

  Disposable uploadProfilePicture(Uri fileUri);

  UserEditRequest buildUserEditRequest();

  int findPreSelectedCountry(List<Country> countries, int countryId);

  boolean isValidCredentials();

  boolean isPermissionGranted();

  boolean hasPermissions(Context context, String... permissions);

  MultipartBody.Part prepareFilePart(Uri fileUri);
}
