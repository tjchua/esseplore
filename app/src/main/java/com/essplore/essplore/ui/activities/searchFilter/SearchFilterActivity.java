package com.essplore.essplore.ui.activities.searchFilter;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CalendarView;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;

import com.essplore.essplore.R;
import com.essplore.essplore.ui.activities.addtotrip.dialogs.CalendarDialog;
import com.essplore.essplore.ui.activities.addtotrip.interfaces.CalendarDialogInterface;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class SearchFilterActivity extends AppCompatActivity implements View.OnClickListener, CalendarDialogInterface{

    private TextView x_btn;
    private Button trip_date_btn;
    private ImageView trip_date_iv;
    private TextView trip_date_tv;
    private TextView trip_date_selected_tv;
    private ImageView minus_iv;
    private ImageView add_iv;
    private TextView travelBuddyTv;
    private Spinner numberOfVisitSp;
    private Spinner typeOfSpotSp;
    private Button filterBtn;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search_filter);
        initViews();

    }

    @Override
    public void onClick(View v) {

        switch (v.getId()){

            case R.id.x_btn:

                finish();

                break;

            case R.id.trip_date_btn:

                showCalendar();

                break;

            case R.id.add_iv:

                incrementTravelBuddy();

                break;

            case R.id.minus_iv:

                decrementTravelBuddy();

                break;

            case R.id.filter_btn:

                finish();

                break;
        }

    }

    @Override
    public void onSelectedDayChange(CalendarView view, int year, int month, int dayOfMonth) {

        int monthNew = month + 1;
        getTrip_date_selected_tv().setText(year + " - " + monthNew + " - " + dayOfMonth);
        getTrip_date_iv().setVisibility(View.INVISIBLE);
        getTrip_date_tv().setVisibility(View.INVISIBLE);

    }

    private void initViews(){

        getX_btn();
        getTrip_date_btn();
        getAdd_iv();
        getMinus_iv();
        setSpinnerNumberOfVisit();
        setSpinnerTypeOfSpot();
        getFilterBtn();

    }

    private TextView getX_btn() {

        if (x_btn == null){

            x_btn = findViewById(R.id.x_btn);
            x_btn.setOnClickListener(this);

        }

        return x_btn;
    }

    private Button getTrip_date_btn() {

        if (trip_date_btn == null){

            trip_date_btn = findViewById(R.id.trip_date_btn);
            trip_date_btn.setOnClickListener(this);

        }

        return trip_date_btn;

    }

    private void showCalendar(){

        CalendarDialog calendarDialog = new CalendarDialog(this);
        calendarDialog.setCalendarDialogInterface(this);
        calendarDialog.show();

    }

    private ImageView getTrip_date_iv() {

        if (trip_date_iv == null){

            trip_date_iv = findViewById(R.id.trip_date_iv);

        }

        return trip_date_iv;

    }

    private TextView getTrip_date_tv() {

        if (trip_date_tv == null){

            trip_date_tv = findViewById(R.id.trip_date_tv);

        }

        return trip_date_tv;

    }

    private TextView getTrip_date_selected_tv() {

        if (trip_date_selected_tv == null){

            trip_date_selected_tv = findViewById(R.id.trip_date_selected_tv);

        }

        return trip_date_selected_tv;

    }

    private ImageView getAdd_iv() {

        if (add_iv == null){

            add_iv = findViewById(R.id.add_iv);
            add_iv.setOnClickListener(this);

        }

        return add_iv;

    }

    private ImageView getMinus_iv() {

        if (minus_iv == null){

            minus_iv = findViewById(R.id.minus_iv);
            minus_iv.setOnClickListener(this);

        }

        return minus_iv;

    }

    private TextView getTravelBuddyTv(){

        if (travelBuddyTv == null){

            travelBuddyTv = findViewById(R.id.travel_buddy_tv);

        }

        return travelBuddyTv;

    }

    private void incrementTravelBuddy(){

        int travelBuddy = Integer.parseInt(getTravelBuddyTv().getText().toString());
        travelBuddy++;
        getTravelBuddyTv().setText(Integer.toString(travelBuddy));

    }

    private void decrementTravelBuddy(){

        int travelBuddy = Integer.parseInt(getTravelBuddyTv().getText().toString());
        if (travelBuddy > 0)
            travelBuddy--;
        getTravelBuddyTv().setText(Integer.toString(travelBuddy));

    }

    private void setSpinnerTypeOfSpot() {

        // Initializing a String Array
        String[] typeOfSpots = new String[]{
                "sights",
                "art",
                "fashion",
                "party"
        };

        final List<String> plantsList = new ArrayList<>(Arrays.asList(typeOfSpots));

        final ArrayAdapter<String> spinnerArrayAdapter = new ArrayAdapter<>(
                this,R.layout.support_simple_spinner_dropdown_item,plantsList);

        spinnerArrayAdapter.setDropDownViewResource(R.layout.support_simple_spinner_dropdown_item);
        getTypeOfSpotSp().setAdapter(spinnerArrayAdapter);

    }

    private void setSpinnerNumberOfVisit() {

        // Initializing a String Array
        String[] numberOfVisit = new String[]{
                "Never",
                "1 time",
                "2 time",
                "3 time",
                "I know the place well"
        };

        final List<String> plantsList = new ArrayList<>(Arrays.asList(numberOfVisit));

        // Initializing an ArrayAdapter
        final ArrayAdapter<String> spinnerArrayAdapter = new ArrayAdapter<>(
                this,R.layout.support_simple_spinner_dropdown_item,plantsList);

        spinnerArrayAdapter.setDropDownViewResource(R.layout.support_simple_spinner_dropdown_item);
        getNumberOfVisitSp().setAdapter(spinnerArrayAdapter);

    }

    private Spinner getNumberOfVisitSp(){

        if (numberOfVisitSp == null){

            numberOfVisitSp = findViewById(R.id.number_of_visit_sp);

        }

        return numberOfVisitSp;

    }

    private Spinner getTypeOfSpotSp(){

        if (typeOfSpotSp == null){

            typeOfSpotSp = findViewById(R.id.type_of_spots_sp);

        }

        return typeOfSpotSp;

    }

    private Button getFilterBtn(){

        if (filterBtn == null){

            filterBtn = findViewById(R.id.filter_btn);
            filterBtn.setOnClickListener(this);

        }

        return filterBtn;

    }

}
