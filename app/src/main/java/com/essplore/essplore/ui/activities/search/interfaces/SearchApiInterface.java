package com.essplore.essplore.ui.activities.search.interfaces;

public interface SearchApiInterface {

    public void onGetCitySuccess(String response);
    public void onGetPlacesSuccess(String response);

}
