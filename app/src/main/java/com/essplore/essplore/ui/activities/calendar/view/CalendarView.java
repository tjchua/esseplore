package com.essplore.essplore.ui.activities.calendar.view;

import com.essplore.essplore.api.requests.CreateTrackRequest;
import com.essplore.essplore.api.response.CreateTrackResponse;
import com.essplore.essplore.models.MyTrack;
import com.essplore.essplore.models.MyTrip;
import com.essplore.essplore.ui.activities.HttpBaseView;

public interface CalendarView extends HttpBaseView {

  void showProgressbar();

  void hideProgressbar();

  void launchPlacesActivity(MyTrip myTrip);

  String provideToken();

  String provideStartDate();

  String provideEndDate();

  CreateTrackRequest getCreateTrackRequest();
}
