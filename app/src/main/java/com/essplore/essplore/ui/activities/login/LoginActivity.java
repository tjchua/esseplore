package com.essplore.essplore.ui.activities.login;

import android.app.Activity;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AlertDialog;
import android.widget.ImageButton;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.OnClick;

import com.essplore.essplore.R;
import com.essplore.essplore.application.EssploreApplication;
import com.essplore.essplore.managers.AppActivityManager;
import com.essplore.essplore.managers.SharedPreferenceManager;
import com.essplore.essplore.ui.activities.ToolBarBaseActivity;
import com.essplore.essplore.ui.activities.login.adapter.LoginFragmentPagerAdapter;
import com.essplore.essplore.ui.activities.login.fragments.view.ForgotPasswordFragment;
import javax.inject.Inject;

public class LoginActivity extends ToolBarBaseActivity implements LoginActivityView {

  @Inject LoginFragmentPagerAdapter pagerAdapter;
  @Inject SharedPreferenceManager sharedPreferenceManager;
  @Inject AppActivityManager manager;
  @Inject AlertDialog alertDialog;
  @Inject ForgotPasswordFragment fragment;

  @BindView(R.id.vpLogin) ViewPager vpLogin;
  @BindView(R.id.tlLogin) TabLayout tlLogin;
  @BindView(R.id.page_title) TextView pageTitle;

  @Override protected void setupActivityLayout() {

    setContentView(R.layout.activity_login);

  }

  @Override protected void setupViewElements() {
    if (!sharedPreferenceManager.isUserLoggedIn()) {
      vpLogin.setAdapter(pagerAdapter);
      tlLogin.setupWithViewPager(vpLogin);
    } else {
      manager.launchHomeActivity(this);
    }

    pageTitle.setText(R.string.my_account);
  }

  @Override protected void injectDaggerComponent() {
    EssploreApplication.get(this).createLoginComponent(this).inject(this);
  }

  @Override protected boolean isActionbarBackButtonEnabled() {
    return Boolean.TRUE;
  }

  @Override protected void onDestroy() {
    super.onDestroy();
    EssploreApplication.get(this).releaseLoginComponent();
  }

  @Override public void onBackPressed() {
    if (!fragment.isVisible()) {
      finish();
    } else {
      getSupportFragmentManager().beginTransaction().remove(fragment).commit();
    }
  }

  @Override public void openForgotPassword() {
    manager.launchForgotPassword(this, R.id.rlContainer, fragment);
  }

  @Override protected Activity getActivity() {
    return this;
  }

  @Override protected AlertDialog getAlertDialog() {
    return alertDialog;
  }

  @OnClick(R.id.back_button) void onBackButtonClicked(){
    finish();
  }
}
