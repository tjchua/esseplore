package com.essplore.essplore.ui.activities.cityhighlights.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;

import butterknife.BindView;
import butterknife.ButterKnife;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.drawable.GlideDrawable;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.essplore.essplore.R;
import com.essplore.essplore.models.PlacePicture;
import com.essplore.essplore.ui.activities.cityhighlights.presenter.CityHighlightsPresenterImpl;
import com.essplore.essplore.ui.utils.OnBindViewListener;
import java.util.List;


public class CityHighlightsPlacePictureAdapter
    extends RecyclerView.Adapter<CityHighlightsPlacePictureAdapter.ViewHolder> {

  private Context context;
  private List<PlacePicture> placePictureList;
  private CityHighlightsPresenterImpl presenter;

  public CityHighlightsPlacePictureAdapter(Context context, List<PlacePicture> placePictureList,
      CityHighlightsPresenterImpl presenter) {
    this.context = context;
    this.placePictureList = placePictureList;
    this.presenter = presenter;
  }

  @NonNull @Override public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
    return new ViewHolder(
        LayoutInflater.from(context).inflate(R.layout.list_item_img, parent, false));
  }

  @Override public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
    holder.onBind(placePictureList.get(position));
  }

  @Override public int getItemCount() {
    return placePictureList.size();
  }

  public class ViewHolder extends RecyclerView.ViewHolder
      implements OnBindViewListener<PlacePicture> {

    @BindView(R.id.imgDisplay) ImageView imgDisplay;
    @BindView(R.id.pbImageDisplay) ProgressBar pbImageDisplay;

    public ViewHolder(View itemView) {
      super(itemView);
      ButterKnife.bind(this, itemView);
    }

    @Override public void onBind(PlacePicture object) {
      if (object.getPlacePictureUrl() != null) {
        pbImageDisplay.setVisibility(View.VISIBLE);
        Glide.with(context)
            .load(object.getPlacePictureUrl())
            .centerCrop()
            .crossFade()
                .listener(new RequestListener<String, GlideDrawable>() {
                  @Override
                  public boolean onException(Exception e, String model, Target<GlideDrawable> target, boolean isFirstResource) {
                    pbImageDisplay.setVisibility(View.GONE);
                    return false;
                  }

                  @Override
                  public boolean onResourceReady(GlideDrawable resource, String model, Target<GlideDrawable> target, boolean isFromMemoryCache, boolean isFirstResource) {
                    pbImageDisplay.setVisibility(View.GONE);
                    return false;
                  }
                })
            .into(imgDisplay);
      }
    }
  }
}
