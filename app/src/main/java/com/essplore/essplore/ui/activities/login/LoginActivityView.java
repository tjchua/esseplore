package com.essplore.essplore.ui.activities.login;

public interface LoginActivityView {
  void openForgotPassword();
}
