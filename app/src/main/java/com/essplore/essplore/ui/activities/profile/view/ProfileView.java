package com.essplore.essplore.ui.activities.profile.view;

import android.content.Context;
import android.net.Uri;
import com.essplore.essplore.models.Country;
import com.essplore.essplore.ui.activities.HttpBaseView;
import java.io.File;
import java.util.List;
import okhttp3.RequestBody;

public interface ProfileView extends HttpBaseView {

  String provideEmail();

  String provideToken();

  String provideMobileNum();

  String provideBday();

  int provideCountryId();

  void setProfileValues();

  void showProgress();

  void hideProgress();

  void setupSpinner(List<Country> countryList);

  void showEmptyFieldError();

  boolean isPermissionGranted();

  boolean checkSelfPermission(Context context, String permission);

  File getImageFile(Uri fileUri);

  RequestBody getRequestFile(File file, Uri fileUri);

  void saveProfilePic(String url);
}
