package com.essplore.essplore.ui.activities.login.fragments.view;

import com.essplore.essplore.ui.activities.HttpBaseView;

public interface ForgotPasswordView extends HttpBaseView {

  void showProgressbar();

  void hideProgressbar();

  void showSuccessApiCall();

  void navigateToLogin();

  boolean isValid();

  String provideEmail();
}
