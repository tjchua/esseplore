package com.essplore.essplore.ui.activities.addtotrip.interfaces;

public interface AddTripFromSearchApiInterface {

    public void onGetTripsSuccess(String response);
    public void onAddPlaceToTrackSuccess();
    public void onAddPlaceToTrackFailed();

}
