package com.essplore.essplore.ui.utils.geolocation;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.location.Location;
import android.support.v4.app.ActivityCompat;

import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;

public class GeoLocationUtil implements com.google.android.gms.location.LocationListener{

//    private LocationManager locationManager;
    private FusedLocationProviderClient mFusedLocationClient;
    private Context context;
    private Location location;
    private LocationListener locationListener;

    public GeoLocationUtil(Context context) {

        this.context = context;
        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(context);
        startLocationUpdates();

    }

    private LocationRequest getLocationRequest(){
        LocationRequest mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(3000);
        mLocationRequest.setFastestInterval(3000);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        return mLocationRequest;
    }

    public void  startLocationUpdates() {

        if (ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.
                PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
//        locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 1000, 0, this);
        mFusedLocationClient.requestLocationUpdates(getLocationRequest(),mLocationCallback,null /* Looper */);

    }

    public Location getrCurrenetLocation() {

        return location;

    }

    public void setLocationListener(LocationListener locationListener){

        this.locationListener = locationListener;

    }

    private LocationCallback mLocationCallback = new LocationCallback() {

        @Override
        public void onLocationResult(LocationResult locationResult) {

            if (location == null) {

                location = locationResult.getLastLocation();
                return;

            }

            if (locationListener != null)
            locationListener.onLocationUpdate(location);
            mFusedLocationClient.removeLocationUpdates(mLocationCallback);

        };

    };

    @Override
    public void onLocationChanged(Location location) {

        if (locationListener != null)
        locationListener.onLocationUpdate(location);
        this.location = location;

    }

    public void removeLocationUpdate(){

        mFusedLocationClient.removeLocationUpdates(mLocationCallback);

    }
}
