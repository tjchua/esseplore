package com.essplore.essplore.ui.activities.album.models;

import android.graphics.Bitmap;
import org.json.JSONException;
import org.json.JSONObject;

public class Album {

    private final String PICTURE_URL = "picture_url";
    private Bitmap image;
    private String imageLink;

    public Album(JSONObject jsonObject){

        try {

            imageLink = jsonObject.getString(PICTURE_URL);

        } catch (JSONException e) {

            e.printStackTrace();

        }

    }

    public Bitmap getImage() {

        return image;

    }

    public String getImageLink() {

        return imageLink;

    }

    public void setImage(Bitmap image) {

        this.image = image;

    }

}
