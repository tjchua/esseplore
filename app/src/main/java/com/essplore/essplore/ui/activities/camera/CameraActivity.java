package com.essplore.essplore.ui.activities.camera;

import android.app.Activity;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TabHost;
import android.widget.TextView;
import android.widget.Toast;
import com.essplore.essplore.R;
import com.essplore.essplore.application.AppConstants;
import com.essplore.essplore.models.MyTrack;
import com.essplore.essplore.ui.activities.camera.interfaces.CameraActivityListener;
import com.essplore.essplore.ui.activities.camera.models.UploadResponse;
import com.essplore.essplore.ui.activities.share.ShareActivity;
import com.essplore.essplore.ui.activities.share.ShareApi;
import com.essplore.essplore.ui.activities.share.ShareAPIinterface;
import com.essplore.essplore.ui.activities.timeline.view.TimelineActivity;
import com.essplore.essplore.ui.utils.geolocation.GeoLocationUtil;
import com.essplore.essplore.ui.utils.saveImageToLocalFiles.SaveImageToLocalFiles;
import com.essplore.essplore.ui.utils.saveImageToLocalFiles.SaveImageToLocalFilesInterface;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.util.ArrayList;

public class CameraActivity extends AppCompatActivity implements View.OnClickListener, TabHost.OnTabChangeListener,
        CameraActivityListener, ShareAPIinterface, SaveImageToLocalFilesInterface {

    private FragmentManager fragmentManager;
    private TabHost tabHost;
    private CameraFragment cameraFragment;
    private GalleryFragment galleryFragment;
    private ImageButton backButton;
    private TextView pageTitle;
    private Button nextBtn;
    private Bitmap selectedImage;
    private final int RESULT_CODE_UPDATE_GALLERY = 1;
    public static CameraActivity instance = null;
    private ShareApi shareApi;
    private ProgressDialog progressDialog;
    public static final String CAPTION_INTENT_KEY = "caption";
    public static final String LOCATION_INTENT_KEY = "location";
    public static final String LOCAL_IMAGES_INTENT_KEY = "localImages";
    public static final String RESULT_INTENT_KEY = "resultStatus";
    public static final String DESCRIPTION_INTENT_KEY = "descriptionIntent";
    public static final String TITLE_INTENT_KEY = "titleIntent";
    private final int REQUESTCODE = 1;
    private GeoLocationUtil geoLocationUtil;
    private ArrayList<String> localImages;
    private String description;
    private String title;
    public static enum ResultStatus {
        UPDATE,
        QUIT
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_camera);
        geoLocationUtil = new GeoLocationUtil(this);
        shareApi = new ShareApi(this);
        shareApi.setShareApiInterface(this);
        fragmentManager = getFragmentManager();
        instance = this;
        initViews();
        showTab();

    }

    @Override
    protected void onResume() {

        super.onResume();

    }

    @Override
    public void onClick(View v) {

        switch (v.getId()){

            case R.id.back_button:

                finish();

                break;

            case R.id.next_btn:

                saveImageToLocal(selectedImage);

                break;

            default:

        }

    }

    @Override
    public void onTabChanged(String tabId) {

        showCameraFragment(tabId);

    }

    @Override
    public void setSelectedImage(Bitmap selectedImage) {

        this.selectedImage = selectedImage;

    }

    @Override
    public void clearSelectedImage() {

        selectedImage = null;

    }

    @Override
    protected void onDestroy() {

        super.onDestroy();
        if (selectedImage != null)
        selectedImage.recycle();
        System.gc();

    }

    @Override
    public void onUploadSuccess(String response) {

        progressDialog.dismiss();
        UploadResponse uploadResponse = new UploadResponse(response);
        showShareActivity(uploadResponse.getCaption(), uploadResponse.getLocation());

    }

    @Override
    public void onUploadSuccess(int position) {

    }

    @Override
    public void onUploadFailed() {

        progressDialog.dismiss();
        Toast.makeText(this, getString(R.string.upload_failed), Toast.LENGTH_LONG).show();

    }

    @Override
    public void onSaveToLocalError() {

    }

    @Override
    public void onSaveToLocalSuccess(File file) {

        localImages.add(file.toString());
        cameraFragment.retakePicture();
        showShareActivity(localImages);

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUESTCODE) {

            if(resultCode == Activity.RESULT_OK){

                String result = data.getStringExtra(RESULT_INTENT_KEY);


                if (result.equalsIgnoreCase(ResultStatus.UPDATE.toString())){

                    localImages.clear();
                    localImages = data.getExtras().getStringArrayList(CameraActivity.LOCAL_IMAGES_INTENT_KEY);
                    title = data.getExtras().getString(TITLE_INTENT_KEY, "");
                    description = data.getExtras().getString(DESCRIPTION_INTENT_KEY, "");

                }else{

                    finish();

                }

            }else{

                localImages.clear();

            }

        }


    }

    private void showCameraFragment(String tabId){

        if (fragmentManager != null) {

            FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
            FragmentTransaction replace;
            if (tabId.equalsIgnoreCase(getString(R.string.photo_tab))){

                if (cameraFragment == null){

                    cameraFragment = new CameraFragment();
                    cameraFragment.setCameraActivityListener(this);

                }

                replace = fragmentTransaction.replace(R.id.fragment_container, cameraFragment);
                pageTitle.setText(getString(R.string.page_title_photo));

            } else {

                if (galleryFragment == null) {

                    galleryFragment = new GalleryFragment();
                    galleryFragment.init(this);

                }

                replace = fragmentTransaction.replace(R.id.fragment_container, galleryFragment);
                pageTitle.setText(getString(R.string.page_title_gallery));

            }

            fragmentTransaction.commit();

        }
    }

    private void initViews(){

        tabHost= (TabHost)findViewById(R.id.tabHost);
        tabHost.setOnTabChangedListener(this);
        backButton = (ImageButton) findViewById(R.id.back_button);
        backButton.setOnClickListener(this);
        pageTitle = (TextView) findViewById(R.id.page_title);
        nextBtn = (Button) findViewById(R.id.next_btn);
        nextBtn.setOnClickListener(this);

    }

    private void showTab(){

        tabHost.setup();
        TabHost.TabSpec spec = tabHost.newTabSpec(getString(R.string.photo_tab));
        spec.setContent(R.id.tab1);
        spec.setIndicator(getString(R.string.photo_tab));
        tabHost.addTab(spec);

        spec = tabHost.newTabSpec(getString(R.string.gallery_tab));
        spec.setContent(R.id.tab2);
        spec.setIndicator(getString(R.string.gallery_tab));
        tabHost.addTab(spec);
        tabHost.setCurrentTab(0);

    }

    private void saveImageToLocal(Bitmap selectedImage){

        int localImagesCount = getLocalImages().size();

        if(getLocalImages().size() <= 0) {

            if (cameraFragment.getCameraStatus() == EnumCameraStatus.TAKING) {

                Toast.makeText(this, getString(R.string.no_image_selected_message), Toast.LENGTH_LONG).show();
                return;

            }

        }

        if(selectedImage != null) {

            SaveImageToLocalFiles saveImageToLocalFiles = new SaveImageToLocalFiles(this);
            saveImageToLocalFiles.setSaveImageToLocalFilesInterface(this);
            saveImageToLocalFiles.convertBitmapToFile(selectedImage);

        }else{

            showShareActivity(localImages);

        }
//        shareApi.sendPost(selectedImage, geoLocationUtil.getrCurrenetLocation());

    }

    private void showProgress(){

        progressDialog = new ProgressDialog(this);
        progressDialog.setCancelable(false);
        progressDialog.setMessage(getString(R.string.uploading));
        progressDialog.show();

    }

    private void showShareActivity(String caption, String location){

        Intent showShareIntent = new Intent(this, ShareActivity.class);
        showShareIntent.putExtra(CAPTION_INTENT_KEY, caption);
        showShareIntent.putExtra(LOCATION_INTENT_KEY, location);
        startActivity(showShareIntent);

    }

    private void showShareActivity(ArrayList<String> localImages){

        Intent showShareIntent = new Intent(this, ShareActivity.class);
        showShareIntent.putStringArrayListExtra(LOCAL_IMAGES_INTENT_KEY, localImages);
        Bundle bundle = new Bundle();
        bundle.putParcelable(AppConstants.MY_TRACK_OBJECT, getMyTrack());
        showShareIntent.putExtra(AppConstants.MY_TRACK_OBJECT, bundle);
        showShareIntent.putExtra(TimelineActivity.CURRENT_PLACE_ID_INTENT_KEY, getCurrentPlaceId());
        showShareIntent.putExtra(DESCRIPTION_INTENT_KEY, description);
        showShareIntent.putExtra(TITLE_INTENT_KEY, title);
        startActivityForResult(showShareIntent, REQUESTCODE);

    }

    private void showShareActivity(){

        Intent showShareIntent = new Intent(this, ShareActivity.class);
        startActivity(showShareIntent);

    }

    private void addLocalImages(String file){

        if(localImages == null){

            localImages = new ArrayList<String>();

        }

        localImages.add(file);

    }

    private ArrayList<String> getLocalImages(){

        if(localImages == null){

            localImages = new ArrayList<String>();

        }

        return localImages;

    }

    private MyTrack getMyTrack() {
        Bundle bundle = getIntent().getBundleExtra(AppConstants.MY_TRACK_OBJECT);
        return bundle.getParcelable(AppConstants.MY_TRACK_OBJECT);
    }

    private String getCurrentPlaceId(){

        return getIntent().getStringExtra(TimelineActivity.CURRENT_PLACE_ID_INTENT_KEY);

    }

}
