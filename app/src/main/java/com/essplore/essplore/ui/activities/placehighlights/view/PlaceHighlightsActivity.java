package com.essplore.essplore.ui.activities.placehighlights.view;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ProgressBar;
import butterknife.BindView;
import com.essplore.essplore.R;
import com.essplore.essplore.application.AppConstants;
import com.essplore.essplore.application.EssploreApplication;
import com.essplore.essplore.managers.AppActivityManager;
import com.essplore.essplore.models.City;
import com.essplore.essplore.models.CityHighlight;
import com.essplore.essplore.ui.activities.HttpToolBarBaseActivity;
import com.essplore.essplore.ui.activities.placehighlights.adapter.PlaceHighlightsAdapter;
import com.essplore.essplore.ui.activities.placehighlights.presenter.PlaceHighlightsPresenterImpl;
import io.reactivex.disposables.CompositeDisposable;
import java.util.ArrayList;
import java.util.List;
import javax.inject.Inject;

public class PlaceHighlightsActivity extends HttpToolBarBaseActivity implements PlaceHighlightsView {

  @Inject CompositeDisposable disposable;
  @Inject AlertDialog alertDialog;
  @Inject AppActivityManager appActivityManager;
  @Inject PlaceHighlightsPresenterImpl presenter;

  @BindView(R.id.rvPlaces) RecyclerView rvPlaces;

  @BindView(R.id.pbPlaces) ProgressBar pbPlace;

  private PlaceHighlightsAdapter placeHighlightsAdapter;
  private List<CityHighlight> placeHighlightList;

  @Override public void onNetworkErrorFound(String message) {
    showAlertDialog(message);
  }

  @Override public void onHttpErrorUnexpectedFound(String message) {
    showAlertDialog(message);
  }

  @Override public void onApiErrorFound(String message) {
    showAlertDialog(message);
  }

  @Override public void onHttpErrorUnexpectedFound() {
    showAlertDialog(getString(R.string.unexpected_error_occurred));
  }

  @Override public void onUnauthorizedErrorFound() {
    showAlertDialog(getString(R.string.unauthorized));
  }

  @Override public CompositeDisposable getCompositeDisposable() {
    return disposable;
  }

  @Override protected Activity getActivity() {
    return this;
  }

  @Override protected AlertDialog getAlertDialog() {
    return alertDialog;
  }

  @Override protected void setupActivityLayout() {
    setContentView(R.layout.activity_place_highlights);
  }

  @Override protected void setupViewElements() {
    setupPlaceHighlightAdapter();
    disposable.add(presenter.getCityHighlights());
  }

  @Override protected void injectDaggerComponent() {
    EssploreApplication.get(this).createPlaceHighlightsComponent(this).inject(this);
  }

  @Override protected boolean isActionbarBackButtonEnabled() {
    return Boolean.TRUE;
  }

  @Override public void hideProgressbar() {
    pbPlace.setVisibility(View.GONE);
  }

  @Override public void showProgressBar() {
    pbPlace.setVisibility(View.VISIBLE);
  }

  @Override public City provideCityObject() {
    return getIntent().getParcelableExtra(AppConstants.CITY_OBJECT);
  }

  @Override protected void onDestroy() {
    super.onDestroy();
    EssploreApplication.get(this).releasePlaceHighlightsComponent();
  }

  @Override public void launchCityHighlights(CityHighlight cityHighlight) {
    appActivityManager.launchCityHighlights(this, cityHighlight, provideCityObject());
  }

  @Override public int getCountryId() {
    return getIntent().getIntExtra(AppConstants.GET_COUNTRY_ID, 0);
  }

  @Override public int getCityId() {
    return getIntent().getIntExtra(AppConstants.CITY_ID, 0);
  }

  private void setupPlaceHighlightAdapter() {
    placeHighlightList = new ArrayList<>();
    placeHighlightsAdapter = new PlaceHighlightsAdapter(this, placeHighlightList, presenter);
    rvPlaces.setAdapter(placeHighlightsAdapter);
    rvPlaces.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));
  }

  @Override public void setPlaceHighlights(List<CityHighlight> cityHighlightList) {
    placeHighlightList.clear();
    placeHighlightList.addAll(cityHighlightList);
    placeHighlightsAdapter.notifyDataSetChanged();
  }

  @Override protected void onActivityResult(int requestCode, int resultCode, Intent data) {
    super.onActivityResult(requestCode, resultCode, data);
    if (requestCode == AppConstants.ADD_PLACE_REQUEST && resultCode == RESULT_OK) {
      setResult(RESULT_OK);
      finish();
    }
  }
}
