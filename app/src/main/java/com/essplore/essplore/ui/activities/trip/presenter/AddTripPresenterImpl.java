package com.essplore.essplore.ui.activities.trip.presenter;

import com.essplore.essplore.api.managers.ApiManager;
import com.essplore.essplore.api.requests.CreateTrackRequest;
import com.essplore.essplore.models.City;
import com.essplore.essplore.ui.activities.HttpBasePresenter;
import com.essplore.essplore.ui.activities.trip.view.AddTripView;

public class AddTripPresenterImpl extends HttpBasePresenter implements AddTripPresenter {

  private final AddTripView view;
  private final ApiManager manager;

  public AddTripPresenterImpl(AddTripView view, ApiManager manager) {
    this.view = view;
    this.manager = manager;
  }

  @Override public CreateTrackRequest buildCreateTrackRequest(City city) {
    return new CreateTrackRequest.Builder(view.getLoggedInUserId(), city.getId(), city.getCountryId(),
        view.provideTrackName()).build();
  }
}
