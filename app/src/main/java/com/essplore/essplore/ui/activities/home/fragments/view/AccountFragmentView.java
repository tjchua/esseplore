package com.essplore.essplore.ui.activities.home.fragments.view;

import android.app.Activity;

public interface AccountFragmentView {

  void setupProfileElements();

  void showProgress();

  void hideProgress();

  void launchLoginActivity(Activity activity);
}
