package com.essplore.essplore.ui.activities.camera.models;

import android.graphics.Bitmap;

import com.essplore.essplore.ui.activities.camera.adapter.GalleryAdapter;

import org.json.JSONException;
import org.json.JSONObject;

public class UploadResponse {

    private String caption;
    private String location;

    private final String CAPTION = "caption";
    private final String LOCATION = "location";

    public UploadResponse(String galleryJson){

        JSONObject jsonObject;

        try {

            jsonObject = new JSONObject(galleryJson);
            caption = jsonObject.getString(CAPTION);
            location = jsonObject.getString(LOCATION);

        } catch (JSONException e) {

            e.printStackTrace();

        }

    }

    public String getCaption() {

        return caption;

    }

    public String getLocation() {

        return location;

    }

}
