package com.essplore.essplore.ui.activities.album.models;

import com.essplore.essplore.application.AppConstants;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import java.util.ArrayList;

public class ApiResponse {

    private final String STATUS = "status";
    private final String MESSAGE = "message";
    private final String PICTURES = "pictures";
    private String messageResponse;
    private int statusResponse;
    private ArrayList<Album> picturesResponse;

    public ApiResponse(String jsonString){

        JSONObject jsonObject;
        picturesResponse = new ArrayList<Album>();

        try {

            jsonObject = new JSONObject(jsonString);
            statusResponse = jsonObject.getInt(STATUS);
            messageResponse = jsonObject.getString(MESSAGE);

            if (statusResponse == AppConstants.STATUS_SUCCESS){

                setPictures(jsonObject.getJSONArray(PICTURES));

            }

        } catch (JSONException e) {

            e.printStackTrace();

        }
    }

    public ArrayList<Album> getPicturesResponse() {
        return picturesResponse;
    }

    public boolean success(){
        if (statusResponse == 200)
            return true;
            return false;
    }

    private void setPictures(JSONArray jsonArray){
        for (int i=0; i < jsonArray.length(); i++){
            try {
                Album album = new Album(jsonArray.getJSONObject(i));
                picturesResponse.add(album);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

    }
}
