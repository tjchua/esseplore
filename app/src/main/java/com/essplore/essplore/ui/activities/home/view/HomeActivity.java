package com.essplore.essplore.ui.activities.home.view;

import android.app.Activity;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AlertDialog;
import android.view.View;
import butterknife.BindView;
import com.essplore.essplore.R;
import com.essplore.essplore.application.EssploreApplication;
import com.essplore.essplore.managers.AppActivityManager;
import com.essplore.essplore.managers.SharedPreferenceManager;
import com.essplore.essplore.models.City;
import com.essplore.essplore.ui.activities.ToolBarBaseActivity;
import com.essplore.essplore.ui.activities.home.adapter.HomeFragmentPagerAdapter;
import com.essplore.essplore.ui.utils.BottomNavigationViewHelper;
import javax.inject.Inject;

public class HomeActivity extends ToolBarBaseActivity implements HomeActivityView {

  @Inject HomeFragmentPagerAdapter homeFragmentPagerAdapter;
  @Inject AlertDialog alertDialog;
  @Inject SharedPreferenceManager sharedPreferenceManager;
  @Inject AppActivityManager appActivityManager;

  @BindView(R.id.bnvHome) BottomNavigationView bnvHome;
  @BindView(R.id.vpHome) ViewPager vpHome;

  @Override protected void setupActivityLayout() {
    setContentView(R.layout.activity_home);
  }

  @Override protected void setupViewElements() {
    BottomNavigationViewHelper.disableShiftMode(bnvHome);
    vpHome.setAdapter(homeFragmentPagerAdapter);
    setViewPageListener();
    setBottomNavigationListener();
    if (sharedPreferenceManager.isUserLoggedIn()) {
      bnvHome.setSelectedItemId(R.id.action_trips);
    }
  }

  @Override protected void injectDaggerComponent() {
    EssploreApplication.get(this).createHomeComponent(this).inject(this);
  }

  @Override protected boolean isActionbarBackButtonEnabled() {
    return Boolean.TRUE;
  }

  @Override protected void onDestroy() {
    super.onDestroy();
    EssploreApplication.get(this).releaseHomeComponent();
  }

  @Override public void onBackPressed() {
    exitApp();
  }

  @Override protected Activity getActivity() {
    return this;
  }

  @Override protected AlertDialog getAlertDialog() {
    return alertDialog;
  }

  @Override public void setBottomNavigationListener() {
    bnvHome.setOnNavigationItemSelectedListener(item -> {
      switch (item.getItemId()) {
        case R.id.action_search:
          hideToolBar();
          vpHome.setCurrentItem(0);
          return true;

        case R.id.action_trips:
          showToolBar();
          vpHome.setCurrentItem(1);
          return true;

        case R.id.action_rewards:
          hideToolBar();
          vpHome.setCurrentItem(2);
          return true;

        case R.id.action_notification:
          hideToolBar();
          vpHome.setCurrentItem(3);
          return true;

        case R.id.action_account:
          hideToolBar();
          vpHome.setCurrentItem(4);
          return true;
      }
      return true;
    });
  }

  @Override public void setViewPageListener() {
    vpHome.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
      @Override
      public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

      }

      @Override public void onPageSelected(int position) {
        switch (position) {
          case 0:
            hideToolBar();
            bnvHome.getMenu().getItem(position).setChecked(true);
            break;

          case 1:
            showToolBar();
            bnvHome.getMenu().getItem(position).setChecked(true);
            break;

          case 2:
            hideToolBar();
            bnvHome.getMenu().getItem(position).setChecked(true);
            break;

          case 3:
            hideToolBar();
            bnvHome.getMenu().getItem(position).setChecked(true);
            break;

          case 4:
            hideToolBar();
            bnvHome.getMenu().getItem(position).setChecked(true);
            break;
        }
      }

      @Override public void onPageScrollStateChanged(int state) {

      }
    });
  }

  @Override public ViewPager getViewPager() {
    return vpHome;
  }

  @Override public void launchPlaceHighlight(City city) {
    appActivityManager.launchPlaceHighlights(this, city);
  }

  @Override public void launchAddTrip(City city) {
    appActivityManager.launchAddTrips(this, city);
  }

  @Override public void hideToolBar() {
    toolbar.setVisibility(View.GONE);
  }

  @Override public void showToolBar() {
    toolbar.setVisibility(View.VISIBLE);
  }

  @Override public void onNetworkErrorFound(String message) {
    showAlertDialog(message);
  }

  @Override public void onApiErrorFound(String message) {
    showAlertDialog(message);
  }

  @Override public void onHttpErrorUnexpectedFound() {
    showAlertDialog(getString(R.string.unexpected_error_occurred));
  }

  @Override public void onUnauthorizedErrorFound() {
    showAlertDialog(getString(R.string.unauthorized));
  }

  @Override
  public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
    super.onRequestPermissionsResult(requestCode, permissions, grantResults);
  }
}
