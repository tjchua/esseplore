package com.essplore.essplore.ui.activities.album;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.widget.ImageView;

import com.essplore.essplore.R;

public class ImageFullViewActivity extends AppCompatActivity {

    private ImageView fullViewImage;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_image_full_view);
        initViews();
        setFullViewImage();

    }

    private void initViews(){

        fullViewImage = findViewById(R.id.full_view_image);

    }

    private void setFullViewImage(){

        fullViewImage.setImageDrawable(AlbumActivity.SELECTED_IMAGE);

    }
}
