package com.essplore.essplore.ui.activities;

import io.reactivex.disposables.CompositeDisposable;

public abstract class HttpToolBarBaseActivity extends ToolBarBaseActivity {
  public abstract void onNetworkErrorFound(final String message);

  public abstract void onHttpErrorUnexpectedFound(final String message);

  public abstract void onApiErrorFound(final String message);

  public abstract CompositeDisposable getCompositeDisposable();

  @Override protected void onDestroy() {
    super.onDestroy();
    if (!getCompositeDisposable().isDisposed()) {
      getCompositeDisposable().clear();
      getCompositeDisposable().dispose();
    }
  }
}
