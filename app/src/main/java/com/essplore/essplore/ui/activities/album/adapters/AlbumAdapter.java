package com.essplore.essplore.ui.activities.album.adapters;

import android.content.Context;
import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.ImageView;
import android.widget.ProgressBar;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.drawable.GlideDrawable;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.essplore.essplore.R;
import com.essplore.essplore.ui.activities.album.AlbumActivity;
import com.essplore.essplore.ui.activities.album.asynctask.DowloadImageTask;
import com.essplore.essplore.ui.activities.album.interfaces.DownloadImageResponseListenter;
import com.essplore.essplore.ui.activities.album.models.Album;
import java.util.ArrayList;

public class AlbumAdapter extends RecyclerView.Adapter<AlbumAdapter.MyViewHolder> implements
        DownloadImageResponseListenter {

    private Context context;
    private ArrayList<Album> albumItems;

    public class MyViewHolder extends RecyclerView.ViewHolder {

        private ImageView album_item_image;
        private ProgressBar progressBar;

        public MyViewHolder(View view) {
            super(view);

            album_item_image = view.findViewById(R.id.album_item_image);
            progressBar = (ProgressBar) view.findViewById(R.id.progress);

        }
    }

    public AlbumAdapter(ArrayList<Album> albumItems, Context context) {

        this.context = context;
        this.albumItems = albumItems;

    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.list_item_album, parent, false);
        return new MyViewHolder(itemView);

    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {

//        if(albumItems.get(position).getImage() == null){
//
//           DowloadImageTask dowloadImageTask =  new DowloadImageTask(albumItems.get(position).getImageLink(),
//                   position);
//           dowloadImageTask.setDownloadImageResponseListenter(this);
//           dowloadImageTask.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
//
//        }else{
//
//            holder.album_item_image.setImageBitmap(albumItems.get(position).getImage());
//            holder.album_item_image.setScaleType(ImageView.ScaleType.CENTER_CROP);
//            holder.album_item_image.setOnClickListener((AlbumActivity)context);
//
//        }
        ViewTreeObserver viewTreeObserver = holder.album_item_image.getViewTreeObserver();
        if (viewTreeObserver.isAlive()) {
            viewTreeObserver.addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
                @Override
                public void onGlobalLayout() {
                    int w = holder.album_item_image.getWidth();
                    holder.album_item_image.getLayoutParams().height = w;
                }
            });
        }
        holder.album_item_image.setOnClickListener((AlbumActivity)context);
        holder.progressBar.setVisibility(View.VISIBLE);
        Glide.with(context)
                .load(albumItems.get(position).getImageLink())
                .crossFade()
                .listener(new RequestListener<String, GlideDrawable>() {
                    @Override
                    public boolean onException(Exception e, String model, Target<GlideDrawable> target, boolean isFirstResource) {

                        return false;

                    }

                    @Override
                    public boolean onResourceReady(GlideDrawable resource, String model, Target<GlideDrawable> target, boolean isFromMemoryCache, boolean isFirstResource) {

                        holder.progressBar.setVisibility(View.GONE);
                        return false;

                    }
                })
                .into(holder.album_item_image);

    }

    @Override
    public int getItemCount() {

        return albumItems.size();

    }

    @Override
    public void onFinishDownload(int listPosition, Bitmap image) {

        albumItems.get(listPosition).setImage(image);
        notifyDataSetChanged();

    }
}
