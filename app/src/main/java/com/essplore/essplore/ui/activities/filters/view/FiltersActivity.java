package com.essplore.essplore.ui.activities.filters.view;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import butterknife.BindView;
import com.essplore.essplore.R;
import com.essplore.essplore.api.requests.CreateTrackRequest;
import com.essplore.essplore.application.AppConstants;
import com.essplore.essplore.application.EssploreApplication;
import com.essplore.essplore.managers.AppActivityManager;
import com.essplore.essplore.managers.SharedPreferenceManager;
import com.essplore.essplore.ui.activities.HttpToolBarBaseActivity;
import com.essplore.essplore.ui.activities.filters.presenter.FiltersPresenterImpl;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import butterknife.OnClick;
import io.reactivex.disposables.CompositeDisposable;
import javax.inject.Inject;

public class FiltersActivity extends HttpToolBarBaseActivity implements FiltersView, View.OnClickListener {

  @Inject CompositeDisposable compositeDisposable;
  @Inject AlertDialog alertDialog;
  @Inject AppActivityManager manager;
  @Inject SharedPreferenceManager sharedPreferenceManager;
  @Inject FiltersPresenterImpl presenter;

  @BindView(R.id.pbFilter) ProgressBar pbFilter;

  @BindView(R.id.edtTravelBuddy) EditText edtTravelBuddy;

  @BindView(R.id.sNumberVisit) Spinner sNumberVisit;

  @BindView(R.id.sTypeOfSpots) Spinner sTypeOfSpots;


  @BindView(R.id.checkFood) CheckBox checkFood;
  @BindView(R.id.checkArt) CheckBox checkArt;
  @BindView(R.id.checkCulture) CheckBox checkCulture;
  @BindView(R.id.checkMusic) CheckBox checkMusic;
  @BindView(R.id.checkShops) CheckBox checkShops;
  @BindView(R.id.checkParty) CheckBox checkParty;

  private ImageView addIv;
  private ImageView minusIv;
  private TextView travelBuddyTv;

  @Override public void onNetworkErrorFound(String message) {
    showAlertDialog(message);
  }

  @Override public void onHttpErrorUnexpectedFound(String message) {
    showAlertDialog(message);
  }

  @Override public void onApiErrorFound(String message) {
    showAlertDialog(message);
  }

  @Override public void onHttpErrorUnexpectedFound() {
    showAlertDialog(getString(R.string.unexpected_error_occurred));
  }

  @Override public void onUnauthorizedErrorFound() {
    showAlertDialog(getString(R.string.unauthorized));
  }

  @Override public CompositeDisposable getCompositeDisposable() {
    return compositeDisposable;
  }

  @Override protected Activity getActivity() {
    return this;
  }

  @Override protected AlertDialog getAlertDialog() {
    return alertDialog;
  }

  @Override protected void setupActivityLayout() {
    setContentView(R.layout.activity_filter);
  }

  @Override protected void setupViewElements() {
    setSpinnerTypeOfSpot();
    setSpinnerNumberOfVisit();
  }

  @Override protected void injectDaggerComponent() {
    EssploreApplication.get(this).createFiltersComponent(this).inject(this);
  }

  @Override protected boolean isActionbarBackButtonEnabled() {
    return Boolean.TRUE;
  }

  @Override protected void onDestroy() {
    super.onDestroy();
    EssploreApplication.get(this).releaseFiltersComponent();
  }

  //TODO get this method to get the createTrackRequest object from the addtrip activity
  @Override public CreateTrackRequest getCreateTrackRequest() {
    return getIntent().getParcelableExtra(AppConstants.CREATE_TRACK_OBJECT);
  }

  @Override public String provideToken() {
    return sharedPreferenceManager.getAccessToken();
  }

  @Override public void showProgressbar() {
    pbFilter.setVisibility(View.VISIBLE);
  }

  @Override public void hideProgressbar() {
    pbFilter.setVisibility(View.GONE);
  }

  @OnClick(R.id.btnAddFilter) void onClickAddFilter() {
//    if(edtTravelBuddy.getText().length() > 0 ) {
      compositeDisposable.add(presenter.addUserPersonality());
//    }else {
//      Toast.makeText(FiltersActivity.this, "Please Complete Put ", Toast.LENGTH_SHORT).show();
//    }
  }

  private void setSpinnerTypeOfSpot() {
    // Initializing a String Array
    String[] typeOfSpots = new String[]{
            "sights",
            "art",
            "fashion",
            "party"
    };

    final List<String> plantsList = new ArrayList<>(Arrays.asList(typeOfSpots));

    final ArrayAdapter<String> spinnerArrayAdapter = new ArrayAdapter<>(
            this,R.layout.support_simple_spinner_dropdown_item,plantsList);

    spinnerArrayAdapter.setDropDownViewResource(R.layout.support_simple_spinner_dropdown_item);
    sTypeOfSpots.setAdapter(spinnerArrayAdapter);
  }

  private void setSpinnerNumberOfVisit() {
    // Initializing a String Array
    String[] numberOfVisit = new String[]{
            "Never",
            "1 time",
            "2 time",
            "3 time",
            "I know the place well"
    };

    final List<String> plantsList = new ArrayList<>(Arrays.asList(numberOfVisit));

    // Initializing an ArrayAdapter
    final ArrayAdapter<String> spinnerArrayAdapter = new ArrayAdapter<>(
            this,R.layout.support_simple_spinner_dropdown_item,plantsList);

    spinnerArrayAdapter.setDropDownViewResource(R.layout.support_simple_spinner_dropdown_item);
    sNumberVisit.setAdapter(spinnerArrayAdapter);
  }

  @Override
  public String provideInterest() {
    String selectedInterest = "";
    if (checkFood.isChecked()) {
      selectedInterest += checkFood.getText() + ", ";
    }
    if (checkArt.isChecked()) {
      selectedInterest += checkArt.getText() + ", ";
    }
    if (checkMusic.isChecked()) {
      selectedInterest += checkMusic.getText() + ", ";
    }
    if (checkShops.isChecked()) {
      selectedInterest += checkShops.getText() + ", ";
    }
    if (checkParty.isChecked()) {
      selectedInterest += checkParty.getText() + ", ";
    }
    return selectedInterest;
  }

  @Override
  public String provideNumberOfVisit() {
    return sNumberVisit.getSelectedItem().toString();
  }

  @Override
  public String provideSpots() {
    return sTypeOfSpots.getSelectedItem().toString();
  }

  @Override
  public int provideBuddies() {
//    if(edtTravelBuddy.getText().toString().length() > 0) {
//      return Integer.parseInt(edtTravelBuddy.getText().toString());
//    }
//      return 0;
    return Integer.parseInt(getTravelBuddyTv().getText().toString());
  }

  @Override public void launchCalendarActivity() {
    manager.launchCalendar(this, getCreateTrackRequest());
  }

  @Override
  protected void onCreate(@Nullable Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    getTravelBuddyTv();
    getAddIv();
    getMinusIv();
  }

  @Override
  public void onClick(View v) {

    switch (v.getId()){

      case R.id.minus_iv:

        decrementTravelBuddy();

        break;

      case R.id.add_iv:

        incrementTravelBuddy();

        break;

    }

  }

  public ImageView getAddIv() {

    if (addIv == null){

      addIv = findViewById(R.id.add_iv);
      addIv.setOnClickListener(this);

    }

    return addIv;

  }

  public ImageView getMinusIv() {

    if (minusIv == null){

      minusIv = findViewById(R.id.minus_iv);
      minusIv.setOnClickListener(this);

    }

    return minusIv;

  }

  public TextView getTravelBuddyTv() {

    if (travelBuddyTv == null){

      travelBuddyTv = findViewById(R.id.travel_buddy_tv);

    }

    return travelBuddyTv;

  }

  private void incrementTravelBuddy(){

    int travelBuddy = Integer.parseInt(getTravelBuddyTv().getText().toString());
    travelBuddy++;
    getTravelBuddyTv().setText(Integer.toString(travelBuddy));

  }

  private void decrementTravelBuddy(){

    int travelBuddy = Integer.parseInt(getTravelBuddyTv().getText().toString());
    if (travelBuddy > 0)
    travelBuddy--;
    getTravelBuddyTv().setText(Integer.toString(travelBuddy));

  }

}
