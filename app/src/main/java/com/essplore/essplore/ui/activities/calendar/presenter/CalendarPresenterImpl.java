package com.essplore.essplore.ui.activities.calendar.presenter;

import com.essplore.essplore.api.managers.ApiManager;
import com.essplore.essplore.api.requests.CreateTrackRequest;
import com.essplore.essplore.api.response.CreateTrackResponse;
import com.essplore.essplore.api.utils.SimpleObserver;
import com.essplore.essplore.models.MyTrip;
import com.essplore.essplore.ui.activities.HttpBasePresenter;
import com.essplore.essplore.ui.activities.calendar.view.CalendarView;
import io.reactivex.disposables.Disposable;

public class CalendarPresenterImpl extends HttpBasePresenter implements CalendarPresenter {

  private final CalendarView view;
  private final ApiManager manager;

  public CalendarPresenterImpl(CalendarView view, ApiManager manager) {
    this.view = view;
    this.manager = manager;
  }

  @Override public Disposable createTrack() {
    view.showProgressbar();
    return manager.createTrack(buildCreateTrackRequest(), view.provideToken())
        .subscribe(new SimpleObserver<CreateTrackResponse>() {
          @Override public void accept(CreateTrackResponse createTrackResponse) {
            view.hideProgressbar();
            view.launchPlacesActivity(new MyTrip(createTrackResponse));
          }
        }, throwable -> {
          view.hideProgressbar();
          handleHttpError(view, throwable);
        });
  }

  @Override public CreateTrackRequest buildCreateTrackRequest() {
    return new CreateTrackRequest(view.getCreateTrackRequest().getLoggedInUserId(),
        view.getCreateTrackRequest().getCityId(), view.getCreateTrackRequest().getCountryId(),
        view.getCreateTrackRequest().getTrackName(), view.provideStartDate(), view.provideEndDate());
  }
}
