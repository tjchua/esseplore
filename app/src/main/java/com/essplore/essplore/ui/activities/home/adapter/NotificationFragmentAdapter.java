package com.essplore.essplore.ui.activities.home.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.essplore.essplore.R;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class NotificationFragmentAdapter extends RecyclerView.Adapter<NotificationFragmentAdapter.ViewHolder> {

    private Context context;

    public NotificationFragmentAdapter(Context context) {
        this.context = context;
    }

    @Override
    public void onBindViewHolder(NotificationFragmentAdapter.ViewHolder holder, int position, List<Object> payloads) {
        super.onBindViewHolder(holder, position, payloads);
    }

    @Override
    public NotificationFragmentAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new NotificationFragmentAdapter.ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item_notif, parent, false));
    }

    @Override
    public int getItemCount() {
        return 0;
    }


    @Override
    public void onBindViewHolder(NotificationFragmentAdapter.ViewHolder holder, int position) {
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.imgType)
        ImageView imgType;

        @BindView(R.id.txDetail)
        TextView txDetail;

        @BindView(R.id.txTitle)
        TextView txTitle;

        @BindView(R.id.txDate)
        TextView txDate;



        public ViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }
    }
}
