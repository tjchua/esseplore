package com.essplore.essplore.ui.activities.directions.presenter;

import com.essplore.essplore.api.managers.ApiManager;
import com.essplore.essplore.api.response.CreateTrackResponse;
import com.essplore.essplore.api.response.PhotoTrackResponse;
import com.essplore.essplore.api.utils.SimpleObserver;
import com.essplore.essplore.models.Tracks;
import com.essplore.essplore.ui.activities.HttpBasePresenter;
import com.essplore.essplore.ui.activities.directions.view.DirectionsView;
import com.essplore.essplore.ui.activities.timeline.view.TimeLineView;
import com.essplore.essplore.ui.utils.OnSingleItemClickListener;

import io.reactivex.disposables.Disposable;

public class DirectionsPresenterImpl extends HttpBasePresenter implements DirectionsPresenter, OnSingleItemClickListener<Tracks> {

    private final DirectionsView view;
    private final ApiManager manager;

    public DirectionsPresenterImpl(DirectionsView view, ApiManager manager) {
        this.view = view;
        this.manager = manager;
    }

    @Override public Disposable getSpecificTrack(int trackId) {
        view.showProgressbar();
        return manager.getSpecificTrack(trackId, view.provideToken()).subscribe(new SimpleObserver<CreateTrackResponse>() {
            @Override public void accept(CreateTrackResponse createTrackResponse) {
                view.hideProgressbar();
                view.setTrackPictureList(createTrackResponse.getMyTrack().getTrackPictures());
                view.setTracksList(createTrackResponse.getTracks());
            }
        }, throwable -> {
            view.hideProgressbar();
            handleHttpError(view, throwable);
        });
    }

    @Override public Disposable getPhotoTrack(int trackId) {
        view.showProgressbar();
        return manager.getPhotoTrack(trackId, view.provideToken()).subscribe(new SimpleObserver<PhotoTrackResponse>() {
            @Override public void accept(PhotoTrackResponse photoTrackResponse) {
                view.hideProgressbar();
                view.setTrackPictureList(photoTrackResponse.getPhotoTracks());
            }
        }, throwable -> {
            view.hideProgressbar();
            handleHttpError(view, throwable);
        });
    }

    @Override
    public Disposable changeRecordStatus(int trackRecordId, int status) {
        //view.showProgressbar();
        return manager.changeRecordStatus(view.buildRequest(trackRecordId, status), view.provideToken())
                .subscribe(new SimpleObserver<String>() {
                @Override public void accept(String string) {
                    view.hideProgressbar();
                }
            }, throwable -> {
                view.hideProgressbar();
                //handleHttpError(view, throwable);
            });
    }

    @Override
    public Disposable deletePlaceFromTrack(int trackPlaceId) {
        view.showProgressbar();
        return manager.removePlaceFromTrack(view.buildRequest(trackPlaceId), view.provideToken()).subscribe(new SimpleObserver<String>() {
            @Override public void accept(String string) {
                view.hideProgressbar();
            }
        }, throwable -> {
            view.hideProgressbar();
            //handleHttpError(view, throwable);
        });
    }

    @Override public void onSingleItemClick(Tracks object) {
        view.launchCityHighlightsForViewing(object);
    }
}

