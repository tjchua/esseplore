package com.essplore.essplore.ui.activities.calendar.presenter;

import com.essplore.essplore.api.requests.CreateTrackRequest;
import io.reactivex.disposables.Disposable;

public interface CalendarPresenter {

  Disposable createTrack();

  CreateTrackRequest buildCreateTrackRequest();
}
