package com.essplore.essplore.ui.utils;

public interface OnBindViewListener<T> {

  void onBind(T object);
}
