package com.essplore.essplore.ui.activities.login.fragments.view;

import com.essplore.essplore.api.response.LoginResponse;
import com.essplore.essplore.models.Country;
import com.essplore.essplore.models.User;
import com.essplore.essplore.ui.activities.HttpBaseView;
import java.util.List;

public interface RegisterFragmentView extends HttpBaseView {

  void showProgress();

  void hideProgress();

  void setUsernameError();

  void setPasswordError();

  void setBirthdayError();

  void setMobileError();

  void navigateToHome();

  void setupSpinner(List<Country> countryList);

  void saveResponse(LoginResponse response, User user);

  String provideUsername();

  String providePassword();

  String provideMobile();

  String provideBirthday();

  String provideEmail();

  int provideCountry();

  void showRegistrationError(String message);
}
