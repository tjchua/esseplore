package com.essplore.essplore.ui.activities.camera.models;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class Galleries extends ArrayList<Gallery> {

    private String PICTURES = "pictures";

    public Galleries(){

    }

    public void addObjects(String json){
        try {
            JSONObject jsonObject = new JSONObject(json);
            JSONArray jsonArray = jsonObject.getJSONArray(PICTURES);
            if (this.size() != jsonArray.length()){
                this.clear();
                for (int i = 0; i < jsonArray.length(); i++){
                    this.add(new Gallery(jsonArray.getJSONObject(i).toString()));
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
}
