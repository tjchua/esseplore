package com.essplore.essplore.ui.utils;

import java.util.ArrayList;

public interface TextToSpeechServiceInterface {

    public void onDoneSpeak();
    public void onStartSpeak();

}
