package com.essplore.essplore.ui.activities.filters.presenter;

import com.essplore.essplore.api.requests.AddUserPersonalityRequest;
import io.reactivex.disposables.Disposable;

public interface FiltersPresenter {

  AddUserPersonalityRequest buildAddUserPersonalityRequest();

  Disposable addUserPersonality();
}
