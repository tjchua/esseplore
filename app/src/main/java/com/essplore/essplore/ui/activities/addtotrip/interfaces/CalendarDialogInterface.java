package com.essplore.essplore.ui.activities.addtotrip.interfaces;

import android.support.annotation.NonNull;
import android.widget.CalendarView;

public interface CalendarDialogInterface {

    public void onSelectedDayChange(CalendarView view, int year, int month, int dayOfMonth);

}
