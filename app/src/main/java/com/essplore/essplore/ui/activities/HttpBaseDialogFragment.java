package com.essplore.essplore.ui.activities;

import android.content.DialogInterface;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;
import com.essplore.essplore.R;

public abstract class HttpBaseDialogFragment extends DialogFragment {

  protected abstract DialogFragment getFragment();

  protected abstract AlertDialog getAlertDialog();

  protected void showAlertDialog(String message) {
    getAlertDialog().setMessage(message);
    getAlertDialog().setCancelable(false);
    getAlertDialog().setButton(DialogInterface.BUTTON_POSITIVE, getFragment().getString(R.string.ok),
        (dialog, which) -> getAlertDialog().dismiss());
    getAlertDialog().show();
  }
}
