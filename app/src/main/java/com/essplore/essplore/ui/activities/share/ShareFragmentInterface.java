package com.essplore.essplore.ui.activities.share;

import com.essplore.essplore.ui.activities.camera.CameraActivity;

import java.util.ArrayList;

public interface ShareFragmentInterface {

    public void quitWithResult(CameraActivity.ResultStatus resultStatus);
    public void quitWithResult(CameraActivity.ResultStatus resultStatus, ArrayList<String> localImages, String title, String description);

}
