package com.essplore.essplore.ui.activities.share.adapters;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ProgressBar;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.drawable.GlideDrawable;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.animation.GlideAnimation;
import com.bumptech.glide.request.target.GlideDrawableImageViewTarget;
import com.bumptech.glide.request.target.Target;
import com.essplore.essplore.R;
import com.essplore.essplore.ui.activities.camera.CameraApi;
import com.essplore.essplore.ui.activities.camera.models.Galleries;
import com.essplore.essplore.ui.activities.camera.models.Gallery;
import com.essplore.essplore.ui.activities.share.ShareAdapterInterface;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class ShareAdapter extends RecyclerView.Adapter<ShareAdapter.MyViewHolder> implements View.OnClickListener{

    private ArrayList<String> images;
    private Context context;
    private ShareAdapterInterface shareAdapterInterface;


    public class MyViewHolder extends RecyclerView.ViewHolder {

        private ImageView imageView;
        private ImageView addPhotoIcon;
        private View addPhotoContainer;
        private Button addPhotoBtn;
        private ImageButton removePhotoBtn;
        private View mainContainer;


        public MyViewHolder(View view) {

            super(view);
            imageView = (ImageView) view.findViewById(R.id.image);
            addPhotoIcon = (ImageView) view.findViewById(R.id.add_photo_icon);
            addPhotoContainer = view.findViewById(R.id.add_photo_container);
            addPhotoBtn = view.findViewById(R.id.add_photo_btn);
            removePhotoBtn = view.findViewById(R.id.remove_photo_btn);
            mainContainer = view.findViewById(R.id.container);

        }
    }


    public ShareAdapter(Context context, ArrayList<String> images) {

        this.context = context;
        this.images = images;
        images.add(0, "");

    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.list_item_share, parent, false);
        return new MyViewHolder(itemView);

    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {

        if(position == 0){

            holder.addPhotoIcon.setImageResource(R.drawable.add_image);
            holder.removePhotoBtn.setVisibility(View.INVISIBLE);
            holder.addPhotoContainer.setVisibility(View.VISIBLE);
            holder.addPhotoBtn.setOnClickListener(this);
            holder.addPhotoBtn.setVisibility(View.VISIBLE);

        }else{

            holder.addPhotoBtn.setVisibility(View.INVISIBLE);
            holder.addPhotoContainer.setVisibility(View.INVISIBLE);
            holder.removePhotoBtn.setVisibility(View.VISIBLE);
            holder.removePhotoBtn.setTag(position);
            holder.removePhotoBtn.setOnClickListener(this);
            Glide.with(context)
                    .load(Uri.fromFile(new File(images.get(position))))
                    .crossFade()
                    .into(holder.imageView);

        }

    }

    @Override
    public int getItemCount() {

        return images.size();

    }

    @Override
    public void onClick(View v) {

        switch (v.getId()){

            case R.id.add_photo_btn:


                if (shareAdapterInterface != null) {

                    addPhoto();

                }

                break;

                default:

                    images.remove((int)v.getTag());

                    if (images.size() < 2){

                        addPhoto();

                    }
                    notifyDataSetChanged();

        }



    }

    private void addPhoto(){

        images.remove(0);
        shareAdapterInterface.addPhoto(images);

    }

    public void setShareAdapterInterface(ShareAdapterInterface shareAdapterInterface){

        this.shareAdapterInterface = shareAdapterInterface;

    }
}
