package com.essplore.essplore.ui.activities.share;

import com.essplore.essplore.ui.activities.camera.CameraActivity;

import java.util.ArrayList;

public interface ShareAdapterInterface {

    public void addPhoto(ArrayList<String> images);
}
