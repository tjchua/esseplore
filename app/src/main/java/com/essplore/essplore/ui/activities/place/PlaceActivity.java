package com.essplore.essplore.ui.activities.place;

import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import com.essplore.essplore.R;
import com.essplore.essplore.application.AppConstants;
import com.essplore.essplore.managers.SharedPreferenceKeys;
import com.essplore.essplore.models.City;
import com.essplore.essplore.ui.activities.addtotrip.AddTripFromSearchActivity;
import com.essplore.essplore.ui.activities.home.fragments.view.SearchFragment;
import com.essplore.essplore.ui.activities.login.LoginActivity;
import com.essplore.essplore.ui.activities.place.fragments.PlaceFragment;
import com.essplore.essplore.ui.activities.search.SearchActivity;
import com.essplore.essplore.ui.activities.search.models.Place;
import com.essplore.essplore.ui.activities.share.ShareFragment;

import org.apache.commons.lang3.StringUtils;

public class PlaceActivity extends AppCompatActivity implements View.OnClickListener{

    private Place place;
    private Button addTripBtn;
    public static final String PLACE_ID_INTENT_KEY = "place_id_intent_key";
    public static final String BACK_TO_PLACE_INTENT_KEY = "back_to_place_key";
    private ImageView backIv;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_place);

        place = (Place) getIntent().getSerializableExtra(SearchActivity.PLACE_INTENT_KEY);
        initViews();
        showPlaceFragment();

    }

    @Override
    public void onClick(View v) {

        switch (v.getId()){

            case R.id.add_trip_btn:

                showAddTrip();

                break;

            case R.id.back_iv:

                finish();

                break;
            default:
        }
    }

    private void showAddTrip(){

        if (!isUserLoggedIn()){

            showLoginPage();
            return;

        }

        Intent addTripIntent = new Intent(this, AddTripFromSearchActivity.class);
        addTripIntent.putExtra(PLACE_ID_INTENT_KEY, getPlace().getId());
        addTripIntent.putExtra(AppConstants.CITY_OBJECT, getCity());
        startActivity(addTripIntent);

    }

    private void initViews(){

        addTripBtn = findViewById(R.id.add_trip_btn);
        addTripBtn.setOnClickListener(this);
        getBackIv();

    }

    private void showPlaceFragment(){

        PlaceFragment placeFragment = new PlaceFragment();
        placeFragment.setPlace(place);
//        searchFragment.setShareFragmentInterface(this);
        FragmentManager fragmentManager = getFragmentManager();

        if (fragmentManager != null) {

            FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
            fragmentTransaction.replace(R.id.fragment_container, placeFragment);
            fragmentTransaction.commit();

        }

    }

    private Place getPlace(){

        if (place == null){

            place = new Place();
        }

        return place;

    }

    private String getAccessToken(){

        return  getSharedPreferences(SharedPreferenceKeys.ESSPLORE_PREFERENCE, Context.
                MODE_PRIVATE).getString(SharedPreferenceKeys.ACCESS_TOKEN, StringUtils.EMPTY);

    }

    private boolean isUserLoggedIn() {

        return  getSharedPreferences(SharedPreferenceKeys.ESSPLORE_PREFERENCE, Context.
                MODE_PRIVATE).getBoolean(SharedPreferenceKeys.IS_USER_LOGGED_IN, Boolean.FALSE);

    }

    private void showLoginPage(){

        Intent loginIntent = new Intent(this, LoginActivity.class);
        loginIntent.putExtra(BACK_TO_PLACE_INTENT_KEY, Boolean.TRUE);
        startActivity(loginIntent);

    }

    private City getCity() {

        return getIntent().getParcelableExtra(AppConstants.CITY_OBJECT);

    }

    private ImageView getBackIv(){

        if (backIv == null){

            backIv = findViewById(R.id.back_iv);
            backIv.setOnClickListener(this);

        }

        return backIv;

    }

}
