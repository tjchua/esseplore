package com.essplore.essplore.ui.activities.addtotrip;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.Toast;

import com.essplore.essplore.R;
import com.essplore.essplore.application.AppConstants;
import com.essplore.essplore.models.City;
import com.essplore.essplore.ui.activities.addtotrip.adapters.AddToTripAdapter;
import com.essplore.essplore.ui.activities.addtotrip.api.AddToTripFromSearchApi;
import com.essplore.essplore.ui.activities.addtotrip.interfaces.AddToTripAdapterInterface;
import com.essplore.essplore.ui.activities.addtotrip.interfaces.AddTripFromSearchApiInterface;
import com.essplore.essplore.ui.activities.addtotrip.interfaces.GetSpecificTrackInterface;
import com.essplore.essplore.ui.activities.addtotrip.models.Trips;
import com.essplore.essplore.ui.activities.place.PlaceActivity;
import com.essplore.essplore.ui.activities.trip.view.AddTripActivity;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class AddTripFromSearchActivity extends AppCompatActivity implements AddTripFromSearchApiInterface, AddToTripAdapterInterface, View.OnClickListener, GetSpecificTrackInterface {

    private AddToTripFromSearchApi addToTripFromSearchApi;
    private Trips trips;
    private AddToTripAdapter addToTripAdapter;
    private RecyclerView recyclerView;
    public static final String TRACK_ID_INTENT_KEY = "track_id_intent_key";
    private ImageButton backBtn;
    private Button addTripBtn;
    public static final String BACK_TO_ADD_TO_TRIP_FROM_SEARCH_INTENT_KEY = "back_to_add_to_trip_from_search_key";
    private final int BACK_TO_ADD_TO_TRIP_FROM_SEARCH_REQUESTCODE = 1;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_trip_from_search);
        getAddToTripFromSearchApi().getGetTrips();
        initViews();

    }

    @Override
    public void onGetTripsSuccess(String response) {

        getTrips().clear();
        getTrips().addTrips(response);
        getAddToTripAdapter().notifyDataSetChanged();

    }

    @Override
    public void onAddPlaceToTrackSuccess() {

    }

    @Override
    public void onClickItem(int trackId) {

        getAddToTripFromSearchApi().getSpecificTrack(trackId, this);

    }

    @Override
    public void onAddPlaceToTrackFailed() {

    }

    @Override
    public void onGetSpecificTrackFailed() {



    }

    @Override
    public void onGetSpecificTrackSuccess(String response, String trackId) {

        checkIfPlaceIsAlreadyAdded(response, trackId);

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){

            case R.id.back_button:

                finish();

                break;

            case R.id.add_trip_btn:

                showAddTripPage();

                break;

                default:

        }
    }

    private void checkIfPlaceIsAlreadyAdded(String json, String trackId){

        JSONObject jsonObject;
        JSONArray jsonArray;
        final String TRACK = "track";
        final String PLACE_ID = "place_id";

        try {

            jsonObject =  new JSONObject(json);
            jsonArray = jsonObject.getJSONArray(TRACK);
            for (int i=0; i < jsonArray.length(); i++){

                int placeId = jsonArray.getJSONObject(i).isNull(PLACE_ID) ? -1 : jsonArray.getJSONObject(i).getInt(PLACE_ID);

                if (placeId == getPlaceId()){

                    Toast.makeText(this, "Place already added", Toast.LENGTH_SHORT).show();
                    return;

                }

            }

        } catch (JSONException e) {

            e.printStackTrace();

        }

        showChooseDatePage(Integer.parseInt(trackId), getPlaceId());

    }

    private void initViews(){

        recyclerView = findViewById(R.id.recycler_view);
        backBtn = findViewById(R.id.back_button);
        backBtn.setOnClickListener(this);
        getAddTripBtn();
        prepareRecyclerView();

    }

    private Trips getTrips(){

        if (trips == null){

            trips = new Trips();

        }

        return trips;

    }

    private AddToTripFromSearchApi getAddToTripFromSearchApi(){

        if (addToTripFromSearchApi == null){

            addToTripFromSearchApi = new AddToTripFromSearchApi(this);
            addToTripFromSearchApi.setAddTripFromSearchApiInterface(this);

        }

        return addToTripFromSearchApi;

    }

    private AddToTripAdapter getAddToTripAdapter(){

        if (addToTripAdapter == null){

            addToTripAdapter = new AddToTripAdapter(this, getTrips());
            addToTripAdapter.setAddToTripAdapterInterface(this);

        }

        return addToTripAdapter;

    }

    private void prepareRecyclerView(){

        recyclerView.setHasFixedSize(true);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(this,
                LinearLayoutManager.VERTICAL, false);
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setAdapter(getAddToTripAdapter());

    }

    private void showChooseDatePage(int trackId, int placeId){

        Intent chooseDateIntent = new Intent(this, ChooseDateActivity.class);
        chooseDateIntent.putExtra(TRACK_ID_INTENT_KEY, trackId);
        chooseDateIntent.putExtra(PlaceActivity.PLACE_ID_INTENT_KEY, placeId);
        startActivity(chooseDateIntent);

    }

    private int getPlaceId(){

        return getIntent().getIntExtra(PlaceActivity.PLACE_ID_INTENT_KEY, 0);

    }

    public Button getAddTripBtn() {

        if (addTripBtn == null){

            addTripBtn = findViewById(R.id.add_trip_btn);
            addTripBtn.setOnClickListener(this);

        }
        return addTripBtn;
    }

    private void showAddTripPage(){

        Intent addTripIntent = new Intent(this, AddTripActivity.class);
        addTripIntent.putExtra(AppConstants.CITY_OBJECT, getCity());
        startActivityForResult(addTripIntent, BACK_TO_ADD_TO_TRIP_FROM_SEARCH_REQUESTCODE);

    }

    public City getCity() {

        return getIntent().getParcelableExtra(AppConstants.CITY_OBJECT);

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        super.onActivityResult(requestCode, resultCode, data);

        if(requestCode == BACK_TO_ADD_TO_TRIP_FROM_SEARCH_REQUESTCODE){

            getAddToTripFromSearchApi().getGetTrips();

        }
    }
}
