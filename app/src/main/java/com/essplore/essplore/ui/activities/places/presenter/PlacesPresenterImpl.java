package com.essplore.essplore.ui.activities.places.presenter;

import com.essplore.essplore.api.managers.ApiManager;
import com.essplore.essplore.api.response.CreateTrackResponse;
import com.essplore.essplore.api.utils.SimpleObserver;
import com.essplore.essplore.models.Tracks;
import com.essplore.essplore.ui.activities.HttpBasePresenter;
import com.essplore.essplore.ui.activities.places.view.PlacesView;
import com.essplore.essplore.ui.utils.OnSingleItemClickListener;
import io.reactivex.disposables.Disposable;

public class PlacesPresenterImpl extends HttpBasePresenter implements PlacesPresenter, OnSingleItemClickListener<Tracks> {

  private final PlacesView view;
  private final ApiManager manager;

  public PlacesPresenterImpl(PlacesView view, ApiManager manager) {
    this.view = view;
    this.manager = manager;
  }

  @Override public Disposable getSpecificTrack(int trackId) {
    view.showProgressbar();
    return manager.getSpecificTrack(trackId, view.provideToken()).subscribe(new SimpleObserver<CreateTrackResponse>() {
      @Override public void accept(CreateTrackResponse createTrackResponse) {
        view.hideProgressbar();
        view.setTrackPictureList(createTrackResponse.getMyTrack().getTrackPictures());
        view.setTracksList(createTrackResponse.getTracks());
      }
    }, throwable -> {
      view.hideProgressbar();
      handleHttpError(view, throwable);
    });
  }

  @Override
  public Disposable deletePlaceFromTrack() {
    view.showProgressbar();
    return manager.removePlaceFromTrack(view.buildRequest(), view.provideToken()).subscribe(new SimpleObserver<String>() {
      @Override public void accept(String string) {
        view.hideProgressbar();
      }
    }, throwable -> {
      view.hideProgressbar();
      handleHttpError(view, throwable);
    });
  }

  @Override
  public Disposable getSpecificTrackForTimeline(int trackId) {
    //view.showProgressbar();
    return manager.getSpecificTrack(trackId, view.provideToken()).subscribe(new SimpleObserver<CreateTrackResponse>() {
      @Override public void accept(CreateTrackResponse createTrackResponse) {
        view.setMyTrack(createTrackResponse.getMyTrack());
      }
    }, throwable -> {
      view.hideProgressbar();
      handleHttpError(view, throwable);
    });
  }

  @Override public void onSingleItemClick(Tracks object) {
    view.launchCityHighlightsForViewing(object);
  }
}
