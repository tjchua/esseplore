package com.essplore.essplore.ui.activities;

import android.app.Activity;
import android.content.DialogInterface;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import butterknife.BindView;
import com.essplore.essplore.R;
import io.reactivex.annotations.Nullable;

public abstract class ToolBarBaseActivity extends BaseActivity {

  protected abstract Activity getActivity();

  protected abstract AlertDialog getAlertDialog();

  @Nullable @BindView(R.id.toolbar) protected Toolbar toolbar;

  @Override protected void setupToolbar() {
    setSupportActionBar(toolbar);
    if (isActionbarBackButtonEnabled()) {
      // TODO uncomment on back button implementation
      //getSupportActionBar().setDisplayShowHomeEnabled(Boolean.TRUE);
      //getSupportActionBar().setDisplayHomeAsUpEnabled(Boolean.TRUE);
    }
  }

  @Override public boolean onOptionsItemSelected(final MenuItem item) {
    int id = item.getItemId();
    if (id == android.R.id.home && isActionbarBackButtonEnabled()) {
      onBackPressed();
      return true;
    }
    return super.onOptionsItemSelected(item);
  }

  protected void exitApp() {
    AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(this, R.style.ExitDialog);
    AlertDialog exitDialog = dialogBuilder.create();
    exitDialog.setMessage(getString(R.string.are_you_sure_you_want_to_exit));
    exitDialog.setButton(DialogInterface.BUTTON_POSITIVE, getString(R.string.yes), (dialog, which) -> {
      exitDialog.dismiss();
      finish();
    });
    exitDialog.setButton(DialogInterface.BUTTON_NEGATIVE, getString(R.string.no), (dialog, which) -> {
      exitDialog.dismiss();
    });
    exitDialog.show();
  }

  protected void showAlertDialog(String message) {
    getAlertDialog().setMessage(message);
    getAlertDialog().setCancelable(false);
    getAlertDialog().setButton(DialogInterface.BUTTON_POSITIVE, getActivity().getString(R.string.ok),
        (dialog, which) -> getAlertDialog().dismiss());
    getAlertDialog().show();
  }
}
