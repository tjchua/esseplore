package com.essplore.essplore.ui.activities.home.fragments.presenter;

import com.essplore.essplore.api.managers.ApiManager;
import com.essplore.essplore.api.requests.CityRequest;
import com.essplore.essplore.api.requests.DeleteSpecificTrackRequest;
import com.essplore.essplore.api.response.CityResponse;
import com.essplore.essplore.api.utils.SimpleObserver;
import com.essplore.essplore.models.City;
import com.essplore.essplore.ui.activities.HttpBasePresenter;
import com.essplore.essplore.ui.activities.home.fragments.view.SearchView;
import io.reactivex.disposables.Disposable;

public class SearchPresenterImpl extends HttpBasePresenter implements SearchPresenter {

  private final SearchView view;
  private final ApiManager manager;

  public SearchPresenterImpl(SearchView view, ApiManager manager) {
    this.view = view;
    this.manager = manager;
  }

  @Override public Disposable getCity() {
    view.showProgressbar();
    return manager.getCity(buildCityRequest(), view.provideToken())
        .subscribe(new SimpleObserver<CityResponse>() {
          @Override public void accept(CityResponse cityResponse) {
            view.hideProgressbar();
            view.setCityListValue(cityResponse.getCityList());
            view.modifyLayout();
          }
        }, throwable -> {
          view.hideProgressbar();
          handleHttpError(view, throwable);
        });
  }


  @Override public CityRequest buildCityRequest() {
    return new CityRequest(view.provideSearch());
  }



  @Override public void onSingleItemClick(City object) {
    view.launchActivity(object);
  }
}
