package com.essplore.essplore.ui.activities.login.fragments.view;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputEditText;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.Toast;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import com.essplore.essplore.R;
import com.essplore.essplore.application.EssploreApplication;
import com.essplore.essplore.ui.activities.HttpBaseFragment;
import com.essplore.essplore.ui.activities.login.LoginActivity;
import com.essplore.essplore.ui.activities.login.fragments.presenter.ForgotPasswordPresenterImpl;
import io.reactivex.disposables.CompositeDisposable;
import javax.inject.Inject;
import org.apache.commons.lang3.StringUtils;


public class ForgotPasswordFragment extends HttpBaseFragment implements ForgotPasswordView {

  @Inject CompositeDisposable disposable;
  @Inject AlertDialog alertDialog;
  @Inject ForgotPasswordPresenterImpl presenter;

  @BindView(R.id.pbForgotPassword) ProgressBar pbForgotPassword;
  @BindView(R.id.edtEmail) TextInputEditText edtEmail;
  @BindView(R.id.btnForgotPassword) Button btnForgotPassword;

  private Unbinder unbinder;
  private LoginActivity activity;

  public ForgotPasswordFragment() {
    // Intended to be empty.
  }

  @Nullable @Override public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
      @Nullable Bundle savedInstanceState) {
    View view = inflater.inflate(R.layout.fragment_forgot_password, container, false);
    unbinder = ButterKnife.bind(this, view);
    return view;
  }

  @Override public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
    super.onViewCreated(view, savedInstanceState);
    EssploreApplication.get(activity).createForgotPasswordComponent(this).inject(this);
  }

  @Override public void onAttach(Context context) {
    super.onAttach(context);
    activity = (LoginActivity) context;
  }

  @Override public void onDestroy() {
    super.onDestroy();
    disposable.clear();
    unbinder.unbind();
    EssploreApplication.get(activity).releaseForgotPasswordFragmentComponent();
  }

  @Override protected Fragment getFragment() {
    return this;
  }

  @Override protected AlertDialog getAlertDialog() {
    return alertDialog;
  }

  @Override public void showProgressbar() {
    pbForgotPassword.setVisibility(View.VISIBLE);
  }

  @Override public void hideProgressbar() {
    pbForgotPassword.setVisibility(View.GONE);
  }

  @Override public void showSuccessApiCall() {
    Toast.makeText(activity, getString(R.string.reset_password_instructions_has_been_sent_to_your_email),
        Toast.LENGTH_SHORT).show();
  }

  @Override public void navigateToLogin() {
    activity.onBackPressed();
  }

  @Override public boolean isValid() {
    return StringUtils.isEmpty(edtEmail.getText().toString());
  }

  @Override public String provideEmail() {
    return edtEmail.getText().toString();
  }

  @Override public void onNetworkErrorFound(String message) {
    showAlertDialog(message);
  }

  @Override public void onApiErrorFound(String message) {
    showAlertDialog(message);
  }

  @Override public void onHttpErrorUnexpectedFound() {
    showAlertDialog(getString(R.string.unexpected_error_occurred));
  }

  @Override public void onUnauthorizedErrorFound() {
    showAlertDialog(getString(R.string.unauthorized));
  }

  @OnClick(R.id.btnForgotPassword) void onForgotPasswordClicked() {
    if (isValid()) {
      disposable.add(presenter.requestForgotPassword());
    }
  }
}
