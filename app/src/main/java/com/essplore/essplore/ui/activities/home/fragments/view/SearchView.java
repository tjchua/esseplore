package com.essplore.essplore.ui.activities.home.fragments.view;

import com.essplore.essplore.models.City;
import com.essplore.essplore.ui.activities.HttpBaseView;
import java.util.List;

public interface SearchView extends HttpBaseView {

  void setupListener();

  void modifyLayout();

  String provideSearch();

  String provideToken();

  void showRecyclerView();

  void hideRecyclerView();

  void showProgressbar();

  void hideProgressbar();

  void setCityListValue(List<City> cityListValue);

  void launchActivity(City city);
}
