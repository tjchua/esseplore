package com.essplore.essplore.ui.activities;

import com.essplore.essplore.api.response.ErrorResponse;
import com.essplore.essplore.api.utils.HttpStatusCode;
import com.google.gson.Gson;
import java.io.IOException;
import org.json.JSONException;
import org.json.JSONObject;
import retrofit2.HttpException;
import retrofit2.Response;

public abstract class HttpBasePresenter {

  public void handleHttpError(final HttpBaseView view, final Throwable throwable) {
    if (throwable instanceof HttpException) {
      final HttpException exception = (HttpException) throwable;
      if (exception.code() == HttpStatusCode.UNAUTHORIZED) {
        handleUnauthorizedError(view);
      } else if (exception.response() != null) {
        handleErrorResponseObject(view, exception);
      }
    } else if (throwable instanceof IOException) {
      view.onNetworkErrorFound(throwable.getMessage());
    } else {
      view.onHttpErrorUnexpectedFound();
    }
  }

  private void handleErrorResponseObject(HttpBaseView view, HttpException exception) {
    try {
      ErrorResponse errorResponse =
          new Gson().fromJson(new JSONObject(exception.response().errorBody().string()).toString(),
              ErrorResponse.class);
      view.onApiErrorFound(errorResponse.getMessage());
    } catch (IOException | JSONException e) {
      view.onApiErrorFound(getErrorMessage(exception.response()));
    }
  }

  private void handleUnauthorizedError(HttpBaseView view) {
    view.onUnauthorizedErrorFound();
  }

  private String getErrorMessage(final Response response) {
    return response.message();
  }
}
