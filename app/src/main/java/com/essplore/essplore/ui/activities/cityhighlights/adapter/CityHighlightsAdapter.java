package com.essplore.essplore.ui.activities.cityhighlights.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import butterknife.BindView;
import butterknife.ButterKnife;
import com.bumptech.glide.Glide;
import com.essplore.essplore.R;
import com.essplore.essplore.models.CityHighlight;
import com.essplore.essplore.ui.activities.cityhighlights.presenter.CityHighlightsPresenterImpl;
import com.essplore.essplore.ui.utils.OnBindViewListener;
import java.util.List;

public class CityHighlightsAdapter extends RecyclerView.Adapter<CityHighlightsAdapter.ViewHolder> {

  private Context context;
  private List<CityHighlight> cityHighlightList;
  private CityHighlightsPresenterImpl presenter;

  public CityHighlightsAdapter(Context context, List<CityHighlight> cityHighlightList,
      CityHighlightsPresenterImpl presenter) {
    this.context = context;
    this.cityHighlightList = cityHighlightList;
    this.presenter = presenter;
  }

  @NonNull @Override public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
    return new ViewHolder(
        LayoutInflater.from(context).inflate(R.layout.item_city_highlights, parent, false));
  }

  @Override public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
    holder.onBind(cityHighlightList.get(position));
  }

  @Override public int getItemCount() {
    return cityHighlightList.size();
  }

  public class ViewHolder extends RecyclerView.ViewHolder
      implements OnBindViewListener<CityHighlight> {

    @BindView(R.id.tvCityHighlightName) TextView tvCityHighlightName;
    @BindView(R.id.ivCityImage) ImageView ivCityImage;

    public ViewHolder(View itemView) {
      super(itemView);
      ButterKnife.bind(this, itemView);
    }

    @Override public void onBind(CityHighlight object) {

      tvCityHighlightName.setText(object.getName());


      if (object.getAvatarUrl() != null) {
        Glide.with(context)
            .load(object.getAvatarUrl())
            .centerCrop()
            .crossFade()
            .into(ivCityImage);
      }
    }
  }
}
