package com.essplore.essplore.ui.activities.timeline.view;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.location.Location;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.ImageView;
import android.widget.PopupMenu;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.essplore.essplore.R;
import com.essplore.essplore.api.requests.ChangeRecordStatusRequest;
import com.essplore.essplore.api.requests.RemovePlaceFromTrackRequest;
import com.essplore.essplore.application.AppConstants;
import com.essplore.essplore.application.EssploreApplication;
import com.essplore.essplore.managers.AppActivityManager;
import com.essplore.essplore.managers.SharedPreferenceManager;
import com.essplore.essplore.models.City;
import com.essplore.essplore.models.CollectionPicture;
import com.essplore.essplore.models.MyTrack;
import com.essplore.essplore.models.MyTrip;
import com.essplore.essplore.models.Timeline;
import com.essplore.essplore.models.TimelineMarker;
import com.essplore.essplore.models.TrackPicture;
import com.essplore.essplore.models.Tracks;
import com.essplore.essplore.ui.activities.HttpToolBarBaseActivity;
import com.essplore.essplore.ui.activities.addtotrip.api.AddToTripFromSearchApi;
import com.essplore.essplore.ui.activities.addtotrip.interfaces.AddTripFromSearchApiInterface;
import com.essplore.essplore.ui.activities.addtotrip.models.Trip;
import com.essplore.essplore.ui.activities.addtotrip.models.Trips;
import com.essplore.essplore.ui.activities.album.AlbumActivity;
import com.essplore.essplore.ui.activities.camera.CameraActivity;
import com.essplore.essplore.ui.activities.places.adapter.UserAction;
import com.essplore.essplore.ui.activities.timeline.adapter.TimelineAdapter;
import com.essplore.essplore.ui.activities.timeline.presenter.TimelinePresenterImpl;
import com.essplore.essplore.ui.utils.GeoLocationService;
import com.essplore.essplore.ui.utils.LocationListener;
import com.essplore.essplore.ui.utils.TextToSpeechService;
import com.essplore.essplore.ui.utils.TextToSpeechServiceInterface;
import com.google.gson.JsonElement;
import com.google.maps.DirectionsApi;
import com.google.maps.GeoApiContext;
import com.google.maps.errors.ApiException;
import com.google.maps.model.DirectionsResult;
import com.google.maps.model.LatLng;
import com.google.maps.model.TravelMode;

import org.joda.time.DateTime;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import javax.inject.Inject;

import ai.api.android.AIConfiguration;
import ai.api.model.AIError;
import ai.api.model.AIResponse;
import ai.api.model.Metadata;
import ai.api.model.Result;
import ai.api.model.Status;
import ai.api.ui.AIDialog;
import butterknife.BindView;
import butterknife.OnClick;
import io.reactivex.disposables.CompositeDisposable;

public class TimelineActivity extends HttpToolBarBaseActivity implements View.OnClickListener, TimeLineView, UserAction, LocationListener, TextToSpeechServiceInterface, AIDialog.AIDialogListener {

    @Inject
    AlertDialog alertDialog;
    @Inject
    AppActivityManager appActivityManager;
    @Inject
    CompositeDisposable compositeDisposable;
    @Inject
    TimelinePresenterImpl presenter;
    @Inject
    SharedPreferenceManager sharedPreferenceManager;

    @BindView(R.id.tv_TrackName)
    TextView mTrackName;
    @BindView(R.id.tv_TrackDate)
    TextView mTrackDate;
    @BindView(R.id.pbSearch)
    ProgressBar pbSearch;
    @BindView(R.id.ll_start_tour)
    LinearLayout ll_start_tour;
    private RecyclerView mRecyclerView;
    private TimelineAdapter mTimelineAdapter;
    //private List<Timeline> mDataList = new ArrayList<>();

    private MyTrack mMyTrack;
    private List<TrackPicture> trackPictureList;
    public List<Tracks> tracks;
    public static TimelineActivity instance;

    private GeoLocationService geoLocationService;
    private Location location;

    private TextToSpeechService textToSpeechService;

    public static final String CURRENT_PLACE_ID_INTENT_KEY = "current_place_id_intent";
    public static final String COLLECTION_PIC_INTENT_KEY = "collection_pics_intent";

    public int item_played;

    private AIDialog aiDialog;

    private ImageView micIv;

    @Override
    protected void setupActivityLayout() {
        setContentView(R.layout.activity_timeline);
    }

    @Override
    protected void setupViewElements() {
        instantiateTrackLists();
        mMyTrack = getMyTrack();

        /*geoLocationService = new GeoLocationService(this);
        geoLocationService.setLocationListener(this);*/
        mTrackName.setText(mMyTrack.getTrackName());
        try {
            mTrackDate.setText(mMyTrack.getDateFrom().substring(0, mMyTrack.getDateFrom().indexOf(" "))
                    + " - " + mMyTrack.getDateTo().substring(0, mMyTrack.getDateTo().indexOf(" ")));
        } catch (StringIndexOutOfBoundsException e) {
            mTrackDate.setText(mMyTrack.getDateFrom()
                    + " - " + mMyTrack.getDateTo());
        }
        mRecyclerView = (RecyclerView) findViewById(R.id.recyclerView);
        mRecyclerView.setHasFixedSize(true);
        instance = this;

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });
        getMicIv();
        enableMicPermission();
    }

    @Override
    protected void injectDaggerComponent() {
        EssploreApplication.get(this).createTimelineComponent(this).inject(this);
    }

    @Override
    protected void onResume() {
        super.onResume();
        instantiateTrackLists();
        if (mTimelineAdapter != null) {
            mTimelineAdapter.notifyDataSetChanged();
        }
    }

    @Override
    protected boolean isActionbarBackButtonEnabled() {
        return Boolean.TRUE;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @OnClick(R.id.img_additem)
    void AddTimelineItem() {
        Intent intent = new Intent(this, CameraActivity.class);
        Bundle bundle = new Bundle();
        bundle.putParcelable(AppConstants.MY_TRACK_OBJECT, getMyTrack());
        intent.putExtra(AppConstants.MY_TRACK_OBJECT, bundle);
        intent.putExtra(CURRENT_PLACE_ID_INTENT_KEY, getCurrentPlaceId());
        this.startActivity(intent);
    }

    /*public void (Bitmap imgUrl) {
        Timeline timeaddTimelineItemline = new Timeline(imgUrl, TimelineMarker.GALLERY);
        mDataList.add(2, timeline);
        mDataList.add(3, new Timeline(imgUrl, TimelineMarker.NOTIF));
        mDataList.add(4, new Timeline(imgUrl, TimelineMarker.GAME));
        mTimelineAdapter.notifyDataSetChanged();
    }*/

    private void initView(/*Location location*/) {
        boolean isVisitedAll = false;
        LinearLayoutManager horizontalLayoutManagaer = new LinearLayoutManager(TimelineActivity.this, LinearLayoutManager.HORIZONTAL, false);
        mRecyclerView.setLayoutManager(horizontalLayoutManagaer);
        mTimelineAdapter = new TimelineAdapter(this, tracks, onClickListenerTimeline, mOnLongClickListenerTimeline);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(getBaseContext()));
        mRecyclerView.setAdapter(mTimelineAdapter);
        mRecyclerView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.e("On Click", mMyTrack.getTrackName());
            }
        });

        for (Tracks track : this.tracks) {
            if (track.getStatus() != null && !track.getStatus().equals("Visited")) {
                isVisitedAll = false;
            } else {
                isVisitedAll = true;
            }
        }

        if (isVisitedAll) {
            ll_start_tour.setVisibility(View.GONE);
        } else {
            ll_start_tour.setVisibility(View.VISIBLE);
        }
    }

    public MyTrack getMyTrack() {
        Bundle bundle = getIntent().getBundleExtra(AppConstants.MY_TRACK_OBJECT);
        return bundle.getParcelable(AppConstants.MY_TRACK_OBJECT);
    }


    private void instantiateTrackLists() {
        trackPictureList = new ArrayList<>();
        tracks = new ArrayList<>();
        trackPictureList.clear();
        compositeDisposable.add(presenter.getSpecificTrack(getMyTrack().getTrackId()));
    }

    @Override
    public void onNetworkErrorFound(String message) {
        showAlertDialog(message);
    }

    @Override
    public void onHttpErrorUnexpectedFound(String message) {
        showAlertDialog(message);
    }

    @Override
    public void onApiErrorFound(String message) {
        showAlertDialog(message);
    }

    @Override
    public void onHttpErrorUnexpectedFound() {
        showAlertDialog(getString(R.string.unexpected_error_occurred));
    }

    @Override
    public void onUnauthorizedErrorFound() {
        showAlertDialog(getString(R.string.unauthorized));
    }

    @Override
    public CompositeDisposable getCompositeDisposable() {
        return compositeDisposable;
    }

    @Override
    protected Activity getActivity() {
        return this;
    }

    @Override
    protected AlertDialog getAlertDialog() {
        return alertDialog;
    }

    @Override
    public MyTrip getMyTrip() {
        return getIntent().getParcelableExtra(AppConstants.CREATE_TRACK_RESPONSE_OBJECT);
    }

    @Override
    public City buildCityObject() {
        return null;
    }

    @Override
    public int getCountryId() {
        return getIntent().getIntExtra(AppConstants.GET_COUNTRY_ID, 0);
    }

    @Override
    public int getCityId() {
        return getIntent().getIntExtra(AppConstants.CITY_ID, 0);
    }

    @Override
    public void showProgressbar() {
        pbSearch.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideProgressbar() {
        pbSearch.setVisibility(View.GONE);
    }

    @Override
    public String provideToken() {
        return sharedPreferenceManager.getAccessToken();
    }

    @Override
    public void setTrackPictureList(List<TrackPicture> trackPictureList) {
        this.trackPictureList.clear();
        this.trackPictureList.addAll(trackPictureList);
    }

    @Override
    public void setTracksList(List<Tracks> tracksList) {
        this.tracks.clear();
        this.tracks.addAll(tracksList);
        initView();
    }

    @Override
    public void launchCityHighlightsForViewing(Tracks tracks) {
        appActivityManager.launchCityHighlightsForViewing(this, tracks, buildCityObject());
    }

    @Override
    public RemovePlaceFromTrackRequest buildRequest(int trackPlaceId) {
        return new RemovePlaceFromTrackRequest(trackPlaceId);
    }

    @Override
    public ChangeRecordStatusRequest buildRequest(int trackPlaceId, int trackStatusId) {
        return new ChangeRecordStatusRequest(trackPlaceId, trackStatusId);
    }

    @Override
    public void onDeleteItem(int position) {

        /*AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(getActivity(), R.style.ExitDialog);
        AlertDialog deleteDialog = dialogBuilder.create();
        deleteDialog.setMessage(getString(R.string.are_you_sure_you_want_to_delete));
        deleteDialog.setButton(DialogInterface.BUTTON_POSITIVE, getString(R.string.yes), (dialog, which) -> {
            compositeDisposable.add(presenter.deletePlaceFromTrack());
            deleteDialog.dismiss();
        });
        deleteDialog.setButton(DialogInterface.BUTTON_NEGATIVE, getString(R.string.no), (dialog, which) -> {
            deleteDialog.dismiss();
        });
        deleteDialog.show();*/

    }

    @Override protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == AppConstants.PLACE_HIGHLIGHT_REQUEST && resultCode == RESULT_OK) {
            compositeDisposable.add(presenter.getSpecificTrack(mMyTrack.getTrackId()));
        }
    }

    public void goToDorections(int track_id){
        appActivityManager.launchDirections(this, track_id);
    }

    private View.OnClickListener onClickListenerTimeline = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            TimelineActivity timelineActivity = (TimelineActivity) getActivity();
            Tracks track = (Tracks) v.getTag();
            CollectionPicture picture = track.getCollectionPictureList().get(track.getCollectionPictureList().size()-1);

            if (track.getCategory().equals("Pictures")) {
                Intent intent = new Intent(timelineActivity, AlbumActivity.class);
                Bundle bundle = new Bundle();
                bundle.putParcelable(AppConstants.MY_TRACK_OBJECT, getMyTrack());
                intent.putExtra(AppConstants.MY_TRACK_OBJECT, bundle);
                intent.putExtra(COLLECTION_PIC_INTENT_KEY, picture);
                timelineActivity.startActivity(intent);
            }
        }
    };

    private View.OnLongClickListener mOnLongClickListenerTimeline = new View.OnLongClickListener() {
        @Override
        public boolean onLongClick(View v) {

            return true;
        }
    };

    @Override
    public void onLocationUpdate(Location location) {
        if (this. location == null) {
            this.location = location;
            //setDataListItems(location, this.tracks);
            initView(/*location*/);
        }
    }

    @Override
    public void onLocationChanged(Location location) {
        //setTimelineView(location);
    }

    /*private void setTimelineView(Location location) {
        final Timeline[] timelineTo = new Timeline[2];
        Location locationFrom = new Location("Point from");
        Location locationTo = new Location("Point to");

        if (location != null) {
            Double latitude = location.getLatitude();
            Double longitude = location.getLongitude();
            if (tracks != null) {
                for (Timeline timeline : mDataList) {
                    if (timeline.getmStatus() != null && !timeline.getmStatus().equals("Visited")) {
                        timelineTo[1] = timeline;
                        break;
                    } else {
                        timelineTo[0] = timeline;
                        continue;
                    }
                }
            }

            if (latitude != null && longitude != null && timelineTo[0] != null) {
                Log.e("Location", "Lat = " + latitude + " " + "lng = " + longitude);
                locationFrom = location;
                locationTo.setLatitude(Double.valueOf(timelineTo[0].getmLatTo()));
                locationTo.setLongitude(Double.valueOf(timelineTo[0].getmLngTo()));
                Log.e("Location", "distance " + " : " +
                        String.valueOf(getDistance(locationFrom, locationTo)));
                if (getDistance(locationFrom, locationTo) < 50f) {
                    for (Tracks tracks : tracks) {
                        if (Double.valueOf(tracks.getLat()) == locationTo.getLatitude() &&
                                Double.valueOf(tracks.getLng()) == locationTo.getLongitude()) {
                            Log.e("tracks.getTrackRecordId", String.valueOf(tracks.getTrackRecordId()));
                            compositeDisposable.add(presenter.changeRecordStatus(tracks.getTrackRecordId(), 2));
                            mDataList.get(mDataList.indexOf(timelineTo[1])).setmStatus("Visited");
                            mTimelineAdapter.notifyItemChanged(mDataList.indexOf(timelineTo[1]));
                            startGuide(tracks.getGuideSpeach());
                        }
                    }
                }
            }
        }
    }*/

    private double getDistance(Location locationFrom, Location locationTo) {
        LatLng origin = new LatLng(locationFrom.getLatitude(), locationFrom.getLongitude());
        LatLng destination = new LatLng(locationTo.getLatitude(), locationTo.getLongitude());

        DirectionsResult results = getDirectionsDetails(origin,destination, TravelMode.WALKING);
        if (results != null) {
            return results.routes[0].legs[0].distance.inMeters;
        } else {
            return 1000d;
        }
    }

    private DirectionsResult getDirectionsDetails(LatLng origin, LatLng destination, TravelMode mode) {
        DateTime now = new DateTime();
        try {
            return DirectionsApi.newRequest(getGeoContext())
                    .mode(mode)
                    .origin(origin)
                    .destination(destination)
                    .departureTime(now)
                    .await();
        } catch (ApiException e) {
            e.printStackTrace();
            return null;
        } catch (InterruptedException e) {
            e.printStackTrace();
            return null;
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }

    private GeoApiContext getGeoContext() {
        GeoApiContext geoApiContext = new GeoApiContext();
        return geoApiContext
                .setQueryRateLimit(3)
                .setApiKey(getString(R.string.google_maps_key))
                .setConnectTimeout(1, TimeUnit.SECONDS)
                .setReadTimeout(1, TimeUnit.SECONDS)
                .setWriteTimeout(1, TimeUnit.SECONDS);
    }

    private String getCurrentPlaceId(){

        for (int i=0; i < tracks.size(); i++) {

            if (tracks.get(i).getStatus() != null) {
                if (tracks.get(i).getStatus().equalsIgnoreCase(getString(R.string.place_visited_status))) {
                    return Integer.toString(tracks.get(i).getPlaceId());
                }
            }
            return "";
        }
        return "";

    }


    public void startTour(View view) {
        goToDorections(getMyTrack().getTrackId());
    }


    public void startGuide(String message){

        if(textToSpeechService == null){

            textToSpeechService = new TextToSpeechService(this);
            textToSpeechService.setTextToSpeechServiceInterface(this);
        }

        textToSpeechService.startGuide(message);

    }

    public void stopGuide() {
        if (textToSpeechService != null) {
            textToSpeechService.stopGuide();
        }
    }


    @Override
    public void onDoneSpeak() {
        mTimelineAdapter.notifyItemChanged(item_played);
    }

    @Override
    public void onStartSpeak() {

    }

    public void backPressed(View view) {
        onBackPressed();
    }

    public void atractionClick(int position) {
        Tracks track = this.tracks.get(position);
        if (track.getCategory().equals("Attraction")) {
            appActivityManager.launchCityHighlightsForViewing(this, track, buildCityObject());
        }
    }

    public void attractionLongClick(View v, int position) {
        Tracks track = this.tracks.get(position);
        if (track.getCategory().equals("Attraction")) {
            if (track.getAddress1() != null && track.getName() != null) {
                PopupMenu popupMenu = new PopupMenu(this, v);
                popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem item) {
                        switch (item.getItemId()) {
                            case R.id.track_visited:
                                compositeDisposable.add(presenter.changeRecordStatus(track.getTrackRecordId(), 2));
                                for (int i = 0; i < instance.tracks.size(); i++) {
                                    if (instance.tracks.get(i) == track) {
                                        track.setStatus("Visited");
                                        //mDataList.set(i, timeline);
                                        instance.mTimelineAdapter.notifyDataSetChanged();
                                        break;
                                    }
                                }
                                Log.e("Visited", String.valueOf(track.getStatus()));
                                instantiateTrackLists();
                                initView();
                                if (mTimelineAdapter != null) {
                                    mTimelineAdapter.notifyDataSetChanged();
                                }
                                return true;

                            case R.id.track_delete:
                                AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(getActivity(), R.style.ExitDialog);
                                AlertDialog deleteDialog = dialogBuilder.create();
                                deleteDialog.setMessage(getString(R.string.are_you_sure_you_want_to_delete));
                                deleteDialog.setButton(DialogInterface.BUTTON_POSITIVE, getString(R.string.yes), (dialog, which) -> {
                                    compositeDisposable.add(presenter.deletePlaceFromTrack(track.getTrackRecordId()));
                                    deleteDialog.dismiss();
                                    //mDataList.clear();
                                    instance.tracks.remove(0);
                                    instance.tracks.removeIf((Tracks t) -> t.getTrackRecordId() == track.getTrackRecordId());
                                    List<Tracks> updatedTracks = instance.tracks;
                                    //timelineActivity.setDataListItems(timelineActivity.location, updatedTracks);
                                    instance.mTimelineAdapter.notifyDataSetChanged();
                                });
                                deleteDialog.setButton(DialogInterface.BUTTON_NEGATIVE, getString(R.string.no), (dialog, which) -> {
                                    deleteDialog.dismiss();
                                });
                                deleteDialog.show();
                                return true;

                            default:
                                return false;
                        }
                    }
                });
                popupMenu.inflate(R.menu.menu_track_status);
                popupMenu.show();
            }
        }
    }

    @Override
    public void onResult(AIResponse response) {

        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                final Result result = response.getResult();
                TextToSpeechService textToSpeechService = new TextToSpeechService(getContext());
                textToSpeechService.setTextToSpeechServiceInterface(new TextToSpeechServiceInterface() {
                    @Override
                    public void onDoneSpeak() {
                        aiDialog.showAndListen();
                    }

                    @Override
                    public void onStartSpeak() {

                    }
                });
//                textToSpeechService.setSpeakType(TextToSpeechService.SpeakType.AI_SPEAK);
//                textToSpeechService.setTextToSpeechServiceInterface(get);
                textToSpeechService.startGuide(result.getFulfillment().getSpeech());
//                String text = result.getFulfillment().getDisplayText();
                String PLACE_ID = "place_id";
                String URGENCY = "urgency";
                if (result.getFulfillment().getDisplayText() != null) {
                    try {
                        JSONObject jsonObject = new JSONObject(result.getFulfillment().getDisplayText());
                        int placeId = jsonObject.isNull(PLACE_ID) ? 0 : jsonObject.getInt(PLACE_ID);
                        String urgency = jsonObject.isNull(URGENCY) ? "" : jsonObject.getString(URGENCY);
                        Log.i("json", placeId + " == " + urgency);
                        AddToTripFromSearchApi addToTripFromSearchApi = new AddToTripFromSearchApi(getContext());
                        addToTripFromSearchApi.setAddTripFromSearchApiInterface(new AddTripFromSearchApiInterface() {
                            @Override
                            public void onGetTripsSuccess(String response) {

                                compositeDisposable.add(presenter.getSpecificTrack(mMyTrack.getTrackId()));

                            }

                            @Override
                            public void onAddPlaceToTrackSuccess() {
//                                Log.i("chatbotka", result.getFulfillment().getDisplayText());
                                addToTripFromSearchApi.getGetTrips();
                            }

                            @Override
                            public void onAddPlaceToTrackFailed() {
//                                Log.i("failedchat", result.getFulfillment().getDisplayText());
                            }
                        });
                        addToTripFromSearchApi.addPlaceToTripFromAi(getMyTrack().getTrackId(), placeId, urgency);
                    } catch (JSONException e) {
                        e.printStackTrace();
//                        Log.i("errorjson", result.getFulfillment().getDisplayText());
                    }

                }

//                Toast.makeText(getContext(), result.getFulfillment().getSpeech(), Toast.LENGTH_SHORT).show();
//                Log.d(TAG, "onResult");
//
//                resultTextView.setText(gson.toJson(response));
//
//                Log.i(TAG, "Received success response");
//
//                // this is example how to get different parts of result object
//                final Status status = response.getStatus();
//                Log.i(TAG, "Status code: " + status.getCode());
//                Log.i(TAG, "Status type: " + status.getErrorType());
//
//                final Result result = response.getResult();
//                Log.i(TAG, "Resolved query: " + result.getResolvedQuery());
//
//                Log.i(TAG, "Action: " + result.getAction());
//                final String speech = result.getFulfillment().getSpeech();
//                Log.i(TAG, "Speech: " + speech);
//                TTS.speak(speech);
//
//                final Metadata metadata = result.getMetadata();
//                if (metadata != null) {
//                    Log.i(TAG, "Intent id: " + metadata.getIntentId());
//                    Log.i(TAG, "Intent name: " + metadata.getIntentName());
//                }
//
//                final HashMap<String, JsonElement> params = result.getParameters();
//                if (params != null && !params.isEmpty()) {
//                    Log.i(TAG, "Parameters: ");
//                    for (final Map.Entry<String, JsonElement> entry : params.entrySet()) {
//                        Log.i(TAG, String.format("%s: %s", entry.getKey(), entry.getValue().toString()));
//                    }
//                }
            }

        });

    }

    @Override
    public void onError(AIError error) {

        Toast.makeText(this, error.toString(), Toast.LENGTH_SHORT).show();

    }

    @Override
    public void onCancelled() {

        Toast.makeText(this, "", Toast.LENGTH_SHORT).show();

    }

    @Override
    public void onClick(View v) {

        switch (v.getId()){

            case R.id.mic_iv:

                getAiDialog().showAndListen();

                return;

                default:

        }

    }

    public ImageView getMicIv() {
        if (micIv == null) {

            micIv = findViewById(R.id.mic_iv);
            micIv.setOnClickListener(this);

        }
        return micIv;
    }

    public AIDialog getAiDialog() {

        if (aiDialog == null){

            final AIConfiguration config = new AIConfiguration("93173350244f48e281f1057392cc7cb5",
                    AIConfiguration.SupportedLanguages.English,
                    AIConfiguration.RecognitionEngine.System);
            aiDialog = new AIDialog(this, config);
            aiDialog.setResultsListener(this);

        }

        return aiDialog;
    }

    private void enableMicPermission(){

        if (ContextCompat.checkSelfPermission(getActivity(),
                Manifest.permission.RECORD_AUDIO) != PackageManager.PERMISSION_GRANTED) {

            ActivityCompat.requestPermissions(getActivity(),
                    new String[]{Manifest.permission.RECORD_AUDIO},
                    1);

        }

    }

    private Context getContext(){

        return this;

    }
}
