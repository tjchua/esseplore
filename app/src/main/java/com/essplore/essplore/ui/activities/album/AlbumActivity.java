package com.essplore.essplore.ui.activities.album;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import com.essplore.essplore.R;
import com.essplore.essplore.application.AppConstants;
import com.essplore.essplore.models.CollectionPicture;
import com.essplore.essplore.models.MyTrack;
import com.essplore.essplore.models.Timeline;
import com.essplore.essplore.models.Tracks;
import com.essplore.essplore.ui.activities.album.adapters.AlbumAdapter;
import com.essplore.essplore.ui.activities.album.asynctask.GetAlbumItemsTask;
import com.essplore.essplore.ui.activities.album.interfaces.ApiResponseListener;
import com.essplore.essplore.ui.activities.album.models.Album;
import com.essplore.essplore.ui.activities.timeline.view.TimelineActivity;

import java.util.ArrayList;

public class AlbumActivity extends AppCompatActivity implements View.OnClickListener, ApiResponseListener {

    private TextView albumTitle;
    private TextView albumDateCreated;
    private TextView albumDescription;
    private TextView albumShowMoreDescription;
    private ImageView albumBackBtn;
    private GetAlbumItemsTask getAlbumItemsTask;
    private ArrayList<Album> albumItems;
    private AlbumAdapter albumAdapter;
    private RecyclerView recyclerView;
    private int albumDescriptionLineCount;
    private final int DESCRIPTION_MAX_LINES = 5;
    public static Drawable SELECTED_IMAGE;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_album);
        albumItems = new ArrayList<Album>();
        albumAdapter = new AlbumAdapter(albumItems, this);
        initViews();
//        setDummyData();
        checkIfDescriptionReachesMax();
        startGetAlbumItemsTask();

    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {

            case R.id.album_back_btn:

                finishActivity();

                break;

            case R.id.album_description:

                formatDescription();

                break;

            case R.id.album_item_image:

                SELECTED_IMAGE = ((ImageView) v).getDrawable();
                showImageFullViewActivity();

                break;

            default:

        }
    }

    @Override
    public void onResponseSucess(ArrayList<Album> albumItems) {

        this.albumItems.addAll(albumItems);
        albumAdapter.notifyDataSetChanged();

    }

    @Override
    public void onResponseFailed() {

    }

    private void initViews() {

        albumShowMoreDescription = findViewById(R.id.album_see_more_desc);
        albumTitle = findViewById(R.id.album_title);
        albumTitle.setText(getMyTrack().getTrackName());
        albumDateCreated = findViewById(R.id.album_date_created);
        albumDateCreated.setText(getCollectionPic().getCreatedAt());
        albumDescription = findViewById(R.id.album_description);
        albumDescription.setText(getCollectionPic().getDescription());
        albumDescription.setOnClickListener(this);
        albumBackBtn = findViewById(R.id.album_back_btn);
        recyclerView = findViewById(R.id.recycler_view);
        recyclerView.setHasFixedSize(true);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(this.getApplicationContext(),
                LinearLayoutManager.VERTICAL, false);
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setAdapter(albumAdapter);
        albumBackBtn.setOnClickListener(this);

    }

    private void setDummyData() {

        albumTitle.setText("Fun day in Paris!");
        albumDateCreated.setText("JULY 07, 2018");
        albumDescription.setText("Lorem Ipsum is simply dummy text of the printing and " +
                "typesetting industry. Lorem Ipsum has been the industry's standard dummy " +
                "text ever since the 1500s, when an unknown printer took a galley of type " +
                "and scrambled it to make a type specimen book. It has survived not only " +
                "five centuries, but also the leap into electronic typesetting, remaining " +
                "essentially unchanged. It was popularised in the 1960s with the release " +
                "of Letraset sheets containing Lorem Ipsum passages, and more recently " +
                "with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.");

    }

    private void finishActivity() {

        finish();

    }

    private void startGetAlbumItemsTask() {

        getAlbumItemsTask = new GetAlbumItemsTask(this, getMyTrack().getTrackId());
        getAlbumItemsTask.setResponseListener(this);
        getAlbumItemsTask.execute();

    }

    private void formatDescription() {

        if (albumDescriptionLineCount > DESCRIPTION_MAX_LINES) {

            if (albumDescription.getMaxLines() == DESCRIPTION_MAX_LINES) {

                albumDescription.setMaxLines(Integer.MAX_VALUE);
                albumShowMoreDescription.setText(R.string.see_less);

            } else {

                albumDescription.setMaxLines(DESCRIPTION_MAX_LINES);
                albumShowMoreDescription.setText(R.string.see_more);
                albumShowMoreDescription.setVisibility(View.VISIBLE);

            }

        }

    }

    private void checkIfDescriptionReachesMax() {

        albumDescription.post(new Runnable() {

            @Override
            public void run() {

                albumDescriptionLineCount = albumDescription.getLineCount();
                formatDescription();

            }

        });
    }

    private void showImageFullViewActivity() {

        Intent showImageFullViewIntent = new Intent(this, ImageFullViewActivity.class);
        startActivity(showImageFullViewIntent);

    }

    private MyTrack getMyTrack() {
        Bundle bundle = getIntent().getBundleExtra(AppConstants.MY_TRACK_OBJECT);
        return bundle.getParcelable(AppConstants.MY_TRACK_OBJECT);
    }

    private CollectionPicture getCollectionPic() {

        return getIntent().getParcelableExtra(TimelineActivity.COLLECTION_PIC_INTENT_KEY);

    }

}