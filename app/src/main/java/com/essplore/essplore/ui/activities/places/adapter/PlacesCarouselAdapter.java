package com.essplore.essplore.ui.activities.places.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import butterknife.BindView;
import butterknife.ButterKnife;
import com.bumptech.glide.Glide;
import com.essplore.essplore.R;
import com.essplore.essplore.models.TrackPicture;
import com.essplore.essplore.ui.utils.OnBindViewListener;
import java.util.List;
import org.apache.commons.lang3.StringUtils;

public class PlacesCarouselAdapter extends RecyclerView.Adapter<PlacesCarouselAdapter.ViewHolder> {

  private Context context;
  private List<TrackPicture> trackPictures;

  public PlacesCarouselAdapter(Context context, List<TrackPicture> trackPictures) {
    this.context = context;
    this.trackPictures = trackPictures;
  }

  @NonNull @Override public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
    return new ViewHolder(LayoutInflater.from(context).inflate(R.layout.item_city_highlights, parent, false));
  }

  @Override public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
    holder.onBind(trackPictures.get(position));
  }

  @Override public int getItemCount() {
    return trackPictures.size();
  }

  public class ViewHolder extends RecyclerView.ViewHolder implements OnBindViewListener<TrackPicture> {

    @BindView(R.id.ivCityImage) ImageView ivCityImage;

    public ViewHolder(View itemView) {
      super(itemView);
      ButterKnife.bind(this, itemView);
    }

    @Override public void onBind(TrackPicture object) {
      if (object.getTrackPictureUrl() != null && StringUtils.isNotEmpty(object.getTrackPictureUrl())) {
        Glide.with(context).load(object.getTrackPictureUrl()).centerCrop().into(ivCityImage);
      }
    }
  }
}
