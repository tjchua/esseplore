package com.essplore.essplore.ui.activities.album.interfaces;

import com.essplore.essplore.ui.activities.album.models.Album;
import java.util.ArrayList;

public interface ApiResponseListener {

    public void onResponseSucess(ArrayList<Album> albumItems);
    public void onResponseFailed();

}
