package com.essplore.essplore.ui.activities.placehighlights.presenter;

import com.essplore.essplore.api.managers.ApiManager;
import com.essplore.essplore.api.requests.CityHighlightRequest;
import com.essplore.essplore.api.response.CityHighlightResponse;
import com.essplore.essplore.api.utils.SimpleObserver;
import com.essplore.essplore.models.CityHighlight;
import com.essplore.essplore.ui.activities.HttpBasePresenter;
import com.essplore.essplore.ui.activities.placehighlights.view.PlaceHighlightsView;
import com.essplore.essplore.ui.utils.OnSingleItemClickListener;
import io.reactivex.disposables.Disposable;

public class PlaceHighlightsPresenterImpl extends HttpBasePresenter
    implements PlaceHighlightsPresenter, OnSingleItemClickListener<CityHighlight> {

  private final PlaceHighlightsView view;
  private final ApiManager manager;

  public PlaceHighlightsPresenterImpl(PlaceHighlightsView view, ApiManager manager) {
    this.view = view;
    this.manager = manager;
  }

  @Override public Disposable getCityHighlights() {
    view.showProgressBar();
    return manager.getCityHighlight(buildCityHighlightsRequest())
        .subscribe(new SimpleObserver<CityHighlightResponse>() {
          @Override public void accept(CityHighlightResponse cityHighlightResponse) {
            view.hideProgressbar();
            view.setPlaceHighlights(cityHighlightResponse.getCityHighlightList());
          }
        }, throwable -> {
          view.hideProgressbar();
          handleHttpError(view, throwable);
        });
  }

  @Override public CityHighlightRequest buildCityHighlightsRequest() {
    return new CityHighlightRequest(view.provideCityObject().getId(), view.provideCityObject().getCountryId());
  }

  @Override public void onSingleItemClick(CityHighlight object) {
    view.launchCityHighlights(object);
  }
}
