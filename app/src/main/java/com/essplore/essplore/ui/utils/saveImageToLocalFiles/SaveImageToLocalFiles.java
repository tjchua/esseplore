package com.essplore.essplore.ui.utils.saveImageToLocalFiles;

import android.content.Context;
import android.graphics.Bitmap;
import android.util.Log;

import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.util.Calendar;
import java.util.Date;

public class SaveImageToLocalFiles {

    private Context context;
    private final String JPEG_EXT = ".jpeg";
    private SaveImageToLocalFilesInterface saveImageToLocalFilesInterface;

    public SaveImageToLocalFiles(Context context){

        this.context = context;

    }

    public void convertBitmapToFile(Bitmap bitmap){

        //create a file to write bitmap data
        String filename = getCurrentTime();
        File file = new File(context.getCacheDir(), filename + JPEG_EXT);

        Bitmap bitmapToSave = bitmap;

        OutputStream os;

        try {

            os = new FileOutputStream(file);
            bitmapToSave.compress(Bitmap.CompressFormat.JPEG, 100, os);
            os.flush();
            os.close();

        } catch (Exception e) {

            if (saveImageToLocalFilesInterface != null)
            saveImageToLocalFilesInterface.onSaveToLocalError();
            return;

        }

        if (saveImageToLocalFilesInterface != null)
            saveImageToLocalFilesInterface.onSaveToLocalSuccess(file);

    }

    private String getCurrentTime(){

        Date currentTime = Calendar.getInstance().getTime();
        return currentTime.toString();

    }

    public void setSaveImageToLocalFilesInterface(SaveImageToLocalFilesInterface saveImageToLocalFilesInterface){

        this.saveImageToLocalFilesInterface = saveImageToLocalFilesInterface;

    }

}
