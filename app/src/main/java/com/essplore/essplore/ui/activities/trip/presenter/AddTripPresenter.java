package com.essplore.essplore.ui.activities.trip.presenter;

import com.essplore.essplore.api.requests.CreateTrackRequest;
import com.essplore.essplore.models.City;

public interface AddTripPresenter {

  CreateTrackRequest buildCreateTrackRequest(City city);
}