package com.essplore.essplore.ui.utils;

public interface OnSingleItemClickListener<T> {

  void onSingleItemClick(T object);
}
