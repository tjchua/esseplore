package com.essplore.essplore.ui.activities.directions.view;

import android.app.Activity;
import android.app.FragmentTransaction;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.graphics.Color;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.PopupMenu;
import android.widget.TextView;

import com.essplore.essplore.R;
import com.essplore.essplore.api.requests.ChangeRecordStatusRequest;
import com.essplore.essplore.api.requests.RemovePlaceFromTrackRequest;
import com.essplore.essplore.application.AppConstants;
import com.essplore.essplore.application.EssploreApplication;
import com.essplore.essplore.managers.AppActivityManager;
import com.essplore.essplore.managers.SharedPreferenceManager;
import com.essplore.essplore.models.City;
import com.essplore.essplore.models.MyTrip;
import com.essplore.essplore.models.TrackPicture;
import com.essplore.essplore.models.Tracks;
import com.essplore.essplore.ui.activities.HttpToolBarBaseActivity;
import com.essplore.essplore.ui.activities.directions.adapter.DirectionsAdapter;
import com.essplore.essplore.ui.activities.directions.presenter.DirectionsPresenterImpl;
import com.essplore.essplore.ui.activities.timeline.adapter.TimelineAdapter;
import com.essplore.essplore.ui.utils.GeoLocationService;
import com.essplore.essplore.ui.utils.LocationListener;
import com.essplore.essplore.ui.utils.TextToSpeechService;
import com.essplore.essplore.ui.utils.TextToSpeechServiceInterface;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.UiSettings;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;
import com.google.maps.DirectionsApi;
import com.google.maps.GeoApiContext;
import com.google.maps.android.PolyUtil;
import com.google.maps.errors.ApiException;
import com.google.maps.model.DirectionsLeg;
import com.google.maps.model.DirectionsResult;
import com.google.maps.model.DirectionsStep;
import com.google.maps.model.LatLng;
import com.google.maps.model.TravelMode;

import org.joda.time.DateTime;
import org.w3c.dom.Text;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.TimeUnit;

import javax.inject.Inject;

import butterknife.BindView;
import io.reactivex.disposables.CompositeDisposable;

public class DirectionsActivity extends HttpToolBarBaseActivity implements OnMapReadyCallback, LocationListener, TextToSpeechServiceInterface, DirectionsView {

    @Inject
    AlertDialog alertDialog;
    @Inject
    AppActivityManager appActivityManager;
    @Inject
    CompositeDisposable compositeDisposable;
    @Inject
    DirectionsPresenterImpl presenter;
    @Inject
    SharedPreferenceManager sharedPreferenceManager;
    private GoogleMap mMap;
    private ProgressDialog dialog;
    private static final int REQUEST_LOCATION = 1;
    private LocationManager locationManager;
    String lat, lng;
    public Criteria criteria;
    public String bestProvider;
    private List<Tracks> tracks;
    private static final int overview = 0;
    private RecyclerView mRecyclerView;
    private DirectionsAdapter mDirectionsAdapter;
    private SupportMapFragment mapFragment;
    private GeoLocationService geoLocationService;
    @BindView(R.id.tv_goingto) TextView tv_goingto;
    @BindView(R.id.tv_goingaddress) TextView tv_goingaddress;
    @BindView(R.id.tv_nextAddress) TextView tv_nextaddress;
    @BindView(R.id.rv_directions) RecyclerView rv_directions;
    private Location location;
    private ImageView imgPlace, imgPlace1;
    private TextView txPlaceName, txPlaceName1, txPlaceAddress, txPlaceAddress1, dist_direction;
    private boolean isAttractionNext;
    private TextToSpeechService textToSpeechService;
    private TimelineAdapter mTimelineAdapter;
    private boolean isCameraPosition = false;
    private int scrollPosition = 0;

    private boolean isDescrptionPlayed = false;

    public static DirectionsActivity instance;
    private Polyline polyline = null;
    GeoApiContext geoApiContext = null;
    private Tracks trackGoingTo = null, trackNext = null;
    private DirectionsResult results;

    private int track_id = 0;

    public int item_played;
    private List<Marker> markers = new ArrayList<>();


    @Override
    protected void setupActivityLayout() {
        setContentView(R.layout.activity_directions);
    }

    @Override
    protected void setupViewElements() {
        if(geoLocationService == null) {
            geoLocationService = new GeoLocationService(this);
            geoLocationService.setLocationListener(this);
        }
        mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
        track_id = getIntent().getIntExtra(AppConstants.TRACKS_OBJECT, 0);
        //tracks.removeIf((Tracks t) -> t.getCategory().equals("Pictures"));
        this.isAttractionNext = getIntent().getBooleanExtra(AppConstants.ISATTRACTIONNEXT, false);
        mRecyclerView = (RecyclerView) findViewById(R.id.recyclerView);
        instance = this;
        instantiateTrackLists();
    }

    private void instantiateTrackLists() {
        tracks = new ArrayList<>();
        compositeDisposable.add(presenter.getSpecificTrack(track_id));
    }

    @Override
    protected boolean isActionbarBackButtonEnabled() {
        return false;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // Obtain the SupportMapFragment and get notified when the map is ready to be used.

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        geoLocationService.removeLocationUpdate();
    }

    @Override
    protected void injectDaggerComponent() {
        EssploreApplication.get(this).createDirectionsComponent(this).inject(this);
    }


    private void initView() {
        LinearLayoutManager horizontalLayoutManagaer = new LinearLayoutManager(DirectionsActivity.this,
                LinearLayoutManager.HORIZONTAL, false);
        mRecyclerView.setLayoutManager(horizontalLayoutManagaer);
        tracks.removeIf((Tracks t) -> t.getCategory() == null);
        mTimelineAdapter = new TimelineAdapter(this, tracks, onClickListenerTimeline, mOnLongClickListenerTimeline);
        mRecyclerView.setAdapter(mTimelineAdapter);
        mRecyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
                if (rv_directions.getVisibility() != View.VISIBLE && newState == RecyclerView.SCROLL_STATE_IDLE){
                    int position = getCurrentItem();
                    if (position != scrollPosition && tracks != null && markers != null) {
                        scrollPosition = position;
                        for (int i = 0; i < markers.size(); i++) {
                            if (markers.get(i) != null) {
                                if (i == position) {
                                    try {
                                        if (tracks.get(i).getStatus().equals("Visited")) {
                                            markers.get(i).setIcon(BitmapDescriptorFactory.fromResource(R.drawable.map_grey_selected));
                                        } else {
                                            markers.get(i).setIcon(BitmapDescriptorFactory.fromResource(R.drawable.map_green_selected));
                                        }
                                    } catch (Exception e) {

                                    }
                                } else {
                                    try {
                                        if (i < markers.size()) {
                                            if (tracks.get(i).getStatus().equals("Visited")) {
                                                markers.get(i).setIcon(BitmapDescriptorFactory.fromResource(R.drawable.map_grey));
                                            } else {
                                                markers.get(i).setIcon(BitmapDescriptorFactory.fromResource(R.drawable.map_green));
                                            }
                                        }
                                    } catch (Exception e) {

                                    }
                                }
                            }
                        }
                    }
                }
            }
        });

    }
    private int getCurrentItem(){
        return ((LinearLayoutManager)mRecyclerView.getLayoutManager())
                .findFirstVisibleItemPosition();
    }

    private DirectionsResult getDirectionsDetails(LatLng origin, LatLng destination, TravelMode mode, LatLng[] waypoints) {
        DateTime now = new DateTime();
        if (geoApiContext == null) {
            geoApiContext = getGeoContext();
        }
        try {
            return DirectionsApi.newRequest(geoApiContext)
                    .mode(mode)
                    .origin(origin)
                    .destination(destination)
                    .waypoints(waypoints)
                    .departureTime(now)
                    .await();
        } catch (ApiException e) {
            e.printStackTrace();
            return null;
        } catch (InterruptedException e) {
            e.printStackTrace();
            return null;
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        this.mMap = googleMap;
        initView();
    }

    private void updateDirectionLine(Location locationUpdate) {
        this.location = locationUpdate;
        int i = 0;
        for (Tracks tracks1 : tracks) {
            if (tracks1.getStatus() != null && !tracks1.getStatus().equals("Visited")) {
                if (trackGoingTo != null && i == 0 && trackGoingTo == tracks1) {
                    break;
                } else {
                    if (i == 0) {
                        trackGoingTo = tracks1;
                        i++;
                        continue;
                    } else {
                        trackNext = tracks1;
                        break;
                    }
                }
            }
        }
        LatLng origin = new LatLng(0,0), destination = new LatLng(0,0);
        LatLng[] waypoints;
        //List<LatLng> latLngs = new ArrayList<>();
        int j = 0;
        /*for (Tracks tracks1 : this.tracks) {
            if (tracks1.getLat() != null && tracks1.getLng() != null &&
                    (tracks1.getStatus() == null || !tracks1.getStatus().equals("Visited"))) {
                latLngs.add(new LatLng(Double.parseDouble(tracks1.getLat()),
                        Double.parseDouble(tracks1.getLng())));
            }
        }
*/
        //if (latLngs.size() > 0) {
            origin = new LatLng(locationUpdate.getLatitude(), locationUpdate.getLongitude());
            destination = new LatLng(Double.valueOf(trackGoingTo.getLat()), Double.valueOf(trackGoingTo.getLng()));
            /*latLngs.remove(0);
            latLngs.remove(latLngs.size() - 1);*/
            //waypoints = latLngs.toArray(new LatLng[latLngs.size()]);

        DirectionsResult result = getDirectionsDetails(origin,destination,
                TravelMode.WALKING, null);
        if (results != result) {
            results = result;
            if (rv_directions != null && rv_directions.getVisibility() == View.VISIBLE) {
                if (results != null && rv_directions != null) {
                    rv_directions.addOnScrollListener(new RecyclerView.OnScrollListener() {
                        @Override
                        public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                            super.onScrollStateChanged(recyclerView, newState);
                            if (getCurrentItem() > 0) {
                                populateDirectionSteps(results.routes[0].legs[0].steps);
                            }
                        }
                    });
                }
            } else {
                if (results != null) {
                    Location from = new Location("point from");
                    Location to = new Location("point to");
                    from.setLatitude(origin.lat);
                    from.setLongitude(origin.lng);
                        /*if (waypoints.length > 0) {
                            to.setLatitude(waypoints[0].lat);
                            to.setLongitude(waypoints[0].lng);
                        } else {*/
                    to.setLatitude(destination.lat);
                    to.setLongitude(destination.lng);
                    //}
                    if (results.routes[0].legs[0].distance.inMeters <= 50d) {
                        changeStatusVisited(tracks.get(1));
                        if (tv_goingto != null &&
                                tv_goingaddress != null &&
                                tv_nextaddress != null) {
                            tv_goingto.setVisibility(View.GONE);
                            tv_goingaddress.setText(trackGoingTo.getName());
                            if (trackNext != null) {
                                tv_nextaddress.setVisibility(View.VISIBLE);
                                tv_nextaddress.setText("Next " + trackNext.getName());
                            }
                        }
                        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(instance, R.style.ExitDialog);
                        AlertDialog playDialog = dialogBuilder.create();
                        playDialog.setMessage("You are near " + tracks.get(1).getName() +
                                "yur audio guide wil start once you arrive.");
                        playDialog.setButton(DialogInterface.BUTTON_POSITIVE, getString(R.string.yes), (dialog, which) -> {
                            startGuide(tracks.get(1).getDescription());
                            isDescrptionPlayed = true;
                        });
                        playDialog.setButton(DialogInterface.BUTTON_NEGATIVE, "Later", (dialog, which) -> {
                            playDialog.dismiss();
                        });
                        if (!isDescrptionPlayed) {
                            playDialog.show();
                        }
                    } else {
                        isDescrptionPlayed = false;
                        if (tv_goingto != null &&
                                tv_goingaddress != null &&
                                tv_nextaddress != null) {
                            tv_goingto.setVisibility(View.VISIBLE);
                            tv_goingaddress.setText(trackGoingTo.getName());
                            tv_nextaddress.setVisibility(View.GONE);
                        }
                    }
                    if (mMap == null) {
                        mapFragment.getMapAsync(this);
                    }
                    //mMap.clear();
                    addMarkerLocationUpdate(locationUpdate, mMap);
                    addPolyline(results, mMap);
                }
        }

                //dist_direction.setText(String.valueOf(results.routes[0].legs[0].distance.inMeters) + "m");
            }
        //}

    }

    private void setupGoogleMapScreenSettings(GoogleMap mMap) {
        mMap.setBuildingsEnabled(true);
        mMap.setIndoorEnabled(true);
        mMap.setTrafficEnabled(true);
        UiSettings mUiSettings = mMap.getUiSettings();
        mUiSettings.setZoomControlsEnabled(true);
        mUiSettings.setCompassEnabled(true);
        mUiSettings.setMyLocationButtonEnabled(true);
        mUiSettings.setScrollGesturesEnabled(true);
        mUiSettings.setZoomGesturesEnabled(true);
        mUiSettings.setTiltGesturesEnabled(true);
        mUiSettings.setRotateGesturesEnabled(true);
    }

    private void addMarkersToMap(List<Tracks> tracks, GoogleMap mMap) {
        BitmapDescriptor bitmapDescriptor;
        for (Marker marker : markers) {
            marker.remove();
        }
        if (!markers.isEmpty()) {
            markers.clear();
        }
        /*mMap.addMarker(new MarkerOptions().position(new com.google.android.gms.maps.model.LatLng(results.routes[overview]
                .legs[overview].startLocation.lat,
                results.routes[overview].legs[overview].startLocation.lng)).
                title(results.routes[overview].legs[overview].startAddress));*/
        for (int i = 0; i < tracks.size(); i++) {
            if (tracks.get(i).getStatus().equals("Visited")) {
                bitmapDescriptor = BitmapDescriptorFactory.fromResource(R.drawable.map_grey);
            } else {
                bitmapDescriptor = BitmapDescriptorFactory.fromResource(R.drawable.map_green);
            }
            markers.add(mMap.addMarker(new MarkerOptions().position(new com.google.android.gms.maps.model.LatLng(
                    Double.valueOf(tracks.get(i).getLat()),
                    Double.valueOf(tracks.get(i).getLng()))).
                    icon(bitmapDescriptor).
                    title(tracks.get(i).getName()).
                    snippet(tracks.get(i).getAddress1())));
        }

        if(tracks.get(0).equals("Visited")) {
            markers.get(0).setIcon(BitmapDescriptorFactory.fromResource(R.drawable.map_grey_selected));
        } else {
            markers.get(0).setIcon(BitmapDescriptorFactory.fromResource(R.drawable.map_green_selected));
        }
    }

    private void addMarkerLocationUpdate(Location locationUpdate, GoogleMap mMap) {
        Marker marker = mMap.addMarker(new MarkerOptions().position(new com.google.android.gms.maps.model.LatLng(
                locationUpdate.getLatitude(),
                locationUpdate.getLongitude())).
                icon(BitmapDescriptorFactory.fromResource(R.drawable.map_red)).
                title("User").
                snippet("User Location"));
    }

    private void positionCamera(LatLng origin, LatLng destination, GoogleMap mMap) {
        LatLngBounds.Builder builder = new LatLngBounds.Builder();

        //the include method will calculate the min and max bound.
        builder.include(new com.google.android.gms.maps.model.LatLng(origin.lat, origin.lng));
        builder.include(new com.google.android.gms.maps.model.LatLng(destination.lat, destination.lng));

        LatLngBounds bounds = builder.build();


        mMap.setOnMapLoadedCallback(new GoogleMap.OnMapLoadedCallback() {
            @Override
            public void onMapLoaded() {
                int width = mapFragment.getView().getWidth();//mItemView.getWidth();
                int height = mapFragment.getView().getHeight();//mItemView.getHeight();
                int padding = (int) (width * 0.10);
                CameraUpdate cu = CameraUpdateFactory.newLatLngBounds(bounds, width, height, padding);
                mMap.animateCamera(cu);
            }
        });
    }

    private void addPolyline(DirectionsResult results, GoogleMap mMap) {
        if (polyline != null) {
            polyline.remove();
        }
        List<com.google.android.gms.maps.model.LatLng> decodedPath = PolyUtil.decode(results.routes[overview].overviewPolyline.getEncodedPath());
        polyline = mMap.addPolyline(new PolylineOptions().addAll(decodedPath).color(Color.parseColor("#03a4e0")).width(10));
    }

    private void addPolylineTracks(List<Tracks> tracks, GoogleMap mMap) {
        for (int i = 0 ; i < tracks.size()-1; i++) {
            if (tracks.get(i).getStatus().equals("Visited")) {
                continue;
            } else {
                Tracks src = tracks.get(i);
                Tracks dest = tracks.get(i+1);

                mMap.addPolyline(new PolylineOptions().add(
                        new com.google.android.gms.maps.model.LatLng(Double.valueOf(src.getLat()), Double.valueOf(src.getLng())),
                        new com.google.android.gms.maps.model.LatLng(Double.valueOf(dest.getLat()), Double.valueOf(dest.getLng()))
                ).color(Color.parseColor("#03a4e0")).width(10));
            }
        }
    }

    private String getEndLocationTitle(DirectionsLeg results){
        return  "Time :"+ results.duration.humanReadable + " Distance :" + results.distance.humanReadable;
    }

    private GeoApiContext getGeoContext() {
        GeoApiContext geoApiContext = new GeoApiContext();
        return geoApiContext
                .setQueryRateLimit(3)
                .setApiKey(getString(R.string.google_maps_key))
                .setConnectTimeout(5, TimeUnit.SECONDS)
                .setReadTimeout(5, TimeUnit.SECONDS)
                .setWriteTimeout(5, TimeUnit.SECONDS);
    }

    private void populateDirectionSteps(DirectionsStep[] directionsStep) {
        mDirectionsAdapter = new DirectionsAdapter(this, directionsStep);
        rv_directions.setLayoutManager(new LinearLayoutManager(getBaseContext()));
        rv_directions.setAdapter(mDirectionsAdapter);
    }

    private com.google.android.gms.maps.model.LatLng computeCentroid(List<LatLng> points) {
        double latitude = 0;
        double longitude = 0;
        int n = points.size();

        for (LatLng point : points) {
            latitude += point.lat;
            longitude += point.lng;
        }

        return new com.google.android.gms.maps.model.LatLng(latitude/n, longitude/n);
    }

    @Override
    public void onLocationUpdate(Location location) {
        Tracks track = new Tracks();
        if (this.mMap != null) {
            /*if (tracks.get(0).getStatus() == null) {
                tracks.remove(0);
            }
            this.location = location;
            track.setLat(String.valueOf(location.getLatitude()));
            track.setLng(String.valueOf(location.getLongitude()));
            tracks.add(0, track);*/
            updateDirectionLine(location);

            if (!isCameraPosition) {
                positionCamera(new LatLng(location.getLatitude(), location.getLongitude()),
                        new LatLng(Double.valueOf(trackGoingTo.getLat()),
                                Double.valueOf(trackGoingTo.getLng())), mMap);
                isCameraPosition = true;
            }
        }
    }

    @Override
    public void onLocationChanged(Location location) {
        Tracks track = new Tracks();
        if (this.mMap != null) {
            //mMap.clear();
            /*if (tracks.size() > 0 && tracks.get(0).getStatus() == null) {
                tracks.remove(0);
            }
            this.location = location;
            track.setLat(String.valueOf(location.getLatitude()));
            track.setLng(String.valueOf(location.getLongitude()));
            tracks.add(0, track);*/

            updateDirectionLine(location);
        }
    }

    public void playClicked(View view) {
        startGuide(tracks.get(item_played).getDescription());
    }

    public void stopClicked(View view) {
        stopGuide();
    }

    @Override
    public void onStartSpeak() {
    }

    private View.OnClickListener onClickListenerTimeline = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
        }
    };

    private View.OnLongClickListener mOnLongClickListenerTimeline = new View.OnLongClickListener() {
        @Override
        public boolean onLongClick(View v) {

            return true;
        }
    };

    public void startGuide(String message){

        if(textToSpeechService == null){

            textToSpeechService = new TextToSpeechService(this);
            textToSpeechService.setTextToSpeechServiceInterface(this);
        }

        textToSpeechService.startGuide(message);

    }

    public void stopGuide() {
        if (textToSpeechService != null) {
            textToSpeechService.stopGuide();
        }
    }


    @Override
    public void onDoneSpeak() {
        mTimelineAdapter.notifyItemChanged(item_played);
    }

    public void backPressed(View view) {
        onBackPressed();
    }

    public void atractionClick(int position) {
        Tracks track = this.tracks.get(position);
        if (track.getCategory().equals("Attraction")) {
            appActivityManager.launchCityHighlightsForViewing(this, track, null);
        }
    }

    public void attractionLongClick(View v, int position) {
        Tracks track = this.tracks.get(position);
        if (track.getCategory().equals("Attraction")) {
            if (track.getAddress1() != null && track.getName() != null) {
                PopupMenu popupMenu = new PopupMenu(this, v);
                popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem item) {
                        switch (item.getItemId()) {
                            case R.id.track_visited:
                                changeStatusVisited(track);
                                return true;

                            case R.id.track_delete:
                                AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(instance.getParent(), R.style.ExitDialog);
                                AlertDialog deleteDialog = dialogBuilder.create();
                                deleteDialog.setMessage(getString(R.string.are_you_sure_you_want_to_delete));
                                deleteDialog.setButton(DialogInterface.BUTTON_POSITIVE, getString(R.string.yes), (dialog, which) -> {
                                    compositeDisposable.add(presenter.deletePlaceFromTrack(track.getTrackRecordId()));
                                    deleteDialog.dismiss();
                                    //mDataList.clear();
                                    instance.tracks.remove(0);
                                    instance.tracks.removeIf((Tracks t) -> t.getTrackRecordId() == track.getTrackRecordId());
                                    List<Tracks> updatedTracks = instance.tracks;
                                    //timelineActivity.setDataListItems(timelineActivity.location, updatedTracks);
                                    instance.mTimelineAdapter.notifyDataSetChanged();
                                });
                                deleteDialog.setButton(DialogInterface.BUTTON_NEGATIVE, getString(R.string.no), (dialog, which) -> {
                                    deleteDialog.dismiss();
                                });
                                deleteDialog.show();
                                return true;

                            default:
                                return false;
                        }
                    }
                });
                popupMenu.inflate(R.menu.menu_track_status);
                popupMenu.show();
            }
        }
    }

    private void changeStatusVisited(Tracks track) {
        compositeDisposable.add(presenter.changeRecordStatus(track.getTrackRecordId(), 2));
        for (int i = 0; i < instance.tracks.size(); i++) {
            if (instance.tracks.get(i) == track) {
                track.setStatus("Visited");
                //mDataList.set(i, timeline);
                instance.mTimelineAdapter.notifyDataSetChanged();
                break;
            }
        }
        if (mTimelineAdapter != null) {
            mTimelineAdapter.notifyDataSetChanged();
        }
    }
    @Override
    public void onNetworkErrorFound(String message) {
        showAlertDialog(message);
    }

    @Override
    public void onHttpErrorUnexpectedFound(String message) {
        showAlertDialog(message);
    }

    @Override
    public void onApiErrorFound(String message) {
        showAlertDialog(message);
    }

    @Override
    public void onHttpErrorUnexpectedFound() {

    }

    @Override
    public void onUnauthorizedErrorFound() {

    }

    @Override
    public CompositeDisposable getCompositeDisposable() {
        return compositeDisposable;
    }

    @Override
    protected Activity getActivity() {
        return this;
    }

    @Override
    protected AlertDialog getAlertDialog() {
        return alertDialog;
    }

    @Override
    public MyTrip getMyTrip() {
        return getIntent().getParcelableExtra(AppConstants.CREATE_TRACK_RESPONSE_OBJECT);
    }

    @Override
    public City buildCityObject() {
        return null;
    }

    @Override
    public int getCountryId() {
        return 0;
    }

    @Override
    public int getCityId() {
        return getIntent().getIntExtra(AppConstants.GET_COUNTRY_ID, 0);
    }

    @Override
    public void showProgressbar() {
        //pbSearch.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideProgressbar() {
        //pbSearch.setVisibility(View.GONE);
    }

    @Override
    public String provideToken() {
        return sharedPreferenceManager.getAccessToken();
    }

    @Override
    public void setTrackPictureList(List<TrackPicture> trackPictureList) {
        /*this.trackPictureList.clear();
        this.trackPictureList.addAll(trackPictureList);*/
    }

    @Override
    public void setTracksList(List<Tracks> tracksList) {
        this.tracks.clear();
        tracksList.removeIf((Tracks t) -> t.getCategory().equals("Pictures"));
        this.tracks.addAll(tracksList);
        initView();
        //updateDirectionLine(this.tracks);
        addMarkersToMap(tracks, mMap);
        addPolylineTracks(tracks, mMap);
        if (location == null) {
            positionCamera(new LatLng(Double.valueOf(tracks.get(0).getLat()),
                            Double.valueOf(tracks.get(0).getLng())),
                    new LatLng(Double.valueOf(tracks.get(tracks.size() - 1).getLat()),
                            Double.valueOf(tracks.get(tracks.size() - 1).getLng())),
                    mMap);
        } else {
            positionCamera(new LatLng(location.getLatitude(), location.getLongitude()),
                    new LatLng(Double.valueOf(tracks.get(tracks.size() - 1).getLat()),
                            Double.valueOf(tracks.get(tracks.size() - 1).getLng())),
                    mMap);
        }
    }

    @Override
    public void launchCityHighlightsForViewing(Tracks tracks) {
        appActivityManager.launchCityHighlightsForViewing(this, tracks, buildCityObject());
    }

    @Override
    public RemovePlaceFromTrackRequest buildRequest(int trackPlaceId) {
        return new RemovePlaceFromTrackRequest(trackPlaceId);
    }

    @Override
    public ChangeRecordStatusRequest buildRequest(int trackPlaceId, int trackStatusId) {
        return new ChangeRecordStatusRequest(trackPlaceId, trackStatusId);
    }

    private double getDistance(Location locationFrom, Location locationTo) {
        LatLng origin = new LatLng(locationFrom.getLatitude(), locationFrom.getLongitude());
        LatLng destination = new LatLng(locationTo.getLatitude(), locationTo.getLongitude());

        DirectionsResult results = getDirectionsDetails(origin,destination, TravelMode.WALKING, null);
        if (results != null) {
            return results.routes[0].legs[0].distance.inMeters;
        } else {
            return 1000d;
        }
    }

    public void compassClicked(View view) {
        if(rv_directions.getVisibility() == View.VISIBLE) {
            rv_directions.setVisibility(View.GONE);
            mapFragment.getView().setVisibility(View.VISIBLE);
            addPolyline(results, mMap);
            //addPolylineTracks(tracks, mMap);
            if (this.location != null) {
                addMarkerLocationUpdate(location, mMap);
            }
        } else {
            rv_directions.setVisibility(View.VISIBLE);
            mapFragment.getView().setVisibility(View.GONE);
            for (Marker marker : markers) {
                marker.remove();
            }
            markers.clear();
            mMap.clear();
            if (results != null) {
                populateDirectionSteps(results.routes[0].legs[0].steps);
            }
        }
        scrollPosition = 0;
        setupViewElements();
    }
}
