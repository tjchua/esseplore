package com.essplore.essplore.ui.activities;

public interface HttpBaseView {
  void onNetworkErrorFound(final String message);

  void onApiErrorFound(final String message);

  void onHttpErrorUnexpectedFound();

  void onUnauthorizedErrorFound();
}
