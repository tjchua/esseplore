package com.essplore.essplore.ui.activities.timeline.presenter;

import io.reactivex.disposables.Disposable;

public interface TimelinePresenter{

    Disposable getSpecificTrack(int trackId);
    Disposable getPhotoTrack(int trackId);
    Disposable changeRecordStatus(int trackRecordId, int status);
    Disposable deletePlaceFromTrack(int trackPlaceId);

}