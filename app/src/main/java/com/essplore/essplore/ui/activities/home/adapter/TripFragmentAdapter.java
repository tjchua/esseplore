package com.essplore.essplore.ui.activities.home.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.PopupMenu;
import android.widget.TextView;
import butterknife.BindView;
import butterknife.ButterKnife;

import com.bumptech.glide.Glide;
import com.essplore.essplore.R;
import com.essplore.essplore.models.MyTrack;
import com.essplore.essplore.ui.activities.home.fragments.presenter.TripPresenterImpl;
import com.essplore.essplore.ui.activities.places.adapter.UserAction;
import com.essplore.essplore.ui.utils.OnBindViewListener;

import java.text.SimpleDateFormat;
import java.util.List;

public class TripFragmentAdapter extends RecyclerView.Adapter<TripFragmentAdapter.ViewHolder> {

  private Context context;
  private List<MyTrack> dataList;
  private TripPresenterImpl presenter;

  public TripFragmentAdapter(Context context, List<MyTrack> dataList, TripPresenterImpl presenter) {
    this.context = context;
    this.dataList = dataList;
    this.presenter = presenter;
  }

  @Override public TripFragmentAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
    return new TripFragmentAdapter.ViewHolder(
        LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item_trips, parent, false));
  }

  @Override public int getItemCount() {
    return dataList.size();
  }

  @Override public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
    holder.onBind(dataList.get(position));
  }

  public class ViewHolder extends RecyclerView.ViewHolder implements OnBindViewListener<MyTrack> {

    @BindView(R.id.imgTrip) ImageView imgTrip;

    @BindView(R.id.txTripDate) TextView txTripDate;

    @BindView(R.id.txTripName) TextView txTripname;

    public ViewHolder(View view) {
      super(view);
      ButterKnife.bind(this, view);
    }

    @Override public void onBind(MyTrack object) {
      txTripname.setText(object.getTrackName());
      txTripDate.setText(String.format("%s - %s", removeTime(object.getDateFrom()), removeTime(object.getDateTo())));

      if (object.getTrackPictures().size() > 0) {
        Glide.with(context).load(object.getTrackPictures().get(0)).centerCrop().crossFade().into(imgTrip);
      }
      itemView.setOnClickListener(view -> presenter.onSingleItemClick(object));

      itemView.setOnLongClickListener(view -> {
        PopupMenu popupMenu = new PopupMenu(context, itemView);
        popupMenu.getMenu().add(context.getString(R.string.delete));
        popupMenu.setOnMenuItemClickListener(menuItem -> {
          presenter.onDeleteItem(getAdapterPosition());
          popupMenu.dismiss();
          return false;
        });
        popupMenu.show();

      return false; });
    }

    private String removeTime(String date) {
      return date.substring(0, 10);
    }
  }

  public List<MyTrack> getDataList() {
    return dataList;
  }
}
