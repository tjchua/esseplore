package com.essplore.essplore.ui.activities.album.interfaces;

import android.graphics.Bitmap;

public interface DownloadImageResponseListenter {
    public void onFinishDownload(int listPosition, Bitmap image);
}
