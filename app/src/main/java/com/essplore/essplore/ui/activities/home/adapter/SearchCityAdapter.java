package com.essplore.essplore.ui.activities.home.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import butterknife.BindView;
import butterknife.ButterKnife;
import com.bumptech.glide.Glide;
import com.essplore.essplore.R;
import com.essplore.essplore.models.City;
import com.essplore.essplore.ui.activities.home.fragments.presenter.SearchPresenterImpl;
import com.essplore.essplore.ui.utils.OnBindViewListener;
import java.util.List;
import org.apache.commons.lang3.StringUtils;

public class SearchCityAdapter extends RecyclerView.Adapter<SearchCityAdapter.ViewHolder> {

  private Context context;
  private List<City> cities;
  private SearchPresenterImpl presenter;

  public SearchCityAdapter(Context context, List<City> cities, SearchPresenterImpl presenter) {
    this.context = context;
    this.cities = cities;
    this.presenter = presenter;
  }

  @NonNull @Override public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
    return new ViewHolder(
        LayoutInflater.from(context).inflate(R.layout.item_search_city, parent, false));
  }

  @Override public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
    holder.onBind(cities.get(position));
  }

  @Override public int getItemCount() {
    return cities.size();
  }

  public class ViewHolder extends RecyclerView.ViewHolder implements OnBindViewListener<City> {

    @BindView(R.id.tvCityName) TextView tvCityName;
    @BindView(R.id.tvCountryName) TextView tvCountryName;
    @BindView(R.id.ivCityPicture) ImageView ivCityPicture;

    ViewHolder(View itemView) {
      super(itemView);
      ButterKnife.bind(this, itemView);
    }

    @Override public void onBind(City object) {
      if (object != null) {
        tvCityName.setText(object.getCityName() != null ? object.getCityName() : StringUtils.EMPTY);
        tvCountryName.setText(
            object.getCountryName() != null ? object.getCountryName() : StringUtils.EMPTY);

        if (object.getAvatarUrl() != null) {
          Glide.with(context)
              .load(object.getAvatarUrl())
              .centerCrop()
              .crossFade()
              .into(ivCityPicture);
        } else {
          ivCityPicture.setImageDrawable(context.getDrawable(R.drawable.img_placeholder));
        }

        itemView.setOnClickListener(view -> presenter.onSingleItemClick(object));
      }
    }
  }
}
