package com.essplore.essplore.ui.activities.album.asynctask;

import android.content.Context;
import android.os.AsyncTask;
import com.essplore.essplore.BuildConfig;
import com.essplore.essplore.application.AppConstants;
import com.essplore.essplore.managers.SharedPreferenceKeys;
import com.essplore.essplore.ui.activities.album.interfaces.ApiResponseListener;
import com.essplore.essplore.ui.activities.album.models.ApiResponse;
import org.apache.commons.lang3.StringUtils;
import java.io.IOException;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

public class GetAlbumItemsTask extends AsyncTask<Void, Void, String>{

    private OkHttpClient client;
    private String url;
    private Request request;
    private String accessToken;
    private ApiResponse apiResponse;
    private ApiResponseListener apiResponseListener;

    public  GetAlbumItemsTask(Context context, int trackId){

        accessToken = context.getSharedPreferences(SharedPreferenceKeys.ESSPLORE_PREFERENCE, Context.
                MODE_PRIVATE).getString(SharedPreferenceKeys.ACCESS_TOKEN, StringUtils.EMPTY);
        client = new OkHttpClient();
        url = BuildConfig.BASE_URL + AppConstants.PHOTO_TRACK + "?" +
                AppConstants.PHOTO_TRACK_TRACK_ID_PARAM + "=" + Integer.toString(trackId);
        request = new Request.Builder()
                .url(url)
                .header(AppConstants.AUTHORIZATION, AppConstants.BEARER + StringUtils.SPACE + accessToken)
                .build();

    }

    @Override
    protected String doInBackground(Void... voids) {

        try (Response response = client.newCall(request).execute()) {
            return response.body().string();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;

    }

    @Override
    protected void onPostExecute(String s) {
        super.onPostExecute(s);

        apiResponse = new ApiResponse(s);
        if(apiResponse.success()){
            apiResponseListener.onResponseSucess(apiResponse.getPicturesResponse());
        }

    }

    public void setResponseListener(ApiResponseListener apiResponseListener){

        this.apiResponseListener = apiResponseListener;

    }
}
