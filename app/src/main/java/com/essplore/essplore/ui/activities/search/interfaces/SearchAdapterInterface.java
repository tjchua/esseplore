package com.essplore.essplore.ui.activities.search.interfaces;

import com.essplore.essplore.ui.activities.search.models.Place;

public interface SearchAdapterInterface {

    public void onPlaceClick(Place place);

}
