package com.essplore.essplore.ui.activities.login.fragments.presenter;

import com.essplore.essplore.api.requests.ResetPasswordRequest;
import io.reactivex.disposables.Disposable;

public interface ForgotPasswordPresenter {
  Disposable requestForgotPassword();

  ResetPasswordRequest buildResetPasswordRequest();
}
