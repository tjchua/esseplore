package com.essplore.essplore.ui.activities.profile.fragments.presenter;

import com.essplore.essplore.api.managers.ApiManager;
import com.essplore.essplore.api.requests.ChangePasswordRequest;
import com.essplore.essplore.api.utils.SimpleObserver;
import com.essplore.essplore.ui.activities.HttpBasePresenter;
import com.essplore.essplore.ui.activities.profile.fragments.view.ChangePasswordFragmentView;
import io.reactivex.disposables.Disposable;
import org.apache.commons.lang3.StringUtils;

public class ChangePasswordPresenterImpl extends HttpBasePresenter implements ChangePasswordPresenter {

  private final ChangePasswordFragmentView view;
  private final ApiManager manager;

  public ChangePasswordPresenterImpl(ChangePasswordFragmentView view, ApiManager manager) {
    this.view = view;
    this.manager = manager;
  }

  @Override public Disposable changePassword() {
    return manager.changePassword(buildChangePasswordRequest(), view.provideToken()).subscribe(new SimpleObserver<>());
  }

  @Override public ChangePasswordRequest buildChangePasswordRequest() {
    return new ChangePasswordRequest(view.provideOldPassword(), view.provideNewPassword());
  }

  @Override public boolean isValidCredentials() {
    if (StringUtils.isEmpty(view.provideOldPassword())
        || StringUtils.isEmpty(view.provideNewPassword())
        || StringUtils.isEmpty(view.provideConfirmNewPassword())) {
      view.showFieldError();
      return false;
    }

    if (!view.provideNewPassword().equals(view.provideConfirmNewPassword())) {
      view.showPasswordDoesNotMatchError();
      return false;
    }
    return true;
  }
}
