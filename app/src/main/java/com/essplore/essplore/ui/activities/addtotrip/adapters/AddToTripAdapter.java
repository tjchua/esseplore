package com.essplore.essplore.ui.activities.addtotrip.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.essplore.essplore.R;
import com.essplore.essplore.ui.activities.addtotrip.api.AddToTripFromSearchApi;
import com.essplore.essplore.ui.activities.addtotrip.interfaces.AddToTripAdapterInterface;
import com.essplore.essplore.ui.activities.addtotrip.models.Trips;
import com.essplore.essplore.ui.activities.search.interfaces.SearchAdapterInterface;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class AddToTripAdapter extends RecyclerView.Adapter<AddToTripAdapter.MyViewHolder>{

    private Trips trips;
    private Context context;
    private AddToTripAdapterInterface addToTripAdapterInterface;

    public class MyViewHolder extends RecyclerView.ViewHolder {

        private ImageView avatar;
        private TextView name;
        private TextView date;


        public MyViewHolder(View view) {

            super(view);
            avatar = view.findViewById(R.id.trip_image_iv);
            name = view.findViewById(R.id.trip_name_tv);
            date = view.findViewById(R.id.trip_date_tv);

        }
    }


    public AddToTripAdapter(Context context, Trips trips) {

        this.context = context;
        this.trips = trips;

    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.list_item_add_to_trip, parent, false);
        return new MyViewHolder(itemView);

    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {

        holder.name.setText(trips.get(position).getTrack_name());
        holder.date.setText(formatDate(trips.get(position).getDate_from()) + " - " + formatDate(trips.get(position).getDate_to()));
        Glide.with(context)
                .load(trips.get(position).getAvatar_url())
                .crossFade()
                .into(holder.avatar);

        holder.itemView.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                if (addToTripAdapterInterface != null)
                    addToTripAdapterInterface.onClickItem(trips.get(position).getId());

            }

        });

    }

    @Override
    public int getItemCount() {

        return trips.size();

    }

    public void setAddToTripAdapterInterface(AddToTripAdapterInterface addToTripAdapterInterface){

        this.addToTripAdapterInterface = addToTripAdapterInterface;

    }

    private String formatDate(String strDate){

        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date date = null;
        try {
            date = format.parse(strDate);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        return dateFormat.format(date);

    }
}
