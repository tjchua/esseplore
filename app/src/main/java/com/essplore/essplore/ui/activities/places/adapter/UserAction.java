package com.essplore.essplore.ui.activities.places.adapter;

public interface UserAction {

    void onDeleteItem(int position);
}
