package com.essplore.essplore.ui.activities.login.fragments.presenter;

import com.essplore.essplore.api.managers.ApiManager;
import com.essplore.essplore.api.requests.ResetPasswordRequest;
import com.essplore.essplore.api.utils.SimpleObserver;
import com.essplore.essplore.ui.activities.HttpBasePresenter;
import com.essplore.essplore.ui.activities.login.fragments.view.ForgotPasswordView;
import io.reactivex.disposables.Disposable;

public class ForgotPasswordPresenterImpl extends HttpBasePresenter implements ForgotPasswordPresenter {

  private final ForgotPasswordView view;
  private final ApiManager apiManager;

  public ForgotPasswordPresenterImpl(ForgotPasswordView view, ApiManager apiManager) {
    this.view = view;
    this.apiManager = apiManager;
  }

  @Override public Disposable requestForgotPassword() {
    view.showProgressbar();
    return apiManager.resetPasswordRequest(buildResetPasswordRequest()).subscribe(new SimpleObserver<String>() {
      @Override public void accept(String s) throws Exception {
        view.hideProgressbar();
        view.showSuccessApiCall();
        view.navigateToLogin();
      }
    }, throwable -> {
      view.hideProgressbar();
      handleHttpError(view, throwable);
    });
  }

  @Override public ResetPasswordRequest buildResetPasswordRequest() {
    return new ResetPasswordRequest(view.provideEmail());
  }
}
