package com.essplore.essplore.ui.activities.home.adapter;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import com.essplore.essplore.application.AppConstants;
import com.essplore.essplore.ui.activities.home.fragments.view.AccountFragment;
import com.essplore.essplore.ui.activities.home.fragments.view.NotificationFragment;
import com.essplore.essplore.ui.activities.home.fragments.view.RewardFragment;
import com.essplore.essplore.ui.activities.home.fragments.view.SearchFragment;
import com.essplore.essplore.ui.activities.home.fragments.view.TripsFragment;

public class HomeFragmentPagerAdapter extends FragmentPagerAdapter {

  private Context context;

  public HomeFragmentPagerAdapter(FragmentManager fm, Context context) {
    super(fm);
    this.context = context;
  }

  @Override public Fragment getItem(int position) {
    switch (position) {
      case 0:
        return new SearchFragment();

      case 1:
        return new TripsFragment();

      case 2:
        return new RewardFragment();

      case 3:
        return new NotificationFragment();

      case 4:
        return new AccountFragment();

      default:
        return new SearchFragment();
    }
  }

  @Override public int getCount() {
    return AppConstants.HOME_FRAGMENT_COUNT;
  }
}
