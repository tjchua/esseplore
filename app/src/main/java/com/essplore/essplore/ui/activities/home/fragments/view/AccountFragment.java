package com.essplore.essplore.ui.activities.home.fragments.view;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import com.bumptech.glide.Glide;
import com.essplore.essplore.R;
import com.essplore.essplore.application.EssploreApplication;
import com.essplore.essplore.managers.AppActivityManager;
import com.essplore.essplore.managers.SharedPreferenceManager;
import com.essplore.essplore.ui.activities.HttpBaseFragment;
import com.essplore.essplore.ui.activities.home.view.HomeActivity;

import org.apache.commons.lang3.StringUtils;

import javax.inject.Inject;

public class AccountFragment extends HttpBaseFragment implements AccountFragmentView {

  @Inject AlertDialog alertDialog;
  @Inject AppActivityManager appActivityManager;
  @Inject SharedPreferenceManager sharedPreferenceManager;

  @BindView(R.id.llAccountUpperPanel) LinearLayout llAccountUpperPanel;
  @BindView(R.id.llNotLoggedIn) LinearLayout llNotLoggedIn;
  @BindView(R.id.ivProfilePicture) ImageView ivProfilePicture;
  @BindView(R.id.llLogout) LinearLayout llLogout;
  @BindView(R.id.llLoggedIn) LinearLayout llLoggedIn;
  @BindView(R.id.txUserName) TextView txUsername;
  @BindView(R.id.txNumber) TextView txNumber;

  private Unbinder unbinder;
  private HomeActivity activity;

  public AccountFragment() {
    // Intended to be empty.
  }

  @Nullable @Override public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
      @Nullable Bundle savedInstanceState) {
    View view = inflater.inflate(R.layout.fragment_account, container, false);
    unbinder = ButterKnife.bind(this, view);
    return view;
  }

  @Override public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
    super.onViewCreated(view, savedInstanceState);
    EssploreApplication.get(activity).createAccountComponent(this).inject(this);
    setupProfileElements();
  }

  @Override public void onAttach(Context context) {
    super.onAttach(context);
    activity = (HomeActivity) context;
  }

  @Override public void onDestroy() {
    super.onDestroy();
    unbinder.unbind();
    EssploreApplication.get(activity).releaseAccountComponent();
  }

  @Override protected Fragment getFragment() {
    return this;
  }

  @Override protected AlertDialog getAlertDialog() {
    return alertDialog;
  }

  @OnClick(R.id.llProfile) void onProfileClicked() {
    if (sharedPreferenceManager.isUserLoggedIn()) {
      appActivityManager.launchProfileActivity(activity);
    } else {
      launchLoginActivity(activity);
    }
  }

  @Override public void setupProfileElements() {
    if (sharedPreferenceManager.isUserLoggedIn()) {
      llNotLoggedIn.setVisibility(View.GONE);
      llLoggedIn.setVisibility(View.VISIBLE);

      txUsername.setText(getString(R.string.hi) + getString(R.string.comma) + StringUtils.SPACE + sharedPreferenceManager.getUsername().toUpperCase() + getString(R.string.punctuation));
      txNumber.setText(sharedPreferenceManager.getMobileNumber());

      if (!sharedPreferenceManager.getProfileImagePath().isEmpty()) {
        Glide.with(this)
            .load(sharedPreferenceManager.getProfileImagePath())
            .centerCrop()
            .crossFade()
            .into(ivProfilePicture);
      } else {
        ivProfilePicture.setImageDrawable(getContext().getDrawable(R.drawable.ic_profile_circle));
      }
    } else {
      llNotLoggedIn.setVisibility(View.VISIBLE);
      llLoggedIn.setVisibility(View.GONE);
    }
  }

  @Override public void showProgress() {

  }

  @Override public void hideProgress() {

  }

  @Override public void launchLoginActivity(Activity activity) {
    appActivityManager.launchLoginActivity(activity);
  }

  @OnClick({ R.id.btnSignIn, R.id.btnSignUp }) void onSignInClicked() {
    launchLoginActivity(activity);
  }

  @OnClick(R.id.logout_button) void onLogoutClicked() {
    sharedPreferenceManager.clearCache();
    activity.getViewPager().setCurrentItem(0);
  }

  @OnClick(R.id.llWallet) void onClickWallet()  {

    appActivityManager.launchWalletActivity(getActivity());

  }

  @OnClick(R.id.llPoints) void onClickPoints()  {

  }

}
