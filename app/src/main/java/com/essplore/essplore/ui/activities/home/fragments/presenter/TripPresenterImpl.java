package com.essplore.essplore.ui.activities.home.fragments.presenter;

import com.essplore.essplore.api.managers.ApiManager;
import com.essplore.essplore.api.requests.DeleteSpecificTrackRequest;
import com.essplore.essplore.api.response.CityResponse;
import com.essplore.essplore.api.response.DeleteSpecificTrackResponse;
import com.essplore.essplore.api.response.GetTrackListResponse;
import com.essplore.essplore.api.utils.SimpleObserver;
import com.essplore.essplore.models.MyTrack;
import com.essplore.essplore.ui.activities.HttpBasePresenter;
import com.essplore.essplore.ui.activities.home.fragments.view.TripsFragmentView;
import io.reactivex.disposables.Disposable;

public class TripPresenterImpl extends HttpBasePresenter implements TripPresenter {

  private final TripsFragmentView view;
  private final ApiManager manager;

  public TripPresenterImpl(TripsFragmentView view, ApiManager manager) {
    this.view = view;
    this.manager = manager;
  }

  @Override public Disposable getTracks() {
    view.showProgressbar();
    return manager.getTrackList(view.provideToken()).subscribe(new SimpleObserver<GetTrackListResponse>() {
      @Override public void accept(GetTrackListResponse trackListResponse) {
        view.hideProgressbar();
        view.hideSwipeRefreshLoading();
        processCorrectResponse(trackListResponse);
      }
    }, throwable -> {
      view.hideProgressbar();
      view.hideSwipeRefreshLoading();
      view.hidePanelsAndShowTrackList();
      handleHttpError(view, throwable);
    });
  }


  @Override public Disposable deleteTrack(MyTrack myTrack) {
    view.showProgressbar();
    return manager.deleteSpecificTrack(new DeleteSpecificTrackRequest(myTrack.getTrackId()), view.provideToken()).subscribe(new SimpleObserver<DeleteSpecificTrackResponse>() {
      @Override public void accept(DeleteSpecificTrackResponse deleteSpecificTrackResponse) {
        view.hideProgressbar();
        view.removeTrackInList(myTrack);
      }
      }, throwable -> {
        view.hideProgressbar();
        handleHttpError(view, throwable);
    });
  }

  private void processCorrectResponse(GetTrackListResponse trackListResponse) {
    if (trackListResponse.getMyTracks() != null && trackListResponse.getMyTracks().size() > 0) {
      view.hidePanelsAndShowTrackList();
      view.showTrackList(trackListResponse.getMyTracks());
    }
  }

  @Override public void onSingleItemClick(MyTrack myTrack) {
    view.launchActivity(myTrack);
  }

  @Override
  public void onDeleteItem(int position) {
    view.deleteTrack(position);
  }
}
