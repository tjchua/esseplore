package com.essplore.essplore.ui.activities.profile.view;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.support.design.widget.TextInputEditText;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;
import butterknife.BindView;
import butterknife.OnClick;

import com.bumptech.glide.Glide;
import com.essplore.essplore.R;
import com.essplore.essplore.application.AppConstants;
import com.essplore.essplore.application.EssploreApplication;
import com.essplore.essplore.managers.SharedPreferenceManager;
import com.essplore.essplore.models.Country;
import com.essplore.essplore.ui.activities.HttpToolBarBaseActivity;
import com.essplore.essplore.ui.activities.profile.fragments.view.ChangePasswordFragment;
import com.essplore.essplore.ui.activities.profile.presenter.ProfilePresenterImpl;
import com.essplore.essplore.ui.utils.FileUtils;
import com.vansuita.pickimage.bean.PickResult;
import com.vansuita.pickimage.bundle.PickSetup;
import com.vansuita.pickimage.dialog.PickImageDialog;
import com.vansuita.pickimage.listeners.IPickResult;

import io.reactivex.annotations.NonNull;
import io.reactivex.disposables.CompositeDisposable;
import java.io.File;
import java.util.List;
import javax.inject.Inject;
import okhttp3.MediaType;
import okhttp3.RequestBody;

public class ProfileActivity extends HttpToolBarBaseActivity implements ProfileView, IPickResult {

  @Inject AlertDialog alertDialog;
  @Inject CompositeDisposable compositeDisposable;
  @Inject SharedPreferenceManager sharedPreferenceManager;
  @Inject ProfilePresenterImpl presenter;
  @Inject ChangePasswordFragment changePasswordFragment;

  private final int REQUEST_PERMISSION = 101;

  @BindView(R.id.rvCoverPhoto) RelativeLayout rvCoverPhoto;
  @BindView(R.id.edtUsername) TextInputEditText edtUsername;
  @BindView(R.id.edtPassword) TextInputEditText edtPassword;
  @BindView(R.id.edtEmail) TextInputEditText edtEmail;
  @BindView(R.id.spnCountry) Spinner spnCountry;
  @BindView(R.id.edtMobile) TextInputEditText edtMobile;
  @BindView(R.id.edtDatePicker) TextInputEditText edtDatePicker;
  @BindView(R.id.btnUpdate) Button btnUpdate;
  @BindView(R.id.pbProfile) ProgressBar pbProfile;
  @BindView(R.id.tvChangePassword) TextView tvChangePassword;
  @BindView(R.id.ivProfilePicture) ImageView ivProfilePicture;
  @BindView(R.id.ivCoverPicture) ImageView ivCoverPicture;

  @Override public void onNetworkErrorFound(String message) {
    showAlertDialog(message);
  }

  @Override public void onHttpErrorUnexpectedFound(String message) {
    showAlertDialog(message);
  }

  @Override public void onApiErrorFound(String message) {
    showAlertDialog(message);
  }

  @Override public void onHttpErrorUnexpectedFound() {
    showAlertDialog(getString(R.string.unexpected_error_occurred));
  }

  @Override public void onUnauthorizedErrorFound() {
    showAlertDialog(getString(R.string.unauthorized));
  }

  @Override public CompositeDisposable getCompositeDisposable() {
    return compositeDisposable;
  }

  @Override protected Activity getActivity() {
    return this;
  }

  @Override protected AlertDialog getAlertDialog() {
    return alertDialog;
  }

  @Override protected void setupActivityLayout() {
    setContentView(R.layout.activity_profile);
  }

  @Override protected void setupViewElements() {
    compositeDisposable.add(presenter.getCountries());
    setProfileValues();
  }

  @Override protected void injectDaggerComponent() {
    EssploreApplication.get(this).createProfileComponent(this).inject(this);
  }

  @Override protected boolean isActionbarBackButtonEnabled() {
    return Boolean.TRUE;
  }

  @Override protected void onDestroy() {
    super.onDestroy();
    EssploreApplication.get(this).releaseProfileComponent();
  }

  @Override public boolean onOptionsItemSelected(MenuItem item) {
    return super.onOptionsItemSelected(item);
  }

  @Override public String provideEmail() {
    return edtEmail.getText().toString();
  }

  @Override public String provideToken() {
    return sharedPreferenceManager.getAccessToken();
  }

  @Override public int provideCountryId() {
    return ((Country) spnCountry.getSelectedItem()).getId();
  }

  @Override
  public String provideMobileNum() {
    return edtMobile.getText().toString();
  }

  @Override
  public String provideBday() {
    return edtDatePicker.getText().toString();
  }

  @Override public void setProfileValues() {
    edtUsername.setText(sharedPreferenceManager.getUsername());
    edtMobile.setText(sharedPreferenceManager.getMobileNumber());
    edtDatePicker.setText(sharedPreferenceManager.getBirthday());
    edtEmail.setText(sharedPreferenceManager.getEmail());

    if(!sharedPreferenceManager.getProfileImagePath().isEmpty()) {
      Glide.with(this)
              .load(sharedPreferenceManager.getProfileImagePath())
              .centerCrop()
              .crossFade()
              .into(ivProfilePicture);
    }
  }

  @Override public void showProgress() {
    pbProfile.setVisibility(View.VISIBLE);
  }

  @Override public void hideProgress() {
    pbProfile.setVisibility(View.GONE);
  }

  @Override public void setupSpinner(List<Country> countryList) {
    ArrayAdapter<Country> countryArrayAdapter =
        new ArrayAdapter<>(this, android.R.layout.simple_spinner_dropdown_item, countryList);
    spnCountry.setAdapter(countryArrayAdapter);
    spnCountry.setSelection(presenter.findPreSelectedCountry(countryList, sharedPreferenceManager.getCountryId()));
  }

  @Override public void showEmptyFieldError() {
    Toast.makeText(this, getString(R.string.any_field_cannot_be_empty), Toast.LENGTH_SHORT).show();
  }

  @Override public boolean isPermissionGranted() {
    int PERMISSION_ALL = 1;
    String[] PERMISSIONS = { Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.CAMERA };

    if (!presenter.hasPermissions(this, PERMISSIONS)) {
      ActivityCompat.requestPermissions(this, PERMISSIONS, PERMISSION_ALL);
    }

    return presenter.hasPermissions(this, PERMISSIONS);
  }

  @Override public boolean checkSelfPermission(Context context, String permission) {
    return ActivityCompat.checkSelfPermission(context, permission) == PackageManager.PERMISSION_GRANTED;
  }

  @Override public File getImageFile(Uri fileUri) {
    return FileUtils.getFile(this, fileUri);
  }

  @Override public RequestBody getRequestFile(File file, Uri fileUri) {
    return RequestBody.create(MediaType.parse("image/png"), file);
  }

  @OnClick(R.id.tvChangePassword) void onChangePasswordClicked() {
    changePasswordFragment.show(getSupportFragmentManager(), AppConstants.CHANGE_PASSWORD_TAG);
  }

  @OnClick(R.id.ivProfilePicture) void onProfilePictureClicked() {
    if (presenter.isPermissionGranted()) {
      if (ContextCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
        ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE},
                REQUEST_PERMISSION);
        //dialog.dismiss();
        //return;
      } else {
        PickImageDialog.build(new PickSetup()).show(this);
      }
    }
  }

  @Override
  public void onRequestPermissionsResult(final int requestCode, @NonNull final String[] permissions, @NonNull final int[] grantResults) {
    super.onRequestPermissionsResult(requestCode, permissions, grantResults);
    if (requestCode == REQUEST_PERMISSION) {
      if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
        // Permission granted.
      } else {
        // User refused to grant permission.
      }
    }
  }

  @OnClick(R.id.btnUpdate) void onUpdateInfoClicked(){
    compositeDisposable.add(presenter.updateUserInfo());
  }

  @Override public void onPickResult(PickResult pickResult) {
    if (pickResult.getError() == null) {
      ivProfilePicture.setImageURI(null);
      ivProfilePicture.setImageURI(pickResult.getUri());
      Log.e("URL", String.valueOf(pickResult.getUri()));
      compositeDisposable.add(presenter.uploadProfilePicture(pickResult.getUri()));
    } else {
      Toast.makeText(this, pickResult.getError().getMessage(), Toast.LENGTH_SHORT).show();
    }
  }

  @Override
  public void saveProfilePic(String url) {
    sharedPreferenceManager.saveImagePath(url);
  }
}
