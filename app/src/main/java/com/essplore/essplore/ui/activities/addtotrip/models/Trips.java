package com.essplore.essplore.ui.activities.addtotrip.models;

import com.essplore.essplore.ui.activities.search.models.Place;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class Trips extends ArrayList<Trip>{

    private final String TRACKS = "tracks";

    public void addTrips(String json){

        try {

            JSONObject jsonObject = new JSONObject(json);
            JSONArray jsonArray = jsonObject.getJSONArray(TRACKS);

            for (int i=0; i < jsonArray.length(); i++){

                Trip trip = new Trip(jsonArray.getJSONObject(i).toString());
                this.add(trip);

            }

        } catch (JSONException e) {

            e.printStackTrace();

        }

    }

    public Trips(){



    }

}
