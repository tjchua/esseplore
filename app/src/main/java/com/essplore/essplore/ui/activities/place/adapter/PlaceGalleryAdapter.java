package com.essplore.essplore.ui.activities.place.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.essplore.essplore.R;
import com.essplore.essplore.ui.activities.search.interfaces.SearchAdapterInterface;
import com.essplore.essplore.ui.activities.search.models.Places;

import java.util.ArrayList;

public class PlaceGalleryAdapter extends RecyclerView.Adapter<PlaceGalleryAdapter.MyViewHolder>{

    private ArrayList<String> photos;
    private Context context;

    public class MyViewHolder extends RecyclerView.ViewHolder {

        private ImageView photo;


        public MyViewHolder(View view) {

            super(view);
            photo = view.findViewById(R.id.photo);

        }
    }


    public PlaceGalleryAdapter(Context context, ArrayList<String> photos) {

        this.context = context;
        this.photos = photos;

    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.list_item_place_photo_gallery, parent, false);
        return new MyViewHolder(itemView);

    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {

        Glide.with(context)
                .load(photos.get(position))
                .crossFade()
                .into(holder.photo);

    }

    @Override
    public int getItemCount() {

        return photos.size();

    }
}
