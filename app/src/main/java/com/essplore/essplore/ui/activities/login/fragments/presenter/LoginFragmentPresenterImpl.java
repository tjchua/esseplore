package com.essplore.essplore.ui.activities.login.fragments.presenter;

import com.essplore.essplore.api.managers.ApiManager;
import com.essplore.essplore.api.requests.LoginRequest;
import com.essplore.essplore.api.response.LoginResponse;
import com.essplore.essplore.api.utils.SimpleObserver;
import com.essplore.essplore.ui.activities.HttpBasePresenter;
import com.essplore.essplore.ui.activities.login.fragments.view.LoginFragmentView;
import io.reactivex.disposables.Disposable;
import org.apache.commons.lang3.StringUtils;

public class LoginFragmentPresenterImpl extends HttpBasePresenter implements LoginFragmentPresenter {

  private final LoginFragmentView view;
  private final ApiManager apiManager;

  public LoginFragmentPresenterImpl(LoginFragmentView view, ApiManager apiManager) {
    this.view = view;
    this.apiManager = apiManager;
  }

  @Override public Disposable loginUser(LoginRequest request) {
    view.showProgress();
    return apiManager.loginUser(request).subscribe(new SimpleObserver<LoginResponse>() {
      @Override public void accept(LoginResponse loginResponse) {
        view.hideProgress();
        view.saveResponse(loginResponse);
        view.navigateToHome();
      }
    }, throwable -> {
      view.hideProgress();
      handleHttpError(view, throwable);
    });
  }

  @Override public LoginRequest buildLoginRequest() {
    return new LoginRequest(view.provideUsername(), view.providePassword());
  }

  @Override public boolean validateCredentials(String username, String password) {
    if (StringUtils.isEmpty(username)) {
      view.setUsernameError();
      return false;
    }

    if (StringUtils.isEmpty(password)) {
      view.setPasswordError();
      return false;
    }
    return true;
  }
}
