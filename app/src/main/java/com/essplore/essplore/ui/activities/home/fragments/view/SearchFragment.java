package com.essplore.essplore.ui.activities.home.fragments.view;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputEditText;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import com.essplore.essplore.R;
import com.essplore.essplore.application.EssploreApplication;
import com.essplore.essplore.managers.SharedPreferenceManager;
import com.essplore.essplore.models.City;
import com.essplore.essplore.ui.activities.HttpBaseFragment;
import com.essplore.essplore.ui.activities.home.adapter.SearchCityAdapter;
import com.essplore.essplore.ui.activities.home.fragments.presenter.SearchPresenterImpl;
import com.essplore.essplore.ui.activities.home.view.HomeActivity;
import com.essplore.essplore.ui.activities.search.SearchActivity;

import io.reactivex.disposables.CompositeDisposable;
import java.util.ArrayList;
import java.util.List;
import javax.inject.Inject;
import org.apache.commons.lang3.StringUtils;

public class SearchFragment extends HttpBaseFragment implements SearchView, View.OnClickListener {

  @Inject AlertDialog alertDialog;
  @Inject SharedPreferenceManager sharedPreferenceManager;
  @Inject SearchPresenterImpl presenter;
  @Inject CompositeDisposable disposable;

  @BindView(R.id.tilSearch) TextInputLayout tilSearch;
  @BindView(R.id.etSearch) TextInputEditText etSearch;
  @BindView(R.id.rvCity) RecyclerView rvCity;
  @BindView(R.id.pbSearch) ProgressBar pbSearch;
  @BindView(R.id.searchBtn) Button searchButton;

  private SearchCityAdapter searchCityAdapter;
  private List<City> cityList;
  private RelativeLayout.LayoutParams layoutParams;
  private Unbinder unbinder;
  private HomeActivity activity;

  public SearchFragment() {
    // Intended to be empty.
  }

  @Nullable @Override
  public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
      @Nullable Bundle savedInstanceState) {
    View view = inflater.inflate(R.layout.fragment_search, container, false);
    unbinder = ButterKnife.bind(this, view);
    return view;
  }

  @Override public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
    super.onViewCreated(view, savedInstanceState);
    layoutParams = (RelativeLayout.LayoutParams) tilSearch.getLayoutParams();
    tilSearch.setHintEnabled(false);
    EssploreApplication.get(activity).createSearchComponent(this).inject(this);
    setupAdapter();
    setupListener();
  }

  @Override public void setUserVisibleHint(boolean isVisibleToUser) {
    super.setUserVisibleHint(isVisibleToUser);
    if (disposable != null && !isVisibleToUser && disposable.size() > 0) {
      disposable.clear();
    }
  }

  @Override public void onAttach(Context context) {
    super.onAttach(context);
    this.activity = (HomeActivity) context;
  }

  @Override public void onDestroy() {
    super.onDestroy();
    unbinder.unbind();
    EssploreApplication.get(activity).releaseSearchComponent();
  }

  @Override protected Fragment getFragment() {
    return this;
  }

  @Override protected AlertDialog getAlertDialog() {
    return alertDialog;
  }

  @Override public void setupListener() {
    etSearch.addTextChangedListener(new TextWatcher() {
      @Override public void beforeTextChanged(CharSequence s, int start, int count, int after) {
      }

      @Override public void onTextChanged(CharSequence s, int start, int before, int count) {
      }

      @Override public void afterTextChanged(Editable s) {
        if (StringUtils.isNotEmpty(provideSearch())) {
          modifyLayout();
          disposable.add(presenter.getCity());
        } else {
          modifyLayout();
        }
      }
    });
    searchButton.setOnClickListener(this);
  }

  @Override public void modifyLayout() {
    if (StringUtils.isNotEmpty(provideSearch())) {
      layoutParams.removeRule(RelativeLayout.CENTER_VERTICAL);
      showRecyclerView();
    } else {
      layoutParams.addRule(RelativeLayout.CENTER_VERTICAL, R.id.tilSearch);
      hideRecyclerView();
    }
  }

  @Override public String provideSearch() {
    return etSearch.getText().toString();
  }

  @Override public String provideToken() {
    return sharedPreferenceManager.getAccessToken();
  }

  @Override public void showRecyclerView() {
    rvCity.setVisibility(View.VISIBLE);
  }

  @Override public void hideRecyclerView() {
    rvCity.setVisibility(View.GONE);
  }

  @Override public void showProgressbar() {
    pbSearch.setVisibility(View.VISIBLE);
  }

  @Override public void hideProgressbar() {
    pbSearch.setVisibility(View.GONE);
  }

  @Override public void setCityListValue(List<City> cityListValue) {
    cityList.clear();
    cityList.addAll(cityListValue);
    searchCityAdapter.notifyDataSetChanged();
  }

  @Override public void launchActivity(City city) {
    activity.launchPlaceHighlight(city);
  }

  @Override public void onNetworkErrorFound(String message) {
    showAlertDialog(message);
  }

  @Override public void onApiErrorFound(String message) {
    showAlertDialog(message);
  }

  @Override public void onHttpErrorUnexpectedFound() {
    showAlertDialog(getString(R.string.unexpected_error_occurred));
  }

  @Override public void onUnauthorizedErrorFound() {
    showAlertDialog(getString(R.string.unauthorized));
  }

  @Override
  public void onClick(View v) {

    switch (v.getId()){

      case R.id.searchBtn:

        showSearchActivity();

        break;

        default:

    }

  }

  private void showSearchActivity(){

    Intent searchIntent = new Intent(getActivity(), SearchActivity.class);
    startActivity(searchIntent);

  }

  private void setupAdapter() {
    cityList = new ArrayList<>();
    searchCityAdapter = new SearchCityAdapter(activity, cityList, presenter);
    rvCity.setAdapter(searchCityAdapter);
    rvCity.setLayoutManager(new LinearLayoutManager(activity, LinearLayoutManager.VERTICAL, false));
  }
}
