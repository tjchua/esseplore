package com.essplore.essplore.ui.activities.share;

import android.app.Activity;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.AttributeSet;
import android.view.View;
import android.widget.ImageButton;
import android.widget.TabHost;
import android.widget.TextView;

import com.essplore.essplore.R;
import com.essplore.essplore.application.AppConstants;
import com.essplore.essplore.ui.activities.camera.CameraActivity;
import com.essplore.essplore.ui.activities.camera.CameraFragment;
import com.essplore.essplore.ui.activities.camera.GalleryFragment;

import java.util.ArrayList;

public class ShareActivity extends AppCompatActivity implements ShareFragmentInterface{

    private ImageButton backBtn;
    private Bitmap selectedImage;
    private final int RESULT_CODE_UPDATE_GALLERY = 1;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_share);

        initViews();
        showShareFragment();

    }

    private void initViews(){

        backBtn = (ImageButton) findViewById(R.id.back_button);
        backBtn.setOnClickListener(backBtnClickListener);

    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    private View.OnClickListener backBtnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {

            Intent returnIntent = new Intent();
            setResult(Activity.RESULT_CANCELED, returnIntent);
            finish();

        }
    };

    @Override
    public void quitWithResult(CameraActivity.ResultStatus resultStatus) {

        Intent returnIntent = new Intent();
        returnIntent.putExtra(CameraActivity.RESULT_INTENT_KEY, resultStatus.toString());
        setResult(Activity.RESULT_OK, returnIntent);
        finish();

    }

    @Override
    public void quitWithResult(CameraActivity.ResultStatus resultStatus, ArrayList<String> localImages, String title, String description) {

        Intent returnIntent = new Intent();
        returnIntent.putExtra(CameraActivity.RESULT_INTENT_KEY, resultStatus.toString());
        returnIntent.putStringArrayListExtra(CameraActivity.LOCAL_IMAGES_INTENT_KEY, localImages);
        returnIntent.putExtra(CameraActivity.TITLE_INTENT_KEY, title);
        returnIntent.putExtra(CameraActivity.DESCRIPTION_INTENT_KEY, description);
        setResult(Activity.RESULT_OK, returnIntent);
        finish();

    }

    private void showShareFragment(){

        ShareFragment shareFragment = new ShareFragment();
        shareFragment.setShareFragmentInterface(this);
        FragmentManager fragmentManager = getFragmentManager();

        if (fragmentManager != null) {

            FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
            fragmentTransaction.replace(R.id.fragment_container, shareFragment);
            fragmentTransaction.commit();

        }

    }

}
