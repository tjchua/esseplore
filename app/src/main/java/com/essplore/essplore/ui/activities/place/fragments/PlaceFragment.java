package com.essplore.essplore.ui.activities.place.fragments;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.essplore.essplore.R;
import com.essplore.essplore.ui.activities.camera.CameraActivity;
import com.essplore.essplore.ui.activities.place.PlaceActivity;
import com.essplore.essplore.ui.activities.place.PlaceGalleryActivity;
import com.essplore.essplore.ui.activities.search.SearchActivity;
import com.essplore.essplore.ui.activities.search.models.Place;
import com.essplore.essplore.ui.activities.share.ShareAPIinterface;
import com.essplore.essplore.ui.activities.share.ShareActivity;
import com.essplore.essplore.ui.activities.share.ShareAdapterInterface;
import com.essplore.essplore.ui.activities.share.ShareApi;
import com.essplore.essplore.ui.activities.share.ShareFragmentInterface;
import com.essplore.essplore.ui.activities.share.adapters.ShareAdapter;
import com.essplore.essplore.ui.utils.geolocation.GeoLocationUtil;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.share.Sharer;
import com.facebook.share.model.SharePhoto;
import com.facebook.share.model.SharePhotoContent;
import com.facebook.share.widget.ShareDialog;

import java.util.ArrayList;

public class PlaceFragment extends android.app.Fragment implements View.OnClickListener{

    private Place place;
    private ImageView banner;
    private TextView placeNameTv;
    private TextView countryNameTv;
    private TextView addressTv;
    private TextView openingHoursTv;
    private TextView descriptionTv;
    private Button photosBtn;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        final View view =  inflater.inflate(R.layout.fragment_place, container, false);
        initViews(view);
        showBanner();
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {

        super.onViewCreated(view, savedInstanceState);

    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()){

            case R.id.photos_btn:

                showPhotos();

                break;

                default:

        }
    }

    public void setPlace(Place place){

        this.place = place;

    }

    private void initViews(View view){

        banner = view.findViewById(R.id.banner);
        placeNameTv = view.findViewById(R.id.place_name_tv);
        countryNameTv = view.findViewById(R.id.country_name_tv);
        addressTv = view.findViewById(R.id.address_tv);
        openingHoursTv = view.findViewById(R.id.store_hours_tv);
        descriptionTv = view.findViewById(R.id.description_tv);
        photosBtn = view.findViewById(R.id.photos_btn);
        photosBtn.setOnClickListener(this);
        descriptionTv.setText(place.getDescription());
        addressTv.setText(place.getAddress1() + ", " + place.getAddress2());
        placeNameTv.setText(place.getName());
        countryNameTv.setText(place.getCity_name() + ", " + place.getCountry_name());
        openingHoursTv.setText(place.getOpening_hours());

    }

    private void showBanner(){

        Glide.with(getActivity())
                .load(place.getAvatar_url())
                .crossFade()
                .into(banner);

    }

    private Place getPlace(){

        if (place == null){

            place = new Place();

        }

        return  place;

    }

    private void showPhotos(){

        Intent intentPlace = new Intent(getActivity(), PlaceGalleryActivity.class);
        intentPlace.putExtra(SearchActivity.PLACE_INTENT_KEY, getPlace());
        startActivity(intentPlace);

    }


}
