package com.essplore.essplore.ui.activities.home.fragments.presenter;

import io.reactivex.disposables.Disposable;

public interface AccountPresenter {

  Disposable getCountries();

}
