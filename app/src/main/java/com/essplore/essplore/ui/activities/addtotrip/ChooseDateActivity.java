package com.essplore.essplore.ui.activities.addtotrip;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CalendarView;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.essplore.essplore.R;
import com.essplore.essplore.ui.activities.addtotrip.adapters.AddToTripAdapter;
import com.essplore.essplore.ui.activities.addtotrip.api.AddToTripFromSearchApi;
import com.essplore.essplore.ui.activities.addtotrip.dialogs.CalendarDialog;
import com.essplore.essplore.ui.activities.addtotrip.dialogs.TimePickerDialog;
import com.essplore.essplore.ui.activities.addtotrip.interfaces.AddTripFromSearchApiInterface;
import com.essplore.essplore.ui.activities.addtotrip.interfaces.CalendarDialogInterface;
import com.essplore.essplore.ui.activities.addtotrip.interfaces.TimeDialogInterface;
import com.essplore.essplore.ui.activities.addtotrip.models.Trips;
import com.essplore.essplore.ui.activities.place.PlaceActivity;

public class ChooseDateActivity extends AppCompatActivity implements View.OnClickListener,
        CalendarView.OnDateChangeListener, CalendarDialogInterface, TimeDialogInterface, AddTripFromSearchApiInterface{

    private Button calendarBtn;
    private Button timeBtn;
    private TextView calendarTv;
    private TextView timeTv;
    private Button doneBtn;
    private ProgressDialog progressDialog;
    private ImageButton backBtn;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_choose_date);
        initViews();

    }

    @Override
    public void onClick(View v) {

        switch (v.getId()){

            case R.id.calender_btn:

                CalendarDialog calendarDialog = new CalendarDialog(this);
                calendarDialog.setCalendarDialogInterface(this);
                calendarDialog.show();

                break;

            case R.id.time_btn:

                TimePickerDialog timeDialog = new TimePickerDialog(this);
                timeDialog.setTimeDialogInterface(this);
                timeDialog.show();

                break;

            case R.id.done:

                AddToTripFromSearchApi addToTripFromSearchApi =  new AddToTripFromSearchApi(this);
                addToTripFromSearchApi.setAddTripFromSearchApiInterface(this);
                getProgressDialog().show();
                addToTripFromSearchApi.addPlaceToTrip(getTrackId(), getPlaceId());

                break;

            case R.id.back_button:

                finish();

                break;

            default:
        }

    }

    @Override
    public void onSelectedDayChange(@NonNull CalendarView view, int year, int month, int dayOfMonth) {

        int monthNew = month + 1;
        calendarTv.setText(year + " - " + monthNew + " - " + dayOfMonth);

    }

    @Override
    public void onSelectedTime(int hourOfDay, int minute) {

        timeTv.setText(hourOfDay + ":" + minute);

    }

    @Override
    public void onGetTripsSuccess(String response) {

    }

    @Override
    public void onAddPlaceToTrackSuccess() {

        getProgressDialog().dismiss();
        Toast.makeText(this, "Place added to your trip", Toast.LENGTH_SHORT).show();
        finish();

    }

    @Override
    public void onAddPlaceToTrackFailed() {

        getProgressDialog().dismiss();

    }

    private void initViews(){

        calendarBtn = findViewById(R.id.calender_btn);
        timeBtn = findViewById(R.id.time_btn);
        calendarTv = findViewById(R.id.calender_tv);
        timeTv = findViewById(R.id.time_tv);
        doneBtn = findViewById(R.id.done);
        backBtn = findViewById(R.id.back_button);
        backBtn.setOnClickListener(this);
        doneBtn.setOnClickListener(this);
        calendarBtn.setOnClickListener(this);
        timeBtn.setOnClickListener(this);

    }

    private int getPlaceId(){

        return getIntent().getIntExtra(PlaceActivity.PLACE_ID_INTENT_KEY, 0);

    }

    private int getTrackId(){

        return getIntent().getIntExtra(AddTripFromSearchActivity.TRACK_ID_INTENT_KEY, 0);

    }

    private ProgressDialog getProgressDialog(){

        if (progressDialog == null){

            progressDialog = new ProgressDialog(this);
            progressDialog.setCancelable(false);
            progressDialog.setMessage(getString(R.string.uploading));

        }

        return progressDialog;

    }


}
