package com.essplore.essplore.ui.utils;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.Looper;
import android.support.v4.app.ActivityCompat;
import android.util.Log;

import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;

public class GeoLocationService implements android.location.LocationListener {

    private LocationManager locationManager;
    private FusedLocationProviderClient mFusedLocationClient;
    private Context context;
    private Location location;
    private LocationListener locationListener;

    public GeoLocationService(Context context) {

        this.context = context;
        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(context);
        startLocationUpdates();

        locationManager = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);
        //time interval needs to be multiplied by 1000 as millisecond is the actual unit.
        if (ActivityCompat.checkSelfPermission(context,
                Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED &&
                ActivityCompat.checkSelfPermission(context,
                        Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            //return TODO;
        }
        locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 1 * 1000, 0, this);
        this.onLocationChanged(null);//It is now initiated whenever app started

    }

    private LocationRequest getLocationRequest(){
        LocationRequest mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(3000);
        mLocationRequest.setFastestInterval(3000);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        return mLocationRequest;
    }

    public void  startLocationUpdates() {

        if (ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.
                PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
//        locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 1000, 0, this);
        mFusedLocationClient.requestLocationUpdates(getLocationRequest(),mLocationCallback,null  /*Looper.myLooper()*/);


    }

   /* public Location getrCurrenetLocation() {

        return location;

    }*/

    public void setLocationListener(LocationListener locationListener){

        this.locationListener = locationListener;

    }

    private LocationCallback mLocationCallback = new LocationCallback() {

        @Override
        public void onLocationResult(LocationResult locationResult) {

            if (location == null) {

                location = locationResult.getLastLocation();
                return;

            }
            locationListener.onLocationUpdate(location);
            Log.e("Location Changed", String.valueOf(location));
            mFusedLocationClient.removeLocationUpdates(mLocationCallback);

        };

    };

    @Override
    public void onLocationChanged(Location location) {
        Log.e("Changed geolocation", String.valueOf(location));
        if (location != null) {
            locationListener.onLocationChanged(location);
        }
    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {

    }

    @Override
    public void onProviderEnabled(String provider) {

    }

    @Override
    public void onProviderDisabled(String provider) {

    }

    public Location getrCurrenetLocation(){

        location = locationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);

        return location;

    };

    public void removeLocationUpdate(){

        mFusedLocationClient.removeLocationUpdates(mLocationCallback);
        locationManager.removeUpdates(this);

    }

}
