package com.essplore.essplore.ui.activities.search.models;

import android.os.Parcelable;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;
import java.util.ArrayList;

public class Place implements Serializable{

    private int id;
    private String avatar_url;
    private String name;
    private String city_name;
    private String country_name;
    private String address1;
    private String address2;
    private String opening_hours;
    private String description;
    private ArrayList<String> place_picture_list;
    private String type;

    private final String AVATAR_URL_KEY = "avatar_url";
    private final String NAME_KEY = "name";
    private final String ID_KEY = "id";
    private final String CITY_NAME_KEY = "city_name";
    private final String COUNTRY_NAME_KEY = "country_name";
    private final String ADDRESS_ONE_KEY = "address1";
    private final String ADDRESS_TWO_KEY = "address2";
    private final String OPENING_HOURS_KEY = "opening_hours";
    private final String DESCRIPTION_KEY = "description";
    private final String PLACE_PICTURE_LIST_KEY = "place_picture_list";
    private final String PLACE_PICTURE_URL_KEY = "place_picture_url";
    private final String TYPE_KEY = "type";

    public Place(){


    }

    public ArrayList<String> getPlacePictureList(){

        if (place_picture_list == null){

            place_picture_list = new ArrayList<String>();

        }

        return place_picture_list;

    }

    public Place(String json){

        try {

            JSONObject jsonObject = new JSONObject(json);
            this.id = jsonObject.isNull(ID_KEY) ? 0 : jsonObject.getInt(ID_KEY);
            this.avatar_url = jsonObject.isNull(AVATAR_URL_KEY) ? "" : jsonObject.getString(AVATAR_URL_KEY);
            this.name = jsonObject.isNull(NAME_KEY) ? "" : jsonObject.getString(NAME_KEY);
            this.city_name = jsonObject.isNull(CITY_NAME_KEY) ? "" : jsonObject.getString(CITY_NAME_KEY);
            this.country_name = jsonObject.isNull(COUNTRY_NAME_KEY) ? "" : jsonObject.getString(COUNTRY_NAME_KEY);
            this.address1 = jsonObject.isNull(ADDRESS_ONE_KEY) ? "" : jsonObject.getString(ADDRESS_ONE_KEY);
            this.address2 = jsonObject.isNull(ADDRESS_TWO_KEY) ? "" : jsonObject.getString(ADDRESS_TWO_KEY);
            this.opening_hours = jsonObject.isNull(OPENING_HOURS_KEY) ? "" : jsonObject.getString(OPENING_HOURS_KEY);
            this.description = jsonObject.isNull(DESCRIPTION_KEY) ? "" : jsonObject.getString(DESCRIPTION_KEY);
            this.type = jsonObject.isNull(TYPE_KEY) ? "" : jsonObject.getString(TYPE_KEY);
            JSONArray jsonArray = jsonObject.isNull(PLACE_PICTURE_LIST_KEY) ? new JSONArray() : jsonObject.getJSONArray(PLACE_PICTURE_LIST_KEY);

            for (int i=0; i < jsonArray.length(); i++){

                String url = jsonArray.getJSONObject(i).isNull(PLACE_PICTURE_URL_KEY) ? "" : jsonArray.getJSONObject(i).getString(PLACE_PICTURE_URL_KEY);
                getPlacePictureList().add(url);

            }

        } catch (JSONException e) {

            e.printStackTrace();

        }

    }

    public int getId() {
        return id;
    }

    public String getAvatar_url() {
        return avatar_url;
    }

    public String getName() {
        return name;
    }

    public String getCity_name() {
        return city_name;
    }

    public String getCountry_name() {
        return country_name;
    }

    public String getAddress1() {
        return address1;
    }

    public String getOpening_hours() {
        return opening_hours;
    }

    public String getAddress2() {

        return address2;

    }

    public String getDescription() {
        return description;
    }

    public String getType() {

        return type;

    }
}
