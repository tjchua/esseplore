package com.essplore.essplore.ui.activities.camera;

import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;

import com.essplore.essplore.R;
import com.essplore.essplore.ui.activities.camera.interfaces.CameraActivityListener;
import com.essplore.essplore.ui.activities.camera.interfaces.ProcessImageTaskInterface;
import com.jsibbold.zoomage.ZoomageView;
import com.otaliastudios.cameraview.CameraListener;
import com.otaliastudios.cameraview.CameraView;
import com.otaliastudios.cameraview.Flash;
import com.otaliastudios.cameraview.Gesture;
import com.otaliastudios.cameraview.GestureAction;

public class CameraFragment extends android.app.Fragment implements ProcessImageTaskInterface {

    private CameraView mCameraView;
    private ImageButton takePictureBtn;
    private ZoomageView imagePreview;
    private View previewContainer;
    private EnumCameraStatus cameraStatus;
    private Bitmap imageCaptured;
    private ImageButton retakePicture;
    private ImageButton flashBtn;
    private Flash flasStatus;
    private AppCompatActivity activity;
    private CameraActivityListener cameraActivityListener;
    private CameraApi cameraApi;

    private enum FlashStatus {
        ON,
        OFF
    }

    public CameraFragment(){

        cameraStatus = EnumCameraStatus.TAKING;

    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);

    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        final View view =  inflater.inflate(R.layout.fragment_camera, container, false);
        initViews(view);
        return view;

    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {

        super.onViewCreated(view, savedInstanceState);

        if (cameraStatus == EnumCameraStatus.TAKING){

            previewContainer.setVisibility(View.INVISIBLE);

        }else{

            imagePreview.setImageBitmap(imageCaptured);
            previewContainer.setVisibility(View.VISIBLE);

        }

        if (flasStatus == null){

            flasStatus = mCameraView.getFlash();

        }

        mCameraView.setFlash(flasStatus);
    }

    @Override
    public void onResume() {

        super.onResume();
        mCameraView.start();

    }

    @Override
    public void onPause() {

        super.onPause();
        mCameraView.stop();

    }

    @Override
    public void onDestroy() {

        super.onDestroy();
        mCameraView.destroy();
        if (imageCaptured != null)
        imageCaptured.recycle();
        System.gc();

    }

    @Override
    public void onCaptureProcessSuccessful(Bitmap bitmap) {

        imageCaptured = bitmap;
        imagePreview.setImageBitmap(imageCaptured);
        cameraActivityListener.setSelectedImage(imageCaptured);

    }

    private void initViews(View view){
        mCameraView = (CameraView) view.findViewById(R.id.camera);
        mCameraView.mapGesture(Gesture.PINCH, GestureAction.ZOOM);
        mCameraView.addCameraListener(cameraListener);
        takePictureBtn = (ImageButton) view.findViewById(R.id.take_picture);
        takePictureBtn.setOnClickListener(takePictureClickListener);
        imagePreview = view.findViewById(R.id.image_preview);
        previewContainer = (View) view.findViewById(R.id.previewContainer);
        retakePicture = (ImageButton) view.findViewById(R.id.retake_picture);
        retakePicture.setOnClickListener(retakePictureClickListener);
        flashBtn = (ImageButton) view.findViewById(R.id.flash_icon);
        flashBtn.setOnClickListener(flashClickListener);
    }

    private View.OnClickListener takePictureClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            mCameraView.capturePicture();
            cameraStatus = EnumCameraStatus.PREVIEW;
        }
    };

    private CameraListener cameraListener = new CameraListener() {
        @Override
        public void onPictureTaken(byte[] jpeg) {
            super.onPictureTaken(jpeg);
            previewCaptured(jpeg);
        }
    };

    private void previewCaptured(byte[] captured){
//        CameraUtils.decodeBitmap(captured, new CameraUtils.BitmapCallback() {
//
//            @Override
//            public void onBitmapReady(Bitmap bitmap) {
//
//                imageCaptured = bitmap;
//                imagePreview.setImageBitmap(imageCaptured);
//                cameraActivityListener.setSelectedImage(imageCaptured);
//
//            }
//
//        });

//        imageCaptured = bitmap;
//        imagePreview.setImageBitmap(imageCaptured);
//        cameraActivityListener.setSelectedImage(imageCaptured);
        getCameraApi().processImage(getActivity(), captured, this);
        previewContainer.setVisibility(View.VISIBLE);
        mCameraView.stop();

    }

    private View.OnClickListener retakePictureClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            retakePicture();
        }
    };

    public void retakePicture(){

        previewContainer.setVisibility(View.INVISIBLE);
        imagePreview.setImageBitmap(null);
        cameraStatus = EnumCameraStatus.TAKING;
        cameraActivityListener.clearSelectedImage();
        mCameraView.start();

    }

    private View.OnClickListener flashClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            if (flasStatus == Flash.ON){
                flasStatus = Flash.OFF;
                flashBtn.setImageResource(R.drawable.no_flash_icon);
                mCameraView.setFlash(flasStatus);
            }else {
                flasStatus = Flash.ON;
                flashBtn.setImageResource(R.drawable.camera_flash);
                mCameraView.setFlash(flasStatus);
            }
        }
    };

    public Bitmap getSelectedImage(){

        return imageCaptured;

    };

    public void setCameraActivityListener(CameraActivityListener cameraActivityListener){

        this.cameraActivityListener = cameraActivityListener;

    }

    public EnumCameraStatus getCameraStatus(){

        return cameraStatus;

    }

    private CameraApi getCameraApi(){

        if (cameraApi == null){

            cameraApi = new CameraApi(getActivity());

        }

        return  cameraApi;
    }

}
