package com.essplore.essplore.ui.activities.share;

import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageButton;

import com.essplore.essplore.R;

public interface ShareAPIinterface {

    public void onUploadSuccess(String response);
    public void onUploadSuccess(int position);
    public void onUploadFailed();

}
