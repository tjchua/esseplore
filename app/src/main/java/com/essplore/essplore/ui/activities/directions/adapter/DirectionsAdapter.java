package com.essplore.essplore.ui.activities.directions.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.text.Spannable;
import android.text.SpannableStringBuilder;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.essplore.essplore.R;
import com.essplore.essplore.ui.activities.directions.view.DirectionsActivity;
import com.essplore.essplore.ui.activities.timeline.adapter.TimelineViewHolder;
import com.google.maps.model.DirectionsStep;

public class DirectionsAdapter extends RecyclerView.Adapter<DirectionsViewHolder> {

    Context mContext;
    LayoutInflater mLayoutInflater;
    DirectionsActivity mDirectionsActivity;
    DirectionsStep[] mDirectionsSteps;
    public DirectionsAdapter(DirectionsActivity directionsActivity, DirectionsStep[] steps) {
        mDirectionsActivity = directionsActivity;
        mDirectionsSteps = steps;
    }
    @NonNull
    @Override
    public DirectionsViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        mContext = parent.getContext();
        mLayoutInflater = LayoutInflater.from(mContext);

        View view = mLayoutInflater.inflate(R.layout.item_directions, parent, false);
        return new DirectionsViewHolder(view);
    }


    @Override
    public void onBindViewHolder(@NonNull DirectionsViewHolder holder, int position) {

        holder.mDirections.setText(Html.fromHtml(mDirectionsSteps[position].htmlInstructions));
        holder.mDistance.setText(mDirectionsSteps[position].distance.humanReadable);
    }

    @Override
    public int getItemCount() {
        return mDirectionsSteps.length;
    }

    private SpannableStringBuilder formatString(String direction) {
        SpannableStringBuilder str = new SpannableStringBuilder(direction);
        str.setSpan(new android.text.style.StyleSpan(android.graphics.Typeface.BOLD), 0, direction.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        return str;
    }
}
