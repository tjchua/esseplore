package com.essplore.essplore.ui.activities.trip.view;

import com.essplore.essplore.models.City;
import com.essplore.essplore.ui.activities.HttpBaseView;

public interface AddTripView extends HttpBaseView {

  City getCity();

  String provideTrackName();

  int getLoggedInUserId();
}
