package com.essplore.essplore.ui.activities.addtotrip.dialogs;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CalendarView;
import android.widget.TimePicker;

import com.essplore.essplore.R;
import com.essplore.essplore.ui.activities.addtotrip.interfaces.TimeDialogInterface;

public class TimePickerDialog extends Dialog implements TimePicker.OnTimeChangedListener, View.OnClickListener{

    private TimePicker timePicker;
    private TimeDialogInterface timeDialogInterface;
    private Button setTimeBtn;
    private int hour;
    private int minute;

    public TimePickerDialog(@NonNull Context context) {
        super(context);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.dialog_time);
        initViews();

    }

    @Override
    public void onTimeChanged(TimePicker view, int hourOfDay, int minute) {

        this.hour = hourOfDay;
        this.minute = minute;

    }

    @Override
    public void onClick(View v) {

        switch (v.getId()){

            case R.id.set_time_btn:

                if (timeDialogInterface != null)
                    timeDialogInterface.onSelectedTime(hour, minute);
                dismiss();

                break;

            default:

        }
    }

    private void initViews(){

        timePicker = findViewById(R.id.timePicker);
        setTimeBtn = findViewById(R.id.set_time_btn);
        setTimeBtn.setOnClickListener(this);
        timePicker.setOnTimeChangedListener(this);

    }

    public void setTimeDialogInterface(TimeDialogInterface timeDialogInterface){

        this.timeDialogInterface = timeDialogInterface;

    }
}
