package com.essplore.essplore.ui.activities.placehighlights.view;

import com.essplore.essplore.models.City;
import com.essplore.essplore.models.CityHighlight;
import com.essplore.essplore.ui.activities.HttpBaseView;

import java.util.List;

public interface PlaceHighlightsView extends HttpBaseView {

  City provideCityObject();

  void launchCityHighlights(CityHighlight cityHighlight);
  void showProgressBar();
  void hideProgressbar();

  void setPlaceHighlights(List<CityHighlight> cityHighlightList);

  int getCountryId();

  int getCityId();


}
