package com.essplore.essplore.ui.activities.camera.interfaces;

import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;

public interface CameraActivityListener {

    public void setSelectedImage(Bitmap selectedImage);
    public void clearSelectedImage();

}
