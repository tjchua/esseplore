package com.essplore.essplore.ui.activities.addtotrip.api;

import android.content.Context;
import android.os.AsyncTask;

import com.essplore.essplore.BuildConfig;
import com.essplore.essplore.application.AppConstants;
import com.essplore.essplore.managers.SharedPreferenceKeys;
import com.essplore.essplore.ui.activities.addtotrip.interfaces.AddTripFromSearchApiInterface;
import com.essplore.essplore.ui.activities.addtotrip.interfaces.GetSpecificTrackInterface;
import com.essplore.essplore.ui.activities.search.interfaces.SearchApiInterface;

import org.apache.commons.lang3.StringUtils;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public class AddToTripFromSearchApi {

    private OkHttpClient client;
    private Context context;
    private String accessToken;
    private AddTripFromSearchApiInterface addTripFromSearchApiInterface;

    private final String TRACK_ID = "track_id";
    private final String PLACE_ID = "place_id";
    private final String URGENCY = "urgency";

    public AddToTripFromSearchApi(Context context){

        this.context = context;
        accessToken = context.getSharedPreferences(SharedPreferenceKeys.ESSPLORE_PREFERENCE, Context.
                MODE_PRIVATE).getString(SharedPreferenceKeys.ACCESS_TOKEN, StringUtils.EMPTY);

    }

    public void getGetTrips(){

        new GetTripsTask().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);

    }

    public void addPlaceToTrip(int trackId, int placeId){

        new AddPlaceToTrackTask(Integer.toString(trackId), Integer.toString(placeId)).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);

    }

    public void addPlaceToTripFromAi(int trackId, int placeId, String urgency){

        new AddPlaceToTrackFromAiTask(Integer.toString(trackId), Integer.toString(placeId), urgency).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);

    }

    public void getSpecificTrack(int trackId, GetSpecificTrackInterface getSpecificTrackInterface){

        new GetSpecificTrackTask(Integer.toString(trackId), getSpecificTrackInterface).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);

    }

    private class GetTripsTask extends AsyncTask<String, Void, String> {

        private final String failed = "FAILED";

        public GetTripsTask(){

        }

        @Override
        protected String doInBackground(String... strings) {

            try {

                return getTrips(BuildConfig.BASE_URL + AppConstants.CREATE_GET_TRACK_LIST);

            } catch (IOException e) {

                e.printStackTrace();
                return failed;

            }

        }

        @Override
        protected void onPostExecute(String s) {

            super.onPostExecute(s);

            if(s != failed){

                if (addTripFromSearchApiInterface != null)
                    addTripFromSearchApiInterface.onGetTripsSuccess(s);

            }

            this.cancel(true);

        }
    }

    private class AddPlaceToTrackTask extends AsyncTask<String, Void, String> {

        private final String failed = "FAILED";
        private final String RESPONSE_STATUS = "status";
        private final int RESPONSE_STATUS_VALUE = 200;
        private String trackId;
        private String placeId;

        public AddPlaceToTrackTask(String trackId, String placeId){

            this.trackId = trackId;
            this.placeId = placeId;

        }

        @Override
        protected String doInBackground(String... strings) {

            try {

                return addPlaceToTrip(BuildConfig.BASE_URL + AppConstants.ADD_PLACE_TO_TRACK, trackId, placeId);

            } catch (IOException e) {

                e.printStackTrace();
                return failed;

            }

        }

        @Override
        protected void onPostExecute(String s) {

            super.onPostExecute(s);

            if(getResponseStatus(s) == RESPONSE_STATUS_VALUE){

                if (addTripFromSearchApiInterface != null)
                    addTripFromSearchApiInterface.onAddPlaceToTrackSuccess();

            }else{

                if (addTripFromSearchApiInterface != null)
                    addTripFromSearchApiInterface.onAddPlaceToTrackFailed();

            }

            this.cancel(true);

        }

        private int getResponseStatus(String response){

            JSONObject jsonObject = null;
            try {
                jsonObject = new JSONObject(response);
                return jsonObject.isNull(RESPONSE_STATUS) ? 0 : jsonObject.getInt(RESPONSE_STATUS);
            } catch (JSONException e) {
                e.printStackTrace();
                return 0;
            }

        }
    }

    private class AddPlaceToTrackFromAiTask extends AsyncTask<String, Void, String> {

        private final String failed = "FAILED";
        private final String RESPONSE_STATUS = "status";
        private final int RESPONSE_STATUS_VALUE = 200;
        private String trackId;
        private String placeId;
        private String urgency;

        public AddPlaceToTrackFromAiTask(String trackId, String placeId, String urgency){

            this.trackId = trackId;
            this.placeId = placeId;
            this.urgency = urgency;

        }

        @Override
        protected String doInBackground(String... strings) {

            try {

                return addPlaceToTripFromAi(BuildConfig.BASE_URL + AppConstants.ADD_PLACE_TO_TRACK, trackId, placeId, urgency);

            } catch (IOException e) {

                e.printStackTrace();
                return failed;

            }

        }

        @Override
        protected void onPostExecute(String s) {

            super.onPostExecute(s);

            if(getResponseStatus(s) == RESPONSE_STATUS_VALUE){

                if (addTripFromSearchApiInterface != null)
                    addTripFromSearchApiInterface.onAddPlaceToTrackSuccess();

            }else{

                if (addTripFromSearchApiInterface != null)
                    addTripFromSearchApiInterface.onAddPlaceToTrackFailed();

            }

            this.cancel(true);

        }

        private int getResponseStatus(String response){

            JSONObject jsonObject = null;
            try {
                jsonObject = new JSONObject(response);
                return jsonObject.isNull(RESPONSE_STATUS) ? 0 : jsonObject.getInt(RESPONSE_STATUS);
            } catch (JSONException e) {
                e.printStackTrace();
                return 0;
            }

        }
    }

    private class GetSpecificTrackTask extends AsyncTask<String, Void, String> {

        private String trackId;
        private GetSpecificTrackInterface getSpecificTrackInterface;
        private final String RESPONSE_STATUS = "status";
        private final int RESPONSE_STATUS_VALUE = 200;

        public GetSpecificTrackTask(String trackId, GetSpecificTrackInterface getSpecificTrackInterface){

            this.trackId = trackId;
            this.getSpecificTrackInterface = getSpecificTrackInterface;

        }

        @Override
        protected String doInBackground(String... strings) {

            try {

                return getSpecificTrack(BuildConfig.BASE_URL + AppConstants.GET_SPECIFIC_TRACK + "?" +
                        AppConstants.PHOTO_TRACK_TRACK_ID_PARAM + "=" + trackId);

            } catch (IOException e) {

                e.printStackTrace();
                return "";

            }
        }

        @Override
        protected void onPostExecute(String s) {

            super.onPostExecute(s);

            if(getResponseStatus(s) == RESPONSE_STATUS_VALUE){

                if (getSpecificTrackInterface != null)
                    getSpecificTrackInterface.onGetSpecificTrackSuccess(s, trackId);

            }else{

                if (getSpecificTrackInterface != null)
                    getSpecificTrackInterface.onGetSpecificTrackFailed();

            }

        }

        private int getResponseStatus(String response){

            JSONObject jsonObject = null;
            try {
                jsonObject = new JSONObject(response);
                return jsonObject.isNull(RESPONSE_STATUS) ? 0 : jsonObject.getInt(RESPONSE_STATUS);
            } catch (JSONException e) {
                e.printStackTrace();
                return 0;
            }

        }
    }


        private String getAddPlaceToTrackReqJson(String trackId, String placeId){
        JSONObject json = new JSONObject();
        try {
            json.put(TRACK_ID, trackId);
            json.put(PLACE_ID, placeId);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return json.toString();
    }

    private String getAddPlaceToTrackFromAiReqJson(String trackId, String placeId, String urgency){
        JSONObject json = new JSONObject();
        try {
            json.put(TRACK_ID, trackId);
            json.put(PLACE_ID, placeId);
            json.put(URGENCY, urgency);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return json.toString();
    }

    private String getTrips(String url) throws IOException{

            Request request = new Request.Builder()
                    .url(url)
                    .header(AppConstants.AUTHORIZATION, AppConstants.BEARER + StringUtils.SPACE + accessToken)
                    .build();

            OkHttpClient.Builder b = new OkHttpClient.Builder();
            b.readTimeout(1, TimeUnit.MINUTES);
            b.writeTimeout(1, TimeUnit.MINUTES);
            OkHttpClient client = b.build();
            try (Response response = client.newCall(request).execute()) {
                return response.body().string();
            }
    }

    private String getSpecificTrack(String url) throws IOException{

        Request request = new Request.Builder()
                .url(url)
                .header(AppConstants.AUTHORIZATION, AppConstants.BEARER + StringUtils.SPACE + accessToken)
                .build();

        OkHttpClient.Builder b = new OkHttpClient.Builder();
        b.readTimeout(1, TimeUnit.MINUTES);
        b.writeTimeout(1, TimeUnit.MINUTES);
        OkHttpClient client = b.build();
        try (Response response = client.newCall(request).execute()) {
            return response.body().string();
        }
    }

    private String addPlaceToTrip(String url, String trackId, String placeId) throws IOException{

        MediaType mediaType = MediaType.parse(AppConstants.JSON_MEDIA_TYPE);
        RequestBody body = RequestBody.create(mediaType, getAddPlaceToTrackReqJson(trackId, placeId));

        Request request = new Request.Builder()
                .url(url)
                .header(AppConstants.AUTHORIZATION, AppConstants.BEARER + StringUtils.SPACE + accessToken)
                .post(body)
                .build();

        OkHttpClient.Builder b = new OkHttpClient.Builder();
        b.readTimeout(1, TimeUnit.MINUTES);
        b.writeTimeout(1, TimeUnit.MINUTES);
        OkHttpClient client = b.build();
        try (Response response = client.newCall(request).execute()) {
            return response.body().string();
        }
    }

    private String addPlaceToTripFromAi(String url, String trackId, String placeId, String urgency) throws IOException{

        MediaType mediaType = MediaType.parse(AppConstants.JSON_MEDIA_TYPE);
        RequestBody body = RequestBody.create(mediaType, getAddPlaceToTrackFromAiReqJson(trackId, placeId, urgency));

        Request request = new Request.Builder()
                .url(url)
                .header(AppConstants.AUTHORIZATION, AppConstants.BEARER + StringUtils.SPACE + accessToken)
                .post(body)
                .build();

        OkHttpClient.Builder b = new OkHttpClient.Builder();
        b.readTimeout(1, TimeUnit.MINUTES);
        b.writeTimeout(1, TimeUnit.MINUTES);
        OkHttpClient client = b.build();
        try (Response response = client.newCall(request).execute()) {
            return response.body().string();
        }
    }

    public void setAddTripFromSearchApiInterface(AddTripFromSearchApiInterface addTripFromSearchApiInterface){

        this.addTripFromSearchApiInterface = addTripFromSearchApiInterface;

    }
}
