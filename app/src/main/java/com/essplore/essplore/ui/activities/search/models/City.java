package com.essplore.essplore.ui.activities.search.models;

import com.essplore.essplore.ui.activities.camera.models.Gallery;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class City {

    private int id;
    private int country_id;

    private final String CITIES = "cities";
    private final String COUNTRY_ID_KEY = "country_id";
    private final String ID_KEY = "id";

    public City(String json){

        try {

            JSONObject jsonObject = new JSONObject(json);
            JSONArray jsonArray = jsonObject.getJSONArray(CITIES);

            for (int i=0; i < jsonArray.length(); i++){

                this.id = jsonArray.getJSONObject(i).getInt(ID_KEY);
                this.country_id = jsonArray.getJSONObject(i).getInt(COUNTRY_ID_KEY);

            }

        } catch (JSONException e) {

            e.printStackTrace();

        }

    }

    public City(){

    }

    public int getId() {
        return id;
    }

    public int getCountry_id() {
        return country_id;
    }

}
