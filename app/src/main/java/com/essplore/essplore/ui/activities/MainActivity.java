package com.essplore.essplore.ui.activities;

import android.app.Activity;
import android.os.Handler;
import android.support.v7.app.AlertDialog;
import com.essplore.essplore.R;
import com.essplore.essplore.application.EssploreApplication;
import com.essplore.essplore.managers.AppActivityManager;
import javax.inject.Inject;

public class MainActivity extends ToolBarBaseActivity {

  @Inject AlertDialog alertDialog;
  @Inject AppActivityManager manager;

  @Override protected void setupActivityLayout() {
    setContentView(R.layout.activity_main);
  }

  @Override protected void setupViewElements() {
    launchLoginActivity();
  }

  @Override protected void injectDaggerComponent() {
    EssploreApplication.get(this).createMainComponent(this).inject(this);
  }

  @Override protected boolean isActionbarBackButtonEnabled() {
    return Boolean.FALSE;
  }

  @Override protected void onDestroy() {
    super.onDestroy();
    EssploreApplication.get(this).releaseMainComponent();
  }

  private void launchLoginActivity() {
    new Handler().postDelayed(() -> manager.launchHomeActivity(MainActivity.this), 2000);
  }

  @Override protected Activity getActivity() {
    return this;
  }

  @Override protected AlertDialog getAlertDialog() {
    return alertDialog;
  }
}
