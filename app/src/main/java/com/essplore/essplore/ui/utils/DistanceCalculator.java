package com.essplore.essplore.ui.utils;

import android.location.Location;

public class DistanceCalculator {

    private static double PI_RAD = Math.PI / 180.0;

    /**
     * Use Great Circle distance formula to calculate distance between 2 coordinates in meters.
     */
    public double greatCircleInFeet(Location latLng1, Location latLng2) {
        return greatCircleInKilometers(latLng1.getLatitude(), latLng1.getLongitude(), latLng2.getLatitude(),
                latLng2.getLongitude()) * 3280.84;
    }

    /**
     * Use Great Circle distance formula to calculate distance between 2 coordinates in meters.
     */
    public double greatCircleInMeters(Location latLng1, Location latLng2) {
        return greatCircleInKilometers(latLng1.getLatitude(), latLng1.getLongitude(), latLng2.getLatitude(),
                latLng2.getLongitude()) * 1000;
    }

    /**
     * Use Great Circle distance formula to calculate distance between 2 coordinates in kilometers.
     * https://software.intel.com/en-us/blogs/2012/11/25/calculating-geographic-distances-in-location-aware-apps
     */
    public double greatCircleInKilometers(double lat1, double long1, double lat2, double long2) {
        double phi1 = lat1 * PI_RAD;
        double phi2 = lat2 * PI_RAD;
        double lam1 = long1 * PI_RAD;
        double lam2 = long2 * PI_RAD;

        return 6371.01 * Math.acos(Math.sin(phi1) * Math.sin(phi2) + Math.cos(phi1) * Math.cos(phi2) * Math.cos(lam2 - lam1));
    }

}
