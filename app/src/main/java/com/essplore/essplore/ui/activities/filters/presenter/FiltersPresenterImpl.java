package com.essplore.essplore.ui.activities.filters.presenter;

import com.essplore.essplore.api.managers.ApiManager;
import com.essplore.essplore.api.requests.AddUserPersonalityRequest;
import com.essplore.essplore.api.response.AddUserPersonalityResponse;
import com.essplore.essplore.api.utils.SimpleObserver;
import com.essplore.essplore.ui.activities.HttpBasePresenter;
import com.essplore.essplore.ui.activities.filters.view.FiltersView;
import io.reactivex.disposables.Disposable;

public class FiltersPresenterImpl extends HttpBasePresenter implements FiltersPresenter {

  private final FiltersView view;
  private final ApiManager manager;

  public FiltersPresenterImpl(FiltersView view, ApiManager manager) {
    this.view = view;
    this.manager = manager;
  }

  //TODO build your personality request here
  @Override public AddUserPersonalityRequest buildAddUserPersonalityRequest() {
    return new AddUserPersonalityRequest(view.getCreateTrackRequest().getCityId(), view.provideBuddies(), view.provideNumberOfVisit(), view.provideSpots(), view.provideInterest());
  }

  @Override public Disposable addUserPersonality() {
    view.showProgressbar();
    return manager.addUserPersonality(buildAddUserPersonalityRequest(), view.provideToken())
        .subscribe(new SimpleObserver<AddUserPersonalityResponse>() {
          @Override public void accept(AddUserPersonalityResponse addUserPersonalityResponse) {
              view.hideProgressbar();
              view.launchCalendarActivity();
          }
        }, throwable -> {
          view.hideProgressbar();
          handleHttpError(view, throwable);
        });
  }
}
