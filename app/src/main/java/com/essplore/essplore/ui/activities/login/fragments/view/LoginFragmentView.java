package com.essplore.essplore.ui.activities.login.fragments.view;

import com.essplore.essplore.api.response.LoginResponse;
import com.essplore.essplore.ui.activities.HttpBaseView;

public interface LoginFragmentView extends HttpBaseView {
  void showProgress();

  void hideProgress();

  void setUsernameError();

  void setPasswordError();

  void navigateToHome();

  void saveResponse(LoginResponse response);

  String provideUsername();

  String providePassword();
}
