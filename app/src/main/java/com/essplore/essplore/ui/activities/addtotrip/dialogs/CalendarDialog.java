package com.essplore.essplore.ui.activities.addtotrip.dialogs;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.CalendarView;
import android.widget.DatePicker;

import com.essplore.essplore.R;
import com.essplore.essplore.ui.activities.addtotrip.interfaces.CalendarDialogInterface;

import java.util.Calendar;

public class CalendarDialog extends Dialog implements CalendarView.OnDateChangeListener{

    private CalendarView calendarView;
    private CalendarDialogInterface calendarDialogInterface;

    public CalendarDialog(@NonNull Context context) {
        super(context);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.dialog_calendar);
        initViews();

    }

    private void initViews(){

        calendarView = findViewById(R.id.calendarView);
        calendarView.setOnDateChangeListener(this);

    }

    @Override
    public void onSelectedDayChange(@NonNull CalendarView view, int year, int month, int dayOfMonth) {
        if (calendarDialogInterface != null)
            calendarDialogInterface.onSelectedDayChange(view, year, month, dayOfMonth);
        dismiss();
    }

    public void setCalendarDialogInterface(CalendarDialogInterface calendarDialogInterface){

        this.calendarDialogInterface = calendarDialogInterface;

    }
}
