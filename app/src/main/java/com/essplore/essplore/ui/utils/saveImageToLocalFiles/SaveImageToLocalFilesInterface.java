package com.essplore.essplore.ui.utils.saveImageToLocalFiles;

import java.io.File;

public interface SaveImageToLocalFilesInterface {

    public void onSaveToLocalSuccess(File file);
    public void onSaveToLocalError();

}
