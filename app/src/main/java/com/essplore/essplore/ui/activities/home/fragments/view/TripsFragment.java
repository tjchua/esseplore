package com.essplore.essplore.ui.activities.home.fragments.view;

import android.Manifest;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.LocationManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputEditText;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import com.essplore.essplore.R;
import com.essplore.essplore.application.AppConstants;
import com.essplore.essplore.application.EssploreApplication;
import com.essplore.essplore.managers.AppActivityManager;
import com.essplore.essplore.managers.SharedPreferenceManager;
import com.essplore.essplore.models.City;
import com.essplore.essplore.models.MyTrack;
import com.essplore.essplore.ui.activities.HttpBaseFragment;
import com.essplore.essplore.ui.activities.home.adapter.SearchCityAdapter;
import com.essplore.essplore.ui.activities.home.adapter.TripFragmentAdapter;
import com.essplore.essplore.ui.activities.home.fragments.presenter.SearchPresenterImpl;
import com.essplore.essplore.ui.activities.home.fragments.presenter.TripPresenterImpl;
import com.essplore.essplore.ui.activities.home.view.HomeActivity;
import com.essplore.essplore.ui.activities.places.adapter.UserAction;
import com.essplore.essplore.ui.activities.places.view.PlacesActivity;

import io.reactivex.disposables.CompositeDisposable;
import java.util.ArrayList;
import java.util.List;
import javax.inject.Inject;
import org.apache.commons.lang3.StringUtils;

public class TripsFragment extends HttpBaseFragment implements SearchView, TripsFragmentView {

    @Inject AlertDialog alertDialog;
    @Inject SharedPreferenceManager sharedPreferenceManager;
    @Inject AppActivityManager appActivityManager;
    @Inject CompositeDisposable disposable;
    @Inject SearchPresenterImpl presenter;
    @Inject TripPresenterImpl tripPresenter;

    @BindView(R.id.llNotLoggedIn) LinearLayout llNotLoggedIn;
    @BindView(R.id.llMyTrackList) LinearLayout llMyTrackList;
    @BindView(R.id.tilSearch) TextInputLayout tilSearch;
    @BindView(R.id.etSearch) TextInputEditText etSearch;
    @BindView(R.id.llLoggedIn) LinearLayout llLoggedIn;
    @BindView(R.id.rvCity) RecyclerView rvCity;
    @BindView(R.id.rvTracks) RecyclerView rvTracks;
    @BindView(R.id.imageView) ImageView imgIcon;
    @BindView(R.id.pbSearch) ProgressBar pbSearch;
    @BindView(R.id.srlTripFragment) SwipeRefreshLayout srlTripFragment;

    private Unbinder unbinder;
    private HomeActivity activity;
    private RelativeLayout.LayoutParams layoutParams;
    private SearchCityAdapter searchCityAdapter;
    private TripFragmentAdapter tripFragmentAdapter;
    private List<MyTrack> myTracks;
    private MyTrack myTrack;
    private List<City> cityList;

    public TripsFragment() {
        // Intended to be empty.
    }

    @Nullable @Override public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                                                 @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_trips, container, false);
        unbinder = ButterKnife.bind(this, view);
        return view;
    }

    @Override public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        layoutParams = (RelativeLayout.LayoutParams) tilSearch.getLayoutParams();
        EssploreApplication.get(activity).createTripsComponent(this).inject(this);
        onViewLoaded();
        setupAdapter();
        setupListener();
        if (sharedPreferenceManager.isUserLoggedIn())
        disposable.add(tripPresenter.getTracks());
    }

    @Override public void onAttach(Context context) {
        super.onAttach(context);
        this.activity = (HomeActivity) context;
    }

    @Override public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (disposable != null && !isVisibleToUser && disposable.size() > 0) {
            disposable.clear();
            disposable.dispose();
        }
    }

    @Override public void onResume() {
        super.onResume();
        if (cityList != null && cityList.size() > 0) {
            activity.showToolBar();
            hidePanelsAndShowTrackList();
            disposable.add(tripPresenter.getTracks());
        } else {
            activity.hideToolBar();
        }

        if (sharedPreferenceManager.isUserLoggedIn()) {

            final Handler handler = new Handler();
            handler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    // Do something after 5s = 5000ms
                    activity.showToolBar();
                    setHasOptionsMenu(true);
                }
            }, 500);
            showLoggedInPanel();
        } else {
            hideLoggedInPanel();
        }
    }

    @Override public void onDestroy() {
        super.onDestroy();
        unbinder.unbind();
        EssploreApplication.get(activity).releaseTripsComponent();
        if (!disposable.isDisposed()) {
            disposable.clear();
            disposable.dispose();
        }
    }

    @Override protected Fragment getFragment() {
        return this;
    }

    @Override protected AlertDialog getAlertDialog() {
        return alertDialog;
    }

    @Override public void onViewLoaded() {

        getActivity().getWindow().setSoftInputMode(
                WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN
        );
    }

    @Override public void modifyLayout() {
        if (StringUtils.isNotEmpty(provideSearch())) {
            layoutParams.removeRule(RelativeLayout.CENTER_VERTICAL);
            showRecyclerView();
        } else {
            layoutParams.addRule(RelativeLayout.CENTER_VERTICAL, R.id.tilSearch);
            hideRecyclerView();
        }
    }

    @Override public String provideSearch() {
        return etSearch.getText().toString();
    }

    @Override public String provideToken() {
        return sharedPreferenceManager.getAccessToken();
    }

    @Override public void showTrackList(List<MyTrack> myTracks) {
        tripFragmentAdapter = new TripFragmentAdapter(activity, myTracks, tripPresenter);
        rvTracks.setAdapter(tripFragmentAdapter);
    }

    @Override public void setupListener() {
        etSearch.addTextChangedListener(new TextWatcher() {
            @Override public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override public void afterTextChanged(Editable s) {
                if (StringUtils.isNotEmpty(provideSearch())) {
                    modifyLayout();
                    disposable.add(presenter.getCity());
                } else {
                    modifyLayout();
                }
            }
        });

        srlTripFragment.setOnRefreshListener(() -> disposable.add(tripPresenter.getTracks()));
    }

    @Override public void showRecyclerView() {
        rvCity.setVisibility(View.VISIBLE);
        imgIcon.setVisibility(View.GONE);
    }

    @Override public void hideRecyclerView() {
        rvCity.setVisibility(View.GONE);
        imgIcon.setVisibility(View.VISIBLE);
    }

    @Override public void launchActivity(City city) {
        activity.launchAddTrip(city);
    }

    @Override public void showProgressbar() {
        pbSearch.setVisibility(View.VISIBLE);
    }

    @Override public void hideProgressbar() {
        pbSearch.setVisibility(View.GONE);
    }

    @Override public void hidePanelsAndShowTrackList() {
        imgIcon.setVisibility(View.GONE);
        llLoggedIn.setVisibility(View.GONE);
        llNotLoggedIn.setVisibility(View.GONE);
        llMyTrackList.setVisibility(View.VISIBLE);
        activity.showToolBar();
        setHasOptionsMenu(true);
    }

    @Override public void showPanelsAndHideTrackList() {
        imgIcon.setVisibility(View.VISIBLE);
        llLoggedIn.setVisibility(View.VISIBLE);
        llNotLoggedIn.setVisibility(View.GONE);
        llMyTrackList.setVisibility(View.GONE);
        activity.showToolBar();
        setHasOptionsMenu(false);

        getActivity().getWindow().setSoftInputMode(
                WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE
        );
    }

    @Override public void showSwipeRefreshLoading() {
        srlTripFragment.setRefreshing(Boolean.TRUE);
    }

    @Override public void hideSwipeRefreshLoading() {
        srlTripFragment.setRefreshing(Boolean.FALSE);
    }

    @Override public void showLoggedInPanel() {
        llLoggedIn.setVisibility(View.VISIBLE);
        llNotLoggedIn.setVisibility(View.GONE);
        activity.hideToolBar();
        setHasOptionsMenu(false);
    }

    @Override public void hideLoggedInPanel() {
        llLoggedIn.setVisibility(View.GONE);
        llNotLoggedIn.setVisibility(View.VISIBLE);
        activity.hideToolBar();
        setHasOptionsMenu(false);
    }

    @OnClick(R.id.btnSignIn) void onSignInClicked() {
        appActivityManager.launchLoginActivity(activity);
    }

    @Override public void onNetworkErrorFound(String message) {
        showAlertDialog(message);
    }

    @Override public void onApiErrorFound(String message) {
        showAlertDialog(message);
    }

    @Override public void onHttpErrorUnexpectedFound() {
        showAlertDialog(getString(R.string.unexpected_error_occurred));
    }

    @Override public void onUnauthorizedErrorFound() {
        showAlertDialog(getString(R.string.unauthorized));
    }

    @Override public void setCityListValue(List<City> cityListValue) {
        cityList.clear();
        cityList.addAll(cityListValue);
        searchCityAdapter.notifyDataSetChanged();
    }

    @Override public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_track, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.menu_add_track) {
            showPanelsAndHideTrackList();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void setupAdapter() {
        cityList = new ArrayList<>();
        searchCityAdapter = new SearchCityAdapter(activity, cityList, presenter);
        rvCity.setAdapter(searchCityAdapter);
        rvCity.setLayoutManager(new LinearLayoutManager(activity, LinearLayoutManager.VERTICAL, false));
    }

    @Override
    public void launchActivity(MyTrack myTrack) {
        this.myTrack = myTrack;
        //appActivityManager.launchPlacesActivityFromTripFragment(getActivity(), myTrack);
        checkGPSEnabled();
    }

    @Override
    public void deleteTrack(int position) {
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(getActivity(), R.style.ExitDialog);
        AlertDialog deleteDialog = dialogBuilder.create();
        deleteDialog.setMessage(getString(R.string.are_you_sure_you_want_to_delete));
        deleteDialog.setButton(DialogInterface.BUTTON_POSITIVE, getString(R.string.yes), (dialog, which) -> {
            MyTrack myTrack = tripFragmentAdapter.getDataList().get(position);
            disposable.add(tripPresenter.deleteTrack(myTrack));
            deleteDialog.dismiss();
        });
        deleteDialog.setButton(DialogInterface.BUTTON_NEGATIVE, getString(R.string.no), (dialog, which) -> {
            deleteDialog.dismiss();
        });
        deleteDialog.show();
    }

    @Override
    public void removeTrackInList(MyTrack myTrack) {
        int position = tripFragmentAdapter.getDataList().indexOf(myTrack);
        tripFragmentAdapter.getDataList().remove(position);
        tripFragmentAdapter.notifyItemRemoved(position);
    }

    private void checkGPSEnabled() {
        LocationManager lm = (LocationManager) getActivity().getSystemService(Context.LOCATION_SERVICE);
        boolean gps_enabled = false;
        boolean network_enabled = false;

        try {
            gps_enabled = lm.isProviderEnabled(LocationManager.GPS_PROVIDER);
        } catch (Exception ex) {
        }

        try {
            network_enabled = lm.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
        } catch (Exception ex) {
        }

        if (!gps_enabled && !network_enabled) {
            // notify user
            AlertDialog.Builder dialog = new AlertDialog.Builder(getContext());
            dialog.setMessage(this.getResources().getString(R.string.gps_network_not_enabled));
            dialog.setPositiveButton(this.getResources().getString(R.string.open_location_settings), new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface paramDialogInterface, int paramInt) {
                    // TODO Auto-generated method stub
                    Intent myIntent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                    getActivity().getApplicationContext().startActivity(myIntent);
                    //get gps
                }
            });
            dialog.setNegativeButton(this.getString(R.string.Cancel), new DialogInterface.OnClickListener() {

                @Override
                public void onClick(DialogInterface paramDialogInterface, int paramInt) {
                    // TODO Auto-generated method stub

                }
            });
            dialog.show();
        } else  {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                requestPermissions(new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, 101);
            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case 101:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    //startTrip();
                    //view -> presenter.onSingleItemClick(this.tracks.);
                    //Log.e("My Track", String.valueOf(this.getMyTrack().getTrackName()));
                    Bundle bundle = new Bundle();
                    if (this.myTrack == null) {
                        //Log.e("Track ID", String.valueOf(getMyTrip().getMyTrack().getTrackId()));
                        //compositeDisposable.add(presenter.getSpecificTrackForTimeline(getMyTrip().getMyTrack().getTrackId()));
                    } else {
                        bundle.putParcelable(AppConstants.MY_TRACK_OBJECT, this.myTrack);
                        appActivityManager.launchTimeline(getActivity(), bundle);
                        //appActivityManager.launchPlacesActivityFromTripFragment(getActivity(), myTrack);
                    }
                    // do your work here
                } else if (Build.VERSION.SDK_INT >= 23 && !shouldShowRequestPermissionRationale(permissions[0])) {
                    //Toast.makeText(PlacesActivity.this, "Go to Settings and Grant the permission to use this feature.", Toast.LENGTH_SHORT).show();
                    // User selected the Never Ask Again Option
                } else {
                    //Toast.makeText(PlacesActivity.this, "Permission Denied", Toast.LENGTH_SHORT).show();
                }
                break;
        }
    }
}
