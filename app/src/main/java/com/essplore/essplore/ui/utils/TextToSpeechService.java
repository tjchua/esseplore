package com.essplore.essplore.ui.utils;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.preference.PreferenceManager;
import android.speech.tts.TextToSpeech;
import android.speech.tts.UtteranceProgressListener;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.Toast;

import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;

import java.util.Locale;

public class TextToSpeechService extends UtteranceProgressListener implements TextToSpeech.OnInitListener {

    private TextToSpeech mTTS = null;
    private final int ACT_CHECK_TTS_DATA = 1000;
    private Context context;
    private TextToSpeech.OnInitListener onInitListener;
    private String text;
    private TextToSpeechServiceInterface textToSpeechServiceInterface;

    public TextToSpeechService(Context context){

        this.context = context;
        mTTS = new TextToSpeech(context, this);

    }

    public void startTts(Context context){

        Intent ttsIntent = new Intent();
        ttsIntent.setAction(TextToSpeech.Engine.ACTION_CHECK_TTS_DATA);
        ((AppCompatActivity)context).startActivityForResult(ttsIntent, ACT_CHECK_TTS_DATA);


    }

    public void startGuide(String text) {

        this.text = text;
        mTTS.speak(text, TextToSpeech.QUEUE_FLUSH, null, "test");

    }

    public void stopGuide(){

        mTTS.stop();

    }

    @Override
    public void onInit(int status) {

        if (status == TextToSpeech.SUCCESS) {

            if (mTTS != null) {

                int result = mTTS.setLanguage(Locale.ENGLISH);
                if (result == TextToSpeech.LANG_MISSING_DATA || result == TextToSpeech.LANG_NOT_SUPPORTED) {

                    Toast.makeText(context, "TTS language is not supported", Toast.LENGTH_LONG).show();

                } else {

                    startGuide(text);

                }

            }

            mTTS.setOnUtteranceProgressListener(this);
        } else {

            Toast.makeText(context, "TTS initialization failed", Toast.LENGTH_LONG).show();

        }

    }

    @Override
    public void onDone(String utteranceId) {
        if (textToSpeechServiceInterface != null)
            textToSpeechServiceInterface.onDoneSpeak();
    }

    @Override
    public void onError(String utteranceId) {

    }

    @Override
    public void onStart(String utteranceId) {
        if (textToSpeechServiceInterface != null)
            textToSpeechServiceInterface.onStartSpeak();
    }

    public void setTextToSpeechServiceInterface(TextToSpeechServiceInterface textToSpeechServiceInterface){

        this.textToSpeechServiceInterface = textToSpeechServiceInterface;

    }
}
