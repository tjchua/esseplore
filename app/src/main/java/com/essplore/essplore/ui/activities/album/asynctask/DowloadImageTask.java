package com.essplore.essplore.ui.activities.album.asynctask;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.util.Log;

import com.essplore.essplore.ui.activities.album.interfaces.DownloadImageResponseListenter;
import com.essplore.essplore.ui.activities.album.models.Album;

import java.io.IOException;

import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

public class DowloadImageTask extends AsyncTask<Void, Void, Bitmap> {

    private OkHttpClient client;
    private Request request;
    private int listPosition;
    private DownloadImageResponseListenter downloadImageResponseListenter;

    public DowloadImageTask(String url, int listPosition){
        this.listPosition = listPosition;
        client = new OkHttpClient();
        request = new Request.Builder()
                .url(url)
                .build();
    }

    @Override
    protected Bitmap doInBackground(Void... voids) {




        Response response = null;
        Bitmap mIcon11 = null;
        try {
            response = client.newCall(request).execute();
        } catch (IOException e) {
            e.printStackTrace();
        }
        if (response.isSuccessful()) {
            try {
                mIcon11 = BitmapFactory.decodeStream(response.body().byteStream());
            } catch (Exception e) {
                Log.e("Error", e.getMessage());
                e.printStackTrace();
            }

        }
        return mIcon11;
    }

    @Override
    protected void onPostExecute(Bitmap bitmap) {
        super.onPostExecute(bitmap);
        downloadImageResponseListenter.onFinishDownload(listPosition, bitmap);
    }

    public void setDownloadImageResponseListenter(DownloadImageResponseListenter downloadImageResponseListenter){
        this.downloadImageResponseListenter = downloadImageResponseListenter;
    }
}
