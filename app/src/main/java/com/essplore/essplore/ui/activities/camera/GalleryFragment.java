package com.essplore.essplore.ui.activities.camera;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.media.Image;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;

import com.essplore.essplore.R;
import com.essplore.essplore.application.AppConstants;
import com.essplore.essplore.models.MyTrack;
import com.essplore.essplore.ui.activities.camera.adapter.GalleryAdapter;
import com.essplore.essplore.ui.activities.camera.models.Galleries;
import com.essplore.essplore.ui.activities.camera.models.Gallery;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class GalleryFragment extends android.app.Fragment{

    private RecyclerView recyclerView;
    private GalleryAdapter mAdapter;
    private ImageView selectedImageView;
    private Galleries galleries;
    private CameraApi cameraApi;
    private Context context;

    public GalleryFragment(){

    }

    public void init(Context context){

        galleries = new Galleries();
        this.context = context;
        cameraApi = new CameraApi(context);
        mAdapter = new GalleryAdapter(galleries, context);

    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        final View view =  inflater.inflate(R.layout.fragment_gallery, container, false);
        initViews(view);
        return view;
    }

    private void initViews(View view){

        selectedImageView = (ImageView) view.findViewById(R.id.selectedImage);
        mAdapter.setSelectedImageView(selectedImageView);
        recyclerView = (RecyclerView) view.findViewById(R.id.recycler_view);
        recyclerView.setHasFixedSize(true);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity().getApplicationContext(),
                LinearLayoutManager.HORIZONTAL, false);
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setAdapter(mAdapter);
        getImages();
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {

        super.onViewCreated(view, savedInstanceState);

    }

    @Override
    public void onResume() {
        super.onResume();
    }

    private void getImages(){

        cameraApi.getImages(this, getMyTrack().getTrackId());

    }

    public void showPhotoList(String galleryJson){
        galleries.addObjects(galleryJson);
        mAdapter.notifyDataSetChanged();
    }

    public Bitmap getSelectedImage(){
        return ((BitmapDrawable) selectedImageView.getDrawable()).getBitmap();
    }

    private MyTrack getMyTrack() {
        Bundle bundle = getActivity().getIntent().getBundleExtra(AppConstants.MY_TRACK_OBJECT);
        return bundle.getParcelable(AppConstants.MY_TRACK_OBJECT);
    }
}
