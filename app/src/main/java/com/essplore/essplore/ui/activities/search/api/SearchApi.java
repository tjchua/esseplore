package com.essplore.essplore.ui.activities.search.api;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.location.Location;
import android.os.AsyncTask;
import android.util.Log;

import com.essplore.essplore.BuildConfig;
import com.essplore.essplore.application.AppConstants;
import com.essplore.essplore.managers.SharedPreferenceKeys;
import com.essplore.essplore.ui.activities.camera.GalleryFragment;
import com.essplore.essplore.ui.activities.search.interfaces.SearchApiInterface;
import com.essplore.essplore.ui.activities.share.ShareAPIinterface;
import com.squareup.okhttp.FormEncodingBuilder;

import org.apache.commons.lang3.StringUtils;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.Calendar;
import java.util.Date;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

import id.zelory.compressor.Compressor;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public class SearchApi {

    private OkHttpClient client;
    private Context context;
    private String accessToken;
    private final String CITY_NAME = "city_name";
    private final String COUNTRY_ID = "country_id";
    private final String CITY_ID = "city_id";
    private final String PLACE_NAME = "place_name";
    private SearchApiInterface searchApiInterface;

    public SearchApi(Context context){

        this.context = context;
        accessToken = context.getSharedPreferences(SharedPreferenceKeys.ESSPLORE_PREFERENCE, Context.
                MODE_PRIVATE).getString(SharedPreferenceKeys.ACCESS_TOKEN, StringUtils.EMPTY);

    }

    public void getCityFromApi(String searchText){

        new SearchCityTask(searchText).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);

    }

    public void getPlacesFromApi(int cityId, int countryId, String search){

        new GetPlacesTask(Integer.toString(cityId), Integer.toString(countryId), search).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);

    }

    private class SearchCityTask extends AsyncTask<String, Void, String> {

        private final String failed = "FAILED";
        private String searchText;

        public SearchCityTask(String searchText){

            this.searchText = searchText;

        }

        @Override
        protected String doInBackground(String... strings) {

            try {

                return getCity(BuildConfig.BASE_URL + AppConstants.CITY_API, searchText);

            } catch (IOException e) {

                e.printStackTrace();
                return failed;

            }

        }

        @Override
        protected void onPostExecute(String s) {

            super.onPostExecute(s);

            if(s != failed){

                if (searchApiInterface != null)
                    searchApiInterface.onGetCitySuccess(s);

            }

            this.cancel(true);

        }
    }

    private class GetPlacesTask extends AsyncTask<String, Void, String> {

        private final String failed = "FAILED";
        private String cityId;
        private String countryId;
        private String search;

        public GetPlacesTask(String cityId, String countryId, String search){

            this.cityId = cityId;
            this.countryId = countryId;
            this.search = search;

        }

        @Override
        protected String doInBackground(String... strings) {

            try {

                return getPlaces(BuildConfig.BASE_URL + AppConstants.CITY_HIGHLIGHTS_API, cityId, countryId, search);

            } catch (IOException e) {

                e.printStackTrace();
                return failed;

            }

        }

        @Override
        protected void onPostExecute(String s) {

            super.onPostExecute(s);

            if(s != failed){

                if (searchApiInterface != null)
                    searchApiInterface.onGetPlacesSuccess(s);

            }

            this.cancel(true);

        }
    }

    private String getCityReqJson(String searchText){
        JSONObject json = new JSONObject();
        try {
            json.put(CITY_NAME, searchText);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return json.toString();
    }

    private String getPlacesReqJson(String cityId, String countryId, String search){
        JSONObject json = new JSONObject();
        try {
            json.put(PLACE_NAME, search);
//            json.put(COUNTRY_ID, countryId);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return json.toString();
    }

    private String getCity(String url, String searchText) throws IOException{

        MediaType mediaType = MediaType.parse(AppConstants.JSON_MEDIA_TYPE);
        RequestBody body = RequestBody.create(mediaType, getCityReqJson(searchText));

            Request request = new Request.Builder()
                    .url(url)
                    .post(body)
                    .build();

            OkHttpClient.Builder b = new OkHttpClient.Builder();
            b.readTimeout(1, TimeUnit.MINUTES);
            b.writeTimeout(1, TimeUnit.MINUTES);
            OkHttpClient client = b.build();
            try (Response response = client.newCall(request).execute()) {
                return response.body().string();
            }
    }

    private String getPlaces(String url, String cityId, String countryId, String search) throws IOException{

        MediaType mediaType = MediaType.parse(AppConstants.JSON_MEDIA_TYPE);
        RequestBody body = RequestBody.create(mediaType, getPlacesReqJson(cityId, countryId, search));

        Request request = new Request.Builder()
                .url(url)
                .post(body)
                .build();

        OkHttpClient.Builder b = new OkHttpClient.Builder();
        b.readTimeout(1, TimeUnit.MINUTES);
        b.writeTimeout(1, TimeUnit.MINUTES);
        OkHttpClient client = b.build();
        try (Response response = client.newCall(request).execute()) {
            return response.body().string();
        }
    }

    public void setShareApiInterface(SearchApiInterface searchApiInterface){

        this.searchApiInterface = searchApiInterface;

    }
}
