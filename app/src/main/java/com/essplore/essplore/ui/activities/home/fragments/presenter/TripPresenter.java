package com.essplore.essplore.ui.activities.home.fragments.presenter;

import com.essplore.essplore.api.requests.DeleteSpecificTrackRequest;
import com.essplore.essplore.models.MyTrack;

import io.reactivex.disposables.Disposable;

public interface TripPresenter {

    Disposable getTracks();
    Disposable deleteTrack(MyTrack myTrack);
    void onSingleItemClick(MyTrack myTrack);
    void onDeleteItem(int position);

}
