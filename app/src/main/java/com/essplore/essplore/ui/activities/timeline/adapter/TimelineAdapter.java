package com.essplore.essplore.ui.activities.timeline.adapter;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.location.Location;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.bumptech.glide.Glide;
import com.essplore.essplore.R;
import com.essplore.essplore.models.TimelineMarker;
import com.essplore.essplore.models.Timeline;
import com.essplore.essplore.models.Tracks;
import com.essplore.essplore.ui.activities.directions.view.DirectionsActivity;
import com.essplore.essplore.ui.activities.timeline.view.TimelineActivity;
import com.essplore.essplore.ui.utils.VectorDrawableUtils;
import com.github.vipulasri.timelineview.TimelineView;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;

import java.io.ByteArrayOutputStream;
import java.util.List;

public class TimelineAdapter extends RecyclerView.Adapter<TimelineViewHolder> implements OnMapReadyCallback{

    //private List<Timeline> mFeedList;
    private List<Tracks> tracks;
    private Context mContext;
    private boolean mWithLinePadding;
    private LayoutInflater mLayoutInflater;
    private TimelineView timelineView;
    private int mLayoutId;

    private TimelineActivity timelineActivity;
    private DirectionsActivity directionsActivity;
    private Location location;

    private View.OnClickListener mOnClickListenerTimeline;
    private View.OnLongClickListener mOnLongClickListenerTimeline;



    public TimelineAdapter(TimelineActivity activity, List<Tracks> tracks,
                           View.OnClickListener mOnClickListenerTimeline,
                           View.OnLongClickListener mOnLongClickListenerTimeline) {
        timelineActivity = activity;
        //mFeedList = feedList;
        this.tracks = tracks;
        this.location = location;
        this.mOnClickListenerTimeline = mOnClickListenerTimeline;
        this.mOnLongClickListenerTimeline = mOnLongClickListenerTimeline;
    }

    public TimelineAdapter(DirectionsActivity activity, List<Tracks> tracks,
                           View.OnClickListener mOnClickListenerTimeline,
                           View.OnLongClickListener mOnLongClickListenerTimeline) {
        directionsActivity = activity;
        //mFeedList = feedList;
        this.tracks = tracks;
        this.location = location;
        this.mOnClickListenerTimeline = mOnClickListenerTimeline;
        this.mOnLongClickListenerTimeline = mOnLongClickListenerTimeline;
    }
    @Override
    public int getItemViewType(int position) {
        return TimelineView.getTimeLineViewType(position,getItemCount());
    }

    @Override
    public TimelineViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        mContext = parent.getContext();
        mLayoutInflater = LayoutInflater.from(mContext);

        View view = mLayoutInflater.inflate(R.layout.item_timeline, parent, false);
        Activity activity = null;
        if (timelineActivity != null) {
            activity = timelineActivity;
        } else {
            activity = directionsActivity;
        }
        return new TimelineViewHolder(view, viewType, mContext, activity, location, mOnClickListenerTimeline, mOnLongClickListenerTimeline);
    }

    @Override
    public void onViewRecycled(@NonNull TimelineViewHolder holder) {
        if (holder.googleMap != null) {
            holder.googleMap.clear();
            holder.googleMap.setMapType(GoogleMap.MAP_TYPE_NONE);
        }
        super.onViewRecycled(holder);
    }

    @Override
    public void onBindViewHolder(TimelineViewHolder holder, int position) {
        ViewGroup.LayoutParams layoutparams;
        int viewHeight = 110;

        float density = mContext.getResources()
                .getDisplayMetrics()
                .density;
        //Timeline timeLineModel = mFeedList.get(position);
        Tracks track = tracks.get(position);
        holder.initializeMapView();
        if (position > 0) {
            setLineStart(holder, tracks.get(position-1));
        }

        /*if (timeLineModel.getmStatus() != null && timeLineModel.getmStatus().equals("Visited")) {
            holder.mIvVisited.setVisibility(View.VISIBLE);
        } else {
            holder.mIvVisited.setVisibility(View.GONE);
        }*/
        if (track.getCategory() != null) {

            if(track.getCategory().equals("Notification")) {
                holder.mIvAvatar.setVisibility(View.GONE);
                holder.mapView.setVisibility(View.GONE);
                holder.mTvNotif.setVisibility(View.VISIBLE);
                holder.ll_gallery.setVisibility(View.GONE);
                holder.mTvGame.setVisibility(View.GONE);
                holder.mTimelineView.setMarker(VectorDrawableUtils.getDrawable(mContext, R.drawable.timeline_tripsnotif));
                holder.mTimelineView.setEndLine(Color.parseColor("#e74c3c"), holder.mViewType);
                holder.itemView.setTag(track);
            } else if(track.getCategory().equals("Map")) {
            /*holder.mIvAvatar.setVisibility(View.GONE);
            holder.mapView.setVisibility(View.VISIBLE);
            holder.mTvNotif.setVisibility(View.GONE);
            holder.mIvGallery.setVisibility(View.GONE);
            holder.mTvGame.setVisibility(View.GONE);
            holder.mLatLngFrom = new com.google.maps.model.LatLng(timeLineModel.getmLatFrom(), timeLineModel.getmLngFrom());
            holder.mLatLngTo = new com.google.maps.model.LatLng(timeLineModel.getmLatTo(), timeLineModel.getmLngTo());
            holder.mTimelineView.setMarker(VectorDrawableUtils.getDrawable(mContext, R.drawable.timeline_tripsmap));
            holder.mTimelineView.setEndLine(Color.parseColor("#03a4e0"), holder.mViewType);
            holder.itemView.setTag(timeLineModel);*/
            } else if(track.getCategory().equals("Attraction")) {
                layoutparams = holder.mItemView.getLayoutParams();
                layoutparams.height = (int)(80 * density);
                holder.mItemView.setLayoutParams(layoutparams);
                holder.mIvAvatar.setVisibility(View.VISIBLE);
                holder.mapView.setVisibility(View.GONE);
                holder.mTvNotif.setVisibility(View.GONE);
                holder.ll_gallery.setVisibility(View.GONE);
                holder.mTvGame.setVisibility(View.GONE);
                if (track.getAvatarUrl() != null) {
                    Glide.clear(holder.mImgPlace);
                    Glide.with(mContext).load(track.getAvatarUrl()).centerCrop().into(holder.mImgPlace);
                } else {
                    Glide.with(mContext).load(R.drawable.ic_logo).centerCrop().into(holder.mImgPlace);
                }
                holder.mTxPlaceName.setText(track.getName());
                holder.mTxPlaceAddress.setText(track.getAddress1());
                holder.mTimelineView.setMarker(VectorDrawableUtils.getDrawable(mContext, R.drawable.timeline_tripspin));
                holder.mTimelineView.setEndLine(Color.parseColor("#03a4e0"), holder.mViewType);
                if (track.getStatus() != null) {
                    if (track.getStatus().equals("Visited")) {
                        holder.mIvAvatar.setBackgroundColor(mContext.getResources().getColor(R.color.mdtp_transparent_black));
                    } else {
                        holder.mIvAvatar.setBackgroundColor(mContext.getResources().getColor(R.color.mdtp_white));
                    }
                }
                holder.itemView.setTag(track);
                holder.description = track.getDescription();
                holder.audio_play.setVisibility(View.VISIBLE);
                holder.audio_stop.setVisibility(View.GONE);
            } else if(track.getCategory().equals("Pictures")) {
                layoutparams = holder.mItemView.getLayoutParams();
                layoutparams.height = (int)(210 * density);
                holder.mItemView.setLayoutParams(layoutparams);
                holder.mIvAvatar.setVisibility(View.GONE);
                holder.mapView.setVisibility(View.GONE);
                holder.mTvNotif.setVisibility(View.GONE);
                holder.ll_gallery.setVisibility(View.VISIBLE);
                holder.mTvGame.setVisibility(View.GONE);
                holder.mTimelineView.setMarker(VectorDrawableUtils.getDrawable(mContext, R.drawable.timeline_tripsgallery));
                holder.mTimelineView.setEndLine(Color.parseColor("#2ecc71"), holder.mViewType);

                if (track.getCollectionPictureList().size() == 1) {
                    holder.ll_gallery23.setVisibility(View.GONE);
                } else if (track.getCollectionPictureList().size() == 2) {
                    holder.rl_gallery3.setVisibility(View.GONE);
                    Glide.with(mContext)
                            .load(track.getCollectionPictureList().get(1).getPictureUrl())
                            .centerCrop()
                            .crossFade()
                            .into(holder.iv_gallery2);
                } else if (track.getCollectionPictureList().size() == 3) {
                    holder.text_picture.setVisibility(View.GONE);

                    Glide.with(mContext)
                            .load(track.getCollectionPictureList().get(1).getPictureUrl())
                            .centerCrop()
                            .crossFade()
                            .into(holder.iv_gallery2);


                    Glide.with(mContext)
                            .load(track.getCollectionPictureList().get(2).getPictureUrl())
                            .centerCrop()
                            .crossFade()
                            .into(holder.iv_gallery3);
                } else {
                    holder.text_picture.setText("+" + String.valueOf(track.getCollectionPictureList().size() - 2));

                    Glide.with(mContext)
                            .load(track.getCollectionPictureList().get(1).getPictureUrl())
                            .centerCrop()
                            .crossFade()
                            .into(holder.iv_gallery2);


                    Glide.with(mContext)
                            .load(track.getCollectionPictureList().get(2).getPictureUrl())
                            .centerCrop()
                            .crossFade()
                            .into(holder.iv_gallery3);
                }
                Glide.with(mContext)
                        .load(track.getCollectionPictureList().get(0).getPictureUrl())
                        .centerCrop()
                        .crossFade()
                        .into(holder.iv_gallery1);
                holder.itemView.setTag(track);
            } else {
                holder.mIvAvatar.setVisibility(View.GONE);
                holder.mapView.setVisibility(View.GONE);
                holder.mTvNotif.setVisibility(View.GONE);
                holder.ll_gallery.setVisibility(View.GONE);
                holder.mTvGame.setVisibility(View.VISIBLE);
                holder.mTimelineView.setMarker(ContextCompat.getDrawable(mContext, R.drawable.timeline_tripsgame));
                holder.mTimelineView.setEndLine(Color.parseColor("#f1c40f"), holder.mViewType);
                holder.itemView.setTag(track);
            }
        }
    }



    private void setLineStart(TimelineViewHolder holder, Tracks tracks) {
        if (tracks.getCategory() != null) {
            if(tracks.getCategory().equals("Notification")) {
                holder.mTimelineView.setStartLine(Color.parseColor("#e74c3c"), holder.mViewType);
            } else if(tracks.getCategory().equals("Map")) {
                holder.mTimelineView.setStartLine(Color.parseColor("#03a4e0"), holder.mViewType);
            } else if(tracks.getCategory().equals("Attraction")) {
                holder.mTimelineView.setStartLine(Color.parseColor("#03a4e0"), holder.mViewType);
            }else if(tracks.getCategory().equals("Pictures")) {
                holder.mTimelineView.setStartLine(Color.parseColor("#2ecc71"), holder.mViewType);
            }else {
                holder.mTimelineView.setStartLine(Color.parseColor("#f1c40f"), holder.mViewType);
            }
        }
    }

    @Override
    public int getItemCount() {
        return (tracks!=null? tracks.size():0);
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {

    }

    private byte[] bitmapToByte(Bitmap bitmap){
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG, 100, stream);
        byte[] byteArray = stream.toByteArray();
        return byteArray;
    }
}
