package com.essplore.essplore.ui.activities.trip.view;

import android.app.Activity;
import android.support.design.widget.TextInputEditText;
import android.support.v7.app.AlertDialog;
import android.widget.ImageView;
import butterknife.BindView;
import butterknife.OnClick;
import com.bumptech.glide.Glide;
import com.essplore.essplore.R;
import com.essplore.essplore.application.AppConstants;
import com.essplore.essplore.application.EssploreApplication;
import com.essplore.essplore.managers.AppActivityManager;
import com.essplore.essplore.managers.SharedPreferenceManager;
import com.essplore.essplore.models.City;
import com.essplore.essplore.ui.activities.HttpToolBarBaseActivity;
import com.essplore.essplore.ui.activities.trip.presenter.AddTripPresenterImpl;
import io.reactivex.disposables.CompositeDisposable;
import javax.inject.Inject;
import org.apache.commons.lang3.StringUtils;

public class AddTripActivity extends HttpToolBarBaseActivity implements AddTripView {

  @Inject AlertDialog alertDialog;
  @Inject CompositeDisposable disposable;
  @Inject SharedPreferenceManager sharedPreferenceManager;
  @Inject AppActivityManager appActivityManager;
  @Inject AddTripPresenterImpl presenter;

  @BindView(R.id.civLocationImage) ImageView cvCity;
  @BindView(R.id.edtTrackName) TextInputEditText edtTrackName;

  @Override public void onNetworkErrorFound(String message) {
    showAlertDialog(message);
  }

  @Override public void onHttpErrorUnexpectedFound(String message) {
    showAlertDialog(message);
  }

  @Override public void onApiErrorFound(String message) {
    showAlertDialog(message);
  }

  @Override public void onHttpErrorUnexpectedFound() {
    showAlertDialog(getString(R.string.unexpected_error_occurred));
  }

  @Override public void onUnauthorizedErrorFound() {
    showAlertDialog(getString(R.string.unauthorized));
  }

  @Override public CompositeDisposable getCompositeDisposable() {
    return disposable;
  }

  @Override protected Activity getActivity() {
    return this;
  }

  @Override protected AlertDialog getAlertDialog() {
    return alertDialog;
  }

  @Override protected void setupActivityLayout() {
    setContentView(R.layout.activity_add_trip);
  }

  @Override protected void setupViewElements() {
    if (getCity().getAvatarUrl() != null) {
      Glide.with(this).load(getCity().getAvatarUrl()).centerCrop().crossFade().into(cvCity);
    }
  }

  @Override protected void injectDaggerComponent() {
    EssploreApplication.get(this).createAddTripComponent(this).inject(this);
  }

  @Override protected boolean isActionbarBackButtonEnabled() {
    return Boolean.TRUE;
  }

  @Override protected void onDestroy() {
    super.onDestroy();
    EssploreApplication.get(this).releaseAddTripComponent();
  }

  @Override public City getCity() {
    return getIntent().getParcelableExtra(AppConstants.CITY_OBJECT);
  }

  @Override public String provideTrackName() {
    return edtTrackName.getText().toString();
  }

  @Override public int getLoggedInUserId() {
    return Integer.parseInt(sharedPreferenceManager.getLoggedInUserId());
  }

  @OnClick(R.id.btnTouristSpots) void onTouristSpotClicked() {
    if (StringUtils.isNotEmpty(provideTrackName())) {
      appActivityManager.launchFilters(this, presenter.buildCreateTrackRequest(getCity()));
    } else {
      showAlertDialog(getString(R.string.trip_name_cannot_be_empty));
    }
  }
}
