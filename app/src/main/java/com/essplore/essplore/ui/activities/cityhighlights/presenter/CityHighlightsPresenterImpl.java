package com.essplore.essplore.ui.activities.cityhighlights.presenter;

import com.essplore.essplore.api.managers.ApiManager;
import com.essplore.essplore.api.requests.AddPlaceToTrackRequest;
import com.essplore.essplore.api.requests.CityHighlightRequest;
import com.essplore.essplore.api.response.AddToPlaceResponse;
import com.essplore.essplore.api.response.CityHighlightResponse;
import com.essplore.essplore.api.utils.SimpleObserver;
import com.essplore.essplore.ui.activities.HttpBasePresenter;
import com.essplore.essplore.ui.activities.cityhighlights.view.CityHighlightsView;
import io.reactivex.disposables.Disposable;

public class CityHighlightsPresenterImpl extends HttpBasePresenter implements CityHighlightsPresenter {

  private CityHighlightsView view;
  private ApiManager manager;

  public CityHighlightsPresenterImpl(CityHighlightsView view, ApiManager manager) {
    this.view = view;
    this.manager = manager;
  }

  @Override public Disposable getCityHighlights() {
    return manager.getCityHighlight(buildCityHighlightsRequest())
        .subscribe(new SimpleObserver<CityHighlightResponse>() {
          @Override public void accept(CityHighlightResponse cityHighlightResponse) {
            view.hideProgressbar();
          }
        }, throwable -> {
          view.hideProgressbar();
          handleHttpError(view, throwable);
        });
  }

  @Override public Disposable addPlaceToTrip() {
    return manager.addPlaceToTrack(buildAddToTrackRequest(), view.provideToken())
        .subscribe(new SimpleObserver<AddToPlaceResponse>() {
          @Override public void accept(AddToPlaceResponse response) {
            view.hideProgressbar();
            view.showToastMessage(response.getMessage());
            view.setIntentResultOk();
          }
        }, throwable -> {
          view.hideProgressbar();
          handleHttpError(view, throwable);
        });
  }

  @Override public AddPlaceToTrackRequest buildAddToTrackRequest() {
    return new AddPlaceToTrackRequest(view.provideCityObject().getTrackId(), view.provideCityHighlight().getId());
  }

  @Override public CityHighlightRequest buildCityHighlightsRequest() {
    return new CityHighlightRequest(view.provideCityObject().getId(), view.provideCityObject().getCountryId());
  }
}
