package com.essplore.essplore.ui.activities.placehighlights.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import butterknife.BindView;
import butterknife.ButterKnife;

import com.bumptech.glide.Glide;
import com.essplore.essplore.R;
import com.essplore.essplore.models.CityHighlight;
import com.essplore.essplore.ui.activities.placehighlights.presenter.PlaceHighlightsPresenterImpl;
import com.essplore.essplore.ui.utils.OnBindViewListener;
import java.util.List;

public class PlaceHighlightsAdapter
    extends RecyclerView.Adapter<PlaceHighlightsAdapter.ViewHolder> {

  private Context context;
  private List<CityHighlight> cityHighlightList;
  private PlaceHighlightsPresenterImpl presenter;

  public PlaceHighlightsAdapter(Context context, List<CityHighlight> cityHighlightList,
      PlaceHighlightsPresenterImpl presenter) {
    this.context = context;
    this.cityHighlightList = cityHighlightList;
    this.presenter = presenter;
  }

  @NonNull @Override
  public PlaceHighlightsAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent,
      int viewType) {
    return new PlaceHighlightsAdapter.ViewHolder(
        LayoutInflater.from(context).inflate(R.layout.list_item_place, parent, false));
  }

  @Override
  public void onBindViewHolder(@NonNull PlaceHighlightsAdapter.ViewHolder holder, int position) {
      holder.onBind(cityHighlightList.get(position));
  }

  @Override public int getItemCount() {
    return cityHighlightList.size();
  }

  public class ViewHolder extends RecyclerView.ViewHolder
      implements OnBindViewListener<CityHighlight> {

    @BindView(R.id.imgPlace)
    ImageView imgPlace;

    @BindView(R.id.txPlaceName)
    TextView txPlaceName;

    @BindView(R.id.txPlaceAddress)
    TextView txPlaceAddress;

    View itemView;

    public ViewHolder(View itemView) {
      super(itemView);
      this.itemView = itemView;
      ButterKnife.bind(this, itemView);
    }

    @Override
    public void onBind(CityHighlight object) {

      itemView.setOnClickListener(view -> presenter.onSingleItemClick(object));
      txPlaceName.setText(object.getName());
      txPlaceAddress.setText(object.getAddress1());
      if (object.getAvatarUrl() != null) {
        Glide.with(context)
                .load(object.getAvatarUrl())
                .centerCrop()
                .crossFade()
                .into(imgPlace);
        } else {
          imgPlace.setImageDrawable(context.getDrawable(R.drawable.img_placeholder));
      }
      }
    }

}

