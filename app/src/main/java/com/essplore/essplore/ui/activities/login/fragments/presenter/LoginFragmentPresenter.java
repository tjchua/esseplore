package com.essplore.essplore.ui.activities.login.fragments.presenter;

import com.essplore.essplore.api.requests.LoginRequest;
import io.reactivex.disposables.Disposable;

public interface LoginFragmentPresenter {

  Disposable loginUser(LoginRequest request);

  LoginRequest buildLoginRequest();

  boolean validateCredentials(String username, String password);
}
