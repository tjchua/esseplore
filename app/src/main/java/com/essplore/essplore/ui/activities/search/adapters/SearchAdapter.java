package com.essplore.essplore.ui.activities.search.adapters;

import android.content.Context;
import android.net.Uri;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.essplore.essplore.R;
import com.essplore.essplore.ui.activities.search.interfaces.SearchAdapterInterface;
import com.essplore.essplore.ui.activities.search.models.Place;
import com.essplore.essplore.ui.activities.search.models.Places;
import com.essplore.essplore.ui.activities.share.ShareAdapterInterface;

import java.io.File;
import java.util.ArrayList;

public class SearchAdapter extends RecyclerView.Adapter<SearchAdapter.MyViewHolder>{

    private Places places;
    private Context context;
    private SearchAdapterInterface searchAdapterInterface;


    public class MyViewHolder extends RecyclerView.ViewHolder {

        private ImageView avatar;
        private TextView name;


        public MyViewHolder(View view) {

            super(view);
            avatar = view.findViewById(R.id.ivCityPicture);
            name = view.findViewById(R.id.tvCityName);

        }
    }


    public SearchAdapter(Context context, Places places) {

        this.context = context;
        this.places = places;

    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.list_item_search_places, parent, false);
        return new MyViewHolder(itemView);

    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {

        holder.name.setText(places.get(position).getName());
        Glide.with(context)
                .load(places.get(position).getAvatar_url())
                .crossFade()
                .into(holder.avatar);

        holder.itemView.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                if (searchAdapterInterface != null)
                searchAdapterInterface.onPlaceClick(places.get(position));

            }

        });

    }

    @Override
    public int getItemCount() {

        return places.size();

    }

    public void setSearchAdapterInterface(SearchAdapterInterface searchAdapterInterface){

        this.searchAdapterInterface = searchAdapterInterface;

    }
}
