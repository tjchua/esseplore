package com.essplore.essplore.ui.activities.wallet.Adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import com.essplore.essplore.R;
import java.util.List;
import butterknife.BindView;
import butterknife.ButterKnife;

public class WalletHistoryAdapter extends RecyclerView.Adapter<WalletHistoryAdapter.ViewHolder> {

    private Context context;
    private List<String> data;

    public WalletHistoryAdapter(Context context, List<String> data) {
        this.context = context;
        this.data = data;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position, List<Object> payloads) {
        super.onBindViewHolder(holder, position, payloads);
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item_history, parent, false));
    }

    @Override
    public int getItemCount() {
        return data.size();
    }


    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.txTitle.setText(data.get(position));
        holder.txDate.setText(data.get(position));
        holder.txAmount.setText(data.get(position));
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.txTitle)
        TextView txTitle;

        @BindView(R.id.txDate)
        TextView txDate;

        @BindView(R.id.txAmount)
        TextView txAmount;

        public ViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }
    }
}
