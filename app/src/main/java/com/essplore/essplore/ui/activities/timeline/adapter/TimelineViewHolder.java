package com.essplore.essplore.ui.activities.timeline.adapter;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.graphics.Path;
import android.location.Location;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.essplore.essplore.R;
import com.essplore.essplore.models.Tracks;
import com.essplore.essplore.ui.activities.directions.view.DirectionsActivity;
import com.essplore.essplore.ui.activities.timeline.view.TimelineActivity;
import com.essplore.essplore.ui.utils.TextToSpeechService;
import com.github.vipulasri.timelineview.TimelineView;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.GoogleMapOptions;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.PolylineOptions;
import com.google.maps.DirectionsApi;
import com.google.maps.GeoApiContext;
import com.google.maps.android.PolyUtil;
import com.google.maps.errors.ApiException;
import com.google.maps.model.DirectionsResult;
import com.google.maps.model.TravelMode;

import org.joda.time.DateTime;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import butterknife.BindView;
import butterknife.ButterKnife;

public class TimelineViewHolder extends RecyclerView.ViewHolder implements OnMapReadyCallback {

    @BindView(R.id.ll_avatar)
    LinearLayout mIvAvatar;
    @BindView(R.id.imgPlace)
    ImageView mImgPlace;
    @BindView(R.id.txPlaceName)
    TextView mTxPlaceName;
    @BindView(R.id.txPlaceAddress)
    TextView mTxPlaceAddress;
    @BindView(R.id.ll_timeline)
    LinearLayout ll_timeline;
    @BindView(R.id.frag_map)
    MapView mapView;
    @BindView(R.id.cv_avatar)
    CardView mCvAvatar;
    @BindView(R.id.tv_notif)
    TextView mTvNotif;
    @BindView(R.id.tv_game)
    TextView mTvGame;
    @BindView(R.id.time_marker)
    TimelineView mTimelineView;
    /*@BindView(R.id.iv_visited)
    ImageView mIvVisited;*/
    @BindView(R.id.audio_play)
    ImageView audio_play;
    @BindView(R.id.audio_stop)
    ImageView audio_stop;
    @BindView(R.id.ll_gallery)
    LinearLayout ll_gallery;
    @BindView(R.id.iv_gallery1)
    ImageView iv_gallery1;
    @BindView(R.id.iv_gallery2)
    ImageView iv_gallery2;
    @BindView(R.id.iv_gallery3)
    ImageView iv_gallery3;
    @BindView(R.id.ll_gallery23)
    LinearLayout ll_gallery23;
    @BindView(R.id.rl_gallery3)
    RelativeLayout rl_gallery3;
    @BindView(R.id.text_picture)
    TextView text_picture;
    @BindView(R.id.ll_attraction_address)
    LinearLayout ll_attraction_address;

    int mViewType = 0;
    int positionPin, positionMap, positionNotif, positionGallery, positionGame;
    Context mContext;
    Activity mActivity;
    TimelineActivity mTimelineActivity;
    DirectionsActivity mDirectionsActivity;
    GoogleMap googleMap;
    LatLng mLatLngFrom;
    LatLng mLatLngTo;
    View mItemView;
    Location location;
    String description;

    private TextToSpeechService textToSpeechService;

    public TimelineViewHolder(View itemView, int viewType, Context mContext, Activity activity,
                              Location location,
                              View.OnClickListener mOnClickListenerTimeline,
                              View.OnLongClickListener mOnLongClickListenerTimeline) {
        super(itemView);

        ButterKnife.bind(this, itemView);

        mTimelineView.initLine(viewType);
        mViewType = viewType;
        this.mContext = mContext;
        this.mActivity = activity;
        this.location = location;

        if (activity instanceof TimelineActivity) {
            mTimelineActivity = (TimelineActivity) activity;
            mTimelineView.setVisibility(View.VISIBLE);
        } else if (activity instanceof DirectionsActivity) {
            mDirectionsActivity = (DirectionsActivity) activity;
            mTimelineView.setVisibility(View.GONE);
        }

        itemView.setOnClickListener(mOnClickListenerTimeline);
        itemView.setOnLongClickListener(mOnLongClickListenerTimeline);

        mItemView = itemView;

        audio_play.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mTimelineActivity != null) {
                    mTimelineActivity.startGuide(description);
                    mTimelineActivity.item_played = getAdapterPosition();
                } else {
                    mDirectionsActivity.startGuide(description);
                    mDirectionsActivity.item_played = getAdapterPosition();
                }
                audio_play.setVisibility(View.GONE);
                audio_stop.setVisibility(View.VISIBLE);
            }
        });

        audio_stop.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mTimelineActivity != null) {
                    mTimelineActivity.stopGuide();
                } else {
                    mDirectionsActivity.stopGuide();
                }
                audio_play.setVisibility(View.VISIBLE);
                audio_stop.setVisibility(View.GONE);
            }
        });

        ll_attraction_address.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mTimelineActivity != null) {
                    mTimelineActivity.atractionClick(getAdapterPosition());
                } else {
                    mDirectionsActivity.atractionClick(getAdapterPosition());
                }
            }
        });

        ll_attraction_address.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                if (mTimelineActivity != null) {
                    mTimelineActivity.attractionLongClick(v, getAdapterPosition());
                } else {
                    mDirectionsActivity.attractionLongClick(v, getAdapterPosition());
                }
                return false;
            }
        });
    }

    public void initializeMapView() {
        if (mapView != null) {
            mapView.onCreate(null);
            mapView.onResume();
            mapView.getMapAsync(this);
        }
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {

        MapsInitializer.initialize(mContext.getApplicationContext());
        this.googleMap = googleMap;
        this.googleMap.getUiSettings().setAllGesturesEnabled(false);
        GoogleMapOptions options = new GoogleMapOptions().liteMode(true);

        if (mLatLngFrom != null && mLatLngTo != null) {
            List<LatLng> latLngs = new ArrayList<>();
            latLngs.add(mLatLngFrom);
            latLngs.add(mLatLngTo);

            DirectionsResult results = getDirectionsDetails(mLatLngFrom, mLatLngTo, TravelMode.WALKING, new LatLng[0]);
            if (results != null) {
                addPolyline(results, googleMap);
                positionCamera(results, googleMap);

            }
            mapClicked(mTimelineActivity);
        }

    }

    public void mapClicked (TimelineActivity timelineActivity) {
        List<Tracks> tracks = new ArrayList<>();
        boolean isNextAttraction = false;
        for (Tracks tracks1 : timelineActivity.tracks) {
            if (Double.parseDouble(tracks1.getLat()) == mLatLngFrom.lat && Double.parseDouble(tracks1.getLng()) == mLatLngFrom.lng &&
                    timelineActivity.tracks.get(timelineActivity.tracks.indexOf(tracks1)).getStatus() != null) {
                if (timelineActivity.tracks.get(timelineActivity.tracks.indexOf(tracks1)).getStatus().equals("Visited")) {
                    isNextAttraction = true;
                } else {
                    isNextAttraction = false;
                }
            }
            if ((Double.parseDouble(tracks1.getLat()) == mLatLngFrom.lat && Double.parseDouble(tracks1.getLng()) == mLatLngFrom.lng) ||
                    (Double.parseDouble(tracks1.getLat()) == mLatLngTo.lat && Double.parseDouble(tracks1.getLng()) == mLatLngTo.lng)) {
                tracks.add(tracks1);
            } else if ((location.getLatitude() == mLatLngFrom.lat &&
                    location.getLongitude() == mLatLngFrom.lng)) {
                tracks.add(tracks1);
                isNextAttraction = true;
            } else {
                continue;
            }
        }
        /*tracks.add(timelineActivity.tracks.get(((position + 1)/2) - 1));
        tracks.add(timelineActivity.tracks.get((position + 1)/2));*/
        boolean finalIsNextAttraction = isNextAttraction;
        googleMap.setOnMapClickListener(new GoogleMap.OnMapClickListener() {
            @Override
            public void onMapClick(com.google.android.gms.maps.model.LatLng latLng) {
                timelineActivity.goToDorections(mTimelineActivity.getMyTrack().getTrackId());
            }
        });
    }

    private LatLng computeCentroid(List<LatLng> points) {
        double latitude = 0;
        double longitude = 0;
        int n = points.size();

        for (LatLng point : points) {
            latitude += point.lat;
            longitude += point.lng;
        }

        return new LatLng(latitude/n, longitude/n);
    }

    private DirectionsResult getDirectionsDetails( com.google.maps.model.LatLng origin,  com.google.maps.model.LatLng destination, TravelMode mode, LatLng[] waypoints) {
        DateTime now = new DateTime();
        try {
            return DirectionsApi.newRequest(getGeoContext())
                    .mode(mode)
                    .origin(origin)
                    .destination(destination)
                    //.waypoints(String.valueOf(waypoints))
                    .departureTime(now)
                    .await();
        } catch (ApiException e) {
            e.printStackTrace();
            return null;
        } catch (InterruptedException e) {
            e.printStackTrace();
            return null;
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }

    private GeoApiContext getGeoContext() {
        GeoApiContext geoApiContext = new GeoApiContext();
        return geoApiContext
                .setQueryRateLimit(3)
                .setApiKey(mContext.getString(R.string.google_maps_key))
                .setConnectTimeout(1, TimeUnit.SECONDS)
                .setReadTimeout(1, TimeUnit.SECONDS)
                .setWriteTimeout(1, TimeUnit.SECONDS);
    }

    private void addPolyline(DirectionsResult results, GoogleMap mMap) {
        List<com.google.android.gms.maps.model.LatLng> decodedPath = PolyUtil.decode(results.routes[0].overviewPolyline.getEncodedPath());
        mMap.addPolyline(new PolylineOptions().addAll(decodedPath).color(Color.RED).width(8));
    }

    private void positionCamera(DirectionsResult results, GoogleMap mMap) {
        LatLngBounds.Builder builder = new LatLngBounds.Builder();

        //the include method will calculate the min and max bound.
        builder.include(new com.google.android.gms.maps.model.LatLng(results.routes[0]
                .legs[0].startLocation.lat,
                results.routes[0].legs[0].startLocation.lng));
        builder.include(new com.google.android.gms.maps.model.LatLng(results.routes[0]
                .legs[0].endLocation.lat,
                results.routes[0].legs[0].endLocation.lng));

        LatLngBounds bounds = builder.build();

        mMap.setOnMapLoadedCallback(new GoogleMap.OnMapLoadedCallback() {
            @Override
            public void onMapLoaded() {
                int width = mItemView.getWidth();
                int height = mItemView.getHeight();
                int padding = (int) (width * 0.10);

                mMap.moveCamera(CameraUpdateFactory.newLatLngBounds(bounds, width, height, padding));
                /*CameraUpdate cu = CameraUpdateFactory.newLatLngBounds(bounds, width, height, padding);

                mMap.animateCamera(cu);*/
            }
        });
    }
}
