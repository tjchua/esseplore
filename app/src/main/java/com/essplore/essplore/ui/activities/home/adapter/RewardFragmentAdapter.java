package com.essplore.essplore.ui.activities.home.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import com.essplore.essplore.R;
import java.util.List;
import butterknife.BindView;
import butterknife.ButterKnife;

public class RewardFragmentAdapter extends RecyclerView.Adapter<RewardFragmentAdapter.ViewHolder> {

    private Context context;

    public RewardFragmentAdapter(Context context) {
        this.context = context;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position, List<Object> payloads) {
        super.onBindViewHolder(holder, position, payloads);
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item_reward, parent, false));
    }

    @Override
    public int getItemCount() {
        return 0;
    }


    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.imgReward)
        ImageView imgReward;

        @BindView(R.id.txAmount)
        TextView txAmount;


        public ViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }
    }
}
