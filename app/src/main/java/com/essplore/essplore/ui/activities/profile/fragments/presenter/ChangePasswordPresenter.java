package com.essplore.essplore.ui.activities.profile.fragments.presenter;

import com.essplore.essplore.api.requests.ChangePasswordRequest;
import io.reactivex.disposables.Disposable;

public interface ChangePasswordPresenter {

  Disposable changePassword();

  ChangePasswordRequest buildChangePasswordRequest();

  boolean isValidCredentials();
}
