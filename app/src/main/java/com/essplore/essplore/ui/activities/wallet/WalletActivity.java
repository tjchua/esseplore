package com.essplore.essplore.ui.activities.wallet;

import android.app.Activity;
import android.content.Intent;
import android.os.Handler;
import android.support.v7.app.AlertDialog;
import android.widget.TextView;
import butterknife.BindView;
import butterknife.OnClick;
import com.essplore.essplore.R;
import com.essplore.essplore.ui.activities.MainActivity;
import com.essplore.essplore.ui.activities.ToolBarBaseActivity;
import com.essplore.essplore.ui.activities.camera.CameraActivity;
import com.essplore.essplore.ui.activities.paymentform.PaymentFormActivity;

public class WalletActivity extends ToolBarBaseActivity {


  @Override protected void setupActivityLayout() {
    setContentView(R.layout.activity_wallet);
  }

  @Override protected void setupViewElements() {
  }

  @Override protected void injectDaggerComponent() {
  }

  @Override protected boolean isActionbarBackButtonEnabled() {
    return Boolean.TRUE;
  }

  @Override protected void onDestroy() {
    super.onDestroy();
  }

  @Override public void onBackPressed() {
      finish();
  }

  @OnClick(R.id.btnAddFunds) void onClickAddFund() {
    showPaymentForm();
  }

  @Override protected Activity getActivity() {
    return this;
  }

  @Override protected AlertDialog getAlertDialog() {
    return null;
  }

  private void showPaymentForm(){
    Intent showPaymentFormIntent = new Intent(this, PaymentFormActivity.class);
    startActivity(showPaymentFormIntent);
  }
}
