package com.essplore.essplore.ui.activities.camera.adapter;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Movie;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.media.Image;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.drawable.GlideDrawable;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.animation.GlideAnimation;
import com.bumptech.glide.request.target.GlideDrawableImageViewTarget;
import com.bumptech.glide.request.target.Target;
import com.essplore.essplore.R;
import com.essplore.essplore.ui.activities.camera.CameraActivity;
import com.essplore.essplore.ui.activities.camera.CameraApi;
import com.essplore.essplore.ui.activities.camera.models.Galleries;
import com.essplore.essplore.ui.activities.camera.models.Gallery;

import java.util.List;

public class GalleryAdapter extends RecyclerView.Adapter<GalleryAdapter.MyViewHolder> {

    private List<Gallery> images;
    private CameraApi cameraApi;
    private ImageView selectedImageView;
    private Drawable selectedImage;
    private ProgressBar selectedImageLoader;
    private Context context;

    public class MyViewHolder extends RecyclerView.ViewHolder {

        private ImageView imageView;
        private ProgressBar progressBar;
        private ImageView checkboxIv;

        public MyViewHolder(View view) {

            super(view);
            imageView = (ImageView) view.findViewById(R.id.image);
            progressBar = (ProgressBar) view.findViewById(R.id.progress);
            imageView.setOnClickListener(selectImageClickListener);
            checkboxIv = view.findViewById(R.id.checkbox_iv);

        }

    }


    public GalleryAdapter(Galleries images, Context context) {

        this.context = context;
        this.images = images;
        cameraApi = new CameraApi();

    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.list_item_gallery, parent, false);
        return new MyViewHolder(itemView);

    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
//        if (images.get(position).getBitmap() == null){
//            holder.imageView.setImageBitmap(null);
//            images.get(position).setGalleryAdapter(this);
//            cameraApi.downloadImage(images.get(position));
//        }else{
//            if (selectedImage == null) {
//                selectedImage = images.get(position).getBitmap();
//                setSelectedImage(selectedImage);
//                selectedImageLoader.setVisibility(View.GONE);
//            }
//            holder.imageView.setImageBitmap(images.get(position).getBitmap());
//            holder.loader.setVisibility(View.GONE);
//        }

        holder.progressBar.setVisibility(View.VISIBLE);
        Glide.with(context)
                .load(images.get(position).getPicture_url())
                .crossFade()
                .listener(new RequestListener<String, GlideDrawable>() {
                    @Override
                    public boolean onException(Exception e, String model, Target<GlideDrawable> target, boolean isFirstResource) {

                        return false;

                    }

                    @Override
                    public boolean onResourceReady(GlideDrawable resource, String model, Target<GlideDrawable> target, boolean isFromMemoryCache, boolean isFirstResource) {

                        if (selectedImage == null) {

                                selectedImage = resource;
                                setSelectedImage(selectedImage);


                        }
                        holder.progressBar.setVisibility(View.GONE);
                        return false;

                    }
                })
                .into(new GlideDrawableImageViewTarget(holder.imageView){
                    @Override
                    public void onResourceReady(GlideDrawable resource, GlideAnimation<? super GlideDrawable> animation) {
                        super.onResourceReady(resource, animation);
                    }
                });

        holder.checkboxIv.setOnClickListener(checkBoxClickListener);
        holder.checkboxIv.setTag(position);

        if (images.get(position).isSelected()){

            holder.checkboxIv.setImageResource(R.drawable.checkbox_selected);

        }else{

            holder.checkboxIv.setImageResource(R.drawable.checkbox);

        }

    }

    @Override
    public int getItemCount() {

        return images.size();

    }

    public void setSelectedImageView(ImageView imageView){

        selectedImageView = imageView;
        selectedImageView.setImageDrawable(selectedImage);

    }

    private void setSelectedImage(Drawable selectedImage){

        selectedImageView.setImageDrawable(selectedImage);

    }

    private View.OnClickListener selectImageClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {

            ImageView imageView = (ImageView) v;
            selectedImage = imageView.getDrawable();
            setSelectedImage(selectedImage);

        }
    };

    private View.OnClickListener checkBoxClickListener = new View.OnClickListener() {

        @Override
        public void onClick(View v) {

            int position = (v.getTag() instanceof Integer) ? (int)v.getTag() : 0;
            images.get(position).setSelected(!images.get(position).isSelected());
            notifyDataSetChanged();

        }

    };
}
