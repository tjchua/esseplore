package com.essplore.essplore.ui.activities.directions.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.essplore.essplore.R;

import butterknife.BindView;
import butterknife.ButterKnife;

public class DirectionsViewHolder extends RecyclerView.ViewHolder {
    @BindView(R.id.tv_distance)
    TextView mDistance;
    @BindView(R.id.tv_directions)
    TextView mDirections;

    public DirectionsViewHolder(View itemView) {
        super(itemView);

        ButterKnife.bind(this, itemView);
    }
}
