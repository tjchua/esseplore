package com.essplore.essplore.ui.activities.camera.interfaces;

import android.graphics.Bitmap;

public interface ProcessImageTaskInterface {

    public void onCaptureProcessSuccessful(Bitmap bitmap);

}
