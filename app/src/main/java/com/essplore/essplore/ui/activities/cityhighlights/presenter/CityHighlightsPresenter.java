package com.essplore.essplore.ui.activities.cityhighlights.presenter;

import com.essplore.essplore.api.requests.AddPlaceToTrackRequest;
import com.essplore.essplore.api.requests.CityHighlightRequest;
import io.reactivex.disposables.Disposable;

public interface CityHighlightsPresenter {

  Disposable getCityHighlights();

  Disposable addPlaceToTrip();

  AddPlaceToTrackRequest buildAddToTrackRequest();

  CityHighlightRequest buildCityHighlightsRequest();
}
