package com.essplore.essplore.ui.activities.places.view;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.LocationManager;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.PagerSnapHelper;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.Toast;

import butterknife.BindView;
import com.essplore.essplore.R;
import com.essplore.essplore.api.requests.RemovePlaceFromTrackRequest;
import com.essplore.essplore.application.AppConstants;
import com.essplore.essplore.application.EssploreApplication;
import com.essplore.essplore.managers.AppActivityManager;
import com.essplore.essplore.managers.SharedPreferenceManager;
import com.essplore.essplore.models.City;
import com.essplore.essplore.models.MyTrack;
import com.essplore.essplore.models.MyTrip;
import com.essplore.essplore.models.TrackPicture;
import com.essplore.essplore.models.Tracks;
import com.essplore.essplore.ui.activities.HttpToolBarBaseActivity;
import com.essplore.essplore.ui.activities.places.adapter.PlacesCarouselAdapter;
import com.essplore.essplore.ui.activities.places.adapter.TracksAdapter;
import com.essplore.essplore.ui.activities.places.adapter.UserAction;
import com.essplore.essplore.ui.activities.places.presenter.PlacesPresenterImpl;

import butterknife.OnClick;
import io.reactivex.disposables.CompositeDisposable;
import java.util.ArrayList;
import java.util.List;
import javax.inject.Inject;

public class PlacesActivity extends HttpToolBarBaseActivity implements PlacesView, UserAction {

  @Inject CompositeDisposable compositeDisposable;
  @Inject SharedPreferenceManager sharedPreferenceManager;
  @Inject AppActivityManager appActivityManager;
  @Inject AlertDialog alertDialog;
  @Inject PlacesPresenterImpl presenter;

  @BindView(R.id.rvPlaces) RecyclerView rvPlaces;
  @BindView(R.id.rvTracks) RecyclerView rvTracks;
  @BindView(R.id.pbPlaces) ProgressBar pbPlaces;
  @BindView(R.id.btnStartTrip) Button btnStartTrip;

  private PlacesCarouselAdapter placesCarouselAdapter;
  private TracksAdapter tracksAdapter;
  private List<TrackPicture> trackPictureList;
  private List<Tracks> tracks;
  private MyTrack myTrack;

  @Override public void onNetworkErrorFound(String message) {
    showAlertDialog(message);
  }

  @Override public void onHttpErrorUnexpectedFound(String message) {
    showAlertDialog(message);
  }

  @Override public void onApiErrorFound(String message) {
    showAlertDialog(message);
  }

  @Override public void onHttpErrorUnexpectedFound() {
    showAlertDialog(getString(R.string.unexpected_error_occurred));
  }

  @Override public void onUnauthorizedErrorFound() {
    showAlertDialog(getString(R.string.unauthorized));
  }

  @Override public CompositeDisposable getCompositeDisposable() {
    return compositeDisposable;
  }

  @Override protected Activity getActivity() {
    return this;
  }

  @Override protected AlertDialog getAlertDialog() {
    return alertDialog;
  }

  @Override protected void setupActivityLayout() {
    setContentView(R.layout.activity_places);
  }

  @OnClick(R.id.btnStartTrip) void btnStartTrip() {
    checkGPSEnabled();
  }

  @Override protected void setupViewElements() {
    instantiateTrackLists();
    final PagerSnapHelper snapHelper = new PagerSnapHelper();
    snapHelper.attachToRecyclerView(rvPlaces);
    placesCarouselAdapter = new PlacesCarouselAdapter(this, trackPictureList);
    rvPlaces.setAdapter(placesCarouselAdapter);
    rvPlaces.setNestedScrollingEnabled(false);
    tracksAdapter = new TracksAdapter(this, tracks, this, presenter);
    rvTracks.setAdapter(tracksAdapter);
    rvTracks.setNestedScrollingEnabled(false);
    myTrack = getMyTrack();
  }



  @Override
  public RemovePlaceFromTrackRequest buildRequest() {
    return null;
  }

  @Override
  public void onDeleteItem(int position) {

    AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(getActivity(), R.style.ExitDialog);
    AlertDialog deleteDialog = dialogBuilder.create();
    deleteDialog.setMessage(getString(R.string.are_you_sure_you_want_to_delete));
    deleteDialog.setButton(DialogInterface.BUTTON_POSITIVE, getString(R.string.yes), (dialog, which) -> {
      compositeDisposable.add(presenter.deletePlaceFromTrack());
      deleteDialog.dismiss();
    });
    deleteDialog.setButton(DialogInterface.BUTTON_NEGATIVE, getString(R.string.no), (dialog, which) -> {
      deleteDialog.dismiss();
    });
    deleteDialog.show();

  }


  @Override protected void injectDaggerComponent() {
    EssploreApplication.get(this).createPlacesComponent(this).inject(this);
  }

  @Override protected boolean isActionbarBackButtonEnabled() {
    return Boolean.TRUE;
  }

  @Override protected void onDestroy() {
    super.onDestroy();
    EssploreApplication.get(this).releasePlacesComponent();
  }

  @Override public MyTrip getMyTrip() {
    return getIntent().getParcelableExtra(AppConstants.CREATE_TRACK_RESPONSE_OBJECT);
  }

  @Override
  public void setMyTrack(MyTrack myTrack) {
    Bundle bundle = new Bundle();
    this.myTrack = myTrack;
    bundle.putParcelable(AppConstants.MY_TRACK_OBJECT, this.myTrack);
    appActivityManager.launchTimeline(this, bundle);
  }

  @Override public int getCountryId() {
    return getIntent().getIntExtra(AppConstants.GET_COUNTRY_ID, 0);
  }

  @Override public int getCityId() {
    return getIntent().getIntExtra(AppConstants.CITY_ID, 0);
  }

  @Override public void showProgressbar() {
    pbPlaces.setVisibility(View.VISIBLE);
  }

  @Override public void hideProgressbar() {
    pbPlaces.setVisibility(View.GONE);
  }

  @Override public String provideToken() {
    return sharedPreferenceManager.getAccessToken();
  }

  @Override public void setTrackPictureList(List<TrackPicture> trackPictureList) {
    this.trackPictureList.clear();
    this.trackPictureList.addAll(trackPictureList);
    for (int i = 0 ; i < trackPictureList.size(); i++) {
      Log.e("Track Picture", String.valueOf(trackPictureList.get(i).getTrackAvatarUrl()));
    }
    placesCarouselAdapter.notifyDataSetChanged();
  }

  @Override public void setTracksList(List<Tracks> tracksList) {
    this.tracks.clear();
    this.tracks.addAll(tracksList);
    tracksAdapter.notifyDataSetChanged();
  }

  @Override public void launchCityHighlightsForViewing(Tracks tracks) {
    appActivityManager.launchCityHighlightsForViewing(this, tracks, buildCityObject());
  }

  @Override public City buildCityObject() {
    int trackId;
    if(getMyTrip() != null) {
      trackId = getMyTrip().getMyTrack().getTrackId();
    }else {
      trackId = getMyTrack().getTrackId();
    }
    return new City.Builder(getCityId(), getCountryId(), trackId).build();
  }

  @Override public boolean onCreateOptionsMenu(Menu menu) {
    getMenuInflater().inflate(R.menu.menu_places, menu);
    return true;
  }

  @Override public boolean onOptionsItemSelected(MenuItem item) {
    int id = item.getItemId();
    if (id == R.id.menu_add_place) {
      appActivityManager.launchPlaceHighlights(this, buildCityObject());
      return true;
    }

    return super.onOptionsItemSelected(item);
  }

  @Override protected void onActivityResult(int requestCode, int resultCode, Intent data) {
    super.onActivityResult(requestCode, resultCode, data);
    if (requestCode == AppConstants.PLACE_HIGHLIGHT_REQUEST && resultCode == RESULT_OK) {
      compositeDisposable.add(presenter.getSpecificTrack(myTrack.getTrackId()));
    }
  }

  public MyTrack getMyTrack() {
    return getIntent().getParcelableExtra(AppConstants.MY_TRACK_OBJECT);
  }

  private void instantiateTrackLists() {
    trackPictureList = new ArrayList<>();
    tracks = new ArrayList<>();
    trackPictureList.clear();
    if(getMyTrip() !=null) {
      trackPictureList.addAll(getMyTrip().getMyTrack().getTrackPictures());
      Log.e("getMyTrip", String.valueOf(getMyTrip()));
      tracks.clear();
      tracks.addAll(getMyTrip().getTracks());
    } else {
      compositeDisposable.add(presenter.getSpecificTrack(getMyTrack().getTrackId()));
    }
  }

  public void startTrip() {
    Log.e("Tracks", String.valueOf(this.tracks));
    for (int i = 0 ; i < this.tracks.size(); i++) {
      Log.e("Track List Name", String.valueOf(this.tracks.get(i).getName()));
      Log.e("Track List Lat", String.valueOf(this.tracks.get(i).getLat()));
      Log.e("Track List Lng", String.valueOf(this.tracks.get(i).getLng()));
    }
  }
  private void checkGPSEnabled() {
    LocationManager lm = (LocationManager) this.getSystemService(Context.LOCATION_SERVICE);
    boolean gps_enabled = false;
    boolean network_enabled = false;

    try {
      gps_enabled = lm.isProviderEnabled(LocationManager.GPS_PROVIDER);
    } catch (Exception ex) {
    }

    try {
      network_enabled = lm.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
    } catch (Exception ex) {
    }

    if (!gps_enabled && !network_enabled) {
      // notify user
      AlertDialog.Builder dialog = new AlertDialog.Builder(this);
      dialog.setMessage(this.getResources().getString(R.string.gps_network_not_enabled));
      dialog.setPositiveButton(this.getResources().getString(R.string.open_location_settings), new DialogInterface.OnClickListener() {
        @Override
        public void onClick(DialogInterface paramDialogInterface, int paramInt) {
          // TODO Auto-generated method stub
          Intent myIntent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
          getApplicationContext().startActivity(myIntent);
          //get gps
        }
      });
      dialog.setNegativeButton(this.getString(R.string.Cancel), new DialogInterface.OnClickListener() {

        @Override
        public void onClick(DialogInterface paramDialogInterface, int paramInt) {
          // TODO Auto-generated method stub

        }
      });
      dialog.show();
    } else  {
      if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
        ActivityCompat.requestPermissions(PlacesActivity.this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, 101);
      }
    }
  }

  @Override
  public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
    super.onRequestPermissionsResult(requestCode, permissions, grantResults);
    switch (requestCode) {
      case 101:
        if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
          startTrip();
          //view -> presenter.onSingleItemClick(this.tracks.);
          //Log.e("My Track", String.valueOf(this.getMyTrack().getTrackName()));
          Bundle bundle = new Bundle();
          if (this.getMyTrack() == null) {
            Log.e("Track ID", String.valueOf(getMyTrip().getMyTrack().getTrackId()));
            compositeDisposable.add(presenter.getSpecificTrackForTimeline(getMyTrip().getMyTrack().getTrackId()));
          } else {
            myTrack = this.getMyTrack();
            bundle.putParcelable(AppConstants.MY_TRACK_OBJECT, this.myTrack);
            appActivityManager.launchTimeline(this, bundle);
          }
          // do your work here
        } else if (Build.VERSION.SDK_INT >= 23 && !shouldShowRequestPermissionRationale(permissions[0])) {
          //Toast.makeText(PlacesActivity.this, "Go to Settings and Grant the permission to use this feature.", Toast.LENGTH_SHORT).show();
          // User selected the Never Ask Again Option
        } else {
          //Toast.makeText(PlacesActivity.this, "Permission Denied", Toast.LENGTH_SHORT).show();
        }
        break;
    }
  }
}
