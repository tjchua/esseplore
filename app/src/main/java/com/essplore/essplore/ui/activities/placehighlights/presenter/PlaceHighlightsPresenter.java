package com.essplore.essplore.ui.activities.placehighlights.presenter;

import com.essplore.essplore.api.requests.CityHighlightRequest;
import io.reactivex.disposables.Disposable;

public interface PlaceHighlightsPresenter {

  Disposable getCityHighlights();

  CityHighlightRequest buildCityHighlightsRequest();

}
