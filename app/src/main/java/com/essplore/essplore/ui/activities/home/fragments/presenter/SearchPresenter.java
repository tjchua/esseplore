package com.essplore.essplore.ui.activities.home.fragments.presenter;

import com.essplore.essplore.api.requests.CityRequest;
import com.essplore.essplore.api.requests.DeleteSpecificTrackRequest;
import com.essplore.essplore.models.City;
import com.essplore.essplore.ui.utils.OnSingleItemClickListener;
import io.reactivex.disposables.Disposable;

public interface SearchPresenter extends OnSingleItemClickListener<City> {

  Disposable getCity();

  CityRequest buildCityRequest();

}
