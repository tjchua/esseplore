package com.essplore.essplore.ui.activities.profile.fragments.view;

public interface ChangePasswordFragmentView {

  String provideOldPassword();

  String provideNewPassword();

  String provideConfirmNewPassword();

  String provideToken();

  void showFieldError();

  void showPasswordDoesNotMatchError();

}
