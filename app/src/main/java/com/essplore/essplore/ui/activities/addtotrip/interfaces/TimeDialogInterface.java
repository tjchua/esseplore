package com.essplore.essplore.ui.activities.addtotrip.interfaces;

import android.widget.CalendarView;
import android.widget.TimePicker;

public interface TimeDialogInterface {

    public void onSelectedTime(int hourOfDay, int minute);

}
