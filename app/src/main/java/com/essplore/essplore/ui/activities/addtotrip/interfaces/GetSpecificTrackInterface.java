package com.essplore.essplore.ui.activities.addtotrip.interfaces;

public interface GetSpecificTrackInterface {

    public void onGetSpecificTrackSuccess(String response, String trackid);
    public void onGetSpecificTrackFailed();

}
