package com.essplore.essplore.ui.activities.home.fragments.view;

import com.essplore.essplore.models.MyTrack;
import com.essplore.essplore.ui.activities.HttpBaseView;
import java.util.List;

public interface TripsFragmentView extends HttpBaseView {

    void onViewLoaded();

    void showLoggedInPanel();

    void hideLoggedInPanel();

    void showTrackList(List<MyTrack> myTracks);

    String provideToken();

    void showProgressbar();

    void hideProgressbar();

    void hidePanelsAndShowTrackList();

    void showPanelsAndHideTrackList();

    void showSwipeRefreshLoading();

    void hideSwipeRefreshLoading();

    void launchActivity(MyTrack myTrack);

    void deleteTrack(int position);

    void removeTrackInList(MyTrack myTrack);
}
