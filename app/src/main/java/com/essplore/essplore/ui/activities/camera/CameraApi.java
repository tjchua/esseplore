package com.essplore.essplore.ui.activities.camera;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.essplore.essplore.BuildConfig;
import com.essplore.essplore.application.AppConstants;
import com.essplore.essplore.managers.SharedPreferenceKeys;
import com.essplore.essplore.ui.activities.camera.adapter.GalleryAdapter;
import com.essplore.essplore.ui.activities.camera.interfaces.ProcessImageTaskInterface;
import com.essplore.essplore.ui.activities.camera.models.Gallery;
import com.otaliastudios.cameraview.CameraUtils;
import com.stripe.android.exception.APIConnectionException;

import org.apache.commons.lang3.StringUtils;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.UnknownHostException;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;

import okhttp3.FormBody;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public class CameraApi {

    private OkHttpClient client;
    private String accessToken;
    private Context context;
    private GalleryFragment galleryFragment;

    public CameraApi(Context context){
        this.context = context;
        accessToken = context.getSharedPreferences(SharedPreferenceKeys.ESSPLORE_PREFERENCE, Context.
                MODE_PRIVATE).getString(SharedPreferenceKeys.ACCESS_TOKEN, StringUtils.EMPTY);
    }

    public CameraApi(){

    }

    public void getImages(GalleryFragment galleryFragment, int trackId){
        this.galleryFragment = galleryFragment;
        new getImagesTask(Integer.toString(trackId)).execute();
    }

    public void processImage(Context context, byte[] image, ProcessImageTaskInterface processImageTaskInterface){

        new ProcessImageTask(context, image, processImageTaskInterface).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);

    }

    private class getImagesTask extends AsyncTask<String, String, String> {

        private String trackId;

        public getImagesTask(String trackId){

            this.trackId = trackId;

        }

        @Override
        protected String doInBackground(String... strings) {
            client = new OkHttpClient();

            String response = null;
            try {
                response = getImages(BuildConfig.BASE_URL + AppConstants.PHOTO_TRACK + "?" +
                        AppConstants.PHOTO_TRACK_TRACK_ID_PARAM + "=" + trackId);
            } catch (IOException e) {
                e.printStackTrace();
            }
            return response;
        }

        @Override
        protected void onPostExecute(String response) {
            super.onPostExecute(response);
            galleryFragment.showPhotoList(response);
        }
    }

    private class ProcessImageTask extends AsyncTask<String, Void, String> {

        private Context context;
        private byte[] image;
        private final String FAILED = "FAILED";
        private Bitmap result;
        private ProcessImageTaskInterface processImageTaskInterface;

        public ProcessImageTask(Context context, byte[] image, ProcessImageTaskInterface processImageTaskInterface){

            this.context = context;
            this.image = image;
            this.processImageTaskInterface = processImageTaskInterface;

        }


        @Override
        protected String doInBackground(String... strings) {

            try {

                result = Glide.
                        with(context).
                        load(image).
                        asBitmap().
                        into(Integer.MAX_VALUE, Integer.MAX_VALUE). // Width and height
                        get();

            } catch (InterruptedException e) {

                e.printStackTrace();
                return FAILED;

            } catch (ExecutionException e) {

                e.printStackTrace();
                return FAILED;

            }

            return "";

        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);

            if(result != FAILED){

                if (processImageTaskInterface != null)
                    processImageTaskInterface.onCaptureProcessSuccessful(this.result);

            }

        }

        public void setProcessImageTaskInterface(ProcessImageTaskInterface processImageTaskInterface){

            this.processImageTaskInterface = processImageTaskInterface;

        }
    }

    private String getImages(String url) throws IOException {
        Request request = new Request.Builder()
                .url(url)
                .header(AppConstants.AUTHORIZATION, AppConstants.BEARER + StringUtils.SPACE + accessToken)
                .build();

        try (Response response = client.newCall(request).execute()) {
            return response.body().string();
        }
    }

}
