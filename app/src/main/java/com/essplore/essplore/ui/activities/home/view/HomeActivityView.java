package com.essplore.essplore.ui.activities.home.view;

import android.support.v4.view.ViewPager;
import com.essplore.essplore.models.City;
import com.essplore.essplore.ui.activities.HttpBaseView;

public interface HomeActivityView extends HttpBaseView {
  void setBottomNavigationListener();

  void setViewPageListener();

  ViewPager getViewPager();

  void launchPlaceHighlight(City city);

  void launchAddTrip(City city);

  void hideToolBar();

  void showToolBar();
}
