package com.essplore.essplore.ui.activities.share;

import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.Bitmap;
import android.location.Location;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.Toast;

import com.essplore.essplore.BuildConfig;
import com.essplore.essplore.R;
import com.essplore.essplore.application.AppConstants;
import com.essplore.essplore.managers.SharedPreferenceKeys;
import com.essplore.essplore.ui.activities.camera.GalleryFragment;

import org.apache.commons.lang3.StringUtils;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.Calendar;
import java.util.Date;
import java.util.concurrent.TimeUnit;

import id.zelory.compressor.Compressor;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public class ShareApi {

    private OkHttpClient client;
    private String accessToken;
    private String userId;
    private Context context;
    private GalleryFragment galleryFragment;

    private final String TRACK_ID = "track_id";
    private final String USER_ID = "user_id";
    private final String DESCRIPTION = "description";
    private final String SHARED_ON = "shared_on";
    private final String PICTURE = "picture";
    private final String LAT = "lat";
    private final String LNG = "lng";
    private final String CURRENT_PLACE_ID = "current_place_id";
    private String currentTime;
    private ShareAPIinterface shareAPIinterface;
    private final String JPEG_EXT = ".jpeg";

    public ShareApi(Context context){
        this.context = context;
        accessToken = context.getSharedPreferences(SharedPreferenceKeys.ESSPLORE_PREFERENCE, Context.
                MODE_PRIVATE).getString(SharedPreferenceKeys.ACCESS_TOKEN, StringUtils.EMPTY);
        userId = context.getSharedPreferences(SharedPreferenceKeys.ESSPLORE_PREFERENCE, Context.
                MODE_PRIVATE).getString(SharedPreferenceKeys.USERID, StringUtils.EMPTY);
    }

    private String getCurrentTime(){
        Date currentTime = Calendar.getInstance().getTime();
        return currentTime.toString();
    }

    public ShareApi(){

    }

    private File convertBitmapToFile(String filename, Bitmap bitmap){
        //create a file to write bitmap data

        File file = new File(context.getCacheDir(), filename + JPEG_EXT);

        Bitmap bitmapToSave = bitmap;

            OutputStream os;

            try {

                os = new FileOutputStream(file);
                bitmapToSave.compress(Bitmap.CompressFormat.JPEG, 100, os);
                os.flush();
                os.close();

            } catch (Exception e) {

                Log.e(getClass().getSimpleName(), "Error writing bitmap", e);

            }

        return file;

    }

    public void sendPost(String imgPath, Location location, int position, int trackId, String currentPlaceId, String description){

        Double lat = 0.0;
        Double lng = 0.0;

        if (location != null){

            lng = location.getLongitude();
            lat = location.getLatitude();

        }
        new UploadImageTask(Integer.toString(trackId), userId, description,
                new File(imgPath), Double.toString(lat), Double.toString(lng),
                currentPlaceId, position).executeOnExecutor(AsyncTask.SERIAL_EXECUTOR);

    }

    private class UploadImageTask extends AsyncTask<String, Void, String> {

        private String track_id;
        private String user_id;
        private String description;
        private String lat;
        private String current_place_id;
        private String lng;
        private File file;
        private int position;
        private final String failed = "FAILED";

        public UploadImageTask(String track_id, String user_id, String description, File file, String lat, String lng, String current_place_id){
            this.track_id = track_id;
            this.user_id = user_id;
            this.description = description;
            this.current_place_id = current_place_id;
            this.lat = lat;
            this.lng = lng;
            this.file = file;
        }

        public UploadImageTask(String track_id, String user_id, String description, File file, String lat, String lng, String current_place_id, int position){
            this.track_id = track_id;
            this.user_id = user_id;
            this.description = description;
            this.current_place_id = current_place_id;
            this.lat = lat;
            this.lng = lng;
            this.file = file;
            this.position = position;
        }

        @Override
        protected String doInBackground(String... strings) {

            try {

                File compressedImageFile = new Compressor(context).compressToFile(file);
                String[] fileNames = compressedImageFile.toString().split(File.separator);
                String filename = fileNames[fileNames.length - 1];
//                Bitmap compressedImageBitmap = new Compressor(context).compressToBitmap(file);
                return uploadImage(BuildConfig.BASE_URL + AppConstants.PHOTO_TRACK,
                        track_id, user_id, description, "", compressedImageFile, filename, lat, lng, current_place_id);

            } catch (IOException e) {

                e.printStackTrace();
                return failed;

            }

        }

        @Override
        protected void onPostExecute(String s) {

            super.onPostExecute(s);

            if(s != failed){

                if (shareAPIinterface != null)
                shareAPIinterface.onUploadSuccess(position);

            }else{

                if (shareAPIinterface != null)
                shareAPIinterface.onUploadFailed();

            }

            this.cancel(true);

        }
    }

    private String uploadImage(String url, String track_id, String user_id, String description,
                               String shared_on, File picture, String fileName, String lat, String lng,
                               String current_place_id) throws IOException{

            final MediaType MEDIA_TYPE_PNG = MediaType.parse("image/png");

            RequestBody req = new MultipartBody.Builder().setType(MultipartBody.FORM)
                    .addFormDataPart(TRACK_ID, track_id)
                    .addFormDataPart(USER_ID, user_id)
                    .addFormDataPart(DESCRIPTION, description)
                    .addFormDataPart(LAT, lat)
                    .addFormDataPart(LNG, lng)
                    .addFormDataPart(CURRENT_PLACE_ID, current_place_id)
                    .addFormDataPart(SHARED_ON, shared_on)
                    .addFormDataPart(PICTURE, fileName, RequestBody.create(MEDIA_TYPE_PNG, picture)).build();

            Request request = new Request.Builder()
                    .url(url)
                    .header(AppConstants.AUTHORIZATION, AppConstants.BEARER + StringUtils.SPACE + accessToken)
                    .post(req)
                    .build();

            OkHttpClient.Builder b = new OkHttpClient.Builder();
            b.readTimeout(1, TimeUnit.MINUTES);
            b.writeTimeout(1, TimeUnit.MINUTES);
            OkHttpClient client = b.build();
            try (Response response = client.newCall(request).execute()) {
                return response.body().string();
            }
    }

    public void setShareApiInterface(ShareAPIinterface shareApiInterface){

        this.shareAPIinterface = shareApiInterface;

    }
}
