package com.essplore.essplore.ui.activities.places.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.PopupMenu;
import android.widget.TextView;
import butterknife.BindView;
import butterknife.ButterKnife;
import com.bumptech.glide.Glide;
import com.essplore.essplore.R;
import com.essplore.essplore.application.AppConstants;
import com.essplore.essplore.models.Tracks;
import com.essplore.essplore.ui.activities.places.presenter.PlacesPresenterImpl;
import com.essplore.essplore.ui.utils.OnBindViewListener;
import java.util.List;

public class TracksAdapter extends RecyclerView.Adapter<TracksAdapter.ViewHolder> {

  private Context context;
  private List<Tracks> tracks;
  private PlacesPresenterImpl presenter;
  UserAction userAction;

  public TracksAdapter(Context context, List<Tracks> tracks, UserAction userAction, PlacesPresenterImpl presenter) {
    this.context = context;
    this.tracks = tracks;
    this.userAction = userAction;
    this.presenter = presenter;
  }

  @Override public int getItemCount() {
    return tracks.size();
  }

  @NonNull @Override public TracksAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
    return new TracksAdapter.ViewHolder(context,
        LayoutInflater.from(context).inflate(R.layout.item_add_tracks, parent, false), userAction);
  }

  @Override public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
    holder.onBind(tracks.get(position));
  }

  public class ViewHolder extends RecyclerView.ViewHolder implements OnBindViewListener<Tracks> {

    @BindView(R.id.txPlaceName) TextView txPlaceName;

    @BindView(R.id.txPlaceAddress) TextView txPlaceAddress;

    @BindView(R.id.imgPlace) ImageView imgPlace;

    Context context;
    UserAction userAction;

    public ViewHolder(Context context, View itemView, UserAction userAction) {
      super(itemView);
      this.context = context;
      this.userAction = userAction;
      ButterKnife.bind(this, itemView);
      itemView.setOnLongClickListener(view -> {
        setLongClick(context, itemView);
        return true;
      });
    }

    private void setLongClick(Context context, View itemView) {
      PopupMenu popupMenu = new PopupMenu(context, itemView);
      popupMenu.getMenu().add(context.getString(R.string.delete));
      popupMenu.setOnMenuItemClickListener(menuItem -> {
        userAction.onDeleteItem(getAdapterPosition());
        popupMenu.dismiss();
        return false;
      });
      popupMenu.show();
    }

    @Override public void onBind(Tracks track) {
      txPlaceName.setText(track.getName());
      txPlaceAddress.setText(
          String.format(AppConstants.PLACES_PROVINCE_STRING_FORMAT, track.getProvinceName(), track.getCountryName()));
      if (track.getPlacePictureList() != null) {
        Glide.with(context).load(track.getPlacePictureList().get(0).getPlacePictureUrl()).centerCrop().into(imgPlace);
      }
      itemView.setOnClickListener(view -> presenter.onSingleItemClick(track));
    }
  }
}
